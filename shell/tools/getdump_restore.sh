#!/bin/bash
date
# Remote Server Data
DBNAME=psbaza
DBUSER=psbuser
SSHAUTH=psbaza

SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../.."
RD="$(pwd)"
SQL_DIR=$RD/storage/sql
SQL_FILE=$SQL_DIR/$DBNAME.sql
BZIP_FILE=$SQL_FILE.bz2
CUR_DATE=$(date +%Y-%m-%d_%H.%M)

if [ -f $BZIP_FILE ]
then
	echo "Delete previous bz2 file if exist..."
	rm -f $BZIP_FILE
fi

if [ -f $SQL_FILE ]
then
	echo "Backup previous SQL file..."
	mv -f $SQL_FILE $SQL_FILE.$CUR_DATE.bkp.sql
fi

echo "Create remotely dump & Download database: $DBNAME"
ssh $SSHAUTH "mysqldump --defaults-extra-file=psbaza.cfg --opt $DBNAME | bzip2 -9" > $BZIP_FILE

if [ -f $BZIP_FILE ]
then
    echo "Unzip file"
    bunzip2 $BZIP_FILE
    echo "Restore database locally."
    mysql --defaults-extra-file=$THIS_DIR/sql/mysqldb.cnf -f $DBNAME < $SQL_FILE
    echo "Re-configure database for local usage."
    mysql --defaults-extra-file=$THIS_DIR/sql/mysqldb.cnf -f $DBNAME < $THIS_DIR/sql/configure.prj.sql

    echo "RSYNC run..."

	TIME_LIMIT=5
	choice=""
    echo "Are you sure you want to RSYNC Storage images? <y/N>"
	read -t $TIME_LIMIT choice

	if [ ! -z "$choice" ]
	then
	    case "$choice" in
          y|Y ) rsync -huzavr $SSHAUTH:www/petrostroybaza.ru/storage/images ../;;
          n|N ) echo "no RSYNCing";;
          * ) echo "invalid choise :( no RSYNCing";;
        esac
    else
        echo "No choice for RSYNCing"
	fi
else
    echo "Not exist downloaded archive file: $BZIP_FILE"
fi

echo "Done "
date
