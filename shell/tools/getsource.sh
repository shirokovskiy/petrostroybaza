#!/usr/bin/env bash
#
#
# @project  Petrostroybaza
# @author   Dmitriy Shirokovskiy
#
date
echo "Start getting source!"
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
ROOT_DIR="../.."

echo "Download sources..."
rsync -Phuvar --size-only --exclude=.git --exclude=webstat --exclude="storage/files/" --exclude="storage/images/" --exclude="storage/maps/" psbaza:~/public_html/ $ROOT_DIR/

