<?php
$arrTplVars['module.name'] = "mail-template-form";
$arrTplVars['module.parent'] = "mail-sender";
$arrTplVars['module.title'] = "Создать шаблон письма";

include_once(SITE_LIB_DIR.'funcs.php');
// Include SPAW editor
include_once(SITE_LIB_DIR.'spaw2/spaw.inc.php');
$objSpawEditor = new SpawEditor("strBodyHtml");

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);


/**
 * Удаление фото
 */
if ( intval($_GET['id']) > 0 ) {
    $intRecordId = intval($_GET['id']);
    if ($_GET['act'] == 'delimg') {
        /**
         * Удалить фото
         */
        foreach(array('jpg','png','gif') as $val) {
            if (file_exists(PRJ_FILES.'email_attachments/pic.'.$intRecordId.'.'.$val)) {
                $ext = $val; break;
            }
        }

        if ( isset($ext) ) {
            unlink(PRJ_FILES."email_attachments/pic.$intRecordId.$ext");
            unlink(PRJ_FILES."email_attachments/pic.$intRecordId.b.$ext");
        }

        header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
        exit();
    }

    if ($_GET['act'] == 'delfile') {
        /**
         * Удалить файл
         */
        foreach(array('doc','docx','xls','xlsx','pdf') as $val) {
            if (file_exists(PRJ_FILES.'email_attachments/document.'.$intRecordId.'.'.$val)) {
                $ext = $val; break;
            }
        }

        if ( isset($ext) ) {
            unlink(PRJ_FILES."email_attachments/document.$intRecordId.$ext");
        }

        header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
        exit();
    }
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strBodyHtml'] = addslashes(trim($_POST['strBodyHtml']));
    $objSpawEditor->getActivePage()->value = trim($_POST['strBodyHtml']);

    if (empty($arrSqlData['strBodyHtml'])) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyContent';
    }

    $arrSqlData['strBodyText'] = strip_tags($arrSqlData['strBodyHtml']);
    $arrSqlData['strBodyText'] = preg_replace("\r\n", "\n", $arrSqlData['strBodyText']);
    $arrSqlData['strBodyText'] = preg_replace("\n\n\n\n", "\n", $arrSqlData['strBodyText']);

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." ste_title = '{$arrSqlData['strTitle']}'"
            .", ste_body = '{$arrSqlData['strBodyText']}'"
            .", ste_html = '{$arrSqlData['strBodyHtml']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_templates_email SET ".$strSqlFields
                .", ste_date_create = UNIX_TIMESTAMP()"
                .", ste_date_update = UNIX_TIMESTAMP()";
        } else {
            $strSqlQuery = "UPDATE site_templates_email SET".$strSqlFields
                .", ste_date_update = UNIX_TIMESTAMP()"
                ." WHERE ste_id = ".$intRecordId;
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }

            /**
             * Загрузка фото
             */
            if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0
                && $_FILES['flPhoto']['size'] < 1000000 && $GLOBALS['manStatusError']!=1 ) {
                $strImageName = "pic.$intRecordId";

                /////////////////////////////////////////////////////////////////////////////////////
                // extended
                $arrExts['pjpeg'] = 'jpg';
                $arrExts['jpeg'] = 'jpg';

                $mimeType = explode("/", $_FILES['flPhoto']['type']);
                $mimeType = $mimeType[1];

                if ( key_exists($mimeType , $arrExts) ) {
                    $mimeType = $arrExts[$mimeType];
                }

                if ( $mimeType != 'jpg' && $mimeType != 'gif' && $mimeType != 'png' ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                } else {
                    $fileName = "$strImageName.$mimeType";
                    $fileNameBig = "$strImageName.b.$mimeType";
                    $fileNameOrig = "temp.$strImageName.$mimeType";

                    $dirStorage = DROOT."storage/";
                    if ( !file_exists($dirStorage) ) {
                        if (!mkdir($dirStorage, 0775)) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                        }
                    }

                    $dirImages = $dirStorage."files/";
                    if ( !file_exists($dirImages) ) {
                        if (!mkdir($dirImages, 0775)) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                        }
                    }

                    $dirImages = $dirImages."email_attachments/";
                    if ( !file_exists($dirImages) ) {
                        if (!mkdir($dirImages, 0775)) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                        }
                    }

                    if ($GLOBALS['manStatusError']!=1) {
                        move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig );

                        if ( !is_object($objImage) ) {
                            include_once("cls.images.php");
                            $objImage = new clsImages();
                        }

                        $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 100, 70 );

                        list($width, $height, $type, $attr) = getimagesize($dirImages."/".$fileNameOrig);

                        if ($width > 660) {
                            $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig, 660 );
                        } else {
                            copy($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig);
                        }

                        unlink($dirImages."/".$fileNameOrig);
                    }
                }
            }

            /**
             * Загрузка файла
             */
            if ( $_FILES['flDoc']['error'] == 0 && $_FILES['flDoc']['size'] > 0
                && $_FILES['flDoc']['size'] < 2200000 && $GLOBALS['manStatusError']!=1 ) {
                $strImageName = "document.$intRecordId";

                $arrExts['vnd.openxmlformats-officedocument.wordprocessingml.document'] = 'docx';

                $mimeType = explode("/", $_FILES['flDoc']['type']);
                $mimeType = $mimeType[1];

                if ( array_key_exists($mimeType, $arrExts) ) {
                    $fileExt = $arrExts[$mimeType];
                }

                if ( !is_object($objFiles) ) {
                    $objFiles = new Files();
                }

                $fileExt = $objFiles->getFileExtension($_FILES['flDoc']['name']);

                if ( !in_array($fileExt, array('doc','docx','xls','xlsx','pdf')) ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                } else {
                    $fileName = "$strImageName.$fileExt";

                    $dirStorage = DROOT."storage/";
                    if ( !file_exists($dirStorage) ) {
                        if (!mkdir($dirStorage, 0775)) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                        }
                    }

                    $dirImages = $dirStorage."files/";
                    if ( !file_exists($dirImages) ) {
                        if (!mkdir($dirImages, 0775)) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                        }
                    }

                    $dirImages = $dirImages."email_attachments/";
                    if ( !file_exists($dirImages) ) {
                        if (!mkdir($dirImages, 0775)) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                        }
                    }

                    if ($GLOBALS['manStatusError']!=1) {
                        move_uploaded_file($_FILES['flDoc']['tmp_name'], $dirImages."/".$fileName );
                    }
                }
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$arrIf['exist.image'] = false;
$arrIf['exist.file'] = false;

if ( isset($_GET['id']) && intval($_GET['id']) > 0 && $GLOBALS['manStatusError']!=1 ) {
    $arrTplVars['module.title'] = "Редактировать шаблон письма";
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_templates_email WHERE ste_id = ".$intRecordId;
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['ste_title']));
        $arrTplVars['strBodyText'] = htmlspecialchars(stripslashes($arrInfo['ste_body']));

        $objSpawEditor->getActivePage()->value = $arrInfo['ste_html'];

        $arrTplVars['strDateCreate'] = date('d.m.Y в H:i', $arrInfo['ste_date_create']);
        $arrTplVars['strDateUpdate'] = date('d.m.Y в H:i', $arrInfo['ste_date_update']);

        $arrTplVars['cbxStatus'] = $arrInfo['ste_status'] == 'Y' ? ' checked' : '';

        foreach(array('jpg','png','gif') as $val) {
            if (file_exists(PRJ_FILES.'email_attachments/pic.'.$intRecordId.'.'.$val)) {
                $ext = $val; break;
            }
        }

        if (isset($ext)) {
            $arrTplVars['picExt'] = $ext;
            $arrIf['exist.image'] = true;
            unset($ext);
        }

        foreach(array('doc','docx','xls','xlsx','pdf') as $val) {
            if (file_exists(PRJ_FILES.'email_attachments/document.'.$intRecordId.'.'.$val)) {
                $ext = $val; break;
            }
        }

        if (isset($ext)) {
            $arrTplVars['fileExt'] = $ext;
            $arrIf['exist.file'] = true;
        }
    }
}

$arrIf['no.image'] = !$arrIf['exist.image'];
$arrIf['no.file'] = !$arrIf['exist.file'];

$objSpawEditor->setDimensions('600px', null);
$arrTplVars['strBodyHtml'] = $objSpawEditor->getHtml();

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

