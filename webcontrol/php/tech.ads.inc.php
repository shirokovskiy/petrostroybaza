<?php
$arrTplVars['module.name'] = "tech.ads";
$arrTplVars['module.child'] = "tech.ad.form";
$arrTplVars['module.title'] = "Строительная техника";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
  $intRecordID = intval($_GET['id']);
  $strSqlQuery = "DELETE FROM ".$_db_tables['stTechnics']." WHERE pt_id='$intRecordID' LIMIT 1";
  if ( !$objDb->query($strSqlQuery) ) {
    $GLOBALS['manStatusError'] = 1;
    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
  } else {
//    $strSqlQuery = "SELECT si_id FROM ".$_db_tables["stImages"]." WHERE si_id_rel = '$intRecordID' AND si_type = 'estate'";
//    $arrSelectedRecordsForDelete = $objDb->fetchall( $strSqlQuery );
//
//    if ( is_array( $arrSelectedRecordsForDelete ) ) {
//      $strSqlQuery = "DELETE FROM ".$_db_tables['stImages']." WHERE si_id_rel = '$intRecordID' AND si_type = 'estate'";
//      if ( !$objDb->query( $strSqlQuery ) ) {
//        $GLOBALS['manStatusError'] = 1;
//        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
//      } else {
//        $dirImages = DROOT."storage/images/objects/";
//        foreach ( $arrSelectedRecordsForDelete as $recordDel ) {
//          $intRecordID = intval( $recordDel['si_id'] );
//          if ( file_exists($dirImages."obj.$intRecordID.jpg")) {
//            unlink($dirImages."obj.$intRecordID.jpg");
//            unlink($dirImages."obj.$intRecordID.b.jpg");
//          }
//        }
//      }
//    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");


// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stTechnics']." ";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stTechnics']." WHERE pt_status != 'X' $strSqlWhere";
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');

// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
* Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
* кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
*/
$objPagination->strPaginatorTpl = "site.global.5.tpl";
$objPagination->iQtyLinksPerPage = 7;
$objPagination->iQtyRecsPerPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stTechnics']
//  . " LEFT JOIN ".$_db_tables["stImages"]." ON (si_id_rel = seo_id)"
  . " LEFT JOIN ".$_db_tables["stTechTypes"]." ON (pt_tech_type_id = ptt_id)"
//  . " LEFT JOIN ".$_db_tables["stAdmReg"]." ON (seo_id_adm_reg = sar_id)"
  . " WHERE pt_status != 'X' $strSqlWhere ORDER BY pt_status, pt_date_update DESC ".$objPagination->strSqlLimit;
$arrTechnics = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrTechnics) ? count($arrTechnics) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrTechnics) ) {
  foreach ( $arrTechnics as $key => $arr_tech_ad ) {
    $arrTechnics[$key]['strAdType'] = $arrAdTechType[$arr_tech_ad['pt_ad_type']];
    $arrTechnics[$key]['strDopinfo'] = nl2br(htmlspecialchars( $arr_tech_ad['pt_dopinfo'] ));
    $arrTechnics[$key]['strContacts'] = nl2br(htmlspecialchars( $arr_tech_ad['pt_contacts'] ));
    $arrTechnics[$key]['pt_date_update'] = !empty($arr_tech_ad['pt_date_update']) ? $objUtil->workDate(6, $arr_tech_ad['pt_date_update'] ) : $objUtil->workDate(6, $arr_tech_ad['pt_date_create'] );
  }
} else {
  $arrIf['no.technics'] = true;
}

$arrIf['is.technics'] = !$arrIf['no.technics'] && !empty($arrTechnics);

$objTpl->tpl_loop( $arrTplVars['module.name'], "technics", $arrTechnics, array( "#FFFFFF", "#EFEFEF" ));

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
