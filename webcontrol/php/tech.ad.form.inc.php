<?php
$arrTplVars['module.name'] = "tech.ad.form";
$arrTplVars['module.parent'] = "tech.ads";
$arrTplVars['module.title'] = "объявлениями о технике";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Сохранение объявления
 */
if ( isset($_REQUEST['frmEditForm']) && $_REQUEST['frmEditForm'] == 'true' ) {
	$intRecordID = $arrSqlData['intRecordID'] = intval(trim($_REQUEST['intRecordID']));
	$arrTplVars['intRecordID'] = ( $arrSqlData['intRecordID'] > 0 ? $arrSqlData['intRecordID'] : '');

	$arrSqlData['sbxAdType'] = mysql_real_escape_string(trim($_REQUEST['sbxAdType']));
	$arrTplVars['sbxAdType'] = htmlspecialchars(trim($_REQUEST['sbxAdType']));

	if ( empty( $arrSqlData['sbxAdType'] ) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorAdType';
	}

	$arrSqlData['strTitle'] = mysql_real_escape_string(trim($_REQUEST['strTitle']));
	$arrTplVars['strTitle'] = htmlspecialchars(trim($_REQUEST['strTitle']));

	if ( empty( $arrSqlData['strTitle'] ) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorTitle';
		$arrTplVars['styleTitle'] = "border:1px solid red;";
	}

	$intTechTypeID = $arrTplVars['intTechTypeID'] = $arrSqlData['intTechTypeID'] = intval($_REQUEST['intTechTypeID']);

	if ( empty($intTechTypeID) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorTechType';
	}

	$arrSqlData['strRentPeriod'] = mysql_real_escape_string(trim($_REQUEST['strRentPeriod']));
	$arrTplVars['strRentPeriod'] = htmlspecialchars(trim($_REQUEST['strRentPeriod']));

	$arrSqlData['strAddressWorks'] = mysql_real_escape_string(trim($_REQUEST['strAddressWorks']));
	$arrTplVars['strAddressWorks'] = htmlspecialchars(trim($_REQUEST['strAddressWorks']));

	$arrSqlData['strMarka'] = mysql_real_escape_string(trim($_REQUEST['strMarka']));
	$arrTplVars['strMarka'] = htmlspecialchars(trim($_REQUEST['strMarka']));

	$arrSqlData['strModel'] = mysql_real_escape_string(trim($_REQUEST['strModel']));
	$arrTplVars['strModel'] = htmlspecialchars(trim($_REQUEST['strModel']));

	$arrSqlData['strYear'] = mysql_real_escape_string(trim($_REQUEST['strYear']));
	$arrTplVars['strYear'] = htmlspecialchars(trim($_REQUEST['strYear']));

	$arrSqlData['strDopinfo'] = mysql_real_escape_string(trim($_REQUEST['strDopinfo']));
	$arrTplVars['strDopinfo'] = htmlspecialchars(trim($_REQUEST['strDopinfo']));

	if ( empty($arrSqlData['strMarka']) && empty($arrSqlData['strModel']) && empty($arrSqlData['strYear']) && empty($arrSqlData['strDopinfo']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorEmptyFields';

		$arrTplVars['styleMarka'] =
		$arrTplVars['styleModel'] =
		$arrTplVars['styleYear'] =
		$arrTplVars['styleDopinfo'] = "border:1px solid red;";
	}

	$arrSqlData['strContacts'] = mysql_real_escape_string(trim($_REQUEST['strContacts']));
	$arrTplVars['strContacts'] = htmlspecialchars(trim($_REQUEST['strContacts']));

	if ( empty($arrSqlData['strContacts']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorContacts';

		$arrTplVars['styleContacts'] = "border:1px solid red;";
	}

	$arrSqlData['cbxStatus'] = ($_REQUEST['cbxStatus']=='on'?'Y':'N');
	$arrTplVars['cbxStatus'] = ($_REQUEST['cbxStatus']=='on'?' checked':'');

	$arrSqlData['cbxReturn'] = ($_REQUEST['cbxReturn']=='on'?'Y':'N');
	$arrTplVars['cbxReturn'] = ($_REQUEST['cbxReturn']=='on'?' checked':'');

	if ( !$arrIf['error'] ) {
		$strSqlFields = ""
			." pt_tech_type_id = '$intTechTypeID'"
			.", pt_title = '{$arrSqlData['strTitle']}'"
			.", pt_rent_period = '{$arrSqlData['strRentPeriod']}'"
			.", pt_address_works = '{$arrSqlData['strAddressWorks']}'"
			.", pt_ad_type = '{$arrSqlData['sbxAdType']}'"
			.", pt_marka = '{$arrSqlData['strMarka']}'"
			.", pt_model = '{$arrSqlData['strModel']}'"
			.", pt_year = '{$arrSqlData['strYear']}'"
			.", pt_dopinfo = '{$arrSqlData['strDopinfo']}'"
			.", pt_contacts = '{$arrSqlData['strContacts']}'"
			.", pt_status = '{$arrSqlData['cbxStatus']}'"
		;

		if ( empty($intRecordID) ) {
			$strSqlQuery = "INSERT INTO ".$_dbt['stTechnics']." SET $strSqlFields, pt_date_create = NOW()";
		} else {
			$strSqlQuery = "UPDATE ".$_dbt['stTechnics']." SET $strSqlFields, pt_date_update = NOW() WHERE pt_id = '$intRecordID'";
		}

		if ( !$objDb->query( $strSqlQuery ) ) {
			$arrIf['error'] = true;
			$arrErrMsg[] = 'DB Error!!!';
		} else {
			$intRecordID = empty($intRecordID) ? $objDb->insert_id() : $intRecordID;
		}

		if ( !$arrIf['error'] ) {
			if ( $arrSqlData['cbxReturn'] == 'Y' ) {
				header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordID");
			} else {
				header('location:'.$arrTplVars['module.parent']."?errMess=msgOk");
			}
			exit;
		}
	}
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
	$intRecordID = $arrTplVars['intRecordId'] = intval($_GET['id']);

	$strSqlQuery = "SELECT * FROM ".$_dbt['stTechnics']
		." LEFT JOIN ".$_dbt['stTechTypes']." ON (ptt_id = pt_tech_type_id)"
		." WHERE pt_id ='$intRecordID'";
	$arrInfo = $objDb->fetch( $strSqlQuery );

	if ( is_array( $arrInfo )) {
		$arrTplVars['intRecordID'] = $arrInfo['pt_id'];
		$arrTplVars['intTechTypeID'] = $arrInfo['pt_tech_type_id'];

		$arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['pt_title']));
		$arrTplVars['strRentPeriod'] = htmlspecialchars(stripslashes($arrInfo['pt_rent_period']));
		$arrTplVars['strAddressWorks'] = htmlspecialchars(stripslashes($arrInfo['pt_address_works']));
		$arrTplVars['strAdType'] = $arrAdTechType[$arrInfo['pt_ad_type']];
		$arrTplVars['sbxAdType'] = $arrInfo['pt_ad_type'];
		$arrTplVars['strTechType'] = $arrInfo['ptt_name'];

		$arrTplVars['strMarka'] = htmlspecialchars($arrInfo['pt_marka']);
		$arrTplVars['strModel'] = htmlspecialchars($arrInfo['pt_model']);
		$arrTplVars['strYear'] = $arrInfo['pt_year'];

		$arrTplVars['strDopinfo'] = htmlspecialchars($arrInfo['pt_dopinfo']);
		$arrTplVars['strContacts'] = htmlspecialchars($arrInfo['pt_contacts']);

		$arrTplVars['cbxStatus'] = $arrInfo['pt_status'] == 'Y' ? ' checked' : '';
	}
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);