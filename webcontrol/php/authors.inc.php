<?php
$arrTplVars['module.name'] = "authors";
$arrTplVars['module.child'] = "author.edit";
$arrTplVars['module.title'] = "Авторы";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);


// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stAuthors'];
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords' );
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stAuthors'];
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords' );
// ***** BEGIN: Построение пейджинга для вывода списка
$objPaginator = new tplPaginator($arrTplVars['intQuantSelectRecords']);
/**
* Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
* кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
*/
$objPaginator->iQtyRecsPerPage = 12;
$objPaginator->strColorLinkStyle = "link-b";
$objPaginator->strColorActiveStyle = "tab-bl-b";
$objPaginator->strNameImageBack = "arrow_one_left.gif";
$objPaginator->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPaginator->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stAuthors']." ORDER BY sa_author ".$objPaginator->strSqlLimit;
$arrSelectedRecords = $objDb->fetchall($strSqlQuery);
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPaginator->paShow();

$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords, array($arrTplVars['cBgColor'], $arrTplVars['cFourthColor']));
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
