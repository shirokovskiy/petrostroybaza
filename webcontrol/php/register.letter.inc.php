<?php
$arrTplVars['module.name'] = "register.letter";
$arrTplVars['module.title'] = "Письмо регистрации";

$filename = PRJ_STORAGE.'files/email_attachments/emailContent.txt';
$attachment = PRJ_STORAGE.'files/email_attachments/attachment.doc';

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if (isset($_POST['frmEditEmailLetter']) && $_POST['frmEditEmailLetter'] == 'true') {
    try {
        if (!empty($_POST['strTextBody'])) {
            file_put_contents($filename, $_POST['strTextBody']);
        }
        $GLOBALS['manCodeError'][]['code'] = 'msgOk';
    } catch (Exception $e) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorWrite';
    }

    /**
     * Загрузка документа
     */
    $maxFILE_SIZE = 5000000; // ~5Mb.
    if ( $_FILES['fileEmailAttachement']['error'] == 0 && $_FILES['fileEmailAttachement']['size'] > 0 && $_FILES['fileEmailAttachement']['size'] < $maxFILE_SIZE ) {
        $strFileName = 'attachment';

        /////////////////////////////////////////////////////////////////////////////////////
        $fileExt = $objFile->getFileExtension($_FILES['fileEmailAttachement']['name']);

        if ( $fileExt != 'doc' ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
        } else {
            $fileName = "$strFileName.$fileExt";

            $dirStorage = DROOT."storage";
            if ( !file_exists($dirStorage) ) {
                if (!mkdir($dirStorage, 0775)) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                }
            }

            $dirImages = $dirStorage."/files";
            if ( !file_exists($dirImages) ) {
                if (!mkdir($dirImages, 0775)) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                }
            }

            $dirImages = $dirImages."/email_attachments";
            if ( !file_exists($dirImages) ) {
                if (!mkdir($dirImages, 0775)) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                }
            }

            if ($GLOBALS['manStatusError']!=1) {
                @move_uploaded_file($_FILES['fileEmailAttachement']['tmp_name'], $dirImages."/".$fileName );
            }
        }
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if (file_exists($filename)) {
    $arrTplVars['strTextBody'] = file_get_contents($filename);
}

$arrIf['attachment.exist'] = file_exists($attachment);
$arrTplVars['fSize'] = $arrIf['attachment.exist'] ? filesize($attachment) : 0;

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

