<?php
$arrTplVars['module.name'] = "company.ref.form";
$arrTplVars['module.obj'] = "estate.object.form";
$arrTplVars['module.cmp'] = "company.form";
$arrTplVars['module.title'] = "Присоеденить к объекту";

$stPage = $arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset($_REQUEST['frmAttachCmp']) ) {
    $intObjID = $arrTplVars['intObjID'] = $arrSqlData['intObjID'] = intval($_REQUEST['intObjID']);

    if ( is_array( $_POST['cmpID'] ) ) {
        foreach ($_POST['cmpID'] as $id => $v) {
            $strSqlQuery = "SELECT * FROM site_objects_contacts_lnk WHERE socl_id_contact=$id AND socl_id_object=$intObjID";
            $arrInfo = $objDb->fetch( $strSqlQuery );
            if ( empty($arrInfo) ) {
                $strSqlQuery = "INSERT INTO site_objects_contacts_lnk SET socl_id_contact = $id, socl_id_object = $intObjID";
                if ( !$objDb->query( $strSqlQuery ) ) {
                    # sql error
                    $GLOBALS['manStatusError'] = 1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                }
            }
        }
    } else {
        $GLOBALS['manStatusError'] = 1;
        $GLOBALS['manCodeError'][]['code'] = 'msgNoCmpArr';
    }

    if ( !$GLOBALS['manStatusError'] ) {
        unset($_SESSION['frmSearchCompanies']);
        header('Location: '.$arrTplVars['module.obj'].'?stPage='.$stPage.'&id='.$intObjID);
        exit();
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_POST['frmSearchCompanies']) ) {
    $_SESSION['frmSearchCompanies'] = $_POST;
}

if ( !isset($_POST['frmSearchCompanies']) && intval($_GET['stPage']) <= 0 ) {
    unset($_SESSION['frmSearchCompanies']);
}

if ( isset($_SESSION['frmSearchCompanies']) ) {
    if ( intval( $_SESSION['frmSearchCompanies']['intCmpID'] )>0 ) {
        $intCmpID = $arrTplVars['intCmpID'] = $_SESSION['frmSearchCompanies']['intCmpID'];
        $arrSqlWhere[] = " sec_id = '$intCmpID' ";
    }
    if ( !empty( $_SESSION['frmSearchCompanies']['strCmp'] ) ) {
        $strCmp = $arrTplVars['strCmp'] = mysql_real_escape_string( $_SESSION['frmSearchCompanies']['strCmp'] );
        $arrSqlWhere[] = " sec_company LIKE '%$strCmp%' ";
    }
    if ( !empty( $_SESSION['frmSearchCompanies']['strInn'] ) ) {
        $strInn = $arrTplVars['strInn'] = mysql_real_escape_string( $_SESSION['frmSearchCompanies']['strInn'] );
        $arrSqlWhere[] = " sec_inn LIKE '$strInn' ";
    }
    if ( !empty( $_SESSION['frmSearchCompanies']['strFIO'] ) ) {
        $strFIO = $arrTplVars['strFIO'] = mysql_real_escape_string( $_SESSION['frmSearchCompanies']['strFIO'] );
        $arrSqlWhere[] = " sec_fio LIKE '%$strFIO%' ";
    }
}

if ( is_array($arrSqlWhere) && !empty($arrSqlWhere) ) {
    $strSqlWhere = implode(' AND ', $arrSqlWhere);

    // Всего записей в базе
    $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_estate_contacts";
    $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
    // Выбрано записей
    $strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM site_estate_contacts".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' );
    $arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.5.tpl";
    $objPagination->iQtyLinksPerPage = 10;
    $objPagination->iQtyRecsPerPage = 30;
    $objPagination->strColorLinkStyle = "link-b";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strNameImageBack = "arrow_one_left.gif";
    $objPagination->strNameImageForward = "arrow_one_right.gif";
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка
    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM site_estate_contacts".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )
        ." ORDER BY sec_company, sec_fio, sec_id DESC ".$objPagination->strSqlLimit;
    $arrSelectedRecords = $objDb->fetchall( $strSqlQuery );

    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
    // Присвоение значения пэйджинга для списка
    $arrTplVars['blockPaginator'] = $objPagination->paShow();

    if ( is_array($arrSelectedRecords) ) {
        $arrIf['show.companies'] = true;
        foreach ( $arrSelectedRecords as $key => $value ) {
            $arrSelectedRecords[$key]['cbxStatus'] = $value['sec_status'] == 'Y' ? ' checked' : '';
//            $arrSelectedRecords[$key]['secType'] = $arrTitleExt[$value['sec_type']];
        }
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "companies", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

if ( isset($_REQUEST['id_obj']) ) {
    $arrTplVars['intObjID'] = intval($_REQUEST['id_obj']);
    $arrIf['exist.obj'] = $arrTplVars['intObjID'] > 0;
    if ( $arrIf['exist.obj'] ) {
        $arrTplVars['strObject'] = getObjectInfo( $arrTplVars['intObjID'] );
        $arrTplVars['strAddress'] = getObjectInfo( $arrTplVars['intObjID'], 'seo_address' );
    }
}

$arrIf['set.obj'] = !$arrIf['exist.obj'];
$arrIf['need.company.set'] = !$arrIf['show.companies'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
