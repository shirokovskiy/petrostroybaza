<?php
$arrTplVars['module.name'] = "users";
$arrTplVars['module.title'] = "МЕНЕДЖЕРЫ WMCS";
$arrTplVars['module.name.child'] = "users.edit";

$arrTplVars['stPage'] = intval($_GET['stPage'])>0 ? intval($_GET['stPage']) : 1 ;

if (isset($_GET['action'])) {
  switch ($_GET['action']) {
  	case 'delete':
  		if ( isset($_GET['id']) && intval($_GET['id'])>0 ) {
  		  $intId = $_GET['id'];
  		  // удалим пользователя
        $strSqlQuery = "DELETE FROM ".$_db_tables["cmsUsers"]." WHERE cu_id='$intId'";
        if ( !$objDb->query( $strSqlQuery ) ) {
          $GLOBALS['manStatusError']=1;
          $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
          // удалим права доступа к проектам
          $strSqlQuery = "DELETE FROM ".$_db_tables["cmsPrjAccess"]." WHERE fpa_id_user = '$intId'";
          if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
          } else {
            // удалим права доступа на модули
            $strSqlQuery = "DELETE FROM ".$_db_tables["cmsAccess"]." WHERE fa_id_owner='$intId'";
            if ( !$objDb->query( $strSqlQuery ) ) {
              $GLOBALS['manStatusError']=1;
              $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            } else {
              header("location: ".$arrTplVars['module.name']."?stPage=".$arrTplVars['stPage']."&errMess=103usr");
      		    exit();
            }
          }
        }
  		} else {
  		  header("location: ".$arrTplVars['module.name']."?stPage=".$arrTplVars['stPage']."");
  		  exit();
  		}
  		break;

  	default:
  		break;
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$strSqlQuery = "SELECT COUNT(*) AS strQuantAllRecords FROM ".$_db_tables['cmsUsers'];
$arrTplVars['intQuantSelectRecords'] = $arrTplVars['strQuantAllRecords'] = $objDb->fetch( $strSqlQuery , 'strQuantAllRecords');

/**
 * Построение пейджинга для вывода списка записей полученных из БД
 */
$objPaginator = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPaginator->strPaginatorTpl = "site.global.1.tpl";
$objPaginator->iQtyRecsPerPage = 18;
// Создаем блок пэйджинга
$objPaginator->paCreate();

/**
 * Выбираем записи из БД с учетом постраничного вывода и заполняем массив для вывода в шаблон
 */
$strSqlQuery = "SELECT * FROM ".$_db_tables['cmsUsers'].", ".$_db_tables['cmsUserGrp']." WHERE cug_id=cu_group ORDER BY cu_lname".$objPaginator->strSqlLimit;
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 ); // кол-во строк показанных на странице

if (is_array($arrSelectedRecords)) {
	foreach($arrSelectedRecords as $key=>$value) {
		$arrSelRecForTpl[$key]['intIdManager'] = $arrSelectedRecords[$key]['cu_id'];
		$arrSelRecForTpl[$key]['strFIOManager'] = $arrSelectedRecords[$key]['cu_lname'].' '.$arrSelectedRecords[$key]['cu_fname'].' '.$arrSelectedRecords[$key]['cu_pname'];
		$arrSelRecForTpl[$key]['strLoginManager'] = $arrSelectedRecords[$key]['cu_login'];
		$arrSelRecForTpl[$key]['strTypeAccess'] = $arrSelectedRecords[$key]['cug_name'];
		$arrSelRecForTpl[$key]['strLastEnter'] = ($arrSelectedRecords[$key]['cu_last_enter']===NULL) ? 'не определено' : $objUtil->workDate(4, $arrSelectedRecords[$key]['cu_last_enter']);

		# Jimmy™, Fri Sep 01 14:08:19 MSD 2006
		$arrSelRecForTpl[$key]['bgRow'] = ($value['cu_status']=='N'? "bgcolor='#{clrInActiveUser}'" : "" );
		$arrSelRecForTpl[$key]['bgRow'] = ($value['cu_id']==$_SESSION['userInfoStorage']['cu_id'] ? "bgcolor='#{clrActiveUser}'" : $arrSelRecForTpl[$key]['bgRow'] );
	}
}
$arrTplVars['blockPaginator'] = $objPaginator->paShow();

$objTpl->tpl_loop($arrTplVars['module.name'], "users", $arrSelRecForTpl, array($arrTplVars['cBgColor'], $arrTplVars['cThirdColor']));

$arrTplVars['clrActiveUser'] = 'B2F1B2';
$arrTplVars['clrInActiveUser'] = 'FFA599';

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
