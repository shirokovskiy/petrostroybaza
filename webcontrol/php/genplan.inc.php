<?php
$arrTplVars['module.name'] = "genplan";
$arrTplVars['module.title'] = "Генплан";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( intval($_GET['id_del']) > 0 ) {
  $intRecordId = intval($_GET['id_del']);
  $strSqlQuery = "SELECT pg_filename FROM ".$_db_tables['stGenplan']." WHERE pg_id='$intRecordId'";
  $strFilename = $objDb->fetch($strSqlQuery, "pg_filename");

  $strSqlQuery = "DELETE FROM ".$_db_tables['stGenplan']." WHERE pg_id='$intRecordId' LIMIT 1";
  if ( !$objDb->query($strSqlQuery) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
  } else {
    if (file_exists(PRJ_FILES.'genplan/'.$strFilename)) {
      if(!unlink(PRJ_FILES.'genplan/'.$strFilename)) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
      }
    }
  }

  if (!$GLOBALS['manStatusError']) {
    header('Location: '.$arrTplVars['module.name'].'?stPage='.$stPage);
    exit;
  }
}


if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
  $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
  $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

  $arrSqlData['strFileTitle'] = addslashes(trim($_POST['strFileTitle']));
  $arrTplVars['strFileTitle'] = htmlspecialchars(stripslashes(trim($_POST['strFileTitle'])));

  if (empty($arrSqlData['strFileTitle']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgEmptyFileDesc';
  }

  if ( ($_FILES['flPhoto']['error'] != 0 || $_FILES['flPhoto']['size'] == 0) && empty($intRecordId) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgNoFile';
  }

  $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
  $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

  $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

  if ( $GLOBALS['manStatusError'] != 1 ) {
    $strSqlFields = ""
      . " pg_title = '{$arrSqlData['strFileTitle']}'"
      . ", pg_status = '{$arrSqlData['cbxStatus']}'"
      ;

    if ( empty($intRecordId) ) {
      $strSqlQuery = "INSERT INTO ".$_db_tables['stGenplan']." SET".$strSqlFields;
    } else {
      $strSqlQuery = "UPDATE ".$_db_tables['stGenplan']." SET".$strSqlFields
        . ", pg_date_change = NOW() WHERE pg_id = '$intRecordId'";
    }

    if ( !$objDb->query( $strSqlQuery ) ) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      $intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;
    }

    if ($GLOBALS['manStatusError']!=1) {
      $maxFileSize = 3000000;
      if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] < $maxFileSize ) {
        $strFileName = strtolower( substr($objUtil->translitKyrToLat($objUtil->translitBadChars($_POST['strFileTitle'])), 0, 16) );
        // дополнительная частная обработка
        $strFileName = str_replace("'", "", $strFileName);
        $strFileName = str_replace(",", "", $strFileName);
        $strFileName = str_replace(".", "", $strFileName);
        $strFileName = str_replace("№", "N_", $strFileName);

        /////////////////////////////////////////////////////////////////////////////////////
        $fileExt = $objFile->getFileExtension($_FILES['flPhoto']['name']);

        if ( $fileExt != 'doc' && $fileExt != 'pdf' && $fileExt != 'txt' && $fileExt != 'xls'
            && $fileExt != 'gif' && $fileExt != 'jpg' && $fileExt != 'rtf' && $fileExt != 'rar' && $fileExt != 'zip'  ) {
          $GLOBALS['manStatusError']=1;
          $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
        } else {
          $fileName = "$strFileName.$fileExt";

//          $strSqlQuery = "SELECT pg_id FROM ".$_db_tables['stGenplan']." WHERE pg_filename='".mysql_real_escape_string($fileName)."'";
//          $arrAlreadyExistTitle = $objDb->fetch( $strSqlQuery );
//
//          if ( is_array( $arrAlreadyExistTitle ) ) {
//            $GLOBALS['manStatusError']=1;
//            $GLOBALS['manCodeError'][]['code'] = 'msgAlreadyExist';
//          }

          $dirFiles = DROOT."storage";
          if ( !file_exists($dirFiles) ) {
            mkdir($dirFiles, 0775);
          }

          $dirFiles = $dirFiles."/files";
          if ( !file_exists($dirFiles) ) {
            mkdir($dirFiles, 0775);
          }

          $dirFiles = $dirFiles."/genplan";
          if ( !file_exists($dirFiles) ) {
            mkdir($dirFiles, 0775);
          }
          move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirFiles."/".$fileName );
        }
        /////////////////////////////////////////////////////////////////////////////////////

        if ($GLOBALS['manStatusError']!=1) {
          $strSqlQuery = "UPDATE ".$_db_tables['stGenplan']." SET pg_filename='".mysql_real_escape_string($fileName)."'"
            . ", pg_date_change = NOW() WHERE pg_id='$intRecordId'";
          if ( !$objDb->query($strSqlQuery) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
          }
        }

      } elseif ($_FILES['flPhoto']['size'] > $maxFileSize) {
      	$GLOBALS['manStatusError']=1;
      	$GLOBALS['manCodeError'][]['code'] = 'msgTooBigFile';
      }
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
      if ( $_POST['cbxReturn']=='on' ) {
        header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId&stPage=$stPage");
      } else {
        header('location:'.$arrTplVars['module.name']."?errMess=msgOk&stPage=$stPage");
      }
      exit;
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( $_GET['action'] == 'add' || (isset($_GET['id']) && intval($_GET['id']) > 0) ) {
  $arrIf['editrecord'] = true;
  $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stGenplan']." WHERE pg_id ='$intRecordId'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strFileTitle'] = htmlspecialchars(stripslashes($arrInfo['pg_title']));
    $arrTplVars['strFileName'] = htmlspecialchars(stripslashes($arrInfo['pg_filename']));

    if (file_exists(PRJ_FILES.'genplan/'.$arrInfo['pg_filename'])) {
      $arrIf['is.file'] = true;
    }

    $arrTplVars['cbxStatus'] = $arrInfo['pg_status'] == 'Y' ? ' checked' : '';
  }
} else {
  // Всего записей в базе
  $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stGenplan'];
  $arrTplVars['intQuantAllRecords'] = $arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
  // ***** BEGIN: Построение пейджинга для вывода списка
  $objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
  /**
  * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
  * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
  */
  $objPagination->strPaginatorTpl = "site.global.1.tpl";
  $objPagination->iQtyRecsPerPage = 12;
  $objPagination->strColorLinkStyle = "link-b";
  $objPagination->strColorActiveStyle = "tab-bl-b";
  $objPagination->strNameImageBack = "arrow_one_left.gif";
  $objPagination->strNameImageForward = "arrow_one_right.gif";
  // Создаем блок пэйджинга
  $objPagination->paCreate();
  // ***** END: Построение пейджинга для вывода списка
  // Запрос для выборки нужных записей
  $strSqlQuery = "SELECT * FROM ".$_db_tables['stGenplan']." ORDER BY pg_id DESC ".$objPagination->strSqlLimit;
  $arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
  // кол-во публикаций показанных на странице
  $arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
  // Присвоение значения пэйджинга для списка
  $arrTplVars['blockPaginator'] = $objPagination->paShow();

  $arrIf['records'] = $arrTplVars['intQuantShowRecOnPage']>0;

  if ( is_array($arrSelectedRecords) ) {
    foreach ( $arrSelectedRecords as $key => $value ) {
      $arrSelectedRecords[$key]['cbxStatus'] = $value['pg_status'] == 'Y' ? ' checked' : '';
    }
  }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

$arrIf['showlist'] = !$arrIf['editrecord'];
$arrIf['norecords'] = !$arrIf['records'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
