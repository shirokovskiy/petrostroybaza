<?php
$arrTplVars['module.name'] = "fragments";
$arrTplVars['module.title'] = "ФРАГМЕНТЫ СТРАНИЦ";
$arrTplVars['module.name.child'] = "fragments.edit";

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars["error".$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars["error".$errSuf] = $objUtil->echoMessage($arrTplVars["error".$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Кол-во фрагментов сайта
$strSqlQuery = "SELECT COUNT(*) AS strQuantFragments FROM ".$_db_tables['stFragments']." WHERE sf_id_project='".$_SESSION['intIdDefaultProject']."'";
$arrTplVars['strQuantAllRecords'] = $objDb->fetch( $strSqlQuery , 'strQuantFragments');

// Формируем список групп
$strSqlQuery = "SELECT * FROM ".$_db_tables['stFragmentsGroup']." WHERE sfg_parent IS NULL AND sfg_id_project='".$_SESSION['intIdDefaultProject']."'";
$arrLstItem = $objDb->fetchall($strSqlQuery);

$arrTplVars['intIdItem'] = intval($_GET['idItem']);

if ( !empty( $arrLstItem ) ) {
  foreach($arrLstItem as $key=>$value) {
  	$strTmpStyleHref = ($arrTplVars['intIdItem']==$arrLstItem[$key]['sfg_id']) ? ' style="color: #FF7707;"' : '';
    $arrItemForTpl[$key] = array('intIdItem'=>$arrLstItem[$key]['sfg_id'], 'strNameItem'=>$arrLstItem[$key]['sfg_name'], 'strHrefStyle'=>$strTmpStyleHref);
    $arrParent[$arrLstItem[$key]['sfg_id']] = true;
  }
} else { // Если каталог пуст
  $arrIf['block.catalogue.empty'] = true;
}
$objTpl->tpl_loop($arrTplVars['module.name'], "lst.item", $arrItemForTpl);

if ($arrTplVars['intIdItem']>0) {
  if ( $arrParent[$arrTplVars['intIdItem']]!=true ) {
    $inIdParentSubItem = $objDb->fetch( "SELECT sfg_parent FROM ".$_db_tables['stFragmentsGroup']." WHERE sfg_id='".$arrTplVars['intIdItem']."'" , 'sfg_parent');
  } else {
    $inIdParentSubItem = $arrTplVars['intIdItem'];
  }

  $arrIf['block.'.$inIdParentSubItem] = true;
  $arrIf['block.show.fragments'] = true;

  $arrLstSubItem = $objDb->fetchall( "SELECT * FROM ".$_db_tables['stFragmentsGroup']." WHERE sfg_parent='".$inIdParentSubItem."'" );
  if ( is_array($arrLstSubItem) ) {
  	foreach ($arrLstSubItem as $key=>$value) {
  		$strTmpStyleHref = ($arrTplVars['intIdItem']==$arrLstSubItem[$key]['sfg_id']) ? ' style="color: #FF7707;"' : '';
      $arrSubItemForTpl[$key] = array('intIdSubItem'=>$arrLstSubItem[$key]['sfg_id'], 'strNameSubItem'=>$arrLstSubItem[$key]['sfg_name'], 'strSubHrefStyle'=>$strTmpStyleHref);
    }
  }

  $objTpl->tpl_loop($arrTplVars['module.name'], "lst.subitem.".$inIdParentSubItem, $arrSubItemForTpl);
}
// **************************************************************************************



$strSqlQuery = "SELECT sf_id, sf_name, sf_id_name, sf_body FROM ".$_db_tables['stFragments']
  . " WHERE sf_id_parent='".$arrTplVars['intIdItem']."' AND sf_id_project='".$_SESSION['intIdDefaultProject']."'";
$arrLstFragments = $objDb->fetchall($strSqlQuery);

if (is_array($arrLstFragments)) {
  foreach($arrLstFragments as $key=>$value) {
    $arrLstFrgForTpl[$key]['intIdFrg'] = $arrLstFragments[$key]['sf_id'];
    $arrLstFrgForTpl[$key]['strNameFrg'] = $arrLstFragments[$key]['sf_name'];
    $arrLstFrgForTpl[$key]['strUNameFrg'] = $arrLstFragments[$key]['sf_id_name'];

    $strFileContent = $objFile->readFile($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/fragments/{$arrLstFragments[$key]['sf_id_name']}.frg");
  	$strBaseContent = stripslashes($arrLstFragments[$key]['sf_body']);

  	if(!$objUtil->strCompared($strBaseContent, $strFileContent)) { // если размеры файла фрагмента и размера из БД не совпадают
      $arrIf["difference.in.content.".$arrLstFragments[$key]['sf_id']] = true;
      $arrTplVars['intDiffs'] = $objUtil->lenCompared;
  	}
  }
  $arrIf['block.lst.yes'] = true;
} else {
  $arrIf['block.lst.no'] = true;
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.fragments", $arrLstFrgForTpl, array($arrTplVars['cBgColor'], $arrTplVars['cThirdColor']));

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);