<?php
$arrTplVars['module.name'] = "pages.edit";
$arrTplVars['module.title'] = "СТРАНИЦЫ ПРОЕКТОВ";
$arrTplVars['module.parent'] = "pages";

$arrTplVars['intIdItem'] = intval($_GET['idItem']); // ID группы в которую входит редактируемая страница
$arrTplVars['intIdEditPage'] = intval($_GET['idRec']); // ID редактируемой страницы

$arrTplVars['cbxReturn_checked'] = ($_GET['strReturn']=='true') ? ' checked' : '';

// ***** BEGIN: СОХРАНЕНИЕ РЕДАКТИРОВАНИЯ/СОЗДАНИЯ СТРАНИЦЫ САЙТА ***********************
if ( $_POST['frmPageAddEdit']=='true' ) {
	$arrDataForSql['intIdEditPage'] = $arrTplVars['intIdEditPage'] = intval($_POST['intIdEditPage']);

	$arrDataForSql['strPageName'] = addslashes(trim($_POST['strPageName']));
	$arrTplVars['strPageName'] = htmlspecialchars(trim($_POST['strPageName']));

	$strOriginalPageAlias = trim($_POST['strPageAlias']);

	$arrDataForSql['strPageAlias'] = addslashes($strOriginalPageAlias);
	$arrTplVars['strPageAlias'] = htmlspecialchars($strOriginalPageAlias);

	if ($objUtil->checkBadChars($strOriginalPageAlias)) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgIncorrectAlias';
	}

	$arrDataForSql['strPageBody'] = addslashes(trim($_POST['strPageBody']));
	$arrTplVars['strPageBody'] = htmlspecialchars(trim($_POST['strPageBody']));
	$arrDataForFile['strPageBody'] = stripslashes(trim($_POST['strPageBody']));

	$arrDataForSql['intPageIdTpl'] = $arrTplVars['intPageIdTpl'] = intval($_POST['intPageIdTpl']);

	$arrDataForSql['strPageTitle'] = addslashes(trim($_POST['strPageTitle']));
	$arrTplVars['strPageTitle'] = htmlspecialchars(trim($_POST['strPageTitle']));

	$arrDataForSql['strPageDescription'] = addslashes(trim($_POST['strPageDescription']));
	$arrTplVars['strPageDescription'] = htmlspecialchars(trim($_POST['strPageDescription']));

	$arrDataForSql['strPageKeywords'] = addslashes(trim($_POST['strPageKeywords']));
	$arrTplVars['strPageKeywords'] = htmlspecialchars(trim($_POST['strPageKeywords']));

	$arrDataForSql['strPageStatus'] = ($_POST['cbxPageStatus']=='on') ? 'Y' : 'N';
	$arrTplVars['cbxPageStatus_checked'] = ($_POST['cbxPageStatus']=='on') ? ' checked' : '';
	$arrDataForSql['intPageGroup'] = $arrTplVars['intPageGroup'] = intval($_POST['intPageGroup']);

	$arrDataForSql['strPublish'] = ($_POST['cbxPublish']=='on') ? "NOW()" : "NULL";

	$arrTplVars['cbxPublish_checked'] = ($_POST['cbxPublish']=='on') ? ' checked' : '';
	$arrTplVars['cbxReturn_checked'] = ($_POST['cbxReturn']=='on') ? ' checked' : '';

	if(empty($arrDataForSql['strPageName'])) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = '105page';
	}

	if(empty($arrDataForSql['strPageAlias'])) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = '107page';
	}

	if($arrDataForSql['intPageIdTpl']==0) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = '106page';
	}

	if(empty($arrDataForSql['strPageBody'])) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = '109page';
	}

	if($arrDataForSql['intPageGroup']==0) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = '104page';
	}

	if ( $GLOBALS['manStatusError']!=1 ) { // Если ошибок нет, приступаем к сохранению
		/**
		 * Проверяем на уникальность ALIAS имени страницы
		 */
		$strSqlQuery = "SELECT COUNT(*) AS idAlreadyPage FROM ".$_db_tables['stPages']
			. " WHERE sp_alias='".$arrDataForSql['strPageAlias']."' AND sp_id_project=".$_SESSION['intIdDefaultProject'];
		$idAlreadyPage = $objDb->fetch( $strSqlQuery , 'idAlreadyPage');
		if ($idAlreadyPage>0 && empty($arrDataForSql['intIdEditPage']) ) { // Если добавляется новая страница и ее ALIAS-имя уже существует
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = '110page';
		} else {
			$strSqlFields = ""
				. " sp_id_parent='".$arrDataForSql['intPageGroup']."'"
				. ", sp_name='".$arrDataForSql['strPageName']."'"
				. ", sp_alias='".$arrDataForSql['strPageAlias']."'"
				. ", sp_body='".$arrDataForSql['strPageBody']."'"
				. ", sp_id_template='".$arrDataForSql['intPageIdTpl']."'"
				. ", sp_status='".$arrDataForSql['strPageStatus']."'"
				. ", sp_publish=".$arrDataForSql['strPublish'].""
				. ", sp_id_project='".$_SESSION['intIdDefaultProject']."'"
				. "";
			if (empty($arrDataForSql['intIdEditPage'])) { // Добавляем новую страницу
				$strSqlQuery = "INSERT INTO ".$_db_tables['stPages']." SET $strSqlFields";
			} else {
				$strSqlQuery = "UPDATE ".$_db_tables['stPages']." SET $strSqlFields WHERE sp_id='".$arrDataForSql['intIdEditPage']."'";
			}

			if(!$objDb->query($strSqlQuery)) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
			} else {
				/**
				 * Мета-теги для страницы
				 */
				$strSqlFields = ""
					. " spm_title='".$arrDataForSql['strPageTitle']."'"
					. ", spm_keywords='".$arrDataForSql['strPageKeywords']."'"
					. ", spm_description='".$arrDataForSql['strPageDescription']."'"
					. "";
				if (empty($arrDataForSql['intIdEditPage'])) {
					$arrDataForSql['intIdEditPage'] = $arrTplVars['intIdEditPage'] = $objDb->insert_id();
					$strSqlQuery = "INSERT INTO ".$_db_tables['stMeta']." SET $strSqlFields, spm_id_rec = '".$arrDataForSql['intIdEditPage']."'";
				} else {
					$strSqlQuery = "UPDATE ".$_db_tables['stMeta']." SET $strSqlFields WHERE spm_id_rec='".$arrDataForSql['intIdEditPage']."' AND spm_subpage_uid IS NULL";
				}

				if(!$objDb->query($strSqlQuery)) { // Добавляем/изменяем META-тэги страницы
					$GLOBALS['manStatusError']=1;
					$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
				} else {
					if ($_POST['cbxPublish']=='on') { // Если нужна публикация страницы (пишем в файл)
						$saveFileNameTpl = $_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/pages/$strOriginalPageAlias.".$arrDataForSql['intIdEditPage'].".tpl";
						$objFile->writeFile(
										$objTpl->parseFormVars($arrDataForFile['strPageBody'], "out")
										, "$strOriginalPageAlias.".$arrDataForSql['intIdEditPage'].".tpl"
										, $_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/pages/"
						);
						@chmod($saveFileNameTpl, 0664);

						/**
						 * Пишем обработчик страницы
						 */
						if ( !file_exists($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."php/pages/$strOriginalPageAlias.".$arrDataForSql['intIdEditPage'].".php")) {
							$bodyFileAction = '<?php'."\n".
							'/** Created by WMBM(c) '.date('Y').' (Shirokovskiy Dmitry aka Jimmy™).'."\n".
							' * Страница: '.stripslashes($arrDataForSql['strPageName']).' ['.stripslashes($arrDataForSql['strPageAlias']).'] */'."\n".
							'$objTpl->Template(SITE_TPL_PAGE_DIR);'."\n".
							'$objTpl->tpl_load("page.contents", "'.$strOriginalPageAlias.'.'.$arrDataForSql['intIdEditPage'].'.tpl");'."\n".
							'$objTpl->tpl_array("page.contents", $arrTplVars);'."\n";

							$objFile->writeFile(
											$bodyFileAction
											, "$strOriginalPageAlias.".$arrDataForSql['intIdEditPage'].".php"
											, $_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."php/pages/"

							);
							@chmod($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."php/pages/$strOriginalPageAlias.".$arrDataForSql['intIdEditPage'].".php", 0664);
						}

						$GLOBALS['manCodeError'][1]['code'] = '101page';
					} else {
						$GLOBALS['manCodeError'][1]['code'] = '100page';
					}

					if ($_POST['cbxReturn']=='on') {
						header("location: ".$arrTplVars['module.name']."?idItem=".$arrTplVars['intPageGroup']."&idRec=".$arrTplVars['intIdEditPage']."&errMess=".$GLOBALS['manCodeError'][1]['code']."&strReturn=true");
					} else {
						header("location: ".$arrTplVars['module.parent']."?idItem=".$arrTplVars['intPageGroup']."&errMess=".$GLOBALS['manCodeError'][1]['code']);
					}
					exit();
				}
			}
		}
	}
}
// ***** END: СОХРАНЕНИЕ РЕДАКТИРОВАНИЯ/СОЗДАНИЯ СТРАНИЦЫ САЙТА *************************



// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

/**
 * Если известен идентификатор страницы, загрузим её
 */
if ( $arrTplVars['intIdEditPage']>0 ) {
	$strSqlQuery = "SELECT * FROM ".$_db_tables['stPages']
		. " LEFT JOIN ".$_db_tables['stMeta']." ON spm_id_rec=sp_id"
		. " WHERE sp_id='".$arrTplVars['intIdEditPage']."'";
	$arrInfoPage = $objDb->fetch( $strSqlQuery );

	if ( is_array($arrInfoPage) ) {
		$arrIf['block.page.edit'] = true; // Открываем блоки свойственные только для редактирования страницы сайта

		if ( $GLOBALS['manStatusError']!=1 ) { // Если нет ошибок при сохранении формы
			$arrTplVars['intPageIdTpl'] = $arrInfoPage['sp_id_template'];
			$arrTplVars['intPageGroup'] = $arrInfoPage['sp_id_parent'];

			$arrTplVars['intIdEditPage'] = $arrInfoPage['sp_id'];
			$arrTplVars['strPageName'] = htmlspecialchars($arrInfoPage['sp_name']);
			$arrTplVars['strPageAlias'] = htmlspecialchars($arrInfoPage['sp_alias']);
			$strOriginalPageAlias = $arrInfoPage['sp_alias'];

			/** >>************************************************************************\
			** Thu Nov 16 00:24:20 MSK 2006, Shirokovskiy Dmitry aka Jimmy™
			* Проверяем идентичность шаблона в базе и на диске **/
            #var_dump(  $_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']  );
            #die("<p><b style='color:red'>".__FILE__.':'.__LINE__."</b></p>");
			$strFileContent = $objFile->readFile($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/pages/$strOriginalPageAlias.".$arrTplVars['intIdEditPage'].".tpl");

			$strFileContent = $objTpl->parseFormVars($strFileContent, "in");
			$arrTplVars['strFileContent'] = htmlspecialchars($strFileContent);

			//$arrTplVars['strFileContent'] = htmlspecialchars($objTpl->parseFormVars($strFileContent, "in"));
			$strBaseContent = stripslashes($arrInfoPage['sp_body']);

			// Перенесено
//			$arrIf["difference.in.content"] = !$objUtil->strCompared($strBaseContent, $strFileContent); // если размеры файла фрагмента и размера из БД не совпадают

			foreach ($objTpl->arrLangWord as $specword) {
                if (!empty($arrInfoPage['sp_body'])) {
                    $arrInfoPage['sp_body'] = str_replace('[if gte mso','',$arrInfoPage['sp_body']);
                }
                if (!empty($arrTplVars['strFileContent'])) {
                    $arrTplVars['strFileContent'] = str_replace('[if gte mso','',$arrTplVars['strFileContent']);
                }
				$specode = preg_match("/$specword/", $arrInfoPage['sp_body']) || preg_match("/$specword/", $arrTplVars['strFileContent']);
				if ($specode) {
					break;
				}
			}

            if ($_GET['debug']) {
                $arrTplVars['strPageBody'] = htmlspecialchars($arrInfoPage['sp_body']);
            } else {
                $arrTplVars['strPageBody'] = htmlspecialchars($strFileContent);
            }

			$arrTplVars['strPageTitle'] = htmlspecialchars($arrInfoPage['spm_title']);
			$arrTplVars['strPageDescription'] = htmlspecialchars($arrInfoPage['spm_description']);
			$arrTplVars['strPageKeywords'] = htmlspecialchars($arrInfoPage['spm_keywords']);

			$arrTplVars['cbxPageStatus_checked'] = ($arrInfoPage['sp_status']=='Y') ? ' checked' : '';
			if(is_null($arrInfoPage['sp_publish'])) {
				$arrTplVars['strNoPublishMessage'] = $errMsg['150page'];
				$arrIf['message.noPublish.yes'] = true;
				$arrIf['block.page.edit'] = true;
			} else {
				$arrTplVars['strLastPublishDate'] = $objUtil->workDate(4, $arrInfoPage['sp_publish']);
			}
		}
	} else {
		$arrIf['block.page.add'] = true; // Открываем блоки свойственные только для создания страницы сайта
	}
} else {
	$arrIf['block.page.add'] = true; // Открываем блоки свойственные только для создания страницы сайта
}

if ($arrIf['block.page.add']) {
    // Include SPAW editor
    include_once(DROOT.'spaw2/spaw.inc.php');
    $objSpawEditor = new SpawEditor("strPageBody", "");
    $objSpawEditor->setConfigValueElement('PG_SPAWFM_SETTINGS', 'allow_modify', 1);
    $objSpawEditor->setConfigValueElement('PG_SPAWFM_SETTINGS', 'allow_upload', 1);
    $arrTplVars['strPageBody'] = $objSpawEditor->getHtml();

	/*include_once(SITE_LIB_DIR."pageditor/editor.php");
	$objFCKEditor = new FCKeditor('strPageBody') ;
	$objFCKEditor->BasePath = SITE_URL.'lib/pageditor/';
	$objFCKEditor->Value = $arrInfoPage['sp_body'];
	$arrTplVars['strPageBody'] = $objFCKEditor->Create();*/
}

$arrIf['specode'] = $specode;
$arrIf['htmlcode'] = !$specode;

// ***** Шаблоны для страниц ************************************************************
$strSqlQuery = "SELECT st_id, st_name FROM ".$_db_tables['stTpl']." WHERE st_project='".$_SESSION['intIdDefaultProject']."' AND st_status='Y'";
$arrTemplates = $objDb->fetchall( $strSqlQuery );

if(is_array($arrTemplates)) {
	foreach($arrTemplates as $key=>$value) {
		$arrTemplatesTpl[$key]['intIdTpl_selected'] = ( $arrTemplates[$key]['st_id']==$arrTplVars['intPageIdTpl']) ? ' selected' : '';
		$arrTemplatesTpl[$key]['intIdTpl'] = $arrTemplates[$key]['st_id'];
		$arrTemplatesTpl[$key]['strNameTpl'] = htmlspecialchars($arrTemplates[$key]['st_name']);
	}
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.templates", $arrTemplatesTpl);
// **************************************************************************************

// ***** Группы страниц *****************************************************************
$strSqlQuery = "SELECT * FROM ".$_db_tables['stPagesGrp']." WHERE spg_id_project='".$_SESSION['intIdDefaultProject']."' AND spg_status='Y'";
$arrListGroup = $objDb->fetchall( $strSqlQuery );

if(is_array($arrListGroup)) {
	foreach($arrListGroup as $key=>$value) {
		if($arrListGroup[$key]['spg_id_parent']===NULL) {
			$arrListGroupForm[$key]['name'] = $arrListGroup[$key]['spg_name'];
			$arrListGroupForm[$key]['id'] = $arrListGroup[$key]['spg_id'];
		} else {
			$arrListSubGroupForm[$arrListGroup[$key]['spg_id_parent']][] = array('id'=>$arrListGroup[$key]['spg_id'], 'name'=>stripslashes($arrListGroup[$key]['spg_name']));
		}
	}

	foreach($arrListGroupForm as $key=>$value) {
		if($arrTplVars['intPageGroup'] == $arrListGroupForm[$key]['id']) // Если эта группа была выбрана, ставим ей "selected"
			$arrListGroupToTpl[] = array('intIdGroup'=>$arrListGroupForm[$key]['id'], 'strNameGroup'=>$arrListGroupForm[$key]['name'], 'intIdGroup_selected'=>' selected');
		else
			$arrListGroupToTpl[] = array('intIdGroup'=>$arrListGroupForm[$key]['id'], 'strNameGroup'=>$arrListGroupForm[$key]['name']);

		if(count($arrListSubGroupForm[$arrListGroupForm[$key]['id']])>0) {
			foreach($arrListSubGroupForm[$arrListGroupForm[$key]['id']] as $kkey=>$vvalue) {
				if($arrTplVars['intPageGroup'] == $arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id']) // Если эта группа была выбрана, ставим ей "selected"
					$arrListGroupToTpl[] = array('intIdGroup'=>$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id'], 'strNameGroup'=>'--> '.$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['name'], 'intIdGroup_selected'=>' selected');
				else
					$arrListGroupToTpl[] = array('intIdGroup'=>$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id'], 'strNameGroup'=>'--> '.$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['name']);
			}
		}
	}
}


$objTpl->tpl_loop($arrTplVars['module.name'], "lst.group.pages", $arrListGroupToTpl);
// **************************************************************************************

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
