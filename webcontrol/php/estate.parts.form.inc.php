<?php
$arrTplVars['module.name'] = "estate.parts.form";
$arrTplVars['module.parent'] = "estate.parts";
$arrTplVars['module.title'] = "Добавить раздел";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset($_POST['frmEstateType']) && $_POST['frmEstateType'] == 'true' ) {
  $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
  $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

  $arrSqlData['strEstateTypeName'] = addslashes(trim($_POST['strEstateTypeName']));
  $arrTplVars['strEstateTypeName'] = htmlspecialchars(stripslashes(trim($_POST['strEstateTypeName'])));

  if (empty($arrSqlData['strEstateTypeName']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
  }

  if (!empty($_POST['strEstateTypeAlias'])) {
    $_POST['strEstateTypeAlias'] = $objUtil->translitKyrToLat($objUtil->translitBadChars($_POST['strEstateTypeAlias']));
  }

  $arrSqlData['strEstateTypeAlias'] = addslashes(trim($_POST['strEstateTypeAlias']));
  $arrTplVars['strEstateTypeAlias'] = htmlspecialchars(stripslashes(trim($_POST['strEstateTypeAlias'])));

  $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
  $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

  $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
  $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

  if ($GLOBALS['manStatusError']!=1) {
    $strSqlFields = ""
      . " set_name = '{$arrSqlData['strEstateTypeName']}'"
      . ", set_alias = '{$arrSqlData['strEstateTypeAlias']}'"
      . ", set_status = '{$arrSqlData['cbxStatus']}'"
      ;

    if ( empty($intRecordId) ) {
      $strSqlQuery = "INSERT INTO ".$_db_tables['stEstTypes']." SET".$strSqlFields
        . ", set_date = NOW()";
    } else {
      $strSqlQuery = "UPDATE ".$_db_tables['stEstTypes']." SET".$strSqlFields
        . " WHERE set_id = '$intRecordId'";
    }

    if ( !$objDb->query( $strSqlQuery ) ) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      if ( empty($intRecordId) ) {
        $intRecordId = $objDb->insert_id();
      }
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
      if ( $arrSqlData['cbxReturn'] == 'Y' ) {
        header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
      } else {
        header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
      }
      exit;
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stEstTypes']." WHERE set_id ='$intRecordId'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strEstateTypeName'] = htmlspecialchars(stripslashes($arrInfo['set_name']));
    $arrTplVars['strEstateTypeAlias'] = htmlspecialchars(stripslashes($arrInfo['set_alias']));

    $arrTplVars['cbxStatus'] = $arrInfo['set_status'] == 'Y' ? ' checked' : '';
  }
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);