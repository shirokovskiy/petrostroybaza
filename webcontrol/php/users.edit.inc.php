<?php
$arrTplVars['module.name'] = "users.edit";
$arrTplVars['module.title'] = "АДМИНИСТРАТОРЫ";
$arrTplVars['module.name.parent'] = "users";

$arrTplVars['stPage'] = ( intval($_GET['stPage']) > 0 ? $_GET['stPage'] : 1 );

/************************************
** Shirokovskiy D.2005 Jimmy™. Mon Oct 10 11:57:31 MSD 2005
**
** При сохранении параметров пользователя, используем 1 форму с несколькими кнопками Submit-а
** чтобы разделить запись данных по порциям, а не использовать "огромный массив данных".
**
** За каждую порцию данных отвечает своя кнопка сабмита, и соответственно свой кусок обработчика
** несмотря на то, что в POST всё равно приходят ВСЕ данные и только одна из нажатых Submit.
*/

if ( isset($_POST['frmUserAddEdit']) && $_POST['frmUserAddEdit'] == 'true' ) {

  if ( isset($_POST['saveUserInfo']) ) {
    # Запись данных о пользователе
    $arrSqlData['strLNameUser'] = addslashes(trim($_POST['strLNameUser']));
    $arrTplVars['strLNameUser'] = htmlspecialchars(stripslashes(trim($_POST['strLNameUser'])));

    $arrSqlData['strFNameUser'] = addslashes(trim($_POST['strFNameUser']));
    $arrTplVars['strFNameUser'] = htmlspecialchars(stripslashes(trim($_POST['strFNameUser'])));

    $arrSqlData['strPNameUser'] = addslashes(trim($_POST['strPNameUser']));
    $arrTplVars['strPNameUser'] = htmlspecialchars(stripslashes(trim($_POST['strPNameUser'])));

    $arrSqlData['strLoginUser'] = addslashes(trim($_POST['strLoginUser']));
    $arrTplVars['strLoginUser'] = htmlspecialchars(stripslashes(trim($_POST['strLoginUser'])));

    $arrSqlData['strPassUser'] = addslashes(trim($_POST['strPassUser']));
    $arrTplVars['strPassUser'] = htmlspecialchars(stripslashes(trim($_POST['strPassUser'])));

    if ( empty($_POST['intIdUser']) && empty($arrSqlData['strPassUser'])) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = '102usr';
    }

    $arrSqlData['strEmailUser'] = addslashes(trim($_POST['strEmailUser']));
    $arrTplVars['strEmailUser'] = htmlspecialchars(stripslashes(trim($_POST['strEmailUser'])));

    $arrSqlData['bolSexUser'] = $_POST['bolSexUser'];
    $arrTplVars['bolSexUser_'.$_POST['bolSexUser']] = ' checked';

    $arrSqlData['chUserStatus'] = ($_POST['chUserStatus']=='on'?'Y':'N');
    $arrTplVars['chUserStatus_ch'] = ($_POST['chUserStatus']=='on'?' checked':'');

    $arrSqlData['intGroupID'] = $arrTplVars['intGroupID'] = intval(trim($_POST['intGroupID']));

    if (empty($arrSqlData['strLNameUser']) || empty($arrSqlData['strFNameUser']) || empty($arrSqlData['strLoginUser']) || empty($arrSqlData['bolSexUser']) ) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = '102usr';
    }

    if ( $GLOBALS['manStatusError']!=1 ) {

      $strSqlFields = ""
        . " cu_group      = '".$arrSqlData['intGroupID']."'"
        . ", cu_fname     = '".$arrSqlData['strFNameUser']."'"
        . ", cu_lname     = '".$arrSqlData['strLNameUser']."'"
        . ", cu_pname     = '".$arrSqlData['strPNameUser']."'"
        . ", cu_login     = '".$arrSqlData['strLoginUser']."'"
        . ( !empty($arrSqlData['strPassUser']) ? ", cu_password  = MD5('".$arrSqlData['strPassUser']."')" : "" )
        . ", cu_status    = '".$arrSqlData['chUserStatus']."'"
        . ", cu_email     = '".$arrSqlData['strEmailUser']."'"
        . ", cu_gender    = '".$arrSqlData['bolSexUser']."'"
        ;
    	if ( intval($_POST['intIdUser']) > 0 ) {
        $strSqlQuery = "UPDATE ".$_db_tables["cmsUsers"]." SET $strSqlFields WHERE cu_id = '".$_POST['intIdUser']."'";
      } else {
        $strSqlQuery = "INSERT INTO ".$_db_tables["cmsUsers"]." SET $strSqlFields";
      }
      if ( !$objDb->query($strSqlQuery) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
      } else {
        if ( intval($_POST['intIdUser']) > 0 ) {
          $editedUserID = $_POST['intIdUser'];
        } else {
        	$editedUserID = $objDb->insert_id();
        }

        header('location: '.$arrTplVars['module.name']."?idManager=$editedUserID&stPage=".$arrTplVars['stPage']."&errMess=100usr");
        exit();
      }
    }
  }

  if ( isset($_POST['saveUserProjects']) ) {
    // сначала всё выключим
    $strSqlQuery = "UPDATE ".$_db_tables["cmsPrjAccess"]." SET fpa_status = 'N' WHERE fpa_id_user='".$_POST['intIdUser']."'";
    if ( !$objDb->query($strSqlQuery) ) {
      echo("Error: $strSqlQuery");echo "<br/>\n";echo(__FILE__);echo "<br/>\n";echo(__LINE__);
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    }

    // и если надо добавим, там где надо
    if ( isset($_POST['arrAccessProject']) && is_array($_POST['arrAccessProject'])) {
      foreach ( $_POST['arrAccessProject'] as $key => $value ) {
        $strSqlQuery = "UPDATE ".$_db_tables["cmsPrjAccess"]." SET fpa_status = 'Y'"
          . " WHERE fpa_id_project='".$key."' AND fpa_id_user='".$_POST['intIdUser']."'";
        if ( !$objDb->query($strSqlQuery) ) {
          echo("Error: $strSqlQuery");echo "<br/>\n";echo(__FILE__);echo "<br/>\n";echo(__LINE__);
          $GLOBALS['manStatusError']=1;
          $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
          if ( $objDb->affected() == 0 ) {
            $strSqlQuery = "INSERT INTO ".$_db_tables["cmsPrjAccess"]." SET"
              . " fpa_id_project='".$key."'"
              . ", fpa_id_user='".$_POST['intIdUser']."'"
              . ", fpa_status = 'Y'"
              ;
            if ( !$objDb->query($strSqlQuery) ) {
              echo("Error: $strSqlQuery");echo "<br/>\n";echo(__FILE__);echo "<br/>\n";echo(__LINE__);
              $GLOBALS['manStatusError']=1;
              $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            }
          }
        }
      }
    }

    $errSuf = '_project';
    $GLOBALS['manCodeError'][1]['code'] = '101usr';
  }

  if ( isset($_POST['saveUserPermissions']) ) {
    $userID = intval($_POST['intIdUser']);
    $arrModulesOn = $_POST['arrModule'];

    if ( !empty( $userID ) ) {
      $strSqlQuery = "UPDATE ".$_db_tables["cmsAccess"]." SET fa_access='N' WHERE fa_id_owner='$userID'";
      if ( !$objDb->query($strSqlQuery) ) {
        print_r( $strSqlQuery );
        die(__FILE__."\n".__LINE__."\n");
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
      }

      if ( !empty( $arrModulesOn ) ) {
        foreach ( $arrModulesOn as $key => $value ) {
          $isAccess = '';
          $strSqlQuery = "SELECT fa_access FROM ".$_db_tables["cmsAccess"]." WHERE fa_id_owner='$userID' AND fa_module='$key'";
          $isAccess = $objDb->fetch( $strSqlQuery , 0);

          if ( empty($isAccess) ) {
            $strSqlQuery = "INSERT INTO ".$_db_tables["cmsAccess"]." SET"
              . " fa_type='user'"
              . ", fa_access='Y'"
              . ", fa_id_owner='$userID'"
              . ", fa_module='$key'";
            if ( !$objDb->query($strSqlQuery) ) {
              print_r( $strSqlQuery );
              die(__FILE__."\n".__LINE__."\n");
              $GLOBALS['manStatusError']=1;
              $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            }
          } else {
            if ( $isAccess == 'N' ) {
              $strSqlQuery = "UPDATE ".$_db_tables["cmsAccess"]." SET fa_access='Y' WHERE fa_id_owner='$userID' AND fa_module='$key'";
              if ( !$objDb->query($strSqlQuery) ) {
                print_r( $strSqlQuery );
                die(__FILE__."\n".__LINE__."\n");
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
              }
            }
          }
        }
      }
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$arrTplVars['intIdEditUser'] = intval($_GET['idManager']);

/**
 * Загрузка данных
 */
if( $arrTplVars['intIdEditUser']>0 ) {
  $arrIf['access.prj'] = true;
  $arrTplVars['urlReload'] = ADMINURL.$arrTplVars['module.name']."?idManager={$arrTplVars['intIdEditUser']}&stPage={$arrTplVars['stPage']}";

  // Информация о пользователе
	$strSqlQuery = "SELECT * FROM ".$_db_tables['cmsUsers']." WHERE cu_id='".$arrTplVars['intIdEditUser']."'";
	$arrInfoEditUser = $objDb->fetch( $strSqlQuery );

	if(!is_array($arrInfoEditUser)) {
		header("location: ".$arrTplVars['module.name.parent']);
		exit;
	}

	$arrTplVars['strEditUserName'] = '['.$arrInfoEditUser['cu_lname'].' '.$arrInfoEditUser['cu_fname'].' '.$arrInfoEditUser['cu_pname'].']';

	$arrTplVars['intIdEditUser'] = $arrInfoEditUser['cu_id'];
	$arrTplVars['strLNameUser'] = htmlspecialchars($arrInfoEditUser['cu_lname']);
	$arrTplVars['strFNameUser'] = htmlspecialchars($arrInfoEditUser['cu_fname']);
	$arrTplVars['strPNameUser'] = htmlspecialchars($arrInfoEditUser['cu_pname']);
	$arrTplVars['strLoginUser'] = htmlspecialchars($arrInfoEditUser['cu_login']);
	//$arrTplVars['strPassUser'] = htmlspecialchars($arrInfoEditUser['cu_password']);
	$arrTplVars['strEmailUser'] = $arrInfoEditUser['cu_email'];
	#$arrTplVars['intUserAccess'] = $arrInfoEditUser['cu_group'];
	$arrTplVars['bolSexUser_'.$arrInfoEditUser['cu_gender']] = ' checked';
	$arrTplVars['chUserStatus'] = ( $arrInfoEditUser['cu_status']=='Y' ? ' checked' : '');


/**
 * BEGIN BLOCK #1: Выбираем доступ пользователя к проектам
 */
	$strSqlQuery = "SELECT * FROM ".$_db_tables['cmsPrjAccess']." WHERE fpa_id_user='".$arrTplVars['intIdEditUser']."' AND fpa_status='Y'";
	$arrAccessProjectUser = $objDb->fetchall( $strSqlQuery );

	if(is_array($arrAccessProjectUser)) {
		foreach($arrAccessProjectUser as $key=>$value) {
			$arrFormatAccessProject[$arrAccessProjectUser[$key]['fpa_id_project']] = true;
		}
	}
/**
 * END BLOCK #1
 */


  /**
   * Доступ к модулям
   */
  if ( isset($_GET['sort']) && !empty($_GET['sort']) ) {
    switch ($_GET['sort']) {
      case 'alias':
        $strSortField = "cm_link";
        break;

      case 'name':
        $strSortField = "cm_name";
        break;

      case 'id':
        $strSortField = "cm_id";
        break;

      default:
        $strSortField = "cm_id";
        break;
    }

    $arrTplVars[$_GET['sort'].'_sel'] = " selected";
  } else {
    $strSortField = "cm_id";
  }

  $strSqlQuery = "SELECT * FROM ".$_db_tables["cmsModules"]
    ." LEFT JOIN ".$_db_tables["cmsAccess"]." ON (fa_module=cm_id AND fa_id_owner='".$arrTplVars['intIdEditUser']."')"
    ." ORDER BY $strSortField";
  $arrModules = $objDb->fetchall( $strSqlQuery );

  if ( !empty( $arrModules ) ) {
    foreach ( $arrModules as $key => $value ) {
      $arrUserModulesAccess[$key]['moduleID'] = intval($value['cm_id']);
      $arrUserModulesAccess[$key]['moduleChecked'] = ((!empty($value['fa_module']) && $value['fa_access'] == 'Y')? ' checked' : '');
      $arrUserModulesAccess[$key]['moduleName'] = $value['cm_name'];
      $arrUserModulesAccess[$key]['moduleCssClass'] = ( empty($value['cm_id_parent']) ? ( $value['cm_type'] == "system" ? "tab-r" : "tab-o" ) : "tab-gb" );
      $arrUserModulesAccess[$key]['moduleAlias'] = $value['cm_link'];
    }

    $countOfColumns = 3;
    $rowsInCol = ceil(count($arrUserModulesAccess)/$countOfColumns);

    $arrUserModulesAccess  = array_chunk($arrUserModulesAccess, $rowsInCol, true);

    foreach ( $arrUserModulesAccess as $key => $value ) {
      $arrUserModulesAccess[$key]['keyArr'] = $key;
    }
  }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "list.user.modules.access", $arrUserModulesAccess);

if ( is_array( $arrUserModulesAccess ) ) {
  $arrIf['is.modules'] = true;
  foreach ( $arrUserModulesAccess as $key => $value ) {
    unset($value['keyArr']); // т.к. keyArr не вписывается в структуру массива для LOOP, keyArr был просто впомогательной переменной
    $objTpl->tpl_loop($arrTplVars['module.name'], "list.sub.modules.".$key, $value);
  }
}



$strSqlQuery = "SELECT * FROM ".$_db_tables['cmsProjects']." WHERE fp_status='Y'";
$arrInfoProjects = $objDb->fetchall( $strSqlQuery );

if(is_array($arrInfoProjects)) {
  foreach ($arrInfoProjects as $key=>$value) {
  	$arrInfoProjects[$key]['arrAccessProjectChecked'] = ($arrFormatAccessProject[$arrInfoProjects[$key]['fp_id']]==true) ? ' checked' : '';
  }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.projects", $arrInfoProjects);
$objTpl->tpl_loop($arrTplVars['module.name'], "lst.projects.module.access", $arrInfoProjects);


/************************************
** Shirokovskiy D.2005 Jimmy™. Mon Oct 10 13:36:17 MSD 2005
**
** Выборка групп пользователей
*/
$strSqlQuery = "SELECT * FROM ".$_db_tables["cmsUserGrp"]." WHERE cug_status='Y'";

$arrUserGroups = $objDb->fetchall( $strSqlQuery );
foreach ( $arrUserGroups as $key => $value ) {
  $arrUserGroups[$key]['intGroupID'] = $value['cug_id'];
  $arrUserGroups[$key]['intGroupID_sel'] = ( $arrInfoEditUser['cu_group']==$value['cug_id'] ? ' selected' : '');
  $arrUserGroups[$key]['strGroupName'] = $value['cug_name'];
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.user.groups", $arrUserGroups);


/************************************
** Shirokovskiy D.2005 Jimmy™. Tue Oct 11 15:19:27 MSD 2005
**
** Выборка модулей для определения доступа по проектам
*/
$strSqlQuery = "SELECT * FROM ".$_db_tables["cmsModules"]." Mls"
  . " LEFT JOIN ".$_db_tables["cmsPrjAccess"]." aPrj ON aPrj.fpa_id_project = Mls.xxxxxxx"
  ." WHERE ";
/* ДОДЕЛАТЬ */


//*********************************************************
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);