<?php
$arrTplVars['module.name'] = "templates.edit";
$arrTplVars['module.title'] = "РЕДАКТИРОВАНИЕ ШАБЛОНА";
$arrTplVars['module.parent'] = "templates"; // Главная в массиве страниц
$arrTplVars['strWorkTemplate'] = 'Новый шаблон';

// ***** Удаление выбранной записи ******************************************************
if($_POST['frmDeleteTpl']=='true') {
  $arrDataForSql['intIdTpl'] = intval($_POST['intIdTpl']);
  if($_POST['frmConfirmDeleteTpl']=='false') {
    header("location: ".$arrTplVars['module.parent']);
    exit();
  } else if($arrDataForSql['intIdTpl']>0) {
    $strSqlQuery = "DELETE FROM ".$_db_tables['stTpl']." WHERE st_id='".$arrDataForSql['intIdTpl']."'";
    if(!$objDb->query($strSqlQuery)) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      @unlink($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/tpl/site.global.".$arrDataForSql['intIdTpl'].".tpl");
      $GLOBALS['manCodeError'][1]['code'] = '106tpl';
      header("location: ".$arrTplVars['module.parent']."?errMess=".$GLOBALS['manCodeError'][1]['code']);
      exit;
    }
  }
}
// **************************************************************************************

// ***** Сохранение редактируемого шаблона **********************************************
if ($_POST['frmAddEditTpl']=='true') {

// ***** Заполнение переменных для вывода в SQL, Шаблон, Файл
	$arrDataForSql['intIdTpl'] = intval($_POST['intIdTpl']);
  $arrTplVars['intIdTpl'] = intval($_POST['intIdTpl']);

  $arrDataForSql['strTplName'] = addslashes(trim($_POST['strTplName']));
  $arrTplVars['strTplName'] = htmlspecialchars(trim($_POST['strTplName']));

  $arrDataForSql['strTplBody'] = addslashes(trim($_POST['strTplBody']));
  $arrTplVars['strTplBody'] = htmlspecialchars(trim($_POST['strTplBody']));
  $arrDataForFile['strTplBody'] = trim($_POST['strTplBody']);

  $arrDataForSql['strTplGroup'] = intval($_POST['intTplGroup']); // Каталог шаблона

  $arrDataForSql['bolTplPublish'] = ($_POST['cbxTplPublish']=='on') ? "NOW()" : "NULL";
  $arrDataForSql['bolTplStatus'] = ($arrDataForSql['intIdTpl']>0 && $_POST['bolTplStatus']!='on') ? 'N' : 'Y';

  $arrTplVars['cbxTplPublish_checked'] = ($_POST['cbxTplPublish']=='on') ? ' checked' : '';
  $arrTplVars['cbxTplReturn_checked'] = ($_POST['cbxTplReturn']=='on') ? ' checked' : '';

// ***** Проверка заполнения обязательных полей
  if(empty($arrDataForSql['strTplName'])) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = '102tpl';
  }

  if(empty($arrDataForSql['strTplBody'])) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = '105tpl';
  }

  if($arrDataForSql['strTplGroup']==0) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = '107tpl';
  }

// ***** Добавляем тело шаблона в базу, и при надобности публикуем (пишем файл)
	if($GLOBALS['manStatusError']!=1) {
    if ($arrDataForSql['intIdTpl']==0) { // Если добавляем новый шаблон
      $strSqlQuery = "INSERT INTO ".$_db_tables['stTpl']." (st_group, st_project, st_name, st_body, st_status, st_publish) VALUES (".$arrDataForSql['strTplGroup'].", '".$_SESSION['intIdDefaultProject']."', '".$arrDataForSql['strTplName']."', '".$arrDataForSql['strTplBody']."', 'Y', ".$arrDataForSql['bolTplPublish'].")";
      if(!$objDb->query($strSqlQuery)) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
      } else {
        $arrDataForSql['intIdTpl'] = $objDb->insert_id();

        if($_POST['cbxTplPublish']=='on') {
          $objFile->writeFile(
                  $objTpl->parseFormVars($arrDataForFile['strTplBody'], "out")
                  , "site.global.".$arrDataForSql['intIdTpl'].".tpl"
                  , $_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/tpl/"
          );
          $GLOBALS['manCodeError'][1]['code'] = '101tpl';
        } else {
          $GLOBALS['manCodeError'][1]['code'] = '100tpl';
        }

      }
    } else { // Если обновляем старый
      $strSqlQuery = "UPDATE ".$_db_tables['stTpl']." SET st_group=".$arrDataForSql['strTplGroup'].", st_name='".$arrDataForSql['strTplName']."', st_body='".$arrDataForSql['strTplBody']."', st_status='".$arrDataForSql['bolTplStatus']."', st_publish=".$arrDataForSql['bolTplPublish']." WHERE st_id='".$arrDataForSql['intIdTpl']."'";
      if(!$objDb->query($strSqlQuery)) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
      } else {
        if($_POST['cbxTplPublish']=='on') {
          $objFile->writeFile(
                  $objTpl->parseFormVars($arrDataForFile['strTplBody'], "out")
                  , "site.global.".$arrDataForSql['intIdTpl'].".tpl"
                  , $_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/tpl/"
          );
          $GLOBALS['manCodeError'][1]['code'] = '101tpl';
        } else {
          $GLOBALS['manCodeError'][1]['code'] = '100tpl';
        }
      }
    }

    if ($_POST['cbxTplReturn']=='on') {
      header("location: ".$arrTplVars['module.name']."?tpl=".$arrDataForSql['intIdTpl']."&errMess=".$GLOBALS['manCodeError'][1]['code']."&pageReturn=true");
    } else {
      header("location: ".$arrTplVars['module.parent']."?errMess=".$GLOBALS['manCodeError'][1]['code']);
    }
    exit();
  }
}
// **************************************************************************************

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$arrTplVars['intIdTpl'] = intval($_GET['tpl']);
$bolEditTpl = ($arrTplVars['intIdTpl']>0) ? true : false;

// Если редактируем шаблон
if($bolEditTpl==true) {
  $strSqlQuery = "SELECT * FROM ".$_db_tables['stTpl']." WHERE st_id='".$arrTplVars['intIdTpl']."'";
  $arrInfoTpl = $objDb->fetch( $strSqlQuery );

  if ( is_array($arrInfoTpl) ) {
    $arrTplVars['strTplBody'] = stripslashes($objTpl->parseFormVars($arrInfoTpl['st_body']));
    $arrTplVars['strWorkTemplate'] = $arrTplVars['strTplName'] = stripslashes(htmlspecialchars($arrInfoTpl['st_name']));
    $arrTplVars['intIdTpl'] = $arrInfoTpl['st_id'];
    $intIdGroupSelected = $arrInfoTpl['st_group'];
    $arrIf['block.tpl.status'] = true;
    $arrTplVars['bolTplStatus_checked'] = ($arrInfoTpl['st_status']=='Y') ? ' checked' : '';
    $arrTplVars['cbxTplReturn_checked'] = ($_GET['pageReturn']=='true') ? ' checked' : '';

    if($arrInfoTpl['st_publish']===NULL) {
      $arrTplVars['strNoPublishMessage'] = $errMsg['150tpl'];
      $arrIf['message.noPublish.yes'] = true;
    }

    $arrTplVars['strLastPublishDate'] = (!empty($arrInfoTpl['st_publish'])) ? $objUtil->workDate(4, $arrInfoTpl['st_publish']) : '-';


    /**
     * Проверяем идентичность шаблона в базе и на диске
     */
		if ( file_exists($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/tpl/site.global.".$arrTplVars['intIdTpl'].".tpl") ) {
    	$strFileContent = $objFile->readFile($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject']."templates/tpl/site.global.".$arrTplVars['intIdTpl'].".tpl");
    	$arrTplVars['strBodyFileFrg'] = htmlspecialchars($objTpl->parseFormVars($strFileContent));
    	$strBaseContent = stripslashes($arrInfoTpl['st_body']);

    	// если размеры файла фрагмента и размера из БД не совпадают
    	if(!$objUtil->strCompared($strBaseContent, $strFileContent))
      	$arrIf["difference.in.content"] = true;
		}
  } else {
    header("location: ".$arrTplVars['module.name']);
    exit();
  }
}


// **** Выбираем группы шаблонов ********************************************************
if (empty($intIdGroupSelected))
  $intIdGroupSelected = intval($_POST['intTplGroup']);

$strSqlQuery = "SELECT * FROM ".$_db_tables['stTplGrp']." WHERE stg_id_project='".$_SESSION['intIdDefaultProject']."' AND stg_status='Y'";
$arrListGroup = $objDb->fetchall( $strSqlQuery );

if(is_array($arrListGroup)) {
  foreach($arrListGroup as $key=>$value) {
    if($arrListGroup[$key]['stg_id_parent']===NULL) {
      $arrListGroupForm[$key]['name'] = $arrListGroup[$key]['stg_name'];
      $arrListGroupForm[$key]['id'] = $arrListGroup[$key]['stg_id'];
    } else {
      $arrListSubGroupForm[$arrListGroup[$key]['stg_id_parent']][] = array('id'=>$arrListGroup[$key]['stg_id'], 'name'=>stripslashes($arrListGroup[$key]['stg_name']));
    }
  }

  foreach($arrListGroupForm as $key=>$value) {
    if($intIdGroupSelected == $arrListGroupForm[$key]['id']) // Если эта группа была выбрана, ставим ей "selected"
      $arrListGroupToTpl[] = array('intIdGroup'=>$arrListGroupForm[$key]['id'], 'strNameGroup'=>$arrListGroupForm[$key]['name'], 'intTplGroup_selected'=>' selected');
    else
      $arrListGroupToTpl[] = array('intIdGroup'=>$arrListGroupForm[$key]['id'], 'strNameGroup'=>$arrListGroupForm[$key]['name']);

    if(count($arrListSubGroupForm[$arrListGroupForm[$key]['id']])>0) {
      foreach($arrListSubGroupForm[$arrListGroupForm[$key]['id']] as $kkey=>$vvalue) {
        if($intIdGroupSelected == $arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id']) // Если эта группа была выбрана, ставим ей "selected"
          $arrListGroupToTpl[] = array('intIdGroup'=>$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id'], 'strNameGroup'=>'--> '.$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['name'], 'intTplGroup_selected'=>' selected');
        else
          $arrListGroupToTpl[] = array('intIdGroup'=>$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id'], 'strNameGroup'=>'--> '.$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['name']);
      }
    }
  }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.group", $arrListGroupToTpl);
// **************************************************************************************

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);