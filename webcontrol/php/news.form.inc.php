<?php
$arrTplVars['module.name'] = "news.form";
$arrTplVars['module.parent'] = "news";
$arrTplVars['module.title'] = "Добавить новость";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление фото
 */
if ( intval($_GET['id']) > 0 ) {
	$intRecordId = intval($_GET['id']);
	if ($_GET['act'] == 'del') {
		$strSqlQuery = "UPDATE `site_news` SET"
			. " sn_image = 'N' WHERE sn_id = '$intRecordId'";
		if ( !$objDb->query($strSqlQuery) ) {
			#mysql error
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		} else {
			if ( file_exists(DROOT."storage/images/news/news.photo.$intRecordId.jpg")) {
				unlink(DROOT."storage/images/news/news.photo.$intRecordId.jpg");
				unlink(DROOT."storage/images/news/news.photo.$intRecordId.b.jpg");
			}

			header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
			exit();
		}
	} elseif ($_GET['act'] == 'delimg' && $_GET['pic'] > 0) {
		/**
		 * Удалить дополнительное фото
		 */
		$strSqlQuery = "DELETE FROM site_images WHERE"
			." si_id = '{$_GET['pic']}' AND si_id_rel = '$intRecordId' AND si_type = 'photorep'";
		if ( !$objDb->query($strSqlQuery) ) {
			#mysql error
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		} else {
			if ( file_exists(DROOT."storage/images/news/news.photo.$intRecordId.{$_GET['pic']}.jpg")) {
				unlink(DROOT."storage/images/news/news.photo.$intRecordId.{$_GET['pic']}.jpg");
				unlink(DROOT."storage/images/news/news.photo.$intRecordId.{$_GET['pic']}.b.jpg");
			}

			header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
			exit();
		}
	}
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEditNews']) && $_POST['frmEditNews'] == 'true' ) {
	$intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
	$arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

	$arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
	$arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

	if (empty($arrSqlData['strTitle']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
	} else {
		$arrSqlData['strUrl'] = strtolower( $objUtil->translitBadChars($objUtil->translitKyrToLat(trim($_POST['strTitle']))) );
		$arrSqlData['strUrl'] = substr($arrSqlData['strUrl'], 0, 255);
	}

	$arrSqlData['strNewsBody'] = addslashes(trim($_POST['strNewsBody']));
	$arrTplVars['strNewsBody'] = htmlspecialchars(stripslashes(trim($_POST['strNewsBody'])));

	if (empty($arrSqlData['strNewsBody']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgEmptyNewsbody';
	}

	if (!empty($_POST['strImgDesc'])) $_POST['strImgDesc'] = substr(trim($_POST['strImgDesc']), 0, 255);
	$arrSqlData['strImgDesc'] = addslashes(trim($_POST['strImgDesc']));
	$arrTplVars['strImgDesc'] = htmlspecialchars(stripslashes(trim($_POST['strImgDesc'])));

	$arrSqlData['intDay'] = intval(trim($_POST['intDay']));
	$arrTplVars['intDay'] = ( $arrSqlData['intDay'] > 0 ? $arrSqlData['intDay'] : '');

	$arrSqlData['intMonth'] = intval(trim($_POST['intMonth']));
	$arrTplVars['intMonth'] = ( $arrSqlData['intMonth'] > 0 ? $arrSqlData['intMonth'] : '');

	$arrSqlData['intYear'] = intval(trim($_POST['intYear']));
	$arrTplVars['intYear'] = ( $arrSqlData['intYear'] > 0 ? $arrSqlData['intYear'] : '');

	$dateNewsPubl = $arrSqlData['intYear'].'-'.str_pad($arrSqlData['intMonth'], 2, '0', STR_PAD_LEFT).'-'.str_pad($arrSqlData['intDay'], 2, '0', STR_PAD_LEFT);

	if (!$objUtil->dateValid($dateNewsPubl)) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgFailDate';
	}

	$arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
	$arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

	$arrSqlData['cbxMain'] = ($_POST['cbxMain']=='on'?'Y':'N');
	$arrTplVars['cbxMain'] = ($_POST['cbxMain']=='on'?' checked':'');

	if ($_POST['cbxMain']=='on') {
		// удалить все возможные статусы главности новостей, потому что только ОДНА новость может быть главной
		$strSqlQuery = "UPDATE `site_news` SET `sn_main` = 'N'";
		if (!$objDb->query($strSqlQuery)) {
			# sql error
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		}
	}

	$arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
	$arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

	$arrIf['loadMorePics'] = $_POST['loadMorePics'] == 1;

	if ($GLOBALS['manStatusError']!=1) {
		$strSqlFields = ""
			. " sn_title = '{$arrSqlData['strTitle']}'"
			. ", sn_body = '{$arrSqlData['strNewsBody']}'"
			. ", sn_url = '{$arrSqlData['strUrl']}'"
			. ", sn_date_publ = '$dateNewsPubl'"
			. ", sn_status = '{$arrSqlData['cbxStatus']}'"
			. ", sn_main = '{$arrSqlData['cbxMain']}'"
		;

		if ( empty($intRecordId) ) {
			$strSqlQuery = "INSERT INTO `site_news` SET ".$strSqlFields.", sn_date_add = NOW()";
		} else {
			$strSqlQuery = "UPDATE `site_news` SET ".$strSqlFields.", sn_date_change = NOW() WHERE sn_id = ".$intRecordId;
		}

		if ( !$objDb->query( $strSqlQuery ) ) {
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		} else {
			if ( empty($intRecordId) ) {
				$intRecordId = $objDb->insert_id();
			}
		}

		/**
		 * Загрузка фото
		 */
		$maxFileSize = 4500000;
		if (isset($_FILES['flPhoto'])) {
			if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < $maxFileSize ) {
				$strImageName = "news.photo.$intRecordId";

				/////////////////////////////////////////////////////////////////////////////////////
				// extended
				$arrExts['pjpeg'] = 'jpg';
				$arrExts['jpeg'] = 'jpg';

				$mimeType = explode("/", $_FILES['flPhoto']['type']);
				$mimeType = $mimeType[1];

				if ( array_key_exists($mimeType , $arrExts) ) {
					$mimeType = $arrExts[$mimeType];
				}

				if ( !in_array($mimeType, array('jpg','png','gif')) ) {
					$GLOBALS['manStatusError']=1;
					$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
				} else {
					$fileName = "$strImageName.$mimeType";
					$fileNameBig = "$strImageName.b.$mimeType";
					$fileNameOrig = "temp.$strImageName.$mimeType";

					$dirStorage = DROOT."storage";
					if ( !file_exists($dirStorage) ) {
						if (!mkdir($dirStorage, 0775)) {
							$GLOBALS['manStatusError']=1;
							$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
						}
					}

					$dirImages = $dirStorage."/images";
					if ( !file_exists($dirImages) ) {
						if (!mkdir($dirImages, 0775)) {
							$GLOBALS['manStatusError']=1;
							$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
						}
					}

					$dirImages = $dirImages."/news";
					if ( !file_exists($dirImages) ) {
						if (!mkdir($dirImages, 0775)) {
							$GLOBALS['manStatusError']=1;
							$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
						}
					}

					if ($GLOBALS['manStatusError']!=1) {
						if (move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig )) {
							if ( !is_object($objImage) ) {
								include_once("cls.images.php");
								$objImage = new clsImages();
							}

							if (!$arrIf['loadMorePics']) {
								$objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 0, 170 );
								$objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig, 600 );
							} else {
								// SQL вставка в БД картинки в site_images
								$strSqlQuery = "INSERT INTO site_images SET"
									. " si_id_rel = '$intRecordId'"
									. ", si_type = 'photorep'"
									. ", si_desc = '".$arrSqlData['strImgDesc']."'"
									. ", si_date_add = NOW()"
									. ", si_status = 'Y'"
								;
								if ( !$objDb->query( $strSqlQuery ) ) {
									#error
									@mail( DEBUG_MAIL, "Error in ".__FILE__."!", "Line: ".__LINE__."\n\n"."SQL query = ".$strSqlQuery ."\n".$objDb->mysqlerror(false));
								} else {
									$intImageID = $objDb->insert_id(); // после INSERT-а в site_images
								}

								// формируем название файла
								$fileName = $strImageName.'.'.$intImageID.'.'.$mimeType;
								$fileNameLarge = $strImageName.'.'.$intImageID.'.b.'.$mimeType;
								$objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 0, 90 );
								$objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameLarge, 900, 0, 0xFFFFFF, 90 );

								if (file_exists($dirImages."/".$fileNameLarge)) {
									$strSqlQuery = "UPDATE site_images SET si_filename='$fileNameLarge'"
										. " WHERE si_id='$intImageID'";
									if ( !$objDb->query($strSqlQuery) ) {
										#mysql error
										@mail( DEBUG_MAIL, "Error in ".__FILE__."!", "Line: ".__LINE__."\n\n"."SQL query = ".$strSqlQuery."\n".$objDb->mysqlerror(false) );
									}
								} else {
									if (intval($intImageID)) { // если файла на диске нет, то зачем нам запись в БД
										$strSqlQuery = "DELETE FROM site_images WHERE si_id='$intImageID'";
										if ( !$objDb->query($strSqlQuery) ) {
											#mysql error
											@mail( DEBUG_MAIL, "Error in ".__FILE__."!", "Line: ".__LINE__."\n\n"."SQL query = ".$strSqlQuery."\n".$objDb->mysqlerror(false) );
										}
									}
								}
							}

							unlink($dirImages."/".$fileNameOrig);
						} else {
							// файл небыл загружен :(
						}
					}
				}
				/////////////////////////////////////////////////////////////////////////////////////

				if ($GLOBALS['manStatusError']!=1 && !$arrIf['loadMorePics']) {
					$strSqlQuery = "UPDATE `site_news` SET"
						. " sn_image = 'Y'"
						. ", sn_img_desc = '".$arrSqlData['strImgDesc']."'"
						. ", sn_date_change = NOW()"
						. " WHERE sn_id = '$intRecordId'";
					if ( !$objDb->query( $strSqlQuery ) ) {
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
					}
				}
			} else {
				if ( $_FILES['flPhoto']['size'] >= $maxFileSize) {
					$GLOBALS['manStatusError']=1;
					$GLOBALS['manCodeError'][]['code'] = 'msgTooBigFile';
				}/* else if (!intval($_FILES['flPhoto']['size'])) {
					$GLOBALS['manStatusError']=1;
					$GLOBALS['manCodeError'][]['code'] = 'msgEmptySizeFile';
				}*/
			}
		}

		if ( $GLOBALS['manStatusError']!=1 ) {
			if ( $arrSqlData['cbxReturn'] == 'Y' ) {
				header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
			} else {
				header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
			}
			exit;
		}
	}
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
	$intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);
	$arrIf['is.news.id'] = true;
	$strSqlQuery = "SELECT * FROM `site_news` WHERE sn_id ='$intRecordId'";
	$arrInfo = $objDb->fetch( $strSqlQuery );

	// Let's check attached images folder
	$newsAttachedFiles = PRJ_FILES.'news_attachments/'.$intRecordId.'/';
	if (is_dir($newsAttachedFiles)) {
		$exclude_list = array(".", "..");
		$arrAttachedFiles = array_diff(scandir($newsAttachedFiles), $exclude_list);
		if (is_array($arrAttachedFiles)) {
			$arrIf['is.attached.files'] = true;
			$arrTplVars['filesAttached'] = json_encode($arrAttachedFiles);
		}
	}

	if ( is_array( $arrInfo )) {
		$arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['sn_title']));
		$arrTplVars['strNewsBody'] = htmlspecialchars(stripslashes($arrInfo['sn_body']));
		$arrTplVars['strMainPicDesc'] = htmlspecialchars(stripslashes($arrInfo['sn_img_desc']));

		$arrTplVars['intDay'] = intval(date('d', strtotime($arrInfo['sn_date_publ'])));
		$arrTplVars['intMonth'] = intval(date('m', strtotime($arrInfo['sn_date_publ'])));
		$arrTplVars['intYear'] = intval(date('Y', strtotime($arrInfo['sn_date_publ'])));

		$arrTplVars['cbxStatus'] = $arrInfo['sn_status'] == 'Y' ? ' checked' : '';
		$arrTplVars['cbxMain'] = $arrInfo['sn_main'] == 'Y' ? ' checked' : '';

		$arrIf['is.main.image'] = $arrInfo['sn_image'] == 'Y';

		$arrIf['can.upload'] = true;

		$arrImages = array();
		if ($arrIf['is.main.image']) {
			$strSqlQuery = "SELECT si_id,si_desc,si_filename FROM site_images WHERE si_id_rel = ".$intRecordId." AND si_type='photorep' AND si_status='Y' ORDER BY si_id";
			$arrImages = $objDb->fetchall( $strSqlQuery );
			if (is_array($arrImages) && !empty($arrImages)) {
				foreach ($arrImages as $key => $image) {
					$arrImages[$key]['si_filename_small'] = str_replace('.b.', '.', $image['si_filename']);
				}
			}

			if (isset($arrImages[3])) {
				$arrImages[2]['strTableRows'] = '</tr><tr>';
			}
			if (isset($arrImages[7])) {
				$arrImages[6]['strTableRows'] = '</tr><tr>';
			}
			if (isset($arrImages[11])) {
				$arrImages[10]['strTableRows'] = '</tr><tr>';
			}
			if (isset($arrImages[15])) {
				$arrImages[14]['strTableRows'] = '</tr><tr>';
			}
			if (isset($arrImages[19])) {
				$arrImages[18]['strTableRows'] = '</tr><tr>';
			}

			$arrIf['can.upload'] = count($arrImages) < 23;
		}
	}
}
$objTpl->tpl_loop($arrTplVars['module.name'], "all.images", $arrImages, array('#FFFFFF', '#EFEFEF'));
$objTpl->tpl_loop($arrTplVars['module.name'], "all.images.js", $arrImages, array('#FFFFFF', '#EFEFEF'));

$arrIf['no.image'] = !$arrIf['exist.image'];

/**
 * Установка дат
 */
// Дни
for ($i=1;$i<=31;$i++) {
	$arrDays[$i]['intDay'] = $i;
	$arrDays[$i]['strDay'] = str_pad($i, 2, "0", STR_PAD_LEFT);
	$arrDays[$i]['selDay'] = ( ($i == $arrTplVars['intDay']) || (!isset($arrTplVars['intDay']) && $i == intval(date('d'))) ? " selected" : "");
}
$objTpl->tpl_loop($arrTplVars['module.name'], "days", $arrDays);


// Месяцы
for ($i=1;$i<=12;$i++) {
	$arrMonths[$i]['intMonth'] = $i;
	$arrMonths[$i]['strMonth'] = $objUtil->arrAtMonth[$i];
	$arrMonths[$i]['selMonth'] = ( ($i == $arrTplVars['intMonth']) || (!isset($arrTplVars['intMonth']) && $i == intval(date('m'))) ? " selected" : "");
}
$objTpl->tpl_loop($arrTplVars['module.name'], "months", $arrMonths);

// Годы
for ($i=2005;$i<=(intval(date("Y"))+3);$i++) {
	$arrYears[$i]['intYear'] = $i;
	$arrYears[$i]['strYear'] = str_pad($i, 2, "0", STR_PAD_LEFT);
	$arrYears[$i]['selYear'] = ( ($i == $arrTplVars['intYear']) || (!isset($arrTplVars['intYear']) && $i == intval(date('Y'))) ? " selected" : "");

}
$objTpl->tpl_loop($arrTplVars['module.name'], "years", $arrYears);



$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

