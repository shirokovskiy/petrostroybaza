<?php
$arrTplVars['module.name'] = "modules";
$arrTplVars['module.access'] = "module.access";
$arrTplVars['module.child'] = "module.form";
$arrTplVars['module.title'] = "Список модулей";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

/************************************
** Shirokovskiy D.2005 Jimmy™. Mon Oct 24 11:20:21 MSD 2005
**
** Список модулей
*/

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['cmsModules'];
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery , 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['cmsModules'];
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery , 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPaginator = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
// Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу, кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
$objPaginator->strPaginatorTpl = 'site.global.1.tpl';
$objPaginator->iQtyRecsPerPage = 15;
$objPaginator->strColorLinkStyle = "link-b";
$objPaginator->strColorActiveStyle = "tab-bl-b";
$objPaginator->strNameImageBack = "arrow_one_left.gif";
$objPaginator->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPaginator->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['cmsModules']." ORDER BY cm_link ".$objPaginator->strSqlLimit;
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
$arrTplVars['intQuantShowRecOnPage'] = count($arrSelectedRecords); // кол-во публикаций показанных на странице
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPaginator->paShow();

foreach ( $arrSelectedRecords as $key => $value ) {
  $arrSelectedRecords[$key]['moduleID'] = $value['cm_id'];
  $arrSelectedRecords[$key]['moduleName'] = $value['cm_name'];
  $arrSelectedRecords[$key]['moduleAlias'] = $value['cm_link'];
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.modules", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));


$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
