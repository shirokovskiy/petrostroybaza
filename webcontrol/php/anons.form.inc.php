<?php
$arrTplVars['module.name'] = "anons.form";
$arrTplVars['module.parent'] = "anonses";
$arrTplVars['module.title'] = "Добавить анонс мероприятия";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset($_POST['frmEditAnonsM']) && $_POST['frmEditAnonsM'] == 'true' ) {
  $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
  $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

  $arrSqlData['strAnonsName'] = addslashes(trim($_POST['strAnonsName']));
  $arrTplVars['strAnonsName'] = htmlspecialchars(stripslashes(trim($_POST['strAnonsName'])));

  if (empty($arrSqlData['strAnonsName']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgAnonsName';
    $arrTplVars['brdErrorAName'] = "border-color: red;";
  }

  $arrSqlData['strAnonsContacts'] = addslashes(trim($_POST['strAnonsContacts']));
  $arrTplVars['strAnonsContacts'] = htmlspecialchars(stripslashes(trim($_POST['strAnonsContacts'])));

  $arrSqlData['strAnonsAddress'] = addslashes(trim($_POST['strAnonsAddress']));
  $arrTplVars['strAnonsAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAnonsAddress'])));

  if (empty($arrSqlData['strAnonsAddress']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgAnonsAddress';
    $arrTplVars['brdErrorAAddress'] = "border-color: red;";
  }

  $arrSqlData['strAnonsDate'] = addslashes(trim($_POST['strAnonsDate']));
  $arrTplVars['strAnonsDate'] = htmlspecialchars(stripslashes(trim($_POST['strAnonsDate'])));

  if (empty($arrSqlData['strAnonsDate']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgAnonsDate';
    $arrTplVars['brdErrorADate'] = "border-color: red;";
  }

  $arrSqlData['strAnonsInfo'] = addslashes(trim($_POST['strAnonsInfo']));
  $arrTplVars['strAnonsInfo'] = htmlspecialchars(stripslashes(trim($_POST['strAnonsInfo'])));

  $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
  $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

  $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
  $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

  if ( $GLOBALS['manStatusError'] != 1 ) {
    $strSqlFields = ""
      . " pam_name = '{$arrSqlData['strAnonsName']}'"
      . ", pam_contacts = '{$arrSqlData['strAnonsContacts']}'"
      . ", pam_address = '{$arrSqlData['strAnonsAddress']}'"
      . ", pam_action_date = '{$arrSqlData['strAnonsDate']}'"
      . ", pam_info = '{$arrSqlData['strAnonsInfo']}'"
      . ", pam_status = '{$arrSqlData['cbxStatus']}'"
      ;

    if ( empty($intRecordId) ) {
      $strSqlQuery = "INSERT INTO ".$_db_tables['stAnonses']." SET $strSqlFields, pam_date_create = NOW()";
    } else {
      $strSqlQuery = "UPDATE ".$_db_tables['stAnonses']." SET $strSqlFields, pam_date_update = NOW()"
      ." WHERE pam_id = '$intRecordId'";
    }

    if ( !$objDb->query( $strSqlQuery ) ) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      $intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
      if ( $arrSqlData['cbxReturn'] == 'Y' ) {
        header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId");
      } else {
        header('location:'.$arrTplVars['module.parent']."?errMess=msgOk");
      }
      exit;
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAnonses']." WHERE pam_id ='$intRecordId'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strAnonsName'] = htmlspecialchars(stripslashes($arrInfo['pam_name']));
    $arrTplVars['strAnonsContacts'] = htmlspecialchars(stripslashes($arrInfo['pam_contacts']));
    $arrTplVars['strAnonsAddress'] = htmlspecialchars(stripslashes($arrInfo['pam_address']));
    $arrTplVars['strAnonsDate'] = htmlspecialchars(stripslashes($arrInfo['pam_action_date']));
    $arrTplVars['strAnonsInfo'] = htmlspecialchars(stripslashes($arrInfo['pam_info']));

    $arrTplVars['cbxStatus'] = $arrInfo['pam_status'] == 'Y' ? ' checked' : '';
  }
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
