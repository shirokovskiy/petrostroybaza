<?php
$arrTplVars['module.name'] = "object.comments";
$arrTplVars['module.parent'] = "estate.objects";
$arrTplVars['module.title'] = "Комментарии к объекту";

if (!is_object($objEstateObject))
    $objEstateObject = new EstateObject();

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( intval($_GET['id']) > 0 &&  intval($_GET['suc_id']) > 0 && $_GET['act'] ) {
    $intRecordId = intval($_GET['suc_id']);
    switch ($_GET['act']) {
        case 'del':
            $strSqlQuery = "DELETE FROM site_users_comments WHERE `suc_id` = $intRecordId AND `suc_seo_id` = ".$_GET['id']." LIMIT 1";
            if ( !$objDb->query($strSqlQuery) ) {
                $GLOBALS['manStatusError'] = 1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            }
            break;

        case 'Y':
        case 'N':
            $strSqlQuery = "UPDATE `site_users_comments` SET"
                . " `suc_status` = '".$_GET['act']."'"
                . " WHERE `suc_id` = ".$intRecordId." AND `suc_seo_id` = ".$_GET['id']." LIMIT 1";
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                $GLOBALS['manStatusError'] = 1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            }
            break;

        default: break;
    }

    header('Location: '.$arrTplVars['module.name'].'?id='.$_GET['id'].'&stPage='.$_GET['stPage']); exit;
}

if ($_POST['object_id'] > 0 && !empty($_POST['object_comment'])) {
    $_POST['strLoginAuth'] = 'administrator';
    $_POST['strPasswAuth'] = '1ZAq2XSw3CDe';
    $objAuth = new wbmAuth();
    if ( !$objAuth->wbmAuthLogin() ) {
        $GLOBALS['manStatusError'] = 1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorWrite';
        unset($objAuth);
    } else {
        $objUser = new User();
        $objUser->saveCommentForObject(intval($_POST['object_id']), $_POST['object_comment']);
        unset($objUser);
        $objAuth->wbmLogout();
        header('Location: '.$arrTplVars['module.name'].'?id='.$_GET['id'].'&stPage='.$_GET['stPage']); exit;
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ($_GET['id']) {
    $intRecordId = $arrTplVars['objID'] = (int) $_GET['id'];
    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM `site_users_comments` WHERE `suc_seo_id` = " . $intRecordId;
    $arrSelectedRecords = $objDb->fetchall($strSqlQuery);

    if ( is_array($arrSelectedRecords) ) {
        foreach ( $arrSelectedRecords as $key => $value ) {
            $arrSelectedRecords[$key]['cbxStatus'] = $value['suc_status'] == 'Y' ? ' checked disabled' : '';
            $arrSelectedRecords[$key]['act'] = $value['suc_status'] == 'Y' ? 'N' : 'Y';
            $arrSelectedRecords[$key]['comment.date'] = $objUtil->workDate(4, $value['suc_created']);
            $arrSelectedRecords[$key]['comment.user'] = $objDb->fetch("SELECT CONCAT(su_fname, ' ',su_lname) uName FROM `site_users` WHERE su_id = ".$value['suc_su_id'], 0);
        }
    }

    $arrTplVars['object.name'] = $objEstateObject->getDataById($intRecordId, 'seo_name', 0);
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

