<?php
$arrTplVars['module.name'] = "videos";
$arrTplVars['module.child'] = "videos.form";
$arrTplVars['module.title'] = "Видео-сюжеты";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `videos`";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM `videos`";
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
* Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
* кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
*/
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM `videos` ORDER BY v_id DESC ".$objPagination->strSqlLimit;
$arrRows = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrRows) ? count($arrRows) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrRows) ) {
  foreach ( $arrRows as $key => $value ) {
    $arrRows[$key]['strDatePubl'] = date( "d.m.Y", strtotime($value['v_date_publ']));
    $arrRows[$key]['strDateAdd'] = date( "d.m.Y H:i", strtotime($value['v_date_add']));
    $arrRows[$key]['strDateChange'] = !empty($value['v_date_change']) ? date( "d.m.Y H:i", strtotime($value['v_date_change'])) : '';
    $arrRows[$key]['cbxStatus'] = $value['v_status'] == 'Y' ? ' checked' : '';
    $arrIf['attach.'.$value['v_id']] = !empty($value['v_code']);
  }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "videos", $arrRows, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
