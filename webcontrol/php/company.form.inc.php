<?php
$arrTplVars['module.name'] = "company.form";
$arrTplVars['module.ref.cmp'] = "company.ref.form";
$arrTplVars['module.parent'] = "companies";
$arrTplVars['module.obj'] = "estate.object.form";
$arrTplVars['module.title'] = "Добавить компанию";

$stPage = $arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( $_GET['act'] == 'del' && intval($_GET['id']) > 0 ) {
    $id = $_GET['id'];
    # удалим компанию вообще
    $strSqlQuery = "DELETE FROM ".$_dbt['stContacts']." WHERE sec_id = '$id' LIMIT 1";
    if ( !$objDb->query( $strSqlQuery ) ) {
        # sql query error
        $GLOBALS['manStatusError'] = 1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        $strSqlQuery = "DELETE FROM ".$_dbt['stObjContacts']." WHERE socl_id_contact = '$id'";
        if ( !$objDb->query( $strSqlQuery ) ) {
            # sql query error
            $GLOBALS['manStatusError'] = 1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        }

        header('Location:'.$arrTplVars['module.parent'].'?stPage='.$stPage);
        exit();
    }
}

if ( isset($_REQUEST['frmEditForm']) && $_REQUEST['frmEditForm'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_REQUEST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $intObjID = $arrTplVars['intObjID'] = $arrSqlData['intObjID'] = intval($_REQUEST['intObjID']);

    $arrSqlData['strTypeSet'] = mysql_real_escape_string(trim($_REQUEST['strTypeSet']));
    $arrTplVars['strTypeSet'] = htmlspecialchars(trim($_REQUEST['strTypeSet']));

    if ( empty($arrSqlData['strTypeSet']) && $intObjID > 0 ) {
        $GLOBALS['manStatusError'] = 1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorC001';
    }

    $arrSqlData["strCompany"] = addslashes(trim($_REQUEST["strCompany"]));
    $arrTplVars["strCompany"] = htmlspecialchars(stripslashes(trim($_REQUEST["strCompany"])));

    $arrSqlData["strCompanyInn"] = addslashes(trim($_REQUEST["strCompanyInn"]));
    $arrTplVars["strCompanyInn"] = htmlspecialchars(stripslashes(trim($_REQUEST["strCompanyInn"])));

    $arrSqlData["strFIO"] = addslashes(trim($_REQUEST["strFIO"]));
    $arrTplVars["strFIO"] = htmlspecialchars(stripslashes(trim($_REQUEST["strFIO"])));

    if ( empty($arrSqlData["strCompany"]) && empty($arrSqlData["strFIO"]) ) {
        $GLOBALS['manStatusError'] = 1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorC002';
    } else {
        if ( $intRecordId <= 0 && isSuchCompany( $arrSqlData["strCompany"] ) ) {
            $GLOBALS['manStatusError'] = 1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorC003';
        }
    }

    $arrSqlData["strProff"] = addslashes(trim($_REQUEST["strProff"]));
    $arrTplVars["strProff"] = htmlspecialchars(stripslashes(trim($_REQUEST["strProff"])));

    $arrSqlData["strContact"] = addslashes(trim($_REQUEST["strContact"]));
    $arrTplVars["strContact"] = htmlspecialchars(stripslashes(trim($_REQUEST["strContact"])));

    $arrSqlData["cbxStatus"] = ($_REQUEST["cbxStatus"]=="on"?"Y":"N");
    $arrTplVars["cbxStatus"] = ($_REQUEST["cbxStatus"]=="on"?" checked":"");

    $arrSqlData["cbxReturn"] = ($_REQUEST["cbxReturn"]=="on"?"Y":"N");
    $arrTplVars["cbxReturn"] = ($_REQUEST["cbxReturn"]=="on"?" checked":"");

    if ( !$GLOBALS['manStatusError'] ) {
        $strSqlFields = ""
            . " sec_company = '{$arrSqlData["strCompany"]}'"
            . ", sec_inn = ".(!empty($arrSqlData["strCompanyInn"]) ? "'{$arrSqlData["strCompanyInn"]}'" : "NULL")
            . ", sec_fio = '{$arrSqlData["strFIO"]}'"
            . ", sec_proff = '{$arrSqlData["strProff"]}'"
            . ", sec_contact = '{$arrSqlData["strContact"]}'"
            . ", sec_status = '{$arrSqlData["cbxStatus"]}'"
        ;

        if (intval($intRecordId) <= 0) {
            $strSqlQuery = "INSERT INTO site_estate_contacts SET $strSqlFields, sec_date_add = NOW(), sec_date_change = NOW()";
        } else {
            $strSqlQuery = "UPDATE site_estate_contacts SET $strSqlFields, sec_date_change = NOW() WHERE sec_id = $intRecordId";
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            # sql query error
            $GLOBALS['manStatusError'] = 1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            $res = "Error in " . __FILE__ . "! Line: " . __LINE__ .print_r("\n".$strSqlQuery, true).print_r("\n".mysql_error(), true);
            $fp = @fopen(DROOT."storage/debug/sql.".date("Ymd").".txt", "a+");
            @fwrite($fp, "".$res);
            @fclose($fp);
        }
        else {
            $intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;

            if ( intval($intRecordId)>0 && intval($intObjID)>0 && !empty($arrSqlData['strTypeSet']) ) {
                insertIfNotExistCmpRelationObj( $intRecordId /* id_company */, $intObjID /* id_object */, $arrSqlData['strTypeSet'] /* relation type */ );
            }

            if ( is_array($_REQUEST['arrRelObjTypes']) ) {
                $arrRelObjTypes = $_REQUEST['arrRelObjTypes'];
                if ( !empty($arrRelObjTypes) ) {
                    foreach ($arrRelObjTypes as $key => $val) {
                        insertIfNotExistCmpRelationObj( $intRecordId /* id_company */, $key /* id_object */, $val /* relation type */ );
                    }
                }
            }
        }

        if ( !$GLOBALS['manStatusError'] ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId");
            } else {
                header('location:'.$arrTplVars['module.parent']."?errMess=msgOk");
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

/**
 * Типы отношений компании к объекту
 */
if ( is_array($arrTitleExt) ) {
    $s = 0;
    foreach ( $arrTitleExt as $key => $value ) {
        $arrTypes[$s]['typeExt'] = $key;
        $arrTypes[$s]['strType'] = $value;
        $s++;
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "types.set", $arrTypes);

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);
    $arrIf['exist.cmp'] = true;

    $strSqlQuery = "SELECT * FROM site_estate_contacts WHERE sec_id = $intRecordId";
    echo "<!-- $strSqlQuery -->\n";
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strCompany'] = htmlspecialchars(stripslashes($arrInfo['sec_company']));
        $arrTplVars['strCompanyInn'] = htmlspecialchars(stripslashes($arrInfo['sec_inn']));
        $arrTplVars['strFIO'] = htmlspecialchars(stripslashes($arrInfo['sec_fio']));
        $arrTplVars['strProff'] = htmlspecialchars(stripslashes($arrInfo['sec_proff']));
        $arrTplVars['strContact'] = htmlspecialchars(stripslashes($arrInfo['sec_contact']));

        $arrTplVars['strType'] = $arrInfo['sec_type'];
        $arrTplVars['cbxStatus'] = $arrInfo['sec_status'] == 'Y' ? ' checked' : '';

        /**
         * Выбираем привязанные объекты
         */
        $strSqlQuery = "SELECT seo_name, seo_id, seo_status, seo_address, socl_relation, site_objects_contacts_lnk.* FROM site_objects_contacts_lnk"
            ." LEFT JOIN site_estate_objects ON (socl_id_object = seo_id)"
            ." WHERE socl_id_contact = '$intRecordId' ORDER BY seo_name";
        echo "<!-- $strSqlQuery -->\n";
        $arrRelObjects = $objDb->fetchall( $strSqlQuery );
        if ( is_array($arrRelObjects) ) {
            $arrIf['rel.objects'] = true;
            foreach ( $arrRelObjects as $key => $value ) {
                $arrRelObjects[$key]['strObject'] = htmlspecialchars($value['seo_name']);
                $arrRelObjects[$key]['strStatus'] = $value['seo_status'] == 'N' ? ' объект неактивен' : '';
            }
        }
    }
} else {
    $arrIf['new.attach'] = true;
}
$objTpl->tpl_loop($arrTplVars['module.name'], "objects", $arrRelObjects, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

/* $fp = fopen("xxxxxxxxxxxxxx.log", "a+");
fwrite($fp, print_r($arrRelObjects, true));
fclose($fp); */


if ( is_array($arrRelObjects) ) {
    foreach ( $arrRelObjects as $key => $value ) {
        $arrTypesTemp = $arrTypes;
        if ( is_array($arrTypesTemp) ) {
            foreach ( $arrTypesTemp as $kTT => $vTT ) {
                $arrTypesTemp[$kTT]['sel.'.$value['seo_id']] = $value['socl_relation'] == $vTT['typeExt'] ? ' selected' : '';
            }
        }
        $objTpl->tpl_loop($arrTplVars['module.name'], "types.".$value['seo_id'], $arrTypesTemp);
        unset($arrTypesTemp);
    }
}



/**
 * Проверить, необходимо ли показать поле ввода ID объекта или предустановить заданный ID
 */
if ( isset($_REQUEST['id_obj']) && intval($_REQUEST['id_obj']) ) {
    $arrIf['exist.object'] = true;
    $arrTplVars['intObjID'] = $id_obj = $_REQUEST['id_obj'];
    $arrTplVars['returnPath'] = urldecode($_REQUEST['returnPath']);
    $arrTplVars['strObjectName'] = getObjectInfo( $id_obj );
    $arrTplVars['strObjectAddress'] = getObjectInfo( $id_obj, 'seo_address' );
} else {
    $arrIf['new.object'] = true;
}

/**
 * Путь для возврата
 */
$arrTplVars['urlReturnPath'] = urlencode( $_SERVER['QUERY_STRING'] );
$arrTplVars['cbxReturn'] = ' checked';

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
