<?php
$arrTplVars['module.name'] = "tender.form";
$arrTplVars['module.parent'] = "tenders";
$arrTplVars['module.title'] = "Добавить тендер";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset($_POST['frmEditTender']) && $_POST['frmEditTender'] == 'true' ) {
  $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
  $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

  $arrSqlData['strName'] = addslashes(trim($_POST['strName']));
  $arrTplVars['strName'] = htmlspecialchars(stripslashes(trim($_POST['strName'])));

  if (empty($arrSqlData['strName']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgName';
  }

  $arrSqlData['strVidRabot'] = addslashes(trim($_POST['strVidRabot']));
  $arrTplVars['strVidRabot'] = htmlspecialchars(stripslashes(trim($_POST['strVidRabot'])));

  if (empty($arrSqlData['strVidRabot']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgVidRabot';
  }

  $arrSqlData['strTenderObjectAddress'] = addslashes(trim($_POST['strTenderObjectAddress']));
  $arrTplVars['strTenderObjectAddress'] = htmlspecialchars(stripslashes(trim($_POST['strTenderObjectAddress'])));

  if (empty($arrSqlData['strTenderObjectAddress']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgTenderObjectAddress';
  }

  $arrSqlData['strTenderCustomer'] = addslashes(trim($_POST['strTenderCustomer']));
  $arrTplVars['strTenderCustomer'] = htmlspecialchars(stripslashes(trim($_POST['strTenderCustomer'])));

  if (empty($arrSqlData['strTenderCustomer']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgTenderCustomer';
  }

  $arrSqlData['strTenderCoordinates'] = addslashes(trim($_POST['strTenderCoordinates']));
  $arrTplVars['strTenderCoordinates'] = htmlspecialchars(stripslashes(trim($_POST['strTenderCoordinates'])));

  if (empty($arrSqlData['strTenderCoordinates']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgTenderCoordinates';
  }

  $arrSqlData['strTenderContacts'] = addslashes(trim($_POST['strTenderContacts']));
  $arrTplVars['strTenderContacts'] = htmlspecialchars(stripslashes(trim($_POST['strTenderContacts'])));

  if (empty($arrSqlData['strTenderContacts']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgTenderContacts';
  }

  $arrSqlData['strTenderUsefullInfo'] = addslashes(trim($_POST['strTenderUsefullInfo']));
  $arrTplVars['strTenderUsefullInfo'] = htmlspecialchars(stripslashes(trim($_POST['strTenderUsefullInfo'])));

  $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
  $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

  $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
  $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

  if ( $GLOBALS['manStatusError'] != 1 ) {
    $strSqlFields = ""
      . " pt_name = '{$arrSqlData['strName']}'"
      . ", pt_vid_rabot = '{$arrSqlData['strVidRabot']}'"
      . ", pt_address = '{$arrSqlData['strTenderObjectAddress']}'"
      . ", pt_customer = '{$arrSqlData['strTenderCustomer']}'"
      . ", pt_coordinates = '{$arrSqlData['strTenderCoordinates']}'"
      . ", pt_contacts = '{$arrSqlData['strTenderContacts']}'"
      . ", pt_usefull_info = '{$arrSqlData['strTenderUsefullInfo']}'"
      . ", pt_status = '{$arrSqlData['cbxStatus']}'"
      ;

    if ( empty($intRecordId) ) {
      $strSqlQuery = "INSERT INTO ".$_db_tables['stTenders']." SET $strSqlFields, pt_date_create = NOW(), pt_date_update = NOW()";
    } else {
      $strSqlQuery = "UPDATE ".$_db_tables['stTenders']." SET $strSqlFields, pt_date_update = NOW() WHERE pt_id = '$intRecordId'";
    }

    if ( !$objDb->query( $strSqlQuery ) ) {
      print_r( $objDb->mysqlerror() );echo "<br />\n";
      print_r( $strSqlQuery );echo "<br />\n";
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = '111';
      die("<hr>\n".'In file:'.__FILE__.' at line:'.__LINE__."<hr>\n");
    } else {
      $intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
      if ( $arrSqlData['cbxReturn'] == 'Y' ) {
        header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId");
      } else {
        header('location:'.$arrTplVars['module.parent']."?errMess=msgOk");
      }
      exit;
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stTenders']." WHERE pt_id ='$intRecordId'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strName'] = htmlspecialchars(stripslashes($arrInfo['pt_name']));
    $arrTplVars['strVidRabot'] = htmlspecialchars(stripslashes($arrInfo['pt_vid_rabot']));
    $arrTplVars['strTenderObjectAddress'] = htmlspecialchars(stripslashes($arrInfo['pt_address']));
    $arrTplVars['strTenderCustomer'] = htmlspecialchars(stripslashes($arrInfo['pt_customer']));
    $arrTplVars['strTenderCoordinates'] = htmlspecialchars(stripslashes($arrInfo['pt_coordinates']));
    $arrTplVars['strTenderContacts'] = htmlspecialchars(stripslashes($arrInfo['pt_contacts']));
    $arrTplVars['strTenderUsefullInfo'] = htmlspecialchars(stripslashes($arrInfo['pt_usefull_info']));

    $arrTplVars['cbxStatus'] = $arrInfo['pt_status'] == 'Y' ? ' checked' : '';
  }
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);