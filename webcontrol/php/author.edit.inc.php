<?php
$arrTplVars['module.name'] = "author.edit";
$arrTplVars['module.parent'] = "authors";
$arrTplVars['module.title'] = "Добавить автора";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
  $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
  $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

  $arrSqlData['strAuthor'] = addslashes(trim($_POST['strAuthor']));
  $arrTplVars['strAuthor'] = htmlspecialchars(stripslashes(trim($_POST['strAuthor'])));

  if (empty($arrSqlData['strAuthor']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
  }

  $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
  $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

  $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
  $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');



  $strSqlFields = ""
    . " sa_author = '{$arrSqlData['strAuthor']}'"
    . ", sa_status = '{$arrSqlData['cbxStatus']}'"
    ;

  if ( $GLOBALS['manStatusError']!=1 ) {
    if ( empty($intRecordId) ) {
      $strSqlQuery = "INSERT INTO ".$_db_tables['stAuthors']." SET".$strSqlFields
        . ", sa_date_add = NOW()";
    } else {
      $strSqlQuery = "UPDATE ".$_db_tables['stAuthors']." SET".$strSqlFields
        . " WHERE sa_id = '$intRecordId'";
    }

    if ( !$objDb->query( $strSqlQuery ) ) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    }
  }

  if ( $GLOBALS['manStatusError']!=1 ) {
    if ( $arrSqlData['cbxReturn'] == 'Y' ) {
      header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId&stPage=".$arrTplVars['stPage']);
      exit;
    } else {
      header('location:'.$arrTplVars['module.parent']."?errMess=msgOk&stPage=".$arrTplVars['stPage']);
      exit;
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAuthors']." WHERE sa_id ='$intRecordId'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strAuthor'] = htmlspecialchars(stripslashes($arrInfo['sa_author']));

    $arrTplVars['cbxStatus'] = $arrInfo['sa_status'] == 'Y' ? ' checked' : '';
  }
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);