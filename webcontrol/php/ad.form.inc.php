<?php
$arrTplVars['module.name'] = "ad.form";
$arrTplVars['module.parent'] = "adds";
$arrTplVars['module.title'] = "Добавить объявление";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
  $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
  $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

  $arrSqlData['intChapterId'] = intval(trim($_POST['intChapterId']));
  $arrTplVars['intChapterId'] = ( $arrSqlData['intChapterId'] > 0 ? $arrSqlData['intChapterId'] : '');

  if (empty($arrSqlData['intChapterId']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgChapters';
    $arrTplVars['brdError_Chapters'] = "border-color: red;";
  }

  $arrSqlData['strName'] = addslashes(trim($_POST['strName']));
  $arrTplVars['strName'] = htmlspecialchars(stripslashes(trim($_POST['strName'])));

  if (empty($arrSqlData['strName']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgName';
    $arrTplVars['brdError_Name'] = "border-color: red;";
  }

  $arrSqlData['strDesc'] = addslashes(trim($_POST['strDesc']));
  $arrTplVars['strDesc'] = htmlspecialchars(stripslashes(trim($_POST['strDesc'])));

  if (empty($arrSqlData['strDesc']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgDesc';
    $arrTplVars['brdError_Desc'] = "border-color: red;";
  }

  $arrSqlData['strContacts'] = addslashes(trim($_POST['strContacts']));
  $arrTplVars['strContacts'] = htmlspecialchars(stripslashes(trim($_POST['strContacts'])));

  if (empty($arrSqlData['strContacts']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgContacts';
    $arrTplVars['brdError_Contacts'] = "border-color: red;";
  }

  $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
  $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

  $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

  if ( $GLOBALS['manStatusError'] != 1 ) {
    $strSqlFields = ""
      . " paa_name = '{$arrSqlData['strName']}'"
      . ", paa_chapter_id = '{$arrSqlData['intChapterId']}'"
      . ", paa_desc = '{$arrSqlData['strDesc']}'"
      . ", paa_contacts = '{$arrSqlData['strContacts']}'"
      . ", paa_status = '{$arrSqlData['cbxStatus']}'"
      ;

    if ( empty($intRecordId) ) {
      $strSqlQuery = "INSERT INTO ".$_db_tables['stAds']." SET $strSqlFields, paa_date_create = NOW()";
    } else {
      $strSqlQuery = "UPDATE ".$_db_tables['stAds']." SET $strSqlFields, paa_date_update = NOW() WHERE paa_id = '$intRecordId'";
    }

    if ( !$objDb->query( $strSqlQuery ) ) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      $intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
      if ( $_POST['cbxReturn']=='on' ) {
        header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId");
      } else {
        header('location:'.$arrTplVars['module.parent']."?errMess=msgOk");
      }
      exit;
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAds']
  ." LEFT JOIN ".$_db_tables["stAdsAdsChapters"]." ON (paa_chapter_id = paac_id)"
  ." WHERE paa_id ='$intRecordId'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrIf['is.edit'] = true;
    $arrTplVars['strName'] = htmlspecialchars(stripslashes($arrInfo['paa_name']));
    $arrTplVars['strDesc'] = htmlspecialchars(stripslashes($arrInfo['paa_desc']));
    $arrTplVars['strContacts'] = htmlspecialchars(stripslashes($arrInfo['paa_contacts']));
    $arrTplVars['intChapterId'] = htmlspecialchars(stripslashes($arrInfo['paa_chapter_id']));

    $arrTplVars['cbxStatus'] = $arrInfo['paa_status'] == 'Y' ? ' checked' : '';
  }
}

$strSqlQuery = "SELECT * FROM ".$_db_tables["stAdsAdsChapters"]." ORDER BY paac_id ";
$arrAdsAdsChapters = $objDb->fetchall( $strSqlQuery );

if ( is_array($arrAdsAdsChapters) ) {
  foreach ( $arrAdsAdsChapters as $key => $value ) {
    $arrAdsAdsChapters[$key]['sel'] = $value['paac_id'] == $arrTplVars['intChapterId'] ? ' selected' : '';
  }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "list.of.ads.chapters", $arrAdsAdsChapters);
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
