<?php
$arrTplVars['module.name'] = "pages.search";
$arrTplVars['module.title'] = "ПОИСК СТРАНИЦ";
$arrTplVars['module.parent'] = "pages";
$arrTplVars['module.child'] = "pages.edit";


if ( $_POST["frmSearchPage"] == "true" ) {

  $dataForSql["strPageSearch"]  = addslashes(trim($_POST["strPageSearch"]));
  $arrTplVars["strPageSearch"]    = htmlspecialchars(trim($_POST["strPageSearch"]));

  $arrSqlData['cbxCorrect'] = ($_POST['cbxCorrect']=='on'?'Y':'N');
  $arrTplVars['cbxCorrect'] = ($_POST['cbxCorrect']=='on'?' checked':'');

  $searchDirect = ( $arrSqlData['cbxCorrect'] == 'N' ) ? '%' : '';

  $arrSqlData['intPageID'] = intval(trim($_POST['intPageID']));
  $arrTplVars['intPageID'] = ( $arrSqlData['intPageID'] > 0 ? $arrSqlData['intPageID'] : '');

  $arrSqlData['strFrgNameInPage'] = addslashes(trim($_POST['strFrgNameInPage']));
  $arrTplVars['strFrgNameInPage'] = htmlspecialchars(stripslashes(trim($_POST['strFrgNameInPage'])));

  if ( !empty($dataForSql["strPageSearch"]) || !empty($arrSqlData['intPageID']) || !empty($arrSqlData['strFrgNameInPage'])) {
    $strSqlQuery = "SELECT * FROM " . $_db_tables["stPages"]
      . " WHERE ("
      . ( !empty($dataForSql["strPageSearch"]) ? "sp_alias LIKE '$searchDirect". $dataForSql["strPageSearch"] ."$searchDirect'" : "" )
      . ( !empty($arrSqlData['intPageID']) ? (!empty($dataForSql["strPageSearch"]) ? " OR " : "" )." sp_id=".$arrSqlData['intPageID'] : "" )
      . ( !empty($arrSqlData['strFrgNameInPage']) ? ((!empty($dataForSql["strFrgNameInPage"])||!empty($arrSqlData['intPageID'])) ? " OR " : "" )." sp_body LIKE '%frg ".$arrSqlData['strFrgNameInPage']."%'" : "" )
      . ")"
      . " AND sp_id_project = '".$_SESSION['intIdDefaultProject']."'";
    $arrPageData = $objDb->fetchall( $strSqlQuery );
  }

  if ( !empty( $arrPageData ) ) {

    foreach ( $arrPageData as $key => $pageData ) {

      if ( !is_null( $pageData["sp_id_parent"] ) ) {

        $strSqlQuery = "SELECT * FROM " . $_db_tables["stPagesGrp"]
          . " WHERE spg_id = " . $pageData["sp_id_parent"]
          . " AND spg_id_project = '".$_SESSION['intIdDefaultProject']."'";
        $arrChapterPage = $objDb->fetch(  $strSqlQuery );

        if ( !empty( $arrChapterPage ) ) {
          $arrPageData[$key]["pChapterName"] = $arrChapterPage["spg_name"];
          $arrPageData[$key]["pChapterID"] = $arrChapterPage["spg_id"];
        }
      }
    }
  }
}


// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( !empty( $arrPageData ) ) {
  $objTpl->tpl_loop($arrTplVars['module.name'], "lst.searched.pages", $arrPageData );
  $arrIf["alias.searched"] = true;
} else {
  if ( $_POST["frmSearchPage"] == "true" ) {
    $arrIf["alias.not.found"] = true;
  }
}

// **************************************************************************************
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);