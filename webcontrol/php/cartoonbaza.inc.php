<?php
$arrTplVars['module.name'] = "cartoonbaza";
$arrTplVars['module.title'] = "CartoonBaza";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);
$siType = 'cartoonbaza';

/**
 * Удаление
 */
if ( intval($_GET['id_del']) > 0 ) {
	$intRecordId = intval($_GET['id_del']);
	$strSqlQuery = "SELECT si_filename FROM site_images WHERE si_id=".$intRecordId;
	$strFilename = $objDb->fetch($strSqlQuery, "si_filename");

	$strSqlQuery = "DELETE FROM site_images WHERE si_id=".$intRecordId." LIMIT 1";
	if ( !$objDb->query($strSqlQuery) ) {
		#mysql error
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
	} else {
		$filePath = PRJ_IMAGES.'fotobank/cartoonbaza/';
		if (file_exists($filePath.$strFilename)) {
			if(!unlink($filePath.$strFilename)) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
			}
		}
		if (file_exists($filePath.'t_h_u_m_b_'.$strFilename)) {
			if(!unlink($filePath.'t_h_u_m_b_'.$strFilename)) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
			}
		}
		if (file_exists($filePath.'s_m_a_l_l_'.$strFilename)) {
			if(!unlink($filePath.'s_m_a_l_l_'.$strFilename)) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
			}
		}
	}

	if (!$GLOBALS['manStatusError']) {
		header('Location: '.$arrTplVars['module.name'].'?stPage='.$stPage);
		exit;
	}
}


/**
 * Запись файла
 */
if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
	$intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
	$arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

	$arrSqlData['strFileTitle'] = addslashes(trim($_POST['strFileTitle']));
	$arrTplVars['strFileTitle'] = htmlspecialchars(stripslashes(trim($_POST['strFileTitle'])));

	if (empty($arrSqlData['strFileTitle']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgEmptyField';
	} else {
		$strImageName = strtolower( substr($objUtil->translitBadChars($objUtil->translitKyrToLat($_POST['strFileTitle'])), 0, 128) );
	}

	$arrSqlData['strDesc'] = addslashes(trim($_POST['strDesc']));
	$arrTplVars['strDesc'] = htmlspecialchars(stripslashes(trim($_POST['strDesc'])));

	$arrSqlData['strReporter'] = addslashes(trim($_POST['strReporter']));
	$arrTplVars['strReporter'] = htmlspecialchars(stripslashes(trim($_POST['strReporter'])));

	$arrSqlData['strTags'] = addslashes(trim($_POST['strTags']));
	$arrTplVars['strTags'] = htmlspecialchars(stripslashes(trim($_POST['strTags'])));

	$arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
	$arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

	$arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

	if ( $GLOBALS['manStatusError'] != 1 ) {
		$strSqlFields = ""
			. " si_title = '".$arrSqlData['strFileTitle']."'"
			. ", si_desc = '".$arrSqlData['strDesc']."'"
			. ", si_reporter = '".$arrSqlData['strReporter']."'"
			. ", si_tags = '".$arrSqlData['strTags']."'"
			. ", si_type = '".$siType."'"
			. ", si_status = '".$arrSqlData['cbxStatus']."'"
//			. ", si_filename = '$strImageName'"
		;

		if ( empty($intRecordId) ) {
			$strSqlQuery = "INSERT INTO site_images SET ".$strSqlFields.", si_date_add = NOW()";
		} else {
			$strSqlQuery = "UPDATE site_images SET ".$strSqlFields.", si_date_change = NOW() WHERE si_id = ".$intRecordId;
		}

		if ( !$objDb->query( $strSqlQuery ) ) {
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		} else {
			$intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;
		}

		if ($GLOBALS['manStatusError']!=1) {
			if (isset($_POST['isImageUploaded']) && $_FILES['flPhoto']['error']) {
				# картинка уже загружена! ничего не надо!
			} elseif ( ($_FILES['flPhoto']['error'] != 0 || $_FILES['flPhoto']['size'] == 0) && !empty($intRecordId) ) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgNoFile';

			} elseif ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < 6500000 ) {
				/////////////////////////////////////////////////////////////////////////////////////
				// extended
				$arrExts['pjpeg'] = 'jpg';
				$arrExts['jpeg'] = 'jpg';

				$mimeType = explode("/", $_FILES['flPhoto']['type']);
				$mimeType = $mimeType[1];

				if ( array_key_exists($mimeType , $arrExts) ) {
					$mimeType = $arrExts[$mimeType];
				}

				if ( !in_array($mimeType, array('jpg','gif','png')) ) {
					$GLOBALS['manStatusError']=1;
					$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
				} else {
					$fileName = $strImageName.'.'.$mimeType;
					$thumbName = 't_h_u_m_b_'.$strImageName.'.'.$mimeType;
					$smallName = 's_m_a_l_l_'.$strImageName.'.'.$mimeType;
					$intSmallWidth = 600;
					$intThumbWidth = 280;
					$intThumbHeight = 200;

					$dirImages = DROOT."storage";
					if ( !file_exists($dirImages) ) {
						mkdir($dirImages, 0775);
					}

					$dirImages = $dirImages."/images";
					if ( !file_exists($dirImages) ) {
						mkdir($dirImages, 0775);
					}

					$dirImages = $dirImages."/fotobank";
					if ( !file_exists($dirImages) ) {
						mkdir($dirImages, 0775);
					}

					$dirImages = $dirImages."/cartoonbaza";
					if ( !file_exists($dirImages) ) {
						mkdir($dirImages, 0775);
					}
				}

				if ($GLOBALS['manStatusError']!=1) {
					if (file_exists($dirImages."/".$fileName)) {
						$rand = substr(time(),-3);
						$fileName = substr($fileName,0,-4) . "_". $rand . substr($fileName, -4);
						$thumbName = substr($thumbName,0,-4) . "_".$rand . substr($thumbName, -4);
						$smallName = substr($smallName,0,-4) . "_".$rand . substr($smallName, -4);
					}
					// предварительно запишем новое имя файла, потому что такой файл может быть уже записан на сервер, а поле БД ещё не изменено
					$strSqlQuery = "UPDATE site_images SET si_filename='".mysql_real_escape_string($fileName)."', si_date_change = NOW() WHERE si_id=".$intRecordId;
					if ( !$objDb->query($strSqlQuery) ) {
						#mysql error
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
					}

					if (!file_exists($dirImages."/".$fileName)) {
						move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileName );

						if ( !is_object($objImage) ) {
							include_once("cls.images.php");
							$objImage = new clsImages();
						}

						$arrPreloadSize = getimagesize($dirImages."/".$fileName);
						if ($arrPreloadSize[1] > $arrPreloadSize[0]) {
							$objImage->resizeImage($dirImages."/".$fileName, $dirImages."/".$thumbName, $intThumbWidth, $intThumbHeight );
						} else
							$objImage->resizeImage($dirImages."/".$fileName, $dirImages."/".$thumbName, $intThumbWidth );
						$markerImage = PRJ_SITEIMG.'content/marker/logo2.psb.png';
						//$objImage->setWaterMark($dirImages."/".$fileName, $markerImage);	// отменено по просьбе от 18/04/2013
						$objImage->resizeImage($dirImages."/".$fileName, $dirImages."/".$smallName, $intSmallWidth );

					} else {
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgFileAlreadyExist';
					}
				}
				/////////////////////////////////////////////////////////////////////////////////////
			} else {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgTooBigFile';
			}
		}

		if ( $GLOBALS['manStatusError']!=1 ) {
			if ( $_POST['cbxReturn']=='on' ) {
				header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId&stPage=$stPage");
			} else {
				header('location:'.$arrTplVars['module.name']."?errMess=msgOk&stPage=$stPage");
			}
			exit;
		}
	}
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");


/**
 * Показать запись
 */
if ( $_GET['action'] == 'add' || (isset($_GET['id']) && intval($_GET['id']) > 0) ) {
	$arrIf['editrecord'] = true;
	$intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

	$strSqlQuery = "SELECT * FROM site_images WHERE si_id =".$intRecordId;
	$arrInfo = $objDb->fetch( $strSqlQuery );

	if ( is_array( $arrInfo )) {
		$arrTplVars['strDesc'] = htmlspecialchars(stripslashes($arrInfo['si_desc']));
		$arrTplVars['strFileTitle'] = htmlspecialchars(stripslashes($arrInfo['si_title']));
		$arrTplVars['strFileName'] = htmlspecialchars(stripslashes($arrInfo['si_filename']));
		$arrTplVars['strReporter'] = htmlspecialchars(stripslashes($arrInfo['si_reporter']));
		$arrTplVars['strTags'] = htmlspecialchars(stripslashes($arrInfo['si_tags']));

		if (file_exists(PRJ_IMAGES.'fotobank/cartoonbaza/'.$arrInfo['si_filename'])) {
			$arrIf['is.image'] = true;
		}

		$arrTplVars['cbxStatus'] = $arrInfo['si_status'] == 'Y' ? ' checked' : '';
	}

} else {
/**
 * Показать список
 */

	// Всего записей в базе
	$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_images WHERE si_type = '".$siType."'";
	$arrTplVars['intQuantAllRecords'] = $arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
	// ***** BEGIN: Построение пейджинга для вывода списка
	$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
	/**
	 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
	 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
	 */
	$objPagination->strPaginatorTpl = "site.global.1.tpl";
	$objPagination->iQtyRecsPerPage = 12;
	$objPagination->strColorLinkStyle = "link-b";
	$objPagination->strColorActiveStyle = "tab-bl-b";
	$objPagination->strNameImageBack = "arrow_one_left.gif";
	$objPagination->strNameImageForward = "arrow_one_right.gif";
	// Создаем блок пэйджинга
	$objPagination->paCreate();
	// ***** END: Построение пейджинга для вывода списка
	// Запрос для выборки нужных записей
	$strSqlQuery = "SELECT * FROM site_images WHERE si_type = '".$siType."' ORDER BY si_id DESC ".$objPagination->strSqlLimit;
	$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
	// кол-во публикаций показанных на странице
	$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
	// Присвоение значения пэйджинга для списка
	$arrTplVars['blockPaginator'] = $objPagination->paShow();

	$arrIf['records'] = $arrTplVars['intQuantShowRecOnPage']>0;

	if ( is_array($arrSelectedRecords) ) {
		foreach ( $arrSelectedRecords as $key => $value ) {
			$arrSelectedRecords[$key]['cbxStatus'] = $value['si_status'] == 'Y' ? ' checked' : '';
		}
	}

	$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));
}

$arrIf['showlist'] = !$arrIf['editrecord'];
$arrIf['norecords'] = !$arrIf['records'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

