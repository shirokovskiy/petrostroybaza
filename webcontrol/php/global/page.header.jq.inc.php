<?php
$arrTplVars['frg.name'] = "header.jquery"; // Имя фрагмента

$objTpl->tpl_load($arrTplVars['frg.name'], "global/page.header.jq.html");

$arrIf['block.login.true'] = (isset($_SESSION['userInfoStorage']['cu_id'])) ? true : false;
$arrIf['block.login.false'] = !$arrIf['block.login.true'];

if ($arrTplVars['strUserAgent'] == 'ie') {
  $arrTplVars['brawser_styles'] = 'ie';
} else {
  $arrTplVars['brawser_styles'] = 'other';
}

$objTpl->tpl_if($arrTplVars['frg.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['frg.name'], $arrTplVars);
