<?php
$arrTplVars['frg.name'] = "footer"; // Имя фрагмента

$objTpl->tpl_load($arrTplVars['frg.name'], "global/page.footer.html");

$arrIf['block.login.true'] = (!empty($_SESSION['userInfoStorage']['cu_id'])) ? true : false;
$arrIf['block.login.false'] = !$arrIf['block.login.true'];

$objTpl->tpl_if($arrTplVars['frg.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['frg.name'], $ConfigManager);
$objTpl->tpl_array($arrTplVars['frg.name'], $arrTplVars);
