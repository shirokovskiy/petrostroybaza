<?php
/**
 * Авторизация пользователя
 */
if (isset($_POST['submitLogin'])) {
	if (!empty($_POST['bss_username']) || !empty($_POST['bss_password'])) {
    $arrDataForSql['userLogin'] = mysql_real_escape_string(trim($_POST['bss_username']));
    $arrDataForSql['userPassword'] = mysql_real_escape_string(trim($_POST['bss_password']));

    // Выбираем пользователя и его группу в сооответствии с введенными данными авторизации
    $strSqlQuery = "SELECT * FROM ".$_db_tables['cmsUsers']
      . " LEFT JOIN ".$_db_tables['cmsUserGrp']." ON (cug_id = cu_group)"
      . " WHERE cu_login='".$arrDataForSql['userLogin']."' AND cu_password=MD5('".$arrDataForSql['userPassword']."') AND cu_status='Y'";
    $arrInfoUser = $objDb->fetch( $strSqlQuery );

    if (is_array($arrInfoUser) && !empty($arrInfoUser)) {

      /**
       * Записываем дату последнего входа
       */
 			$strSqlQuery = "UPDATE ".$_db_tables['cmsUsers']." SET cu_last_enter=NOW() WHERE cu_id='".$arrInfoUser['cu_id']."'";
 			$objDb->query($strSqlQuery);

      /**
       * Проверяем к каким проектам есть доступ
       */
			$_SESSION['userInfoStorage'] = $arrInfoUser;
      $_SESSION['userInfoStorage']['strUserName'] = $arrInfoUser['cu_lname'].' '.$arrInfoUser['cu_fname'].' '.$arrInfoUser['cu_pname'];


      /**
       * Заполняем $_SESSION['arrInfoAccessProject'] доступными для пользователя проектами
       */
      $strSqlQuery = "SELECT * FROM ".$_db_tables['cmsPrjAccess']
        . " LEFT JOIN ".$_db_tables['cmsProjects']." ON (fp_id=fpa_id_project)"
        . " WHERE fpa_id_user='".$_SESSION['userInfoStorage']['cu_id']."' AND fpa_status='Y'";

      $arrInfoAccessProject = $objDb->fetchall( $strSqlQuery );
			if ( is_array($arrInfoAccessProject) ) {
				foreach ( $arrInfoAccessProject as $key=>$value ) {
					$_SESSION['arrInfoAccessProject'][$arrInfoAccessProject[$key]['fp_id']]['intIdProject'] = $arrInfoAccessProject[$key]['fp_id'];
					$_SESSION['arrInfoAccessProject'][$arrInfoAccessProject[$key]['fp_id']]['strNameProject'] = htmlspecialchars($arrInfoAccessProject[$key]['fp_name']);
					$_SESSION['arrInfoAccessProject'][$arrInfoAccessProject[$key]['fp_id']]['strPathProject'] = $arrInfoAccessProject[$key]['fp_path'];
					$_SESSION['arrInfoAccessProject'][$arrInfoAccessProject[$key]['fp_id']]['strURLProject'] = $arrInfoAccessProject[$key]['fp_url'];
					$_SESSION['arrAccessProjectId'][$arrInfoAccessProject[$key]['fp_id']] = true;
				}
			}

			$_SESSION['intIdDefaultProject'] = $arrInfoUser['cu_default_project']; // Проект по умолчанию для пользователя


			/**
			 * Выбираем конфигурацию CMS
			 */
      $strSqlQuery = "SELECT * FROM ".$_db_tables['cmsConfig']." WHERE cc_user_id='".$arrInfoUser['cu_id']."' OR (cc_user_id IS NULL AND cc_default='Y')";
      $arrInfoUserConfig = $objDb->fetchall( $strSqlQuery );

      if (is_array($arrInfoUserConfig)) {
        foreach($arrInfoUserConfig as $key=>$conFig) {
          if ( intval($conFig['cc_user_id'])> 0 ) {
            $arrUserConfig = $conFig;
          }
        }

        if (empty($arrUserConfig)) {
          $arrUserConfig = $arrInfoUserConfig[0];
        }
      }

      $_SESSION['userConfig']['cId'] = $arrUserConfig['cc_id'];
      $_SESSION['userConfig']['cBgColor'] = $arrUserConfig['cc_bg_color'];
      $_SESSION['userConfig']['cFirstColor'] = $arrUserConfig['cc_first_color'];
      $_SESSION['userConfig']['cSecondColor'] = $arrUserConfig['cc_second_color'];
      $_SESSION['userConfig']['cThirdColor'] = $arrUserConfig['cc_third_color'];
      $_SESSION['userConfig']['cFourthColor'] = $arrUserConfig['cc_fourth_color'];
      $_SESSION['userConfig']['strNumVersion'] = $arrUserConfig['cc_ver'];
      $_SESSION['userConfig']['strCMSName'] = $arrUserConfig['cc_name'];


      header ("Location: ".ADMINURL."user.main");
		  exit();
		} else {
			$arrTplVars['strLoginError'] = $errMsg['msgAuthError1']; // Логин и/или пароль не верны
		}
	} else {
		$arrTplVars['strLoginError'] = $errMsg['msgAuthError2']; // Пустые поля login и password
	}
}

$objTpl->tpl_load("login", "global/login.html");

$arrIf['block.error.login'] = (!empty($arrTplVars['strLoginError'])) ? true : false;

$objTpl->tpl_if("login", $arrIf);
$objTpl->tpl_array("login", $arrTplVars);
$objTpl->tpl_parse("login");
