<?php
$arrTplVars['frg.name'] = "popup.header"; // Имя фрагмента

$objTpl->tpl_load($arrTplVars['frg.name'], "global/page.popup.header.html");

if ($arrTplVars['strUserAgent'] == 'ie') {
  $arrTplVars['brawser_styles'] = 'ie';
} else {
  $arrTplVars['brawser_styles'] = 'other';
}

$objTpl->tpl_if($arrTplVars['frg.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['frg.name'], $arrTplVars);
