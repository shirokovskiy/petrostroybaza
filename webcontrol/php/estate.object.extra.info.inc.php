<?php
$arrTplVars['module.name'] = "estate.object.extra.info";
$arrTplVars['module.parent'] = "estate.objects";
$arrTplVars['module.title'] = "Доп.инфо объекта";
$objEstateObject = new EstateObject();
$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление
 */
if ( intval($_GET['id']) > 0 && intval($_GET['oid']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $intObjectId = intval($_GET['oid']);
    if (!empty($intRecordId) && !empty($intObjectId)) {
        $strSqlQuery = "DELETE FROM `psb_estate_object_extra_info` WHERE `oid` = $intObjectId AND `id` = $intRecordId";
        if ( !$objDb->query($strSqlQuery) ) {
            #mysql error
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            header('location:'.$arrTplVars['module.name'].'?errMess=msgRmOk&stPage='.$arrTplVars['stPage'].'&oid='.$intObjectId);
            exit;
        }
    }
}

/**
 * Сохранение
 */
if ($_POST['submit']) {
    $arrSqlData['intRecordId'] = $intRecordId = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ($arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['intObjectId'] = $intObjectId = intval(trim($_POST['intObjectId']));
    $arrTplVars['intObjectId'] = ($arrSqlData['intObjectId'] > 0 ? $arrSqlData['intObjectId'] : '');

    $arrSqlData['strTitle'] = mysql_real_escape_string(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyField';
    }

    $arrSqlData['strSource'] = mysql_real_escape_string(trim($_POST['strSource']));
    $arrTplVars['strSource'] = htmlspecialchars(stripslashes(trim($_POST['strSource'])));

    if (empty($arrSqlData['strSource']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyField';
    }

    $arrSqlData['strBody'] = mysql_real_escape_string(trim($_POST['strBody']));
    $arrTplVars['strBody'] = htmlspecialchars(stripslashes(trim($_POST['strBody'])));

    if (empty($arrSqlData['strBody']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyField';
    }

    $arrSqlData['strDate'] = mysql_real_escape_string(trim($_POST['strDate']));
    $arrTplVars['strDate'] = htmlspecialchars(stripslashes(trim($_POST['strDate'])));

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn'] == 'on' ? 'Y' : 'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn'] == 'on' ? ' checked' : '');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            . " `title` = '{$arrSqlData['strTitle']}'"
            . ", `content` = '{$arrSqlData['strBody']}'"
            . ", `source` = '{$arrSqlData['strSource']}'"
            . ", `oid` = ".$arrSqlData['intObjectId']
            . ", `date` = '".date('Y-m-d', strtotime($arrSqlData['strDate']))."'";
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO `psb_estate_object_extra_info` SET ".$strSqlFields;
        } else {
            $strSqlQuery = "UPDATE `psb_estate_object_extra_info` SET ".$strSqlFields." WHERE id = ".$intRecordId;
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId.'&oid='.$intObjectId);
            } else {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&oid='.$intObjectId);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ($_GET['oid'] > 0) {
    $arrTplVars['objectName'] = $objEstateObject->getDataById(intval($_GET['oid']), 'seo_name', 'seo_name');
}

if (($_GET['id'] > 0 || $_GET['add']=='new') && $_GET['oid'] > 0) {
    $intRecordId = $arrTplVars['id'] = intval($_GET['id']);
    $intObjectId = $arrTplVars['oid'] = intval($_GET['oid']);
    $arrIf['form']=true;

    $strSqlQuery = "SELECT * FROM `psb_estate_object_extra_info` WHERE `id` = " . $intRecordId . " AND `oid` = ".$intObjectId;
    $arrInfo = $objDb->fetch($strSqlQuery);
    if (is_array($arrInfo) && !empty($arrInfo)) {
        $arrTplVars['strTitle'] = htmlspecialchars($arrInfo['title']);
        $arrTplVars['strSource'] = htmlspecialchars($arrInfo['source']);
        $arrTplVars['strBody'] = htmlspecialchars($arrInfo['content']);
        $arrTplVars['strDate'] = $objUtil->workDate(3, $arrInfo['date']);
    }

} elseif ($_GET['oid'] > 0) {
    $oid = $arrTplVars['oid'] = $_GET['oid'];
    $arrIf['form']=false;
    $strSqlQuery = "SELECT *, DATE_FORMAT(`date`, '%d.%m.%Y') strDate FROM `psb_estate_object_extra_info` WHERE `oid` = ".$oid;
    $arrList = $objDb->fetchall($strSqlQuery);
    $objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrList, array());
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

