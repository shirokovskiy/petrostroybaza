<?php
$arrTplVars['module.name'] = "ad_estate_land.form";
$arrTplVars['module.parent'] = "ad_estate_lands";
$arrTplVars['module.title'] = "Добавить недвижимость, участок";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset($_POST['frmEditEstateLands']) && $_POST['frmEditEstateLands'] == 'true' ) {
  $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
  $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

  $arrSqlData['intChapterId'] = intval(trim($_POST['intChapterId']));
  $arrTplVars['intChapterId'] = ( $arrSqlData['intChapterId'] > 0 ? $arrSqlData['intChapterId'] : '');

  if (empty($arrSqlData['intChapterId']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgChapterId';
    $arrTplVars['brdError_intChapterId'] = "border-color: red;";
  }

  $arrSqlData['strName'] = addslashes(trim($_POST['strName']));
  $arrTplVars['strName'] = htmlspecialchars(stripslashes(trim($_POST['strName'])));

  if (empty($arrSqlData['strName']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgName';
    $arrTplVars['brdError_strName'] = "border-color: red;";
  }

  $arrSqlData['strAddress'] = addslashes(trim($_POST['strAddress']));
  $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

  if (empty($arrSqlData['strAddress']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgAddress';
    $arrTplVars['brdError_strAddress'] = "border-color: red;";
  }

  $arrSqlData['strOrgName'] = addslashes(trim($_POST['strOrgName']));
  $arrTplVars['strOrgName'] = htmlspecialchars(stripslashes(trim($_POST['strOrgName'])));

  if (empty($arrSqlData['strOrgName']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgOrgName';
    $arrTplVars['brdError_strOrgName'] = "border-color: red;";
  }

  $arrSqlData['strContact'] = addslashes(trim($_POST['strContact']));
  $arrTplVars['strContact'] = htmlspecialchars(stripslashes(trim($_POST['strContact'])));

  if (empty($arrSqlData['strContact']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgContact';
    $arrTplVars['brdError_strContact'] = "border-color: red;";
  }

  $arrSqlData['strPhone'] = addslashes(trim($_POST['strPhone']));
  $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

  if (empty($arrSqlData['strPhone']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgPhone';
    $arrTplVars['brdError_strPhone'] = "border-color: red;";
  }

  $arrSqlData['strInfo'] = addslashes(trim($_POST['strInfo']));
  $arrTplVars['strInfo'] = htmlspecialchars(stripslashes(trim($_POST['strInfo'])));

  $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
  $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

  $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
  $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

  if ($GLOBALS['manStatusError']!=1) {
    $strSqlFields = ""
      . " pael_name = '{$arrSqlData['strName']}'"
      . ", pael_chapter_id = '{$arrSqlData['intChapterId']}'"
      . ", pael_address = '{$arrSqlData['strAddress']}'"
      . ", pael_orgname = '{$arrSqlData['strOrgName']}'"
      . ", pael_contact = '{$arrSqlData['strContact']}'"
      . ", pael_phone = '{$arrSqlData['strPhone']}'"
      . ", pael_info = '{$arrSqlData['strInfo']}'"
      . ", pael_status = '{$arrSqlData['cbxStatus']}'"
      ;

    if ( empty($intRecordId) ) {
      $strSqlQuery = "INSERT INTO ".$_db_tables['stAdsEstateLands']." SET $strSqlFields, pael_date_create = NOW()";
    } else {
      $strSqlQuery = "UPDATE ".$_db_tables['stAdsEstateLands']." SET $strSqlFields, pael_date_update = NOW() WHERE pael_id='$intRecordId'";
    }

    if ( !$objDb->query( $strSqlQuery ) ) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      if ( empty($intRecordId) ) {
        $intRecordId = $objDb->insert_id();
      }
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
      if ( $arrSqlData['cbxReturn'] == 'Y' ) {
        header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
      } else {
        header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
      }
      exit;
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAdsEstateLands']." WHERE pael_id ='$intRecordId'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strName'] = htmlspecialchars(stripslashes($arrInfo['pael_name']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes($arrInfo['pael_address']));
    $arrTplVars['strOrgName'] = htmlspecialchars(stripslashes($arrInfo['pael_orgname']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes($arrInfo['pael_phone']));
    $arrTplVars['strContact'] = htmlspecialchars(stripslashes($arrInfo['pael_contact']));
    $arrTplVars['strInfo'] = htmlspecialchars(stripslashes($arrInfo['pael_info']));
    $arrTplVars['intChapterId'] = htmlspecialchars(stripslashes($arrInfo['pael_chapter_id']));

    $arrTplVars['cbxStatus'] = $arrInfo['pael_status'] == 'Y' ? ' checked' : '';
  }
}

$strSqlQuery = "SELECT * FROM ".$_db_tables["stEstateAdsChapters"]." ORDER BY paec_name ";
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
if ( is_array($arrSelectedRecords) ) {
  foreach ( $arrSelectedRecords as $key => $value ) {
    $arrSelectedRecords[$key]['sel'] = $value['paec_id'] == $arrTplVars['intChapterId'] ? ' selected' : '';
  }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords);
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
