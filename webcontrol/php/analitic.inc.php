<?php
$arrTplVars['module.name'] = "analitic";
$arrTplVars['module.child'] = "analitic.form";
$arrTplVars['module.title'] = "Аналитика";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
  $intRecordId = intval($_GET['id']);
  $strSqlQuery = "DELETE FROM ".$_db_tables['stAnalitic']." WHERE pa_id='$intRecordId'";
  if ( !$objDb->query($strSqlQuery) ) {
    #mysql error
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
  } else {
    $fileNamePath = DROOT."storage/images/analitics/article.photo.$intRecordId";
    if ( file_exists("$fileNamePath.jpg")) {
      unlink("$fileNamePath.jpg");
      unlink("$fileNamePath.b.jpg");
    }

    $strSqlQuery = "SELECT * FROM ".$_db_tables["stAnaliticFiles"]." WHERE paf_rel_id = '$intRecordId'";
    $arrAttachedFiles = $objDb->fetchall( $strSqlQuery );

    if ( is_array( $arrAttachedFiles ) ) {
      foreach ($arrAttachedFiles as $key => $value) {
        $delFileName = DROOT."storage/files/analitics/".$value['paf_filename'];
        if ( file_exists($delFileName) && is_file($delFileName)) {
          unlink($delFileName);
        }
      }

      $strSqlQuery = "DELETE FROM ".$_db_tables['stAnaliticFiles']." WHERE paf_rel_id = '$intRecordId'";
      if ( !$objDb->query( $strSqlQuery ) ) {
        # sql error
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '111';
      }
    }

    header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
    exit();
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stAnalitic'];
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stAnalitic'];
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);

$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка

// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stAnalitic']." ORDER BY pa_id DESC ".$objPagination->strSqlLimit;
$arrNews = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrNews) ? count($arrNews) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrNews) ) {
  foreach ( $arrNews as $key => $value ) {
    $arrNews[$key]['strDatePubl'] = date( "d.m.Y", strtotime($value['pa_date_publ']));
    $arrNews[$key]['strDateAdd'] = date( "d.m.Y H:i", strtotime($value['pa_date_add']));
    $arrNews[$key]['strDateChange'] = !empty($value['pa_date_change']) ? date( "d.m.Y H:i", strtotime($value['pa_date_change'])) : '';
    $arrNews[$key]['cbxStatus'] = $value['pa_status'] == 'Y' ? ' checked' : '';
    $arrIf['attach.'.$value['pa_id']] = ( file_exists(DROOT."storage/images/analitic/article.photo.".$value['pa_id'].".jpg") && $value['pa_image'] == 'Y');
  }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "articles", $arrNews, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
