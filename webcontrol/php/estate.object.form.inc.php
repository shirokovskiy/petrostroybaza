<?php
/**
 * Форма редактирования объекта недвижимости
 */
$arrTplVars['module.name'] = "estate.object.form";
$arrTplVars['module.parent'] = "estate.objects";
$arrTplVars['module.add.contact'] = "company.form";
$arrTplVars['module.ref.contact'] = "company.ref.form";
$arrTplVars['module.companies'] = "companies";
$arrTplVars['module.fotobank'] = "fotobank";
$arrTplVars['module.title'] = "Добавить объект";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

$arrCntExt['zak'] = "Zak";
$arrCntExt['zas'] = "Zas";
$arrCntExt['inv'] = "Inv";
$arrCntExt['genpod'] = "GenPod";
$arrCntExt['proekt'] = "Proekt";
$arrCntExt['pod'] = "Pod";

$arrImagesInfo = $arrAttachedFiles = array();

unset($_SESSION['frmSearchCompanies']);

/**
 * Удаление фото
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
	$intRecordId = intval($_GET['id']);
	$intObjectId = intval($_GET['obj']);
	if (!empty($intRecordId) && !empty($intObjectId)) {
		$strSqlQuery = "DELETE FROM `site_images` WHERE si_id_rel = $intObjectId AND si_type = 'estate' AND si_id = $intRecordId";
		if ( !$objDb->query($strSqlQuery) ) {
			#mysql error
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		} else {
			$dirImages = DROOT."storage/images/objects/";
			if ( file_exists($dirImages."obj.$intRecordId.jpg")) {
				unlink($dirImages."obj.$intRecordId.jpg");
				unlink($dirImages."obj.$intRecordId.b.jpg");
			}

			header("Location: {$arrTplVars['module.name']}?id=$intObjectId&stPage={$arrTplVars['stPage']}");
			exit();
		}
	} else {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorDelObjImg';
	}
}

/** >***************************************************************************\
 ** Shirokovskiy D.2006 Jimmy™. Sun Dec 24 23:40:33 MSK 2006
 *
 * Сохранение объекта
 **
\*******************************************************************************/
if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
	$intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
	$arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

	$arrSqlData['strObjectName'] = addslashes(trim($_POST['strObjectName']));
	$arrTplVars['strObjectName'] = htmlspecialchars(stripslashes(trim($_POST['strObjectName'])));

	if (empty($arrSqlData['strObjectName']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorObjName';
	} else {
		$arrSqlData['strUrl'] = strtolower( $objUtil->translitBadChars($objUtil->translitKyrToLat(trim($_POST['strObjectName']))) );
        $arrSqlData['strUrl'] = preg_replace("/[^a-zA-Z_-]/", "_", $arrSqlData['strUrl']);
        $arrSqlData['strUrl'] = mysql_real_escape_string( substr($arrSqlData['strUrl'], 0, 255) );
	}

	$arrSqlData['strObjFunction'] = addslashes(trim($_POST['strObjFunction']));
	$arrTplVars['strObjFunction'] = htmlspecialchars(stripslashes(trim($_POST['strObjFunction'])));

	$arrSqlData['intEstTypeId'] = intval(trim($_POST['intEstTypeId']));
	$arrTplVars['intEstTypeId'] = ( $arrSqlData['intEstTypeId'] > 0 ? $arrSqlData['intEstTypeId'] : '');

	if (empty($arrSqlData['intEstTypeId']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorEstType';
	}

    $arrSqlData['intEstGoalId'] = intval(trim($_POST['intEstGoalId']));
    $arrSqlData['intEstGoalId'] = $arrSqlData['intEstGoalId']?:'NULL';
    $arrTplVars['intEstGoalId'] = ( $arrSqlData['intEstGoalId'] > 0 ? $arrSqlData['intEstGoalId'] : '');

    $arrSqlData['intAdmReg'] = intval(trim($_POST['intAdmReg']));
	$arrTplVars['intAdmReg'] = ( $arrSqlData['intAdmReg'] > 0 ? $arrSqlData['intAdmReg'] : '');

	$arrSqlData['strAdmRegionOther'] = addslashes(trim($_POST['strAdmRegionOther']));
	$arrTplVars['strAdmRegionOther'] = htmlspecialchars(stripslashes(trim($_POST['strAdmRegionOther'])));

	if (empty($arrSqlData['intAdmReg']) && empty($arrSqlData['strAdmRegionOther'])) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorAdmReg';
	}

	$arrSqlData['strObjectAddress'] = addslashes(trim($_POST['strObjectAddress']));
	$arrTplVars['strObjectAddress'] = htmlspecialchars(stripslashes(trim($_POST['strObjectAddress'])));

	if (empty($arrSqlData['strObjectAddress']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorObjAdr';
	}

	$arrSqlData['intVidId'] = intval(trim($_POST['intVidId']));
	$arrTplVars['intVidId'] = ( $arrSqlData['intVidId'] > 0 ? $arrSqlData['intVidId'] : '');

	$arrSqlData['strVidOther'] = addslashes(trim($_POST['strVidOther']));
	$arrTplVars['strVidOther'] = htmlspecialchars(stripslashes(trim($_POST['strVidOther'])));

	if (empty($arrSqlData['strVidOther']) && empty($arrSqlData['intVidId'])) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorVid';
	}

	$arrSqlData['strEtap'] = addslashes(trim($_POST['strEtap']));
	$arrTplVars['strEtap'] = htmlspecialchars(stripslashes(trim($_POST['strEtap'])));

	if (empty($arrSqlData['strEtap']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorEtap';
	}

	$arrSqlData['strEtapDate'] = addslashes(trim($_POST['strEtapDate']));
	$arrTplVars['strEtapDate'] = htmlspecialchars(trim($_POST['strEtapDate']));

	if ( preg_match("/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/", $arrSqlData['strEtapDate'], $arrDatePieces) ) {
		$arrSqlData['strEtapDate'] = "'".$arrDatePieces[3].'-'.$arrDatePieces[2].'-'.$arrDatePieces[1]."'";
	} else {
		$arrSqlData['strEtapDate'] = 'NULL';
	}

	$arrStr=  array('strSqZemUch','strSqZastr','strSqTotal','strEtazh','strKarkas',
		'strFundament','strSteni','strWindows','strRoof','strFloor','strDoors','strPravoDoc',
		'strPeriodProject','strPeriodBuild','strBuildVolume');
	foreach ($arrStr as $value) {
		$arrSqlData[$value]= addslashes(trim($_POST[$value]));
		$arrTplVars[$value] = htmlspecialchars(stripslashes(trim($_POST[$value])));
	}

	$arrCbx=  array('cbxStatus','cbxAccess','cbxAnons','cbxSB','cbxReturn');
	foreach ($arrCbx as $value) {
		$arrSqlData[$value]= ($_POST[$value]=='on'?'Y':'N');
		$arrTplVars[$value] = ($_POST[$value]=='on'?' checked':'');
	}

	if ($GLOBALS['manStatusError']!=1) {
		$strSqlFields = ""
			. " seo_name                = '{$arrSqlData['strObjectName']}'"
			. ", seo_naznach            = '{$arrSqlData['strObjFunction']}'"
			. ", seo_id_etype           = '{$arrSqlData['intEstTypeId']}'"
			. ", seo_id_goal            = ".$arrSqlData['intEstGoalId']
			. ( !empty($arrSqlData['intAdmReg']) ? ", seo_id_adm_reg = '{$arrSqlData['intAdmReg']}'" : '' )
			. ( !empty($arrSqlData['strAdmRegionOther']) && empty($arrSqlData['intAdmReg']) ? ", seo_other_adm_reg = '{$arrSqlData['strAdmRegionOther']}'" : '' )
			. ", seo_address            = '{$arrSqlData['strObjectAddress']}'"
			. ( !empty($arrSqlData['intVidId']) ? ", seo_id_vid = '{$arrSqlData['intVidId']}'" : '' )
			. ( !empty($arrSqlData['strVidOther']) && empty($arrSqlData['intVidId']) ? ", seo_other_vid = '{$arrSqlData['strVidOther']}'" : '' )
			. ", seo_other_etap         = '{$arrSqlData['strEtap']}'"
			. ", seo_etap_date          = {$arrSqlData['strEtapDate']}"
			. ", seo_sq_zem_uch         = '{$arrSqlData['strSqZemUch']}'"
			. ", seo_sq_zastr           = '{$arrSqlData['strSqZastr']}'"
			. ", seo_sq_total           = '{$arrSqlData['strSqTotal']}'"
			. ", seo_etazh              = '{$arrSqlData['strEtazh']}'"
			. ", seo_karkas             = '{$arrSqlData['strKarkas']}'"
			. ", seo_fundament          = '{$arrSqlData['strFundament']}'"
			. ", seo_steni              = '{$arrSqlData['strSteni']}'"
			. ", seo_windows            = '{$arrSqlData['strWindows']}'"
			. ", seo_roof               = '{$arrSqlData['strRoof']}'"
			. ", seo_floor              = '{$arrSqlData['strFloor']}'"
			. ", seo_doors              = '{$arrSqlData['strDoors']}'"
			. ", seo_pravo_doc          = '{$arrSqlData['strPravoDoc']}'"
			. ", seo_period_prj         = '{$arrSqlData['strPeriodProject']}'"
			. ", seo_period_bld         = '{$arrSqlData['strPeriodBuild']}'"
			. ", seo_bld_volume         = '{$arrSqlData['strBuildVolume']}'"
			. ", seo_status             = '{$arrSqlData['cbxStatus']}'"
			. ", seo_anons              = '{$arrSqlData['cbxAnons']}'"
			. ", seo_pa                 = '{$arrSqlData['cbxAccess']}'"
			. ", seo_sb                 = '{$arrSqlData['cbxSB']}'"
		;

		if ( empty($intRecordId) ) {
			$strSqlQuery = "INSERT INTO site_estate_objects SET ".$strSqlFields.", seo_date_add = NOW()";
		} else {
			$strSqlQuery = "UPDATE site_estate_objects SET ".$strSqlFields.", seo_date_change = NOW(), seo_coords = NULL WHERE seo_id = ".$intRecordId;
		}

		if ( !$objDb->query( $strSqlQuery ) ) {
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		} else {
			if ( empty($intRecordId) ) {
				$intRecordId = $objDb->insert_id();
			}

            $strSqlQuery = "UPDATE `site_estate_objects` SET `seo_url` = '".$arrSqlData['strUrl']."_$intRecordId' WHERE `seo_id` = ".$intRecordId;
            if (!$objDb->query($strSqlQuery)) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            }
		}


        /**
		 * Загрузка фото
		 */
		if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0
			&& $_FILES['flPhoto']['size'] < 2000000 && $GLOBALS['manStatusError']!=1 ) {
			$strImageName = "object.photo.$intRecordId";

			/////////////////////////////////////////////////////////////////////////////////////
			// extended
			$arrExts['pjpeg'] = 'jpg';
			$arrExts['jpeg'] = 'jpg';

			$mimeType = explode("/", $_FILES['flPhoto']['type']);
			$mimeType = $mimeType[1];

			if ( key_exists($mimeType , $arrExts) ) {
				$mimeType = $arrExts[$mimeType];
			}

			if ( $mimeType != 'jpg' && $mimeType != 'gif' && $mimeType != 'png' ) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
			} else {
				$fileName = "$strImageName.$mimeType";
				$fileNameBig = "$strImageName.b.$mimeType";
				$fileNameOrig = "temp.$strImageName.$mimeType";

				$dirStorage = DROOT."storage/";
				if ( !file_exists($dirStorage) ) {
					if (!mkdir($dirStorage, 0775)) {
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
					}
				}

				$dirImages = $dirStorage."images/";
				if ( !file_exists($dirImages) ) {
					if (!mkdir($dirImages, 0775)) {
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
					}
				}

				$dirImages = $dirImages."objects/";
				if ( !file_exists($dirImages) ) {
					if (!mkdir($dirImages, 0775)) {
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
					}
				}

				if ($GLOBALS['manStatusError']!=1) {
					move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig );

					if ( !is_object($objImage) ) {
						include_once("cls.images.php");
						$objImage = new clsImages();
					}

					$objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 100, 70 );
					$objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig, 1900 );

					unlink($dirImages."/".$fileNameOrig);
				}

				if (file_exists($dirImages."/".$fileName) && file_exists($dirImages."/".$fileNameBig)) {
					$strSqlQuery = "INSERT INTO ".$_db_tables['stImages']." SET"
						. " si_id_rel = '$intRecordId'"
						. ", si_filename = '$fileNameBig'"
						. ", si_type = 'estate'"
						. ", si_date_add = NOW()"
						. ", si_status = 'Y'"
						. "";
					if ( !$objDb->query( $strSqlQuery ) ) {
						#error
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
					} else {
						$intPhotoId = $objDb->insert_id();

						rename($dirImages."/".$fileName, $dirImages."/$fileName");
						rename($dirImages."/".$fileNameBig, $dirImages."/$fileNameBig");
					}
				}
			}
			/////////////////////////////////////////////////////////////////////////////////////
		}


		if (!$GLOBALS['manStatusError']) {
			/**
			 * Запишем данные о контактах
			 */
			if ( 1!=1 && is_array($arrCntExt) ) {
				foreach ( $arrCntExt as $key => $value ) {
					if (!$GLOBALS['manStatusError']) {
						$arrSqlData["int{$value}Id"] = intval(trim($_POST["int{$value}Id"]));
						$arrTplVars["int{$value}Id"] = ( $arrSqlData["int{$value}Id"] > 0 ? $arrSqlData["int{$value}Id"] : "");

						$arrSqlData["str{$value}Fio"] = addslashes(trim($_POST["str{$value}Fio"]));
						$arrTplVars["str{$value}Fio"] = htmlspecialchars(stripslashes(trim($_POST["str{$value}Fio"])));

						$arrSqlData["str{$value}Company"] = addslashes(trim($_POST["str{$value}Company"]));
						$arrTplVars["str{$value}Company"] = htmlspecialchars(stripslashes(trim($_POST["str{$value}Company"])));

						$arrSqlData["str{$value}Proff"] = addslashes(trim($_POST["str{$value}Proff"]));
						$arrTplVars["str{$value}Proff"] = htmlspecialchars(stripslashes(trim($_POST["str{$value}Proff"])));

						$arrSqlData["str{$value}Contact"] = addslashes(trim($_POST["str{$value}Contact"]));
						$arrTplVars["str{$value}Contact"] = htmlspecialchars(stripslashes(trim($_POST["str{$value}Contact"])));

						$arrSqlData["cbx{$value}Status"] = ($_POST["cbx{$value}Status"]=="on"?"Y":"N");
						$arrTplVars["cbx{$value}Status"] = ($_POST["cbx{$value}Status"]=="on"?" checked":"");

						if ( !empty($arrSqlData["str{$value}Contact"]) || !empty($arrSqlData["str{$value}Fio"])
							|| !empty($arrSqlData["str{$value}Company"]) || !empty($arrSqlData["str{$value}Proff"]) ) {

							$strSqlFields = ""
								. " sec_type = '$key'"
								. ", sec_id_obj = ".$intRecordId
								. ", sec_company = '{$arrSqlData["str{$value}Company"]}'"
								. ", sec_fio = '{$arrSqlData["str{$value}Fio"]}'"
								. ", sec_proff = '{$arrSqlData["str{$value}Proff"]}'"
								. ", sec_contact = '{$arrSqlData["str{$value}Contact"]}'"
								. ", sec_status = '{$arrSqlData["cbx{$value}Status"]}'"
							;

							if (empty($arrSqlData["int{$value}Id"])) {
								$strSqlQuery = "INSERT INTO site_estate_contacts SET". $strSqlFields
									. ", sec_date_add = NOW()";
							} else {
								$strSqlQuery = "UPDATE site_estate_contacts SET". $strSqlFields
									. ", sec_date_change = NOW()"
									. " WHERE sec_id = '{$arrSqlData["int{$value}Id"]}'";
							}

							if ( !$objDb->query( $strSqlQuery ) ) {
								#error
								$GLOBALS['manStatusError']=1;
								$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
								$res = "Error in " . __FILE__ . "! Line: " . __LINE__ .print_r("\n".$strSqlQuery, true).print_r("\n".mysql_error() );
								$fp = @fopen(DROOT."storage/debug/sql.txt", "a+");
								@fwrite($fp, "".$res);
								@fclose($fp);
							}
						}
					}
				}
			}


            /**
             * Запись регионов
             */
            if (is_array($_POST['cbxRegion']) && !empty($_POST['cbxRegion'])) {
                $strSqlQuery = "DELETE FROM `site_objects_regions_lnk` WHERE sorl_seo_id = ".$intRecordId;
                if (!$objDb->query($strSqlQuery)) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                }

                foreach ($_POST['cbxRegion'] as $k => $r_id) {
                    $strSqlQuery = "INSERT INTO `site_objects_regions_lnk` SET"
                        . " sorl_seo_id = ".$intRecordId
                        . ", sorl_sr_id = ".$r_id;
                    if (!$objDb->query($strSqlQuery)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    }
                }
            } else {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorReg';
            }
		}

		if ( $GLOBALS['manStatusError']!=1 ) {
			if ( $arrSqlData['cbxReturn'] == 'Y' ) {
				header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
			} else {
				header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
			}
			exit;
		}
	}
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");


/** >***************************************************************************\
 ** Shirokovskiy D.2006 Jimmy™. Mon Dec 25 00:21:27 MSK 2006
 *
 * Отображаем выбранный объект
 **
\*******************************************************************************/
if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
	$intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

	$strSqlQuery = "SELECT * FROM site_estate_objects WHERE seo_id = ".$intRecordId;
	$arrInfo = $objDb->fetch( $strSqlQuery );

	/*if ( is_array($arrInfo) ) {
		foreach ( $arrInfo as $key => $value ) {
			if ($objUtil->checkBadChars($value)) {
				$arrInfo[$key] = $objUtil->translitBadChars( $value, false );
			}
		}
	}*/

	if ( is_array( $arrInfo )) {
        $arrTplVars['module.title'] = "Редактировать объект";

		$arrIf['can.add.companies'] = true;
		$arrTplVars['strObjectName']    = htmlspecialchars(stripslashes($arrInfo['seo_name']));
		$arrTplVars['strObjFunction']   = htmlspecialchars(stripslashes($arrInfo['seo_naznach']));
		$arrTplVars['strObjectAddress'] = htmlspecialchars(stripslashes($arrInfo['seo_address']));
		$arrTplVars['strEtap']          = htmlspecialchars(stripslashes($arrInfo['seo_other_etap']));
		if ( !empty($arrInfo['seo_etap_date']) ) {
			$arrTplVars['strEtapDate']      = date('d.m.Y', strtotime($arrInfo['seo_etap_date']));
		}
		$arrTplVars['strSqZemUch']      = htmlspecialchars(stripslashes($arrInfo['seo_sq_zem_uch']));
		$arrTplVars['strSqZastr']       = htmlspecialchars(stripslashes($arrInfo['seo_sq_zastr']));
		$arrTplVars['strSqTotal']       = htmlspecialchars(stripslashes($arrInfo['seo_sq_total']));
		$arrTplVars['strEtazh']         = htmlspecialchars(stripslashes($arrInfo['seo_etazh']));
		$arrTplVars['strKarkas']        = htmlspecialchars(stripslashes($arrInfo['seo_karkas']));
		$arrTplVars['strFundament']     = htmlspecialchars(stripslashes($arrInfo['seo_fundament']));
		$arrTplVars['strSteni']         = htmlspecialchars(stripslashes($arrInfo['seo_steni']));
		$arrTplVars['strWindows']       = htmlspecialchars(stripslashes($arrInfo['seo_windows']));
		$arrTplVars['strRoof']          = htmlspecialchars(stripslashes($arrInfo['seo_roof']));
		$arrTplVars['strFloor']         = htmlspecialchars(stripslashes($arrInfo['seo_floor']));
		$arrTplVars['strDoors']         = htmlspecialchars(stripslashes($arrInfo['seo_doors']));
		$arrTplVars['strPravoDoc']      = htmlspecialchars(stripslashes($arrInfo['seo_pravo_doc']));
		$arrTplVars['strPeriodProject'] = htmlspecialchars(stripslashes($arrInfo['seo_period_prj']));
		$arrTplVars['strPeriodBuild']   = htmlspecialchars(stripslashes($arrInfo['seo_period_bld']));
		$arrTplVars['strBuildVolume']   = htmlspecialchars(stripslashes($arrInfo['seo_bld_volume']));
		$arrTplVars['cbxStatus']        = $arrInfo['seo_status'] == 'Y' ? ' checked' : '';
		$arrTplVars['cbxAnons']         = $arrInfo['seo_anons'] == 'Y' ? ' checked' : '';
		$arrTplVars['cbxAccess']        = $arrInfo['seo_pa'] == 'Y' ? ' checked' : '';
		$arrTplVars['cbxSB']            = $arrInfo['seo_sb'] == 'Y' ? ' checked' : '';
		$arrTplVars['objUrl']           = !empty($arrInfo['seo_url']) ? '<br />URL: <a href="'.SITE_URL.'objectinfo/'.$arrInfo['seo_url'].'" target=_blank title="Открыть в новом окне">'.chunk_split($arrInfo['seo_url'], 108, "<br />").'</a>' : '';

        /**
         * Выбераем регионы
         */
        if (empty($arrInfo['seo_id_adm_reg'])) {
			$arrTplVars['strAdmRegionOther'] = htmlspecialchars(stripslashes($arrInfo['seo_other_adm_reg']));
		} else {
            $arrTplVars['preselectedAdmReg'] = $arrInfo['seo_id_adm_reg'];
        }
		if (empty($arrInfo['seo_id_vid'])) {
			$arrTplVars['strVidOther'] = htmlspecialchars(stripslashes($arrInfo['seo_other_vid']));
		}

		/**
		 * Выбрать контакты
		 */
		$strSqlQuery = "SELECT * FROM ".$_dbt["stObjContacts"]." AS stOC"
			." LEFT JOIN ".$_dbt["stContacts"]." AS stC ON (stC.sec_id = stOC.socl_id_contact)"
			." WHERE stOC.socl_id_object = ".$intRecordId;
		$arrContactRecords = $objDb->fetchall( $strSqlQuery );
		if ( is_array($arrContactRecords) ) {
			foreach ( $arrContactRecords as $key => $value ) {
				$arrContactRecords[$key]['sec_contact'] = nl2br($value['sec_contact']);
				$arrContactRecords[$key]['strCompanyType'] = $arrTitleExt[$value['socl_relation']];
			}
		}

		/**
		 * Выбрать главную картинку
		 */
		$strSqlQuery = "SELECT * FROM ".$_db_tables['stImages']." WHERE si_type='estate' AND si_id_rel= $intRecordId ORDER BY si_id DESC LIMIT 1";
		$arrPhotoInfo = $objDb->fetch( $strSqlQuery );
		$arrIf['photo'] = !empty($arrPhotoInfo['si_filename']);
		$arrTplVars['intPhotoId'] = $arrPhotoInfo['si_id'];
		$arrTplVars['si_filename_big'] = $arrPhotoInfo['si_filename'];
		$arrTplVars['si_filename'] = str_replace(".b.", ".", $arrPhotoInfo['si_filename']);

        /**
         * Выбераем картинки из фотобанка если есть
         */
        $strSqlQuery = "SELECT tSI.*, tSIG.sig_name FROM site_images tSI LEFT JOIN site_images_group tSIG ON (sig_id = si_id_rel) WHERE si_type='fotobank' AND si_foto_object_id = $intRecordId ORDER BY si_id";
        $arrPhotoBankInfo = $objDb->fetchall( $strSqlQuery );
        if (is_array($arrPhotoBankInfo) && !empty($arrPhotoBankInfo)) {
            $arrIf['fotobank'] = true;

            foreach ($arrPhotoBankInfo as $jk => $image) {
                if (!empty($image['sig_name'])) {
                    $arrTplVars['intPhotoBankID'] = $image['si_id_rel'];
                    $arrTplVars['strPhotoBankName'] = $image['sig_name'];
                    break;
                }
            }

            // Let's check attached images folder
            $attachedFiles = PRJ_IMAGES.'fotobank/groups/'.$arrTplVars['intPhotoBankID'].'/';
            $arrAttachedFiles = array();
            if (is_dir($attachedFiles)) {
                $exclude_list = array(".", "..");
                $arrAttachedFiles = array_diff(scandir($attachedFiles), $exclude_list);
                foreach ($arrAttachedFiles as $key => $fileName) {
                    if (preg_match("/^t_h_u_m_b_/", $fileName)) {
                        unset($arrAttachedFiles[$key]);
                    }
                    if (preg_match("/^s_m_a_l_l_/", $fileName)) {
                        unset($arrAttachedFiles[$key]);
                    }
                }
            }

            foreach ($arrPhotoBankInfo as $jk => $image) {
                /**
                 * Проверить, есть ли выбранный из БД файл на диске?
                 */
                $isKey = array_search($image['si_filename'], $arrAttachedFiles);
                if ($isKey!==false) {
                    $arrAttachedFiles[$image['si_id']] = $image['si_filename'];
                    unset($arrAttachedFiles[$isKey]);
                    $arrImagesInfo[$image['si_id']]['title'] = $image['si_title'];
                    $arrImagesInfo[$image['si_id']]['desc'] = $image['si_desc'];
                    $arrImagesInfo[$image['si_id']]['tags'] = $image['si_tags'];
                    $arrImagesInfo[$image['si_id']]['author'] = $image['si_reporter'];
                    $arrImagesInfo[$image['si_id']]['oid'] = $image['si_foto_object_id'];
                    $arrImagesInfo[$image['si_id']]['active'] = $image['si_status']=='Y';
                    $arrImagesInfo[$image['si_id']]['main'] = $image['sig_img_id']==$image['si_id'];
                }
            }
        }
	}
}
$objTpl->tpl_loop($arrTplVars['module.name'], "companies", $arrContactRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));
$objTpl->tpl_loop($arrTplVars['module.name'], "list.photos", $arrPhotoBankInfo);

if (empty($arrTplVars['preselectedAdmReg'])) $arrTplVars['preselectedAdmReg'] = 0;
if (empty($arrTplVars['intPhotoBankID'])) $arrTplVars['intPhotoBankID'] = 0;

/**
 * Регионы страны
 */
$strSqlQuery = "SELECT * FROM `site_regions` ORDER BY `sr_id`";
$arrRegions = $objDb->fetchall($strSqlQuery);
if (is_array($arrRegions) && !empty($arrRegions)) {
    foreach ($arrRegions as $k => $arr) {
        $arrObjectRegion = array();
        if($arr['sr_id'] > 0 && $intRecordId > 0) {
            $strSqlQuery = "SELECT * FROM `site_objects_regions_lnk` WHERE `sorl_seo_id` = ".$intRecordId." AND sorl_sr_id = ".$arr['sr_id'];
            $arrObjectRegion = $objDb->fetch($strSqlQuery);
        }
        $arrRegions[$k]['cbxRegionSel'] = $arr['sr_id'] == $arrObjectRegion['sorl_sr_id'] ? ' checked' : '';
//        $arrRegions[$k]['cbxRegionSel'] = ' checked';
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.regions", $arrRegions);




/**
 * Вид строительства
 */
$strSqlQuery = "SELECT * FROM site_estate_vid ORDER BY sev_name";
$arrBuildForms = $objDb->fetchall( $strSqlQuery );

if ( is_array($arrBuildForms) ) {
	foreach ( $arrBuildForms as $key => $value ) {
		$arrBuildForms[$key]['sel'] = $value['sev_id'] == $arrInfo['seo_id_vid'] ? ' selected' : '';
	}
}
$objTpl->tpl_loop($arrTplVars['module.name'], "vid", $arrBuildForms);

/**
 * Этапы строительства
 */
//$strSqlQuery = "SELECT * FROM ".$_db_tables["stEtap"]." ORDER BY see_name";
//$arrEtaps = $objDb->fetchall( $strSqlQuery );
//
//$objTpl->tpl_loop($arrTplVars['module.name'], "etap", $arrEtaps);


/**
 * Путь для возврата
 */
$arrTplVars['urlReturnPath'] = urlencode( $_SERVER['QUERY_STRING'] );

/**
 * Разделы недвижимости
 */
$strSqlQuery = "SELECT * FROM ".$_db_tables["stEstTypes"]." ORDER BY set_name";
$arrEstTypes = $objDb->fetchall( $strSqlQuery );
if ( is_array($arrEstTypes) ) {
	foreach ( $arrEstTypes as $key => $value ) {
		$arrEstTypes[$key]['sel'] = $value['set_id'] == $arrInfo['seo_id_etype'] ? ' selected' : '';
	}
}
$objTpl->tpl_loop($arrTplVars['module.name'], "est.types", $arrEstTypes);

/**
 * Типовое назначение недвижимости
 */
$strSqlQuery = "SELECT * FROM `site_estate_goals` ORDER BY `seg_goal`";
$arrEstGoals = $objDb->fetchall( $strSqlQuery );
if ( is_array($arrEstGoals) ) {
	foreach ( $arrEstGoals as &$value ) {
        $value['sel'] = $value['seg_id'] == $arrInfo['seo_id_goal'] ? ' selected' : '';
	}
}
$objTpl->tpl_loop($arrTplVars['module.name'], "est.goals", $arrEstGoals);

$arrIf['no.photo'] = !$arrIf['photo'];
$arrIf['no.object.info'] = !$arrIf['can.add.companies'];

$arrIf['list.images.empty'] = !$arrIf['fotobank'];

$arrTplVars['filesAttached'] = json_encode($arrAttachedFiles);
$arrTplVars['filesInfo'] = json_encode($arrImagesInfo);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
