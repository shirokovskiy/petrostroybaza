<?php
$arrTplVars['page.name'] = 'helper';

// ***** Обработка ошибок для вывода ****************************************************
${"error".$errSuf} = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
${"error".$errSuf} = $objUtil->echoMessage(${"error".$errSuf}, $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['page.name'], $arrTplVars['page.name'].".html");
$objTpl->tpl_array($arrTplVars['page.name'], $ConfigManager);

$intIdHelpModule = intval($_GET['helpModule']);

if($intIdHelpModule==0)
  $arrIf['close.window'] = true;


$strSqlQuery = "SELECT * FROM ".$_db_tables['cmsHelp']." WHERE fh_id='".$intIdHelpModule."'";

$arrInfoHelpModule = $objDb->fetch( $strSqlQuery );

if(!is_array($arrInfoHelpModule))
  $arrIf['close.window'] = true;

if($arrInfoHelpModule['fh_status']=='B')
  $arrIf['block.beta'] = true;

$arrTplVars['strHelpModuleName'] = $arrInfoHelpModule['fh_name_module'];
$arrTplVars['strBodyHelp'] = $objUtil->parseBrText($arrInfoHelpModule['fh_text']);


$objTpl->tpl_if($arrTplVars['page.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['page.name'], $arrTplVars);