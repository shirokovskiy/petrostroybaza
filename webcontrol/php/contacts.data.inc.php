<?php
$arrTplVars['module.name'] = "contacts.data";
$arrTplVars['module.title'] = "Контакты для связи";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

$filename = PRJ_FILES . 'contacts.data.txt';

if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
    if (!empty($_POST['strContacts'])) {
        file_put_contents($filename, trim($_POST['strContacts']));
        header('location:'.$arrTplVars['module.name']."?errMess=msgOk");
        exit;
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$arrTplVars['strContacts'] = file_get_contents($filename);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

