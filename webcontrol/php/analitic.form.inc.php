<?php
$arrTplVars['module.name'] = "analitic.form";
$arrTplVars['module.parent'] = "analitic";
$arrTplVars['module.title'] = "Добавить статью";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление фото
 */
if ( intval($_GET['paf_id']) > 0 && $_GET['act'] == 'del' ) {
  $intRecordId = intval($_GET['paf_id']);
  $strSqlQuery = "SELECT paf_filename FROM ".$_db_tables['stAnaliticFiles']." WHERE paf_id='$intRecordId'";
  $paf_filename = $objDb->fetch( $strSqlQuery, 0 );

  if ( !empty($paf_filename) ) {
    $strSqlQuery = "DELETE FROM ".$_db_tables['stAnaliticFiles']." WHERE paf_id = '$intRecordId'";
    if ( !$objDb->query($strSqlQuery) ) {
      #mysql error
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      $delFileName = DROOT."storage/files/analitics/$paf_filename";
      if ( file_exists($delFileName)) {
        unlink($delFileName);
      }

      $intId = intval($_GET['id']);
      header("Location: {$arrTplVars['module.name']}?id=$intId&stPage={$arrTplVars['stPage']}");
      exit();
    }
  }
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEditArticle']) && $_POST['frmEditArticle'] == 'true' ) {
  $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
  $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

  $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
  $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

  if (empty($arrSqlData['strTitle']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
  }

  $arrSqlData['strAnonsBody'] = addslashes(trim($_POST['strAnonsBody']));
  $arrTplVars['strAnonsBody'] = htmlspecialchars(stripslashes(trim($_POST['strAnonsBody'])));

  if (empty($arrSqlData['strAnonsBody']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgEmptyAnonsbody';
  }

  $arrSqlData['strTextBody'] = addslashes(trim($_POST['strTextBody']));
  $arrTplVars['strTextBody'] = htmlspecialchars(stripslashes(trim($_POST['strTextBody'])));

  if (empty($arrSqlData['strTextBody']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTextbody';
  }

  $arrSqlData['intDay'] = intval(trim($_POST['intDay']));
  $arrTplVars['intDay'] = ( $arrSqlData['intDay'] > 0 ? $arrSqlData['intDay'] : '');

  $arrSqlData['intMonth'] = intval(trim($_POST['intMonth']));
  $arrTplVars['intMonth'] = ( $arrSqlData['intMonth'] > 0 ? $arrSqlData['intMonth'] : '');

  $arrSqlData['intYear'] = intval(trim($_POST['intYear']));
  $arrTplVars['intYear'] = ( $arrSqlData['intYear'] > 0 ? $arrSqlData['intYear'] : '');

  $dateNewsPubl = $arrSqlData['intYear'].'-'.str_pad($arrSqlData['intMonth'], 2, '0', STR_PAD_LEFT).'-'.str_pad($arrSqlData['intDay'], 2, '0', STR_PAD_LEFT);

  if (!$objUtil->dateValid($dateNewsPubl)) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgFailDate';
  }

  $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
  $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

  $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
  $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

  if ($GLOBALS['manStatusError']!=1) {
    $strSqlFields = ""
      . " pa_title = '{$arrSqlData['strTitle']}'"
      . ", pa_anons = '{$arrSqlData['strAnonsBody']}'"
      . ", pa_article = '{$arrSqlData['strTextBody']}'"
      . ", pa_date_publ = '$dateNewsPubl'"
      . ", pa_status = '{$arrSqlData['cbxStatus']}'"
      ;

    if ( empty($intRecordId) ) {
      $strSqlQuery = "INSERT INTO ".$_db_tables['stAnalitic']." SET".$strSqlFields
        . ", pa_date_add = NOW()";
    } else {
      $strSqlQuery = "UPDATE ".$_db_tables['stAnalitic']." SET".$strSqlFields
        . ", pa_date_change = NOW()"
        . " WHERE pa_id = '$intRecordId'";
    }

    if ( !$objDb->query( $strSqlQuery ) ) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      if ( empty($intRecordId) ) {
        $intRecordId = $objDb->insert_id();
      }
    }

    /**
     * Загрузка фото
     */
    $maxFILE_SIZE = 5000000; // ~5Mb.
    if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < $maxFILE_SIZE ) {
      $strFileName = strtolower( substr($objUtil->translitKyrToLat($objUtil->translitBadChars($_POST['strFileName'])), 0, 16) );
      // дополнительная частная обработка
      $strFileName = str_replace("'", "", $strFileName);
      $strFileName = str_replace(",", "", $strFileName);
      $strFileName = str_replace(".", "", $strFileName);
      $strFileName = str_replace("№", "N_", $strFileName);

      /////////////////////////////////////////////////////////////////////////////////////
      $fileExt = $objFile->getFileExtension($_FILES['flPhoto']['name']);

      if ( $fileExt != 'doc' && $fileExt != 'pdf' && $fileExt != 'txt' && $fileExt != 'xls'
            && $fileExt != 'gif' && $fileExt != 'jpg' && $fileExt != 'rtf' && $fileExt != 'rar' && $fileExt != 'zip'  ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
      } else {
        $fileName = "$strFileName.$fileExt";

        $dirStorage = DROOT."storage";
        if ( !file_exists($dirStorage) ) {
          if (!mkdir($dirStorage, 0775)) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
          }
        }

        $dirImages = $dirStorage."/files";
        if ( !file_exists($dirImages) ) {
          if (!mkdir($dirImages, 0775)) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
          }
        }

        $dirImages = $dirImages."/analitics";
        if ( !file_exists($dirImages) ) {
          if (!mkdir($dirImages, 0775)) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
          }
        }

        if ($GLOBALS['manStatusError']!=1) {
          @move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileName );

          $strSqlQuery = "INSERT INTO ".$_db_tables['stAnaliticFiles']." SET"
            . " paf_filename='".mysql_real_escape_string($fileName)."'"
            . ", paf_rel_id = '$intRecordId'"
            . ", paf_title = '".mysql_real_escape_string($_POST['strFileName'])."'"
            . ", paf_status = 'Y'"
            . ", paf_date = NOW()"
            ;
          if ( !$objDb->query($strSqlQuery) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
          }
        }
      }
      /////////////////////////////////////////////////////////////////////////////////////
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
      if ( $arrSqlData['cbxReturn'] == 'Y' ) {
        header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
      } else {
        header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
      }
      exit;
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAnalitic']." WHERE pa_id ='$intRecordId'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['pa_title']));
    $arrTplVars['strAnonsBody'] = htmlspecialchars(stripslashes($arrInfo['pa_anons']));
    $arrTplVars['strTextBody'] = htmlspecialchars(stripslashes($arrInfo['pa_article']));

    $arrTplVars['intDay'] = intval(date('d', strtotime($arrInfo['pa_date_publ'])));
    $arrTplVars['intMonth'] = intval(date('m', strtotime($arrInfo['pa_date_publ'])));
    $arrTplVars['intYear'] = intval(date('Y', strtotime($arrInfo['pa_date_publ'])));

    $arrTplVars['cbxStatus'] = $arrInfo['pa_status'] == 'Y' ? ' checked' : '';

    $strSqlQuery = "SELECT * FROM ".$_db_tables["stAnaliticFiles"]." WHERE paf_rel_id = '$intRecordId' ORDER BY paf_id DESC";
    $arrAnaliticFiles = $objDb->fetchall( $strSqlQuery );

    echo "<!-- Debug output\n";
    print_r($arrAnaliticFiles);
    echo "-->";

    $arrIf['exist.files'] = is_array($arrAnaliticFiles) && !empty($arrAnaliticFiles);
  }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.files", $arrAnaliticFiles);

$arrIf['no.files'] = !$arrIf['exist.files'];

/**
 * Установка дат
 */
// Дни
for ($i=1;$i<=31;$i++) {
  $arrDays[$i]['intDay'] = $i;
  $arrDays[$i]['strDay'] = str_pad($i, 2, "0", STR_PAD_LEFT);
  $arrDays[$i]['selDay'] = ( ($i == $arrTplVars['intDay']) || (!isset($arrTplVars['intDay']) && $i == intval(date('d'))) ? " selected" : "");
}
$objTpl->tpl_loop($arrTplVars['module.name'], "days", $arrDays);


// Месяцы
for ($i=1;$i<=12;$i++) {
  $arrMonths[$i]['intMonth'] = $i;
  $arrMonths[$i]['strMonth'] = $objUtil->arrAtMonth[$i];
  $arrMonths[$i]['selMonth'] = ( ($i == $arrTplVars['intMonth']) || (!isset($arrTplVars['intMonth']) && $i == intval(date('m'))) ? " selected" : "");
}
$objTpl->tpl_loop($arrTplVars['module.name'], "months", $arrMonths);

// Годы
for ($i=2005;$i<=(intval(date("Y"))+3);$i++) {
  $arrYears[$i]['intYear'] = $i;
  $arrYears[$i]['strYear'] = str_pad($i, 2, "0", STR_PAD_LEFT);
  $arrYears[$i]['selYear'] = ( ($i == $arrTplVars['intYear']) || (!isset($arrTplVars['intYear']) && $i == intval(date('Y'))) ? " selected" : "");

}
$objTpl->tpl_loop($arrTplVars['module.name'], "years", $arrYears);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
