<?php
$arrTplVars['module.name'] = "companies";
$arrTplVars['module.child'] = "company.form";
$arrTplVars['module.obj'] = "estate.object.form";
$arrTplVars['module.title'] = "Компании";

$stPage = $arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $strSqlQuery = "DELETE FROM ".$_db_tables['stContacts']." WHERE sec_id='$intRecordId' LIMIT 1";
    if ( !$objDb->query($strSqlQuery) ) {
        $GLOBALS['manStatusError'] = 1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        header('Location:'.$arrTplVars['module.name'].'?errMess=msgDel');
        exit();
    }
}

if ( intval($_GET['id_cmp']) > 0 && intval($_GET['id_obj']) > 0 && !empty($_GET['type']) && ($_GET['act'] == 'unset' || $_GET['act'] == 'exclude') ) {
    $strSqlQuery = "DELETE FROM ".$_dbt['stObjContacts']." WHERE socl_id_contact = '{$_GET['id_cmp']}' AND socl_id_object = '{$_GET['id_obj']}' AND socl_relation = '{$_GET['type']}'";
    if ( !$objDb->query( $strSqlQuery ) ) {
        # sql query error
        $GLOBALS['manStatusError'] = 1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        if ( $_GET['act'] == 'unset' ) {
            header('Location: '.$arrTplVars['module.obj'].'?'.urldecode($_GET['returnPath']).'#companies');
        }
        if ( $_GET['act'] == 'exclude' ) {
            header('Location:'.$arrTplVars['module.child'].'?'.urldecode($_GET['returnPath']));
        }
        exit();
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_POST['frmSearchCompanies']) ) {
    $_SESSION['frmSearchCompanies'] = $_POST;
}

if ( !isset($_POST['frmSearchCompanies']) && intval($_GET['stPage']) <= 0 ) {
    unset($_SESSION['frmSearchCompanies']);
}

if ( isset($_SESSION['frmSearchCompanies']) ) {
    if ( intval( $_SESSION['frmSearchCompanies']['intCompanyID2Find'] )>0 ) {
        $intCompanyID2Find = $arrTplVars['intCompanyID2Find'] = $_SESSION['frmSearchCompanies']['intCompanyID2Find'];
        $arrSqlWhere[] = " sec_id = '$intCompanyID2Find' ";
    }
    if ( !empty( $_SESSION['frmSearchCompanies']['strCompanyName2Find'] ) ) {
        $strCompanyName2Find = $arrTplVars['strCompanyName2Find'] = mysql_real_escape_string( $_SESSION['frmSearchCompanies']['strCompanyName2Find'] );
        $arrSqlWhere[] = " sec_company LIKE '%$strCompanyName2Find%' ";
    }
    if ( !empty( $_SESSION['frmSearchCompanies']['strFIO2Find'] ) ) {
        $strFIO2Find = $arrTplVars['strFIO2Find'] = mysql_real_escape_string( $_SESSION['frmSearchCompanies']['strFIO2Find'] );
        $arrSqlWhere[] = " sec_fio LIKE '%$strFIO2Find%' ";
    }
}

if ( is_array($arrSqlWhere) && !empty($arrSqlWhere) ) {
    $strSqlWhere = implode(' AND ', $arrSqlWhere);
}

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stContacts'];
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stContacts'].( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' );
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.5.tpl";
$objPagination->iQtyLinksPerPage = 15;
$objPagination->iQtyRecsPerPage = 30;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stContacts']." ".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )
    ." ORDER BY sec_company, sec_fio, sec_id DESC ".$objPagination->strSqlLimit;
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );

// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrSelectedRecords) ) {
    foreach ( $arrSelectedRecords as $key => $value ) {
        $arrSelectedRecords[$key]['cbxStatus'] = $value['sec_status'] == 'Y' ? ' checked' : '';
    }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "companies", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));
$arrIf['no.companies'] = empty($arrSelectedRecords);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
