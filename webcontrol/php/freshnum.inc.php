<?php
$arrTplVars['module.name'] = "freshnum";
$arrTplVars['module.title'] = "Свежий номер";

$stPage = $arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if (isset($_GET['download'])) {
    // Грузим нужный файл
    $intRecordId = intval($_GET['download']);
    $strSqlQuery = "SELECT sr_filename FROM site_releases WHERE sr_id= ".$intRecordId;
    $strFileNameInfo = $objDb->fetch( $strSqlQuery, 0 );

    if (!empty($strFileNameInfo)) {
        $SourceFile = PRJ_FILES.'releases/'.$strFileNameInfo;

        if (file_exists($SourceFile)) {
            $sizeResFile = filesize($SourceFile);

            if (!is_object($objFile)) {
                $objFile = new Files();
            }

            $fileExt = $objFile->getFileExtension($strFileNameInfo);

            if ( ! function_exists ( 'mime_content_type' ) ) {
                function mime_content_type ( $f ) {
                    return trim ( exec ('file -bi ' . escapeshellarg ( $f ) ) ) ;
                }
            }

            $ft = mime_content_type($SourceFile);

            Header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT\r\n" );
            Header( "Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT\r\n" );
            Header( "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
            Header( "Cache-Control: post-check=0, pre-check=0, false\r\n" );
            Header( "Pragma: no-cache\r\n" );
            Header( "HTTP/1.1 200 OK\r\n" );

            Header( "Content-Disposition: attachment; filename=release_".$strFileNameInfo."\r\n" );
            Header( "Accept-Ranges: bytes\r\n" );
            Header( "Content-Type: application/force-download" );
            Header( "Content-Length: $sizeResFile\r\n\r\n" );

            readfile( $SourceFile );
            die('Закройте это окно после скачивания файла!');
        } else {
            header ("HTTP/1.0 404 Not Found");
            if (!$_SERVER['_IS_DEVELOPER_MODE']) {
                $SourceFile = '';
            }
            die("Ошибка! Файл $SourceFile не существует.");
        }
    } else {
        die("Ошибка! Файл не найден.");
    }
}

if ( intval($_GET['id_del']) > 0 ) {
    $intRecordId = intval($_GET['id_del']);
    $strSqlQuery = "SELECT sr_filename FROM site_releases WHERE sr_id='$intRecordId'";
    $strFilename = $objDb->fetch($strSqlQuery, "sr_filename");

    $strSqlQuery = "DELETE FROM site_releases WHERE sr_id='$intRecordId' LIMIT 1";
    if ( !$objDb->query($strSqlQuery) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
    } else {
        if (file_exists(PRJ_FILES.'releases/'.$strFilename)) {
            if(!unlink(PRJ_FILES.'releases/'.$strFilename)) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
            }
        }
    }

    if (!$GLOBALS['manStatusError']) {
        header('Location: '.$arrTplVars['module.name'].'?stPage='.$stPage);
        exit;
    }
}

if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strFileTitle'] = addslashes(trim($_POST['strFileTitle']));
    $arrTplVars['strFileTitle'] = htmlspecialchars(stripslashes(trim($_POST['strFileTitle'])));

    if (empty($arrSqlData['strFileTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyFileDesc';
    }

    if ( ($_FILES['flPhoto']['error'] != 0 || $_FILES['flPhoto']['size'] == 0) && empty($intRecordId) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgNoFile';
    }

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ( $GLOBALS['manStatusError'] != 1 ) {
        $strSqlFields = ""
            . " sr_title = '{$arrSqlData['strFileTitle']}'"
            . ", sr_status = '{$arrSqlData['cbxStatus']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_releases SET".$strSqlFields;
        } else {
            $strSqlQuery = "UPDATE site_releases SET".$strSqlFields
                . ", sr_date_change = NOW() WHERE sr_id = '$intRecordId'";
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            $intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;

            /**
             * Release ID is known anyway
             * So, lets insert access if defined.
             * But in the beginning delete old.
             */
            if ($intRecordId > 0) {
                $strSqlQuery = "DELETE FROM `site_releases_access_lnk` WHERE srlal_sr_id = ".$intRecordId;
                if (!$objDb->query($strSqlQuery)) {
                    # sql query error
                }

                if (isset($_POST['cbxAccess']) && is_array($_POST['cbxAccess'])) {
                    $arrAccess = $_POST['cbxAccess'];
                    foreach ($arrAccess as $k => $accessId) {
                        $strSqlQuery = "INSERT INTO `site_releases_access_lnk` SET"
                            . " srlal_sr_id = ".$intRecordId
                            . ", srlal_srat_id = ".$accessId;
                        if (!$objDb->query($strSqlQuery)) {
                            # sql error
                        }
                    }
                }
            }
        }

        if ($GLOBALS['manStatusError']!=1) {
            if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] < 5000000 ) {
                $strFileName = strtolower( substr($objUtil->translitKyrToLat($objUtil->translitBadChars($_POST['strFileTitle'])), 0, 32) );
                // дополнительная частная обработка
                $strFileName = str_replace("'", "", $strFileName);
                $strFileName = str_replace(".", "", $strFileName);
                $strFileName = str_replace("№", "N_", $strFileName);

                /////////////////////////////////////////////////////////////////////////////////////
                $fileExt = $objFile->getFileExtension($_FILES['flPhoto']['name']);

                if ( $fileExt != 'doc' && $fileExt != 'pdf' && $fileExt != 'txt' ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                }

                $fileName = "$strFileName.$fileExt";

                $dirFiles = DROOT."storage";
                if ( !file_exists($dirFiles) ) {
                    mkdir($dirFiles, 0775);
                }

                $dirFiles = $dirFiles."/files";
                if ( !file_exists($dirFiles) ) {
                    mkdir($dirFiles, 0775);
                }

                $dirFiles = $dirFiles."/releases";
                if ( !file_exists($dirFiles) ) {
                    mkdir($dirFiles, 0775);
                }
                move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirFiles."/".$fileName );
                /////////////////////////////////////////////////////////////////////////////////////

                $strSqlQuery = "UPDATE site_releases SET sr_filename='".mysql_real_escape_string($fileName)."'"
                    . ", sr_date_change = NOW() WHERE sr_id='$intRecordId'";
                if ( !$objDb->query($strSqlQuery) ) {
                    #mysql error
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                }
            } elseif ($_FILES['flPhoto']['size'] > 1500000) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgTooBigFile';
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $_POST['cbxReturn']=='on' ) {
                header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId&stPage=$stPage");
            } else {
                header('location:'.$arrTplVars['module.name']."?errMess=msgOk&stPage=$stPage");
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( $_GET['action'] == 'add' || (isset($_GET['id']) && intval($_GET['id']) > 0) ) {
    $arrIf['editrecord'] = true;
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_releases WHERE sr_id = ".$intRecordId;
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strFileTitle'] = htmlspecialchars(stripslashes($arrInfo['sr_title']));
        $arrTplVars['strFileName'] = htmlspecialchars(stripslashes($arrInfo['sr_filename']));

        if (file_exists(PRJ_FILES.'releases/'.$arrInfo['sr_filename'])) {
            $arrIf['is.file'] = true;
        }

        $arrTplVars['cbxStatus'] = $arrInfo['sr_status'] == 'Y' ? ' checked' : '';
    }

    // Доступы по регионам
    $strSqlQuery = "SELECT * FROM `site_regions_access_type` LEFT JOIN `site_releases_access_lnk` ON (srlal_sr_id = $intRecordId AND srlal_srat_id = srat_id) ORDER BY srat_id";
    $arrRegAccess = $objDb->fetchall($strSqlQuery);
    if (is_array($arrRegAccess) && !empty($arrRegAccess)) {
        foreach ($arrRegAccess as $k => $arr) {
            $arrRegAccess[$k]['cbxAccess'] = ($arr['srlal_sr_id'] > 0 ? ' checked' : '');
        }

    }
    $objTpl->tpl_loop($arrTplVars['module.name'], "list.regions.access", $arrRegAccess);
} else {
    // Всего записей в базе
    $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stReleases'];
    $arrTplVars['intQuantAllRecords'] = $arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.1.tpl";
    $objPagination->iQtyRecsPerPage = 20;
    $objPagination->iQtyLinksPerPage = 7;
    $objPagination->strColorLinkStyle = "link-b";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strNameImageBack = "arrow_one_left.gif";
    $objPagination->strNameImageForward = "arrow_one_right.gif";
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка
    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM site_releases ORDER BY sr_id DESC ".$objPagination->strSqlLimit;
    $arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
    // Присвоение значения пэйджинга для списка
    $arrTplVars['blockPaginator'] = $objPagination->paShow();

    $arrIf['records'] = $arrTplVars['intQuantShowRecOnPage']>0;

    if ( is_array($arrSelectedRecords) ) {
        foreach ( $arrSelectedRecords as $key => $value ) {
            $arrSelectedRecords[$key]['cbxStatus'] = $value['sr_status'] == 'Y' ? ' checked' : '';
        }
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

$arrIf['showlist'] = !$arrIf['editrecord'];
$arrIf['norecords'] = !$arrIf['records'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
