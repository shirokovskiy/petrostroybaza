<?php
$arrTplVars['module.name'] = "site.user.form";
$arrTplVars['module.parent'] = "site.users";
$arrTplVars['module.title'] = "Добавить пользователя";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset($_POST['frmEditUser']) && $_POST['frmEditUser'] == 'true' ) {

    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strLName'] = addslashes(trim($_POST['strLName']));
    $arrTplVars['strLName'] = htmlspecialchars(stripslashes(trim($_POST['strLName'])));

    if (empty($arrSqlData['strLName']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyLName';
    }

    $arrSqlData['strFName'] = addslashes(trim($_POST['strFName']));
    $arrTplVars['strFName'] = htmlspecialchars(stripslashes(trim($_POST['strFName'])));

    if (empty($arrSqlData['strFName']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyFName';
    }

    $arrSqlData['strMName'] = addslashes(trim($_POST['strMName']));
    $arrTplVars['strMName'] = htmlspecialchars(stripslashes(trim($_POST['strMName'])));

    $arrSqlData['strCompanyName'] = addslashes(trim($_POST['strCompanyName']));
    $arrTplVars['strCompanyName'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyName'])));

    if (empty($arrSqlData['strCompanyName']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyFName';
    }

    $arrSqlData['strEmail'] = addslashes(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    if (empty($arrSqlData['strEmail']) || !$objUtil->checkEmail($arrSqlData['strEmail'])) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyEmail';
    }

    if (empty($intRecordId)) {
        $arrSqlData['strLogin'] = addslashes(trim($_POST['strLogin']));
        $arrTplVars['strLogin'] = htmlspecialchars(stripslashes(trim($_POST['strLogin'])));

        if (empty($arrSqlData['strLogin']) || strlen($arrSqlData['strLogin'])<4 ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgEmptyLogin';
        }

        $arrSqlData['strPassw'] = addslashes(trim($_POST['strPassw']));
        $arrTplVars['strPassw'] = htmlspecialchars(stripslashes(trim($_POST['strPassw'])));

        if (empty($arrSqlData['strPassw']) || strlen($arrSqlData['strPassw'])<6 ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgEmptyPassw';
        }
    }

    $arrSqlData['strPhone'] = addslashes(trim($_POST['strPhone']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

    $arrSqlData['intDay'] = intval(trim($_POST['intDay']));
    $arrSqlData['strDay'] = str_pad($arrSqlData['intDay'], 2, '0', STR_PAD_LEFT);
    $arrTplVars['intDay'] = ( $arrSqlData['intDay'] > 0 ? $arrSqlData['intDay'] : '');

    $arrSqlData['intMonth'] = intval(trim($_POST['intMonth']));
    $arrSqlData['strMonth'] = str_pad($arrSqlData['intMonth'], 2, '0', STR_PAD_LEFT);
    $arrTplVars['intMonth'] = ( $arrSqlData['intMonth'] > 0 ? $arrSqlData['intMonth'] : '');

    $arrSqlData['intYear'] = intval(trim($_POST['intYear']));
    $arrTplVars['intYear'] = ( $arrSqlData['intYear'] > 0 ? $arrSqlData['intYear'] : '');

    $arrSqlData['strDate'] = $arrSqlData['intYear'].'-'.$arrSqlData['strMonth'].'-'.$arrSqlData['strDay'];

    if ( !empty($intRecordId) && !$objUtil->dateValid($arrSqlData['strDate']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgDateInvalid';
        //$arrTplVars['errMsg'] .= "<li>Дата некорректна. Укажите День / Месяц / Год";
    }

    $arrSqlData['strSumm'] = addslashes(trim($_POST['strSumm']));
    $arrTplVars['strSumm'] = htmlspecialchars(trim($_POST['strSumm']));

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            . " su_lname = '{$arrSqlData['strLName']}'"
            . ", su_fname = '{$arrSqlData['strFName']}'"
            . ", su_mname = '{$arrSqlData['strMName']}'"
            . ", su_company = '{$arrSqlData['strCompanyName']}'"
            . ", su_email = '{$arrSqlData['strEmail']}'"
            . ", su_phone = '{$arrSqlData['strPhone']}'"
            . ", su_status = '{$arrSqlData['cbxStatus']}'"
            . ", su_active = 'Y'"
            . ( empty($intRecordId) ? ", su_login = '{$arrSqlData['strLogin']}'" : '' )
            . ( empty($intRecordId) ? ", su_passw = MD5('{$arrSqlData['strPassw']}')" : '' )
            . (!empty($intRecordId) ? ", su_summ = '{$arrSqlData['strSumm']}'" : '' )
            . (!empty($intRecordId) ? ", su_date_payment = NOW()" : '' )
            . (!empty($intRecordId) ? ", su_date_untill = '{$arrSqlData['strDate']}'" : '' )
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_users SET $strSqlFields, su_date_add = NOW()";
        } else {
            $strSqlQuery = "UPDATE site_users SET $strSqlFields, su_date_change = NOW() WHERE su_id = ".$intRecordId;
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }
        }

        /**
         * User ID is known anyway
         * So, lets insert access if defined.
         * But in the beginning delete old.
         */
        if ($intRecordId > 0) {
            $strSqlQuery = "DELETE FROM `site_users_access_lnk` WHERE sual_su_id = ".$intRecordId;
            if (!$objDb->query($strSqlQuery)) {
                # sql query error
            }

            if (isset($_POST['cbxAccess']) && is_array($_POST['cbxAccess'])) {
                $arrAccess = $_POST['cbxAccess'];
                foreach ($arrAccess as $accessId) {
                    $strSqlQuery = "INSERT INTO `site_users_access_lnk` SET"
                        . " sual_su_id = ".$intRecordId
                        . ", sual_srat_id = ".$accessId
                        . ", sual_type = 'estate'";
                    if (!$objDb->query($strSqlQuery)) {
                        # sql error
                    }
                }
            }

            if (isset($_POST['cbxPaperAccess']) && is_array($_POST['cbxPaperAccess'])) {
                $arrAccess = $_POST['cbxPaperAccess'];
                foreach ($arrAccess as $accessId) {
                    $strSqlQuery = "INSERT INTO `site_users_access_lnk` SET"
                        . " sual_su_id = ".$intRecordId
                        . ", sual_srat_id = ".$accessId
                        . ", sual_type = 'magazine'";
                    if (!$objDb->query($strSqlQuery)) {
                        # sql error
                    }
                }
            }
        }


        if ( $GLOBALS['manStatusError']!=1 ) {
            /**
             * Вот здесь добавить отправку письма пользователю с логином и паролем!
             */


            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_users WHERE su_id =".$intRecordId;
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strLName'] = htmlspecialchars(stripslashes($arrInfo['su_lname']));
        $arrTplVars['strFName'] = htmlspecialchars(stripslashes($arrInfo['su_fname']));
        $arrTplVars['strMName'] = htmlspecialchars(stripslashes($arrInfo['su_mname']));
        $arrTplVars['strCompanyName'] = htmlspecialchars(stripslashes($arrInfo['su_company']));
        $arrTplVars['strLogin'] = htmlspecialchars(stripslashes($arrInfo['su_login']));
        $arrTplVars['strEmail'] = htmlspecialchars(stripslashes($arrInfo['su_email']));
        $arrTplVars['strPhone'] = htmlspecialchars(stripslashes($arrInfo['su_phone']));
        $arrTplVars['strDateLastVisit'] = !empty($arrInfo['su_last_visit']) ? $objUtil->workDate(6, $arrInfo['su_last_visit']) : ' ';
        $arrTplVars['strIPLastVisit'] = !empty($arrInfo['su_ip']) ? long2ip($arrInfo['su_ip']) : '';
        $arrTplVars['strSumm'] = !empty($arrInfo['su_summ']) ? $arrInfo['su_summ'] : ' ';

        $arrTplVars['cbxStatus'] = $arrInfo['su_status'] == 'Y' ? ' checked' : '';
        if ($objUtil->dateValid($arrInfo['su_date_untill'])) {
            $arrTplVars['intDay'] = intval(date("d", strtotime($arrInfo['su_date_untill'])));
            $arrTplVars['intMonth'] = intval(date("m", strtotime($arrInfo['su_date_untill'])));
            $arrTplVars['intYear'] = intval(date("Y", strtotime($arrInfo['su_date_untill'])));
        }

        $User = new User($intRecordId);
        $User->createAutoLoginSign();
    }
    $arrIf['show.login.data'] = true;

    $arrTplVars['autologin'] = md5( md5( $arrTplVars['strLogin'].$intRecordId ) . $arrTplVars['strEmail'] );

    // Доступы по регионам
    $strSqlQuery = "SELECT * FROM `site_regions_access_type` LEFT JOIN `site_users_access_lnk` ON (sual_su_id = $intRecordId AND sual_srat_id = srat_id AND sual_type = 'estate') ORDER BY srat_id";
    $arrRegAccess = $objDb->fetchall($strSqlQuery);
    if (is_array($arrRegAccess) && !empty($arrRegAccess)) {
        foreach ($arrRegAccess as &$arr) {
            $arr['cbxAccess'] = ($arr['sual_su_id'] > 0 ? ' checked' : '');
        }
    }
    $objTpl->tpl_loop($arrTplVars['module.name'], "list.regions.access.estate", $arrRegAccess);

    // Доступы по регионам
    $strSqlQuery = "SELECT * FROM `site_regions_access_type` LEFT JOIN `site_users_access_lnk` ON (sual_su_id = $intRecordId AND sual_srat_id = srat_id AND sual_type = 'magazine') ORDER BY srat_id";
    $arrRegAccess = $objDb->fetchall($strSqlQuery);
    if (is_array($arrRegAccess) && !empty($arrRegAccess)) {
        foreach ($arrRegAccess as &$arr) {
            $arr['cbxPaperAccess'] = ($arr['sual_su_id'] > 0 ? ' checked' : '');
        }
    }
    $objTpl->tpl_loop($arrTplVars['module.name'], "list.regions.access.paper", $arrRegAccess);

    // Дни
    for ($i=1;$i<=31;$i++) {
        $arrDays[$i]['intDay'] = $i;
        $arrDays[$i]['strDay'] = str_pad($i, 2, "0", STR_PAD_LEFT);
        if (intval($arrTplVars['intDay'])>0) {
            $arrDays[$i]['selDay'] = ( $i == $arrTplVars['intDay'] ? " selected" : "");
        } else {
            $arrDays[$i]['selDay'] = ( $i == intval(date('d')) ? " selected" : "");
        }

    }
    $objTpl->tpl_loop($arrTplVars['module.name'], "days", $arrDays);


    // Месяцы
    for ($i=1;$i<=12;$i++) {
        $arrMonths[$i]['intMonth'] = $i;
        $arrMonths[$i]['strMonth'] = $objUtil->arrAtMonth[$i];
        if (intval($arrTplVars['intMonth'])>0) {
            $arrMonths[$i]['selMonth'] = ( $i == $arrTplVars['intMonth'] ? " selected" : "");
        } else {
            $arrMonths[$i]['selMonth'] = ( $i == intval(date('m')) ? " selected" : "");
        }
    }
    $objTpl->tpl_loop($arrTplVars['module.name'], "months", $arrMonths);

    // Годы
    for ($i=2006;$i<=(intval(date("Y"))+3);$i++) {
        $arrYears[$i]['intYear'] = $i;
        $arrYears[$i]['strYear'] = str_pad($i, 2, "0", STR_PAD_LEFT);
        if (intval($arrTplVars['intMonth'])>0) {
            $arrYears[$i]['selYear'] = ( $i == $arrTplVars['intYear'] ? " selected" : "");
        } else {
            $arrYears[$i]['selYear'] = ( $i == intval(date('Y')) ? " selected" : "");
        }
    }
    $objTpl->tpl_loop($arrTplVars['module.name'], "years", $arrYears);


} else {
    $arrIf['set.login.data'] = true;
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
