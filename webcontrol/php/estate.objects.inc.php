<?php
$arrTplVars['module.name'] = "estate.objects";
$arrTplVars['module.child'] = "estate.object.form";
$arrTplVars['module.info'] = "estate.object.extra.info";
$arrTplVars['module.comments'] = "object.comments";
$arrTplVars['module.title'] = "Объекты недвижимости";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление объекта
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $strSqlQuery = "DELETE FROM site_estate_objects WHERE seo_id=$intRecordId LIMIT 1";
    if ( !$objDb->query($strSqlQuery) ) {
        $GLOBALS['manStatusError'] = 1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        $strSqlQuery = "SELECT si_id FROM ".$_db_tables["stImages"]." WHERE si_id_rel = $intRecordId AND si_type = 'estate'";
        $arrSelectedRecordsForDelete = $objDb->fetchall( $strSqlQuery );

        if ( is_array( $arrSelectedRecordsForDelete ) ) {
            $strSqlQuery = "DELETE FROM ".$_db_tables['stImages']." WHERE si_id_rel = $intRecordId AND si_type = 'estate'";
            if ( !$objDb->query( $strSqlQuery ) ) {
                $GLOBALS['manStatusError'] = 1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            } else {
                $dirImages = DROOT."storage/images/objects/";
                foreach ( $arrSelectedRecordsForDelete as $recordDel ) {
                    $intRecordId = intval( $recordDel['si_id'] );
                    if ( file_exists($dirImages."obj.$intRecordId.jpg")) {
                        unlink($dirImages."obj.$intRecordId.jpg");
                        unlink($dirImages."obj.$intRecordId.b.jpg");
                    }
                }
            }
        }

        /**
         * Удалить связки с Компаниями
         */
        $strSqlQuery = "DELETE FROM `site_objects_contacts_lnk` WHERE socl_id_object = ".$intRecordId;
        if (!$objDb->query($strSqlQuery)) {
            # sql query error
            $GLOBALS['manStatusError'] = 1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        }
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_POST['frmSearchObjects']) ) {
    $_SESSION['frmSearchObjects'] = $_POST;
}

if ( !isset($_POST['frmSearchObjects']) && intval($_GET['stPage']) <= 0 ) {
    unset($_SESSION['frmSearchObjects']);
}

if ( isset($_SESSION['frmSearchObjects']) ) {
    if ( intval( $_SESSION['frmSearchObjects']['intObjectID2Find'] )>0 ) {
        $intFindObjID = $arrTplVars['intObjectID2Find'] = $_SESSION['frmSearchObjects']['intObjectID2Find'];
        $arrSqlWhere[] = " seo_id = $intFindObjID ";
    }
    if ( intval( $_SESSION['frmSearchObjects']['intEstTypeId'] )>0 ) {
        $intFindEstTypeID = $arrTplVars['intEstTypeId'] = $_SESSION['frmSearchObjects']['intEstTypeId'];
        $arrSqlWhere[] = " seo_id_etype = $intFindEstTypeID ";
    }
    if ( !empty( $_SESSION['frmSearchObjects']['strObjectName2Find'] ) ) {
        $strFindObjName = $arrTplVars['strObjectName2Find'] = mysql_real_escape_string( $_SESSION['frmSearchObjects']['strObjectName2Find'] );
        $arrSqlWhere[] = " seo_name LIKE '%$strFindObjName%' ";
    }
    if ( !empty( $_SESSION['frmSearchObjects']['strObjectAddress2Find'] ) ) {
        $strFindObjAddress = $arrTplVars['strObjectAddress2Find'] = mysql_real_escape_string( $_SESSION['frmSearchObjects']['strObjectAddress2Find'] );
        $arrSqlWhere[] = " seo_address LIKE '%$strFindObjAddress%' ";
    }
    if ( $_SESSION['frmSearchObjects']['strObjectComments2Find'] == 'Y' ) {
        $arrTplVars['strObjectComments2Find'] = ' checked';
        $arrSqlLeftJoin[] = "LEFT JOIN `site_users_comments` ON (seo_id = suc_seo_id)";
        $arrSqlWhere[] = " suc_seo_id > 0 ";
    }

    if ( $_SESSION['frmSearchObjects']['strPublicObjects2Find'] == 'Y' ) {
        $arrTplVars['strPublicObjects2Find'] = ' checked';
        $arrSqlWhere[] = " seo_pa = 'Y' ";
    }
}

if ( is_array($arrSqlLeftJoin) && !empty($arrSqlLeftJoin) ) {
    $strSqlLeftJoin = implode(' ', $arrSqlLeftJoin);
}
if ( is_array($arrSqlWhere) && !empty($arrSqlWhere) ) {
    $strSqlWhere = implode(' AND ', $arrSqlWhere);
}


// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_estate_objects";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM site_estate_objects"
    .( !empty( $strSqlLeftJoin ) ? ' '.$strSqlLeftJoin : '' )
    .( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )
;
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.5.tpl";
$objPagination->iQtyLinksPerPage = 10;
$objPagination->iQtyRecsPerPage = (isset($_GET['count']) && intval($_GET['count'])?$_GET['count']:30);
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT O.seo_id, O.seo_name, O.seo_address, O.seo_status, O.seo_anons, O.seo_pa, O.seo_sb, I.si_id, I.si_filename, ET.set_name"
    . " FROM site_estate_objects AS O"
    . " LEFT JOIN `site_images` AS I ON (I.si_id_rel = O.seo_id AND I.si_type = 'estate')"
    . " LEFT JOIN `site_estate_types` AS ET ON (O.seo_id_etype = ET.set_id)"
    .( !empty( $strSqlLeftJoin ) ? ' '.$strSqlLeftJoin : '' )
    .( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )
    .( $_SESSION['frmSearchObjects']['strObjectComments2Find'] == 'Y' ? ' GROUP BY suc_seo_id' : '' )
    . " ORDER BY O.seo_id DESC ".$objPagination->strSqlLimit;
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );

// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();
$objEstateObject = new EstateObject();
if ( is_array($arrSelectedRecords) ) {
    foreach ( $arrSelectedRecords as $key => $value ) {
        $arrSelectedRecords[$key]['cbxStatus'] = $value['seo_status'] == 'Y' ? ' checked' : '';
        $arrSelectedRecords[$key]['cbxAnons'] = $value['seo_anons'] == 'Y' ? ' checked' : '';
        $arrSelectedRecords[$key]['cbxAccess'] = $value['seo_pa'] == 'Y' ? ' checked' : '';
        $arrSelectedRecords[$key]['intCountComments'] = $objEstateObject->getCommentsCount($value['seo_id']);
        $arrSelectedRecords[$key]['intCountExtraInfo'] = $objEstateObject->getExtraInfoCount($value['seo_id']);
        $arrSelectedRecords[$key]['strSB'] = $value['seo_sb'] == 'Y' ? '[СБ]' : '';
        $arrIf['is.comments.'.$value['seo_id']] = $arrSelectedRecords[$key]['intCountComments'] > 0;
        $arrIf['photo.'.$value['seo_id']] = ( file_exists(DROOT."storage/images/objects/".$value['si_filename']) && !empty($value['si_filename']) );
    }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

/**
 * Разделы недвижимости
 */
$strSqlQuery = "SELECT * FROM ".$_db_tables["stEstTypes"]." ORDER BY set_name";
$arrEstTypes = $objDb->fetchall( $strSqlQuery );
if ( is_array($arrEstTypes) ) {
    foreach ( $arrEstTypes as $key => $value ) {
        $arrEstTypes[$key]['sel'] = $value['set_id'] == $intFindEstTypeID ? ' selected' : '';
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "est.types", $arrEstTypes);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
