<?php
$arrTplVars['module.name'] = "site.users";
$arrTplVars['module.child'] = "site.user.form";
$arrTplVars['module.title'] = "Пользователи сайта";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Принудительная очистка переменной сессии для поиска
 */
if ( isset($_GET['search']) && $_GET['search'] == 'clear' ) {
	if ( !empty($_SESSION['formFilterSubscribers']) ) {
		unset($_SESSION['formFilterSubscribers']);
	}
	header("location: ".$arrTplVars['module.name']."?errMsg=msgSearchCleaned");
	exit();
}

/**
 * Удаление пользователя
 */
if ( isset($_GET['id']) && intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
	$intRecordId = intval($_GET['id']);
	$strSqlQuery = "DELETE FROM ".$_db_tables['stUsers']." WHERE su_id='$intRecordId' LIMIT 1";
	if ( !$objDb->query($strSqlQuery) ) {
		$GLOBALS['manStatusError'] = 1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
	}
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Очистка переменной поиска в том случае если пришли на данную страницу из другого места
if (!preg_match("/".$arrTplVars['module.name']."/", $_SERVER['HTTP_REFERER']) || !isset($_GET['stPage'])) {
	if ( !empty($_SESSION['formFilterSubscribers']) ) {
		unset($_SESSION['formFilterSubscribers']);
	}
}

$strSqlFilterWhere = "";
if ( (isset($_POST['formFilterSubscribers']) && $_POST['formFilterSubscribers'] == 'true') || !empty($_SESSION['formFilterSubscribers']) ) {
	if (!empty($_POST)) {
		$_SESSION['formFilterSubscribers'] = $_POST;
	}
	if (empty($_POST) && !empty($_SESSION['formFilterSubscribers'])) {
		$_POST = $_SESSION['formFilterSubscribers'];
	}
	if ( !empty($_POST['strFilterUserName']) && strlen($_POST['strFilterUserName']) >= 2 ) {
		$arrSqlData['strFilterUserName'] = mysql_real_escape_string(trim($_POST['strFilterUserName']));
		$arrTplVars['strFilterUserName'] = htmlspecialchars(stripslashes(trim($_POST['strFilterUserName'])));

		$strSqlFilterWhere .= " AND ( su_lname LIKE '%".$arrSqlData['strFilterUserName']."%'"
			. " OR su_fname LIKE '%".$arrSqlData['strFilterUserName']."%'"
			. " OR su_mname LIKE '%".$arrSqlData['strFilterUserName']."%'"
			. " OR su_login LIKE '%".$arrSqlData['strFilterUserName']."%'"
			. " OR su_email LIKE '%".$arrSqlData['strFilterUserName']."%')"
        ;
	}

	if ( !empty($_POST['strFilterOrgName']) && strlen($_POST['strFilterOrgName']) >= 2 ) {
		$arrSqlData['strFilterOrgName'] = mysql_real_escape_string(trim($_POST['strFilterOrgName']));
		$arrTplVars['strFilterOrgName'] = htmlspecialchars(stripslashes(trim($_POST['strFilterOrgName'])));

		$strSqlFilterWhere .= " AND su_company LIKE '%".$arrSqlData['strFilterOrgName']."%'";
	}

	// Финальная обработка фильтра
	if ( !empty( $strSqlFilterWhere ) ) {
		$strSqlFilterWhere = substr($strSqlFilterWhere, 4);
	}
}

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stUsers'];
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stUsers'].( !empty($strSqlFilterWhere) ? " WHERE $strSqlFilterWhere" : '');
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyLinksPerPage = 7;
$objPagination->iQtyRecsPerPage = 20;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stUsers']
	.( !empty($strSqlFilterWhere) ? " WHERE $strSqlFilterWhere" : '' )
	." ORDER BY su_id DESC ".$objPagination->strSqlLimit;
$arrSiteUsers = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSiteUsers) ? count($arrSiteUsers) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrSiteUsers) ) {
	foreach ( $arrSiteUsers as $key => $value ) {
		$arrSiteUsers[$key]['cbxStatus'] = $value['su_status'] == 'Y' ? ' checked' : '';
	}
}

$objTpl->tpl_loop($arrTplVars['module.name'], "site.users", $arrSiteUsers, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
