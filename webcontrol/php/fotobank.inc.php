<?php
$arrTplVars['module.name'] = "fotobank";
$arrTplVars['module.title'] = "ФотоБанк";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);


/**
 * Запись файла
 */
if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
	$intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
	$arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

	$arrSqlData['strName'] = mysql_real_escape_string(trim($_POST['strName']));
	$arrTplVars['strName'] = htmlspecialchars(trim($_POST['strName']));

	if (empty($arrSqlData['strName']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgEmptyField';
	}

	$arrSqlData['strDesc'] = addslashes(trim($_POST['strDesc']));
	$arrTplVars['strDesc'] = htmlspecialchars(stripslashes(trim($_POST['strDesc'])));

	$arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
	$arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

	$arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

	if ( $GLOBALS['manStatusError'] != 1 ) {
		$strSqlFields = ""
			. " sig_name = '{$arrSqlData['strName']}'"
			. ", sig_desc = '{$arrSqlData['strDesc']}'"
			. ", sig_status = '{$arrSqlData['cbxStatus']}'"
			. ", sig_type = 'fotobank'"
		;

		if ( empty($intRecordId) ) {
			$strSqlQuery = "INSERT INTO site_images_group SET ".$strSqlFields.", sig_date_add = NOW()";
		} else {
			$strSqlQuery = "UPDATE site_images_group SET ".$strSqlFields." WHERE sig_id = '$intRecordId'";
		}

		if ( !$objDb->query( $strSqlQuery ) ) {
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		} else {
			$intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;
		}

		if ( $GLOBALS['manStatusError']!=1 ) {
			if ( $_POST['cbxReturn']=='on' ) {
				header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId&stPage=$stPage");
			} else {
				header('location:'.$arrTplVars['module.name']."?errMess=msgOk&stPage=$stPage");
			}
			exit;
		}
	}
}


if ( intval($_GET['id_del']) > 0 ) {
	$intRecordId = intval($_GET['id_del']);
	$strSqlQuery = "SELECT si_filename FROM site_images WHERE si_id='$intRecordId'";
	$strFilename = $objDb->fetch($strSqlQuery, "si_filename");

	$strSqlQuery = "DELETE FROM site_images_group WHERE sig_id='".$intRecordId."' AND sig_type='fotobank'";
	if ( !$objDb->query($strSqlQuery) ) {
		#mysql error
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
	}

	$strSqlQuery = "DELETE FROM site_images WHERE si_id_rel='".$intRecordId."' AND si_type='fotobank'";
	if ( !$objDb->query($strSqlQuery) ) {
		#mysql error
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
	} else {

		$dir = PRJ_IMAGES.'fotobank/groups/'.$intRecordId;
		if (is_dir($dir)) {
			foreach (scandir($dir) as $item) {
				if ($item == '.' || $item == '..') continue;
				if (!unlink($dir.DIRECTORY_SEPARATOR.$item)){
					$GLOBALS['manStatusError']=1;
					$GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
				}
			}

			if(!rmdir($dir)) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
			}
		}
	}

	if (!$GLOBALS['manStatusError']) {
		header('Location: '.$arrTplVars['module.name'].'?errMess=msgRmAllOk&stPage='.$stPage);
		exit;
	}
}


// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtil->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtil->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************



// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( $_GET['action'] == 'add' || (isset($_GET['id']) && intval($_GET['id']) > 0) ) {
	$arrIf['editrecord'] = true;
	$intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

	$strSqlQuery = "SELECT * FROM site_images_group WHERE sig_id ='".$intRecordId."'";
	$arrInfo = $objDb->fetch( $strSqlQuery );

	if ( is_array( $arrInfo )) {
		$arrTplVars['strDesc'] = htmlspecialchars(stripslashes($arrInfo['sig_desc']));
		$arrTplVars['strName'] = htmlspecialchars(stripslashes($arrInfo['sig_name']));

		#$arrIf['is.image'] = true;

		$arrTplVars['cbxStatus'] = $arrInfo['sig_status'] == 'Y' ? ' checked' : '';

		$arrIf['can.upload.images'] = true;

		// Let's check attached images folder
		$attachedFiles = PRJ_IMAGES.'fotobank/groups/'.$intRecordId.'/';
		$arrAttachedFiles = array();
		if (is_dir($attachedFiles)) {
			$exclude_list = array(".", "..");
			$arrAttachedFiles = array_diff(scandir($attachedFiles), $exclude_list);
			foreach ($arrAttachedFiles as $key => $fileName) {
				if (preg_match("/^t_h_u_m_b_/", $fileName)) {
					unset($arrAttachedFiles[$key]);
				}
				if (preg_match("/^s_m_a_l_l_/", $fileName)) {
					unset($arrAttachedFiles[$key]);
				}
			}
		}

		$strSqlQuery = "SELECT si_id, si_filename, si_title, si_desc, si_reporter, si_tags, si_status, sig_img_id, si_foto_object_id FROM `site_images`"
        ." LEFT JOIN `site_images_group` ON (si_id_rel=sig_id)"
        ." WHERE `si_id_rel` = ".$intRecordId." AND `si_type`='fotobank'";
		$arrImages = $objDb->fetchall($strSqlQuery);
		$arrImagesInfo = array();
		if (is_array($arrImages) && !empty($arrImages)) {
			foreach ($arrImages as $key => $arrImg) {
                /**
                 * Проверить, есть ли выбранный из БД файл на диске?
                 */
                $isKey = array_search($arrImg['si_filename'], $arrAttachedFiles);
				if ($isKey!==false) {
					$arrAttachedFiles[$arrImg['si_id']] = $arrImg['si_filename'];
					unset($arrAttachedFiles[$isKey]);
//					$arrImagesInfo[$arrImg['si_id']]['title'] = iconv('CP1251', 'UTF-8', $arrImg['si_title']);
					$arrImagesInfo[$arrImg['si_id']]['title'] = $arrImg['si_title'];
//					$arrImagesInfo[$arrImg['si_id']]['desc'] = iconv('CP1251', 'UTF-8', $arrImg['si_desc']);
					$arrImagesInfo[$arrImg['si_id']]['desc'] = $arrImg['si_desc'];
//					$arrImagesInfo[$arrImg['si_id']]['tags'] = iconv('CP1251', 'UTF-8', $arrImg['si_tags']);
					$arrImagesInfo[$arrImg['si_id']]['tags'] = $arrImg['si_tags'];
//					$arrImagesInfo[$arrImg['si_id']]['author'] = iconv('CP1251', 'UTF-8', $arrImg['si_reporter']);
					$arrImagesInfo[$arrImg['si_id']]['author'] = $arrImg['si_reporter'];
					$arrImagesInfo[$arrImg['si_id']]['oid'] = $arrImg['si_foto_object_id'];
					$arrImagesInfo[$arrImg['si_id']]['active'] = $arrImg['si_status']=='Y';
					$arrImagesInfo[$arrImg['si_id']]['main'] = $arrImg['sig_img_id']==$arrImg['si_id'];
				}
			}
		} else {
			$arrIf['list.images.empty'] = true;
		}
		$arrTplVars['filesAttached'] = json_encode($arrAttachedFiles);
		$arrTplVars['filesInfo'] = json_encode($arrImagesInfo);
	}
} else {
	// Всего записей в базе
	$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_images_group";
	$arrTplVars['intQuantAllRecords'] = $arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
	// ***** BEGIN: Построение пейджинга для вывода списка
	$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
	$objPagination->strPaginatorTpl = "site.global.1.tpl";
	$objPagination->iQtyRecsPerPage = 12;
	$objPagination->strColorLinkStyle = "link-b";
	$objPagination->strColorActiveStyle = "tab-bl-b";
	$objPagination->strNameImageBack = "arrow_one_left.gif";
	$objPagination->strNameImageForward = "arrow_one_right.gif";
	// Создаем блок пэйджинга
	$objPagination->paCreate();
	// ***** END: Построение пейджинга для вывода списка
	// Запрос для выборки нужных записей
	$strSqlQuery = "SELECT * FROM site_images_group ORDER BY sig_id DESC ".$objPagination->strSqlLimit;
	$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
	// кол-во публикаций показанных на странице
	$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
	// Присвоение значения пэйджинга для списка
	$arrTplVars['blockPaginator'] = $objPagination->paShow();

	$arrIf['records'] = $arrTplVars['intQuantShowRecOnPage']>0;

	if ( is_array($arrSelectedRecords) ) {
		foreach ( $arrSelectedRecords as $key => $value ) {
			$arrSelectedRecords[$key]['cbxStatus'] = $value['sig_status'] == 'Y' ? ' checked' : '';
		}
	}

	$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));
}

$arrIf['showlist'] = !$arrIf['editrecord'];
$arrIf['norecords'] = !$arrIf['records'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
