<?php
/** >***************************************************************************\
 ** Shirokovskiy D.2008 Jimmy™. Fri May 30 13:58:17 MSD 2008
 *
 * Usefull functions
 **
\*******************************************************************************/

function getObjectInfo ( $id, $field = 'seo_name' ) {
    global $objDb;
    $str = null;
    $strSqlQuery = "SELECT `$field` FROM site_estate_objects WHERE seo_id='$id'";
    $arrObjInfo = $objDb->fetch( $strSqlQuery );
    if ( is_array( $arrObjInfo ) ) {
        $str = $arrObjInfo[$field];
    }
    return $str;
}


function isSuchCompany ( $str ) {
    if ( empty( $str ) ) {
        return false;
    }

    global $objDb;

    $strSqlQuery = "SELECT sec_id FROM site_estate_contacts WHERE sec_company='$str'";
    $arrInfo = $objDb->fetch( $strSqlQuery );
    if ( is_array($arrInfo) ) {
        $id = $arrInfo['sec_id'];
    }

    return $id > 0 ? $id : false;
}


function insertIfNotExistCmpRelationObj ( $id_company, $id_object, $rel_type, $do_change = true ) {
    if ( $id_company <= 0 || $id_object <= 0 || empty($rel_type) ) {
        return false;
    }
    global $objDb;

    $strSqlQuery = "SELECT * FROM site_objects_contacts_lnk WHERE socl_id_contact=$id_company AND socl_id_object=$id_object";
    $arrExistRef = $objDb->fetch( $strSqlQuery );
    if ( !is_array($arrExistRef) ) {
        $strSqlQuery = "INSERT INTO site_objects_contacts_lnk SET"
            ." socl_id_contact = $id_company"
            .", socl_id_object = $id_object"
            .", socl_relation = '$rel_type'"
        ;
        if ( !$objDb->query( $strSqlQuery ) ) {
            # sql query error
            $GLOBALS['manStatusError'] = 1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            $res = "Error in " . __FILE__ . "! Line: " . __LINE__ .print_r("\n".$strSqlQuery, true).print_r("\n".mysql_error(), true);
            $fp = @fopen(DROOT."storage/debug/sql.".date('Ymd').".txt", "a+");
            @fwrite($fp, $res."\n");
            @fclose($fp);

            return false;
        }
    } else {
        if ( $do_change ) {
            $strSqlQuery = "UPDATE site_objects_contacts_lnk SET socl_relation = '$rel_type' WHERE socl_id_contact = $id_company AND socl_id_object = $id_object";
            if ( !$objDb->query( $strSqlQuery ) ) {
                # sql query error
                $GLOBALS['manStatusError'] = 1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                $res = "Error in " . __FILE__ . "! Line: " . __LINE__ .print_r("\n".$strSqlQuery, true).print_r("\n".mysql_error() );
                $fp = @fopen(DROOT."storage/debug/sql.".date('Ymd').".txt", "a+");
                @fwrite($fp, "".$res);
                @fclose($fp);

                return false;
            }
        }
    }

    return true;
}

if (!function_exists("mysql_real_escape_string")) {
    function mysql_real_escape_string($string) {
        $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
        $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

        return str_replace($search, $replace, $string);
    }
}

if (!function_exists("mysql_close")) {
    function mysql_close($connection) {
        return mysqli_close($connection);
    }
}
