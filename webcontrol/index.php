<?php
if (!defined("DROOT")) {
	define('DROOT', $_SERVER['DOCUMENT_ROOT'].(preg_match("/.*\/$/i", $_SERVER['DOCUMENT_ROOT']) ? "" : "/") );
}

// **** Подгрузка конфига
include_once(DROOT."access_conf/web.cfg.php");
include_once("funcs.php");

$objTpl = new Template(ADMTPL);
$objFile  = new Files();

$strReqUri = str_replace($_SERVER['HTTP_HOST'], "", $_SERVER['REQUEST_URI']);
$strReqUri = str_replace(dirname($strReqUri), "", $strReqUri);
$strReqUri = str_replace("//", "", $strReqUri);

if ( false !== strpos($strReqUri, "/")) {
	$strReqUri = substr($strReqUri, 1);
}

if ( false !== strpos($strReqUri, '?') )
	$strReqUri = substr($strReqUri, 0, strpos($strReqUri, '?'));

//$arrTplVars['reqModule'] = str_replace("//", "", substr($_SERVER["PATH_INFO"], 1));
$arrTplVars['reqModule'] = $strReqUri;

if (chop($strReqUri)=='' || $strReqUri=='/' || $strReqUri == 'webcontrol' ) {
    unset($_SESSION['frmSearchCompanies']);
	header("Location: ".$arrTplVars['strAdminUrl']."user.main");
	exit();
}

$arrTplVars['reqQuery'] = $_SERVER["QUERY_STRING"];
$arrTplVars['reqQueryParse'] = explode("&", $arrTplVars['reqQuery']);

if (empty($_SESSION['userInfoStorage']['cu_id'])) {
	$arrInfoConfig = $objDb->fetch($strSqlQuery = "SELECT * FROM ".$_db_tables['cmsConfig']." WHERE cc_default='Y' ORDER BY cc_id LIMIT 1");
	$arrTplVars['cId'] = $arrInfoConfig['cc_id'];
	$arrTplVars['cBgColor'] = $arrInfoConfig['cc_bg_color'];
	$arrTplVars['cFirstColor'] = $arrInfoConfig['cc_first_color'];
	$arrTplVars['cSecondColor'] = $arrInfoConfig['cc_second_color'];
	$arrTplVars['cThirdColor'] = $arrInfoConfig['cc_third_color'];
	$arrTplVars['cFourthColor'] = $arrInfoConfig['cc_fourth_color'];
	$arrTplVars['strNumVersion'] = $arrInfoConfig['cc_ver'];
	$arrTplVars['strCMSName'] = $arrInfoConfig['cc_name'];
} else {
	$arrTplVars['cId'] = $_SESSION['userConfig']['cId'];
	$arrTplVars['cBgColor'] = $_SESSION['userConfig']['cBgColor'];
	$arrTplVars['cFirstColor'] = $_SESSION['userConfig']['cFirstColor'];
	$arrTplVars['cSecondColor'] = $_SESSION['userConfig']['cSecondColor'];
	$arrTplVars['cThirdColor'] = $_SESSION['userConfig']['cThirdColor'];
	$arrTplVars['cFourthColor'] = $_SESSION['userConfig']['cFourthColor'];
	$arrTplVars['strNumVersion'] = $_SESSION['userConfig']['strNumVersion'];
	$arrTplVars['strCMSName'] = $_SESSION['userConfig']['strCMSName'];
	$arrTplVars['strUserName'] = $_SESSION['userInfoStorage']['strUserName'];
	$arrTplVars['strUserTypeAccess'] = $_SESSION['userInfoStorage']['cug_name'];
}

$arrTplVars['dot1px'] = '<table width="100%"><tr><td background='.$arrTplVars['strAdminImg'].'bg_dotted_horiz.gif height="7"></td></tr></table>';

/**
 * Shirokovskiy D. [Jimmy]
 * Определяем какое окно грузим, Pop-up или обычное
 */
$strSqlQuery = "SELECT cm_popup FROM `cms_modules` WHERE cm_link='".$arrTplVars['reqModule']."'";
$cbxPopupField = $objDb->fetch( $strSqlQuery, "cm_popup");

if ($cbxPopupField == 'Y') {
	include_once(ADMPHP."global/page.popup.header.inc.php");
	include_once(ADMPHP."global/page.popup.footer.inc.php");
} else {
	include_once(ADMPHP."global/page.header.inc.php");
	include_once(ADMPHP."global/page.footer.inc.php");
}

// ***** Обработка сообщений ошибок передаваемых GET методом ****************************
if (!empty($_GET['errSt'])) $GLOBALS['manStatusError'] = 1; else $GLOBALS['manStatusError'] = 0;
if (!empty($_GET['errSuf'])) $errSuf = trim($_GET['errSuf']); else $errSuf = '';
$GLOBALS['manCodeError'] = array();
if (!empty($_GET['errMess'])) {
	$arrError = explode("|", $_GET['errMess']);
	foreach ($arrError as $key=>$value) {
		$GLOBALS['manCodeError'][]['code'] = $value;
	}
}
// **************************************************************************************

if (!isset($_SESSION['userInfoStorage']['cu_id'])) {
	include_once(ADMPHP.'global/login.php');
} else {
	include_once(ADMPHP."global/user.menu.inc.php");
	include_once(ADMPHP."global/header.login.true.inc.php");
}

$objTpl->tpl_parse_root($arrTplVars['reqModule']); // Выставляем блоки доступные только для администратора
$objTpl->tpl_parse($arrTplVars['reqModule']);
