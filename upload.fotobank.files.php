<?php
/**
 * Created by Dmitry Shirokovskiy.
 * Email: info@phpwebstudio.com
 * Date: 10/5/12
 * Time: 11:49 AM
 * Files uploader
 */
header("Content-Type: content=text/html; charset=utf-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Pragma: no-cache");

require_once( getcwd().DIRECTORY_SEPARATOR.'access_conf'.DIRECTORY_SEPARATOR.'web.cfg.php' );

$uploadDir = getcwd().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'fotobank'.DIRECTORY_SEPARATOR;
if (!is_dir($uploadDir)) {
	if (!mkdir($uploadDir, 0775)) {
//		header("HTTP/1.1 403 Forbidden");
		die('Не могу создать директорию '.$uploadDir);
	}
}
$uploadDir .= 'groups'.DIRECTORY_SEPARATOR;
if (!is_dir($uploadDir)) {
	if (!mkdir($uploadDir, 0775)) {
//		header("HTTP/1.1 403 Forbidden");
		die('Не могу создать директорию '.$uploadDir);
	}
}

function createGroupFolder(&$dir, $id) {
    $dir .= $id.DIRECTORY_SEPARATOR;
    if(!is_dir($dir)) {
        if (!mkdir($dir, 0775)) {
            return false;
        }
    }

    return true;
}

// Если что-то делаем с фотобанком
if (isset($_GET['fb_id']) && intval($_GET['fb_id'])>0) {
	# Create folder for the FotoGroup
	$intRecordId = intval($_GET['fb_id']);
	if(!createGroupFolder($uploadDir, $intRecordId)) {
        die('Не могу создать директорию '.$uploadDir);
	} else {
		// Если решили удалить файл (картинку)
		if( isset($_GET['a']) && $_GET['a']=='del') {
			$fn=$uploadDir.urldecode($_GET['filename']);
			if( file_exists($fn)) {
				unlink($fn);
			}
			// TODO: also delete record from DB
			die();
		}
	}
} elseif (isset($_GET['eo_id']) && $_GET['eo_id'] > 0) {
    /**
     * Тот случай когда записываем файл картинку для объекта без предварительного создания фотобазы
     */
    $objectId = intval($_GET['eo_id']);

    $strSqlQuery = "SELECT * FROM `site_estate_objects` WHERE `seo_id` = " . $objectId;
    $arrInfo = $objDb->fetch($strSqlQuery);

    /**
     * Записать в БД
     */
    $strSqlQuery = "INSERT INTO `site_images_group` SET"
        . " sig_type = 'fotobank'"
        . ", sig_name = '".mysql_real_escape_string($arrInfo['seo_name'])."'"
        . ", sig_desc = '".mysql_real_escape_string($arrInfo['seo_name'])."'"
        . ", sig_date_add = NOW()"
        . ", sig_status = 'Y'"
    ;
    if (!$objDb->query($strSqlQuery)) {
        # sql error
        header("HTTP/1.1 403 Forbidden");
        die('Запись в базу данных не выполнена!');
    } else {
        $groupId = $intRecordId = $objDb->insert_id();

        if(!createGroupFolder($uploadDir, $intRecordId)) {
            die('Не могу создать директорию '.$uploadDir);
        }
    }

    unset($arrInfo);
}

/* ************************************************************************************** */

if ( !is_object($objImage) ) {
	include_once("cls.images.php");
	$objImage = new clsImages();
}

if (time() < strtotime('2013-09-06')) {
	$fh = fopen(getcwd().DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'debug.log', "a+");
	fwrite($fh, date('H:i:s d.M ').'$_REQUEST='.print_r($_REQUEST, true)."\n");
	fclose($fh);
}

/**
 * Описание фото
 */
if ($_GET['act']=='idesc' && $_POST['intImageId'] > 0) {
	$intRecordId = intval($_POST['intImageId']);
	$intGroupId = intval($_POST['intGroupId']);
	$intObjectId = intval($_POST['intObjectId']);
	$filePath = PRJ_IMAGES.'fotobank/groups/'.$intGroupId.'/';

	if ($_POST['rbGroupImage']=='on') {
		$_POST['cbxImageStatus']='on'; // нельзя ставить изображение главным и чтобы оно было не активным
	}

	// изменить имя файла если указан Title картинки
	$new_filename = '';
	if (!empty($_POST['strImageTitle'])) {
//		$new_filename = iconv('UTF-8', 'CP1251',$_POST['strImageTitle']);
		$new_filename = $_POST['strImageTitle'];
		$new_filename = strtolower( $objUtil->translitKyrToLat($objUtil->translitBadChars($new_filename)) ).substr($_POST['strFilename'],-4);
	}

	if (!empty($new_filename) && file_exists($filePath.$new_filename)) {
		$new_filename = '';
	}

	$strSqlQuery = "UPDATE `site_images` SET"
		. " `si_title` = '".mysql_real_escape_string( $_POST['strImageTitle'] )."'"
		. ", `si_desc` = '".mysql_real_escape_string( $_POST['strImageDesc'] )."'"
		. ", `si_tags` = '".mysql_real_escape_string( $_POST['strTags'] )."'"
		. ", `si_reporter` = '".mysql_real_escape_string( $_POST['strAuthor'] )."'"
		. ", `si_status` = '".($_POST['cbxImageStatus']=='on'?'Y':'N')."'"
		. (!empty($new_filename) ? ", `si_filename` = '".$new_filename."'" : '')
		. ", `si_foto_object_id` = ".$intObjectId
		. ", `si_date_change` = NOW()"
		. " WHERE `si_id_rel` = $intGroupId AND `si_type` = 'fotobank' AND `si_filename` = '".mysql_real_escape_string($_POST['strFilename'])."' AND `si_id` = ".$intRecordId;
	if (!$objDb->query($strSqlQuery)) {
		# sql error
		header("HTTP/1.1 403 Forbidden");
		die('Запись в базу данных не выполнена!');
	} else {
		/**
		 * Обновить запись группы, если указана главная картинка
		 */
		if (isset($_POST['rbGroupImage']) && $_POST['rbGroupImage']=='on') {
			$strSqlQuery = "UPDATE `site_images_group` SET `sig_img_id` = '$intRecordId' WHERE `sig_id` = '$intGroupId' AND `sig_type` = 'fotobank'";
			if (!$objDb->query($strSqlQuery)) {
				# sql error
				header("HTTP/1.1 403 Forbidden");
				die('Запись в базу данных не выполнена!');
			}
		}

		/**
		 * Обновить имя файла, если изменилось
		 */
		if (!empty($new_filename)) {
			if (file_exists($filePath.$_POST['strFilename'])) {
				rename($filePath.$_POST['strFilename'], $filePath.$new_filename);
                chmod($filePath.$new_filename, 0666);
			}
			if (file_exists($filePath.'t_h_u_m_b_'.$_POST['strFilename'])) {
				rename($filePath.'t_h_u_m_b_'.$_POST['strFilename'], $filePath.'t_h_u_m_b_'.$new_filename);
                chmod($filePath.'t_h_u_m_b_'.$new_filename, 0666);
			}
			if (file_exists($filePath.'s_m_a_l_l_'.$_POST['strFilename'])) {
				rename($filePath.'s_m_a_l_l_'.$_POST['strFilename'], $filePath.'s_m_a_l_l_'.$new_filename);
                chmod($filePath.'s_m_a_l_l_'.$new_filename, 0666);
			}
		}
	}
	die("Данные сохранены!");
}

/**
 * Удаление
 */
if ($_GET['act']=='del' && $_GET['fb_id']>0 && $_GET['iid']>0) {
	$intRecordId = intval($_GET['iid']);
	$intGroupId = intval($_GET['fb_id']);
	$strFilename = $_GET['filename'];

	$strSqlQuery = "DELETE FROM `site_images` WHERE "
		." si_id = ".$intRecordId
		." AND si_filename = '".mysql_real_escape_string($strFilename)."'"
		." AND si_id_rel = ".$intGroupId
		." AND si_type = 'fotobank'";
	;
	if (!$objDb->query($strSqlQuery)) {
		# sql query error
		header("HTTP/1.1 403 Forbidden");
		die('Запрос к базе данных не увенчался успехом!');
	} else {
		$filepathToDel = PRJ_IMAGES.'fotobank/groups/'.$intGroupId.'/'.$strFilename;
		if (file_exists($filepathToDel))
			unlink($filepathToDel);
		$filepathToDel = PRJ_IMAGES.'fotobank/groups/'.$intGroupId.'/t_h_u_m_b_'.$strFilename;
		if (file_exists($filepathToDel))
			unlink($filepathToDel);
		$filepathToDel = PRJ_IMAGES.'fotobank/groups/'.$intGroupId.'/s_m_a_l_l_'.$strFilename;
		if (file_exists($filepathToDel))
			unlink($filepathToDel);
	}
	die;
}

/**
 * Загрузка файла
 */
if (is_array($_FILES) && !empty($_FILES) && !$_FILES['file']['error']) {
	/**
	 * Обработать имя файла так, чтобы потом было удобно браузеру
	 */
	$basename = basename($_FILES['file']['name']);

//	$fp = fopen(DROOT."logs/debug.log", "a+");
//	fwrite($fp, $basename."\n");

//	$basename = iconv('UTF-8', 'CP1251', $basename);

//	fwrite($fp, $basename."\n");
//	fclose($fp);

	$strImageName = strtolower( $objUtil->translitKyrToLat($objUtil->translitBadChars($basename)) );
	$uploadImage = $uploadDir.$strImageName;
	$uploadThumb = $uploadDir.'t_h_u_m_b_'.$strImageName;
	$uploadSmall = $uploadDir.'s_m_a_l_l_'.$strImageName;

	if (file_exists($uploadImage)) {
		header("HTTP/1.1 403 Forbidden");
		die('Файл "'.$_FILES['file']['name'].'" уже существует!');
	}

	if (is_uploaded_file($_FILES['file']['tmp_name'])) {
		if ( false == move_uploaded_file($_FILES['file']['tmp_name'], $uploadImage) ) {
			header("HTTP/1.1 403 Forbidden");
			die('Не могу записать загруженый файл!');
		} else {
			/**
			 * Сделать превьюху, и поставить клеймо на большое изображение
			 */
			$objImage->resizeImage($uploadImage, $uploadThumb, 180 );
			$markerImage = PRJ_SITEIMG.'content/marker/logo2.psb.png';
			//$objImage->setWaterMark($uploadImage, $markerImage);		// отменено по просьбе от 18/04/2013
			//$markerImage = PRJ_SITEIMG.'content/marker/logo2.psb.png';
			//$objImage->setWaterMark($uploadImage, $markerImage, 30, 30);
			$objImage->resizeImage($uploadImage, $uploadSmall, 600 );
			chmod($uploadImage, 0664);
			chmod($uploadThumb, 0664);
			chmod($uploadSmall, 0664);

			$objectId = intval($_REQUEST['eo_id']);

			/**
			 * Записать в БД
			 */
			$strSqlQuery = "INSERT INTO `site_images` SET"
				. " si_id_rel = $intRecordId" // запись из таблицы site_images_group
				. ($objectId > 0 ? ", si_foto_object_id = $objectId" : '')
				. ($objectId > 0 ? ", si_title = '".date("d-m-Y")."'" : '')
				. ", si_type = 'fotobank'"
				. ", si_filename = '".mysql_real_escape_string($strImageName)."'"
				. ", si_date_add = NOW()"
				. ", si_status = 'Y'"
			;
			if (!$objDb->query($strSqlQuery)) {
				# sql error
				header("HTTP/1.1 403 Forbidden");
				die('Запись в базу данных не выполнена!');
			} else {
				$intImgId = $objDb->insert_id();
			}

			echo "-OK-".$intImgId."|".$strImageName.(isset($_GET['eo_id'])?"@".$intRecordId:'');
		}
	} else {
		header("HTTP/1.1 403 Forbidden");
		die('Файл не загружен!');
	}
} else {
	header("HTTP/1.1 403 Forbidden");
	die('Не могу загрузить файл!');
}
