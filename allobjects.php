<?php
setlocale(LC_ALL, "ru_RU.UTF8");
// **** Уровень ошибок *********************************************************
//error_reporting(E_ALL ^ E_NOTICE); // all but notices
error_reporting(E_ERROR); // only error
ini_set("display_errors", "On");
date_default_timezone_set ( 'Europe/Moscow' );
include_once "access_conf/web.cfg.php";

// *****************************************************************************
$file_name='result.csv'; //имя выходного файла

if ($_GET['action']=='download'){
    if (!headers_sent() && file_exists(dirname(__FILE__).'/'.$file_name)) {
        Header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT\r\n" );
        Header( "Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT\r\n" );
        Header( "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
        Header( "Cache-Control: post-check=0, pre-check=0, false\r\n" );
        Header( "Pragma: no-cache\r\n" );
        Header( "HTTP/1.1 200 OK\r\n" );

        Header( "Content-Disposition: attachment; filename=".$file_name."\r\n" );
        Header( "Accept-Ranges: bytes\r\n" );
        Header( "Content-Type: application/force-download\r\n" );

        readfile( "./".$file_name );
        exit;
    }
    echo "Ошибка!";
}

// *****************************************************************************


//*****************************************************************************/
?><html><head>
<title>All objects of PetroStroyBaza.Ru</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head><body>
    <p>Формирование файла CSV базы данных объектов...</p>

<?php
// *****************************************************************************
if ($_GET['action']=='create'){
    include_once "access_conf/db.cfg.php";
    if (!mysql_connect($_db_config['host'], $_db_config['login'], $_db_config['password'])) {
        echo "Ошибка подключения к серверу MySQL";
        echo "</body></html>";
        exit;
    }
    if(!mysql_select_db($_db_config['dbname'])) {
        echo "Ошибка выбора БД: ".$_db_config['dbname'];
        echo "</body></html>";
        exit;
    } else {
        mysql_query("SET NAMES '".$_db_config['charset']."'"); // Необходимо для вытягивания данных из БД в нужной кодировке (Если не UTF-8)
    }

    $resTablLink = mysql_query("SELECT * FROM site_objects_contacts_lnk");
    $rows = mysql_num_rows($resTablLink);

    $pFile = fopen($file_name,'w');

    for ($row=0;$row<$rows;$row++) {
        $Link = mysql_fetch_array($resTablLink,1);    //получаем очередную запись из таблицы линков
        $arrDataResult=array();

        $Object=GetRowFromeTable("site_estate_objects","seo_id",$Link['socl_id_object']);
        if(empty($Object)) continue;

        $arrDataResult[]=$Object['seo_id'];                                                                         // 1
        $arrDataResult[]= iconv('utf-8', 'windows-1251', $Object['seo_name']);                                       // 2

        $contactId=GetRowFromeTable("site_estate_contacts","sec_id",$Link['socl_id_contact']);
        if($contactId!=0) {
            $arrDataResult[]=iconv('utf-8', 'windows-1251', $contactId['sec_company']);                              // 3
            $arrDataResult[]=iconv('utf-8', 'windows-1251', $contactId['sec_fio']);                                  // 4
            $arrDataResult[]=iconv('utf-8', 'windows-1251', $contactId['sec_proff']);                                // 5
            $arrDataResult[]=iconv('utf-8', 'windows-1251', $contactId['sec_contact']);                              // 6
            $type = isset($arrTitleExt[$contactId['sec_type']]) ? $arrTitleExt[$contactId['sec_type']] : (isset($arrTitleExt[$Link['socl_relation']]) ? $arrTitleExt[$Link['socl_relation']] : $contactId['sec_type'] );
            $arrDataResult[]=iconv('utf-8', 'windows-1251', $type);                                                  // 7
            $arrDataResult[]=iconv('utf-8', 'windows-1251', $contactId['sec_inn']);                                  // 8
        } else {
            $arrDataResult[]="";
            $arrDataResult[]="";
            $arrDataResult[]="";
            $arrDataResult[]="";
            $arrDataResult[]="";
            $arrDataResult[]="";
        }

        /**************/
        if($Object['seo_id_adm_reg']!=NULL)                                                                          // 9
        {
            $AdmRegion=GetRowFromeTable("site_regions_adm","sra_id",$Object['seo_id_adm_reg']);
            $arrDataResult[]=($AdmRegion!=0)?$AdmRegion['sra_id']." (".iconv('utf-8', 'windows-1251', $AdmRegion['sra_name']).")":"";
        }
        else $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_other_adm_reg']);
        /**************/
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_address']);                                     // 10
        /**************/
        $Type=GetRowFromeTable("site_estate_types","set_id",$Object['seo_id_etype']);   // 11
        $arrDataResult[]=($Type!=0)? $Type['set_id']." (".iconv('utf-8', 'windows-1251', $Type['set_name']).")":"";
        /**************/
        if($Object['seo_id_vid']!=NULL)                                                 // 12
        {
            $Vid=GetRowFromeTable("site_estate_vid","sev_id",$Object['seo_id_vid']);
            $arrDataResult[]=($Vid!=0)?$Vid['sev_id']." (".iconv('utf-8', 'windows-1251', $Vid['sev_name']).")":"";
        }
        else $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_other_vid']);
        /**************/

        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_other_etap']);                                     // 13
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_etap_date']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_sq_zem_uch']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_sq_zastr']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_sq_total']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_etazh']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_karkas']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_fundament']);                                  // 20
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_steny']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_windows']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_roof']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_floor']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_doors']);                              // 25
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_pravo_doc']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_period_prj']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_period_bld']);
        $arrDataResult[]=iconv('utf-8', 'windows-1251', $Object['seo_bld_volume']);

        foreach($arrDataResult as $DataResult)
            $str.= preg_replace( "/[\r]|[\n]|\;/", " ", $DataResult ).";";
        unset($arrDataResult);

        if(!$pFile) {
            echo "ОШИБКА: не могу создать или открыть файл $file_name!<br />\n";
            echo "<a href='".basename(__FILE__)."'>".basename(__FILE__)."</a><br />\n";
            echo "<br></body></html>";
            exit;
        }

        fwrite($pFile, $str."\n");
        unset($str);
    }
    fclose($pFile);

    @chmod($file_name, 0664);

    /* Если всё окей, ссылку */
    $date_time = date('H:i:s');
    $filesize = filesize($file_name);

    $filesize = number_format( ($filesize/1024)/1024, 2, '.', '');
    $sizeRows = number_format( $rows, 0, '.', ' ');
echo <<<_OUT_
    <p><a href="?action=download" target=_blank>скачать файл</a> (создано в $date_time, размер: $filesize Mb, кол-во строк: $sizeRows)</p>
    <p><a href="allobjects.php">Запустить процесс заново</a></p>
_OUT_;

    include_once "lib/cls.mail.php";
    $objMail = new clsMail();

    $sendFrom = "info@phpwebstudio.com";
    $sendTo = "jimmy.webstudio@gmail.com";
    $sendSubject = "Petrostroybaza Report: allobjects process run.";
    $sendMessage = "Скачано с IP: ".$_SERVER['REMOTE_ADDR'];

    $objMail->new_mail( $sendFrom, $sendTo, $sendSubject, $sendMessage );
    if ( !$objMail->send() ) {
        echo "<!-- ERROR: can't send mail -->";
    }
} else {
echo <<<_OUT_
    <p>это может занять более 5 минут &gt; <a href="?action=create">начать</a></p>
    <p>&#9733; Рекомендация &#9733;<br />&rarr; не делайте выгрузку в часы-пик.</p>
_OUT_;
}

//Функция для получения записи из таблицы $tableName для которой имя поля($key) равен значению $param
function GetRowFromeTable($tableName, $key,$param)
{
    $result=mysql_query("SELECT * FROM $tableName WHERE $key=$param");
    $row=mysql_fetch_array($result,1);
    return is_array($row)? $row:0;
}
?>
</body></html>
