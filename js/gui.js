/********************************************************
 * Created by Shirokovskiy D. jimmy.webstudio@gmail.com
 *
 */

function getClickURL() {
	if (self.clickURL) return clickURL;
	return document.location.href;
}

function getClickTitle() {
	if (self.clickTitle) return clickTitle;
	return document.title;
}

function jsShowModal(strPagePath, intWidth, intHeight, stScroll, stResize, strWinName) {
  var scroll = stScroll;
  var resize = stResize;

  if ( isEmpty(strWinName) )
    strWinName = 'winModal';

  if ( isEmpty(resize) )
    resize = 'yes';

  var top=0, left=0;
  if(intWidth > screen.width-10 || intHeight > screen.height-28) {
    scroll = 'yes';
  }

  if ( intHeight < screen.height-28 )
    top = Math.floor((screen.height - intHeight)/2-14);

  if ( intWidth < screen.width-10 )
    left = Math.floor((screen.width - intWidth)/2);

  intWidth = Math.min(intWidth, screen.width-10);
  intHeight = Math.min(intHeight, screen.height-28);
  window.open(strPagePath, strWinName, 'menubar=no, scrollbars='+scroll+', resizable='+resize+', width='+intWidth+', height='+intHeight+', left='+left+', top='+top);
}

/** ********** Created by Jimmy 2006.11.25 22:45 **************************
* Закрывание модального окна с предварительной перезагрузкой родительского
**************************************************************************/
function doClose( iTime, obj ) {
  var objHrefReload;

  iTime = parseInt(iTime);

  if( !iTime ) {
    iTime = 1000;
  }

  if( !obj ) {
    obj = window.opener;
  }

  objHrefReload = obj.location.href;
  obj.location.reload();
  setTimeout(window.close(), iTime);
}

function showBlockSize( obj ) {
  alert(obj.clientWidth);
}

function chImg ( obj, turn ) {
  if (obj && turn) {
    if ( turn == 'on' ) {
      obj.src = str_replace( '.gif', '_.gif', obj.src );
    } else {
      obj.src = str_replace( '_.gif', '.gif', obj.src );
    }
  }
}

function str_replace(search, replace, subject) {
  return subject.split(search).join(replace);
}

function isEmail(str) {
  if (!reSupport) { return (str.indexOf(".") > 2) && (str.indexOf("@") > 0); }
  var r1 = new RegExp("(@.*@)|(\\.\\.)|(@\\.)|(^\\.)");
  var r2 = new RegExp("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
  return (!r1.test(str) && r2.test(str));
}

function hideBlock( obj_id ) {
  if ( document.getElementById( obj_id ) ) {
    document.getElementById( obj_id ).style.display = 'none';
  }
}

function showBlock( obj_id, type ) {
  if (type == undefined) type = 'block';
  if ( document.getElementById( obj_id ) ) {
    document.getElementById( obj_id ).style.display = type;
  }
}

function HideShow ( el ) {
  if (document.getElementById(el).style.display != 'none') {
    document.getElementById(el).style.display = 'none';
  } else {
    document.getElementById(el).style.display = 'block';
  }
}

function reloadPage() {
  window.location.reload();
}

function setFocusNext( obj, i, obj_id ) {
  if (obj.value.length >= i)
    document.getElementById(obj_id).focus();
}

function setValue( id, val ) {
  document.getElementById( id ).value = val;
}

function ElementDisabled( obj, status ) {
  obj.disabled = status;
}

function ReturnValueCbx( obj ) {
  obj.checked = !obj.checked;
}

function getSelectBoxValue ( tagID ) {
  if (document.getElementById( tagID ))
    return document.getElementById( tagID ).options[document.getElementById( tagID ).selectedIndex].value;
  else
    alert ('Объект не найден!');
  return false;
}

function getValue( tagID ) {
  if (document.getElementById( tagID ))
    return document.getElementById( tagID ).value;
  else
    alert ('Объект не найден!');
  return false;
}

/**
 * Get unique value
 *
 * @return {Number}
 */
function getUnique() {var date = new Date();return (Math.round(Math.random() * date.getTime()) +1)}

/** ********** Created by Jimmy 2006.04.23 16:35 **************************
 * Подтверждение на удаление, с переходом по ссылке
 **************************************************************************/
function confirmDelete(url, msgtxt) {
    if( !msgtxt ) {
        msgtxt = '';
    }
    if(confirm("Вы действительно хотите УДАЛИТЬ запись?"+"\n"+msgtxt)) {
        if ( !isEmpty(url) ) {
            gotoUrl(url);
        }
    }
}

/** ********** Created by Jimmy 2008.05.06 15:23 **************************
 * Подтверждение действия, с переходом по ссылке
 **************************************************************************/
function confirmAction(url, msgtxt) {
    if( !msgtxt ) {
        msgtxt = '';
    }
    if( confirm(msgtxt+"\n"+"Вы уверены?") ) {
        if ( !isEmpty(url) ) {
            gotoUrl(url);
        }
    }
}

/** ********** Created by Jimmy 2009.03.31 16:29 **************************
 * Переход по ссылке
 **************************************************************************/
function gotoUrl( url ) {
    window.location.href=url;
}

/* Проверка правильно написанного Email */
function isEmail(str) {
    if (!reSupport) { return (str.indexOf(".") > 2) && (str.indexOf("@") > 0); }
    var r1 = new RegExp("(@.*@)|(\\.\\.)|(@\\.)|(^\\.)");
    var r2 = new RegExp("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
    return (!r1.test(str) && r2.test(str));
}

/**
 * Checks if variable is empty
 *
 * @param el
 * @return {Boolean}
 * @author Dimitry Shirokovskiy
 */
function isEmpty(el) {
    if (typeof(el) === 'undefined' || el == '' || el == null || el == false) {
        return true;
    }
    return false;
}

function insertParam(key, value)
{
	key = encodeURI(key); value = encodeURI(value);

	var kvp = document.location.search.substr(1).split('&');

	var i=kvp.length; var x; while(i--)
	{
		x = kvp[i].split('=');

		if (x[0]==key)
		{
			x[1] = value;
			kvp[i] = x.join('=');
			break;
		}
	}

	if(i<0) {kvp[kvp.length] = [key,value].join('=');}

	//this will reload the page, it's likely better to store this until finished
	document.location.search = kvp.join('&');
}

function showEstateObjectMap(address) {
	var myMap = new ymaps.Map('map', {
		center: [60.000000, 30.000000], // Saint-Petersburg
		zoom: 10
	});

	// Поиск координат центра Нижнего Новгорода.
	ymaps.geocode(address, {
		/**
		 * Опции запроса
		 * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
		 */
		// boundedBy: myMap.getBounds(), // Сортировка результатов от центра окна карты
		// strictBounds: true, // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy
		results: 1 // Если нужен только один результат, экономим трафик пользователей
	}).then(function (res) {
		//console.log('res', res);
		// Выбираем первый результат геокодирования.
		var firstGeoObject = res.geoObjects.get(0),
		// Координаты геообъекта.
		//	coords = firstGeoObject.geometry.getCoordinates(),
		// Область видимости геообъекта.
        bounds = firstGeoObject.properties.get('boundedBy');

		// Добавляем первый найденный геообъект на карту.
		myMap.geoObjects.add(firstGeoObject);
		// Масштабируем карту на область видимости геообъекта.
		myMap.setBounds(bounds, {
			checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
		});

		/**
		 * Все данные в виде javascript-объекта.
		 */
//            console.log('Все данные геообъекта: ', firstGeoObject.properties.getAll());
		/**
		 * Метаданные запроса и ответа геокодера.
		 * @see http://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderResponseMetaData.xml
		 */
//            console.log('Метаданные ответа геокодера: ', res.metaData);
		/**
		 * Метаданные геокодера, возвращаемые для найденного объекта.
		 * @see http://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderMetaData.xml
		 */
//            console.log('Метаданные геокодера: ', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData'));
		/**
		 * Точность ответа (precision) возвращается только для домов.
		 * @see http://api.yandex.ru/maps/doc/geocoder/desc/reference/precision.xml
		 */
//            console.log('precision', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.precision'));
		/**
		 * Тип найденного объекта (kind).
		 * @see http://api.yandex.ru/maps/doc/geocoder/desc/reference/kind.xml
		 */
//            console.log('Тип геообъекта: %s', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
//            console.log('Название объекта: %s', firstGeoObject.properties.get('name'));
//            console.log('Описание объекта: %s', firstGeoObject.properties.get('description'));
//            console.log('Полное описание объекта: %s', firstGeoObject.properties.get('text'));

		/**
		 * Если нужно добавить по найденным геокодером координатам метку со своими стилями и контентом балуна, создаем новую метку по координатам найденной и добавляем ее на карту вместо найденной.
		 */
		/**
		 var myPlacemark = new ymaps.Placemark(coords, {
             iconContent: 'моя метка',
             balloonContent: 'Содержимое балуна <strong>моей метки</strong>'
             }, {
             preset: 'islands#violetStretchyIcon'
             });

		 myMap.geoObjects.add(myPlacemark);
		 */
	});
}
