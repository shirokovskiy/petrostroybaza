<?php
include_once("access_conf/web.cfg.php");

////////////////////////////////////////////////////////////////////////////////
// Check time execution
////////////////////////////////////////////////////////////////////////////////
if (isset($_SESSION[$_SERVER['REMOTE_ADDR']]['TIME_EXEC'])) {
	$dt = $objUtil->start_time - $_SESSION[$_SERVER['REMOTE_ADDR']]['TIME_EXEC'];

	if ($dt < 1) {
		$_SESSION[$_SERVER['REMOTE_ADDR']]['EXEC_TIME']++;
	} else {
		$_SESSION[$_SERVER['REMOTE_ADDR']]['EXEC_TIME'] = 0;
	}
}

$_SESSION[$_SERVER['REMOTE_ADDR']]['TIME_EXEC'] = $objUtil->start_time;

if (isset($_SESSION[$_SERVER['REMOTE_ADDR']]['EXEC_TIME']) && $_SESSION[$_SERVER['REMOTE_ADDR']]['EXEC_TIME'] > 7) {
	$_SESSION[$_SERVER['REMOTE_ADDR']]['EXEC_TIME'] = 0;
	$_SESSION[$_SERVER['REMOTE_ADDR']]['NEXT_TIME'] = $objUtil->start_time + 120;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
$objTpl = new Template(SITE_TPL_TPL_DIR);

/**
 * Авторизация пользователя
 */
if (isset($_POST['frmAuthUser']) && $_POST['frmAuthUser']=='true') {
	$objUser = new wbmAuth();

	if ( !$objUser->wbmAuthLogin() ) {
		header("Location: /autherror");
		exit;
	} else {
		if ( preg_match( '/autherror/i', $_SERVER["REQUEST_URI"]) ) {
			header("Location: /");
			exit;
		}
	}
	unset($objUser);
}

/**
 * Выход из авторизации
 */
if (isset($_GET['logout']) && $_GET['logout'] == 1 ) {
	$objUser = new wbmAuth();
	$objUser->wbmLogout();
	$_SERVER['REQUEST_URI'] = preg_replace("/.logout=1/", "", $_SERVER['REQUEST_URI']);
	header("Location: ".$_SERVER['REQUEST_URI']);
	exit;
}

/**
 * Проверка авторизации
 */
$arrIf['authed'] = $_SESSION['signInStatus'];
$arrIf['not.authed'] = !$arrIf['authed'];

if (!is_object($objUser)) {
    $objUser = new User();
}

/**
 * Добавим объект в блокнот
 */
if (isset($_GET['addtonb'])) {
    if ($objUser->isAuthed()) {
        // записать данные
        $objUser->saveObjectToNotebook($_GET['addtonb']);
    }
    $_SERVER['REQUEST_URI'] = preg_replace("/addtonb=".$_GET['addtonb']."/i", "", $_SERVER['REQUEST_URI']);
    if (!preg_match("/stPage/",$_SERVER['REQUEST_URI'])) {
        # Добавить параметр страницы, иначе слетит сессия фильтрации, т.к. будет думать что это первый визит на страницу
        if (preg_match("/\?/", $_SERVER['REQUEST_URI'])) {
            # Проверим, есть ли ещё что-то после знака вопроса
            if (preg_match("/\?.+/", $_SERVER['REQUEST_URI'])) {
                $_SERVER['REQUEST_URI'] .= "&stPage=1";
            } else {
                $_SERVER['REQUEST_URI'] .= "stPage=1";
            }
        } else {
            $_SERVER['REQUEST_URI'] .= "?stPage=1";
        }
    }

    header('Location: '.$_SERVER['REQUEST_URI']);
    exit;
}

/**
 * Удалить объект из блокнота
 */
if (isset($_GET['rmfromnb'])) {
    if ($objUser->isAuthed()) {
        // обновить данные
        $objUser->removeFromNotebook($_GET['rmfromnb']);
    }

    $_SERVER['REQUEST_URI'] = preg_replace("/[\?|\&]rmfromnb=".$_GET['rmfromnb']."/i", "", $_SERVER['REQUEST_URI']);

    header('Location: '.$_SERVER['REQUEST_URI']);
    exit;
}

/**
 * ОБРАБОТКА запроса пользователя
 */
$strTmpReqUri = substr($_SERVER['REQUEST_URI'], 1);

if (strpos($strTmpReqUri, '?')!==false){
	$strReqUri = substr($strTmpReqUri, 0, strpos($strTmpReqUri, '?'));
}else{
	$strReqUri = $strTmpReqUri; //
}

// ** Запрашиваемая страница
if(!empty($strReqUri)) {
	$arrReqUri = explode('/', $strReqUri);
	if (SITE_DIR_FROM_ROOT!='' && (strpos($strReqUri, SITE_DIR_FROM_ROOT) !== false) ) {
		$intCountOffset = count(explode('/', SITE_DIR_FROM_ROOT));
		for ($i=0; $i<$intCountOffset; $i++) {
			array_shift($arrReqUri);
		}
	}
	$strReqUri = (is_array($arrReqUri)) ? $arrReqUri[0] : $arrReqUri;
}

if (empty($strReqUri)){
	$strReqUri = 'index'; // главная страница
}else{
	$strReqUri = str_replace('\'', '', trim($strReqUri));
}

// ** Передаваемые параметры
$arrReqQuery = substr(strstr($strTmpReqUri, '?'), 1);
// **************************************************************************************

$strSqlQuery = "SELECT * FROM `site_pages`, `site_pages_meta` WHERE sp_alias='".$strReqUri."' AND sp_id_project='".SITE_ID_PROJECT."' AND spm_id_rec=sp_id AND spm_subpage_uid IS NULL AND sp_status='Y'";
$arrInfoPage = $objDb->fetch($strSqlQuery);

/** >>************************************************************************\
 ** Thu Nov 16 00:27:08 MSK 2006, Shirokovskiy Dmitry aka Jimmy™
 * Если страница не найдена - пусть все видят! Пусть будет стыдно! Вот! Это 404 ошибка!!! **/
if ( !is_array($arrInfoPage) && empty($arrInfoPage) ) {
	$strSqlQuery = "SELECT * FROM ".$_db_tables['stPages']." WHERE sp_alias='error' AND sp_id_project='".SITE_ID_PROJECT."'";
	$arrInfoPage = $objDb->fetch($strSqlQuery);
}

/**
 * Если существует второй параметр для страницы (элемент массива $arrReqUri[1])
 * пробуем найти информацию о мета-тегах страницы по этому параметру.
 * /
if ( !empty($arrReqUri[1]) ) {
$strSqlQuery = "SELECT * FROM site_pages_meta WHERE spm_id_rec='".$arrInfoPage['sp_id']."' AND spm_subpage_uid='".$arrReqUri[1]."'";
$arrInfoPageMeta = $objDb->fetch($strSqlQuery);
if(is_array($arrInfoPageMeta))
$arrInfoPage = array_merge($arrInfoPage, $arrInfoPageMeta);
}*/

/**
 * Запишем в лог URL-запрос
 */
if (preg_match("/(YandexBot|Googlebot|Mail\.Ru|Baiduspider|SearchBot|bingbot)/i", $_SERVER['HTTP_USER_AGENT'])){
	# исключения для поисковиков, часто посещающих сайт
} else {
	/*$strUserAgent = mysql_real_escape_string($_SERVER['HTTP_USER_AGENT']);
	if(empty($strUserAgent)) $strUserAgent = mysql_real_escape_string( implode("~~~~~~~", $_SERVER) );
	$strSqlQuery = "INSERT INTO `site_pages_url_logs` SET"
		. " spul_url = '" .mysql_real_escape_string($_SERVER['REQUEST_URI'])."'"
		. ", spul_ip = '" .$_SERVER['REMOTE_ADDR']."'"
		. ", spul_agent = '" .$strUserAgent."'"
	;
	if (!$objDb->query($strSqlQuery)) {
		# sql error
	}*/
}

/**
 * Делаем исключение для загрузки библиотеки Яндекс.Карт
 */
$arrIf['is.maps.pages'] = in_array($arrReqUri[0], array('objects', 'objectinfo'));

// ***** Если страница не имеет обработчика тегов, ставим теги принадлежащие ей
//if( is_null($arrInfoPage['sp_meta_method']) ) {
$arrTplVars['m_title'] = htmlspecialchars($arrInfoPage['spm_title']).' | PetroStroyBaza.RU';
$arrTplVars['m_description'] = htmlspecialchars($arrInfoPage['spm_description']);
$arrTplVars['m_keywords'] = htmlspecialchars($arrInfoPage['spm_keywords']);
//}

// Загрузка шаблона
$objTpl->tpl_load("global.tpl", "site.global.".$arrInfoPage['sp_id_template'].".tpl");
$objTpl->tpl_parse_frg($objTpl->files['global.tpl']); // определяем нужные фрагменты для загрузки

// Загрузка обработчика страницы
require(SITE_PHP_PAGE_DIR.$arrInfoPage['sp_alias'].".".$arrInfoPage['sp_id'].".php");
$objTpl->tpl_parse_frg($objTpl->files['page.contents']); // определяем нужные фрагменты для загрузки

$objTpl->Template(SITE_TPL_FRG_DIR);
do {
	$strCurrentFrg = current($objTpl->arrFrgForLoad);
	if (!empty($strCurrentFrg)) {
		$objTpl->tpl_load($strCurrentFrg, $strCurrentFrg.".frg");
		$objTpl->tpl_parse_frg($objTpl->files[$strCurrentFrg]);
	}
} while (next($objTpl->arrFrgForLoad));

foreach($objTpl->arrFrgForLoad as $key=>$value) {
	if (file_exists(SITE_PHP_FRG_DIR.$value.".inc.php"))
		include_once(SITE_PHP_FRG_DIR.$value.".inc.php"); // подгружаем обработчики найденных фрагментов
}

////////////////////////////////////////////////////////////////////////////////
// Check time execution
////////////////////////////////////////////////////////////////////////////////
if ($objUtil->start_time <= $_SESSION[$_SERVER['REMOTE_ADDR']]['NEXT_TIME']) {
	echo $objTpl->files['page.header'];
	echo "<font size=3>Вы слишком часто обращаетесь к сайту!"; echo "<br />\n";
	echo("Попробуйте через <b>".(int)($_SESSION[$_SERVER['REMOTE_ADDR']]['NEXT_TIME'] - $objUtil->start_time)."</b> секунд!"); echo "<br />\n";
	die();
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

$objTpl->tpl_array('global.tpl', $arrTplVars);

if ($arrInfoPage['sp_alias'] == 'error') {
	header('HTTP/1.0 404 Not Found');
	header('HTTP/1.1 404 Not Found');
	header('Status: 404 Not Found');
}

if (isset($_SESSION['SF']) && $arrReqUri[0] != "search" && $arrReqUri[0] != "objectinfo" ) {
	//unset($_SESSION['SF']);
}

$objTpl->tpl_parse('global.tpl');
$objDb->disconnect();


echo '<!-- '.$objUtil->stop().' -->', "\n", '<!-- '.$objUtil->getSqlQuery($objDb).' -->';
