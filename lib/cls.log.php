<?php
// *******************************************************************
// *  Класс ведения логов по работе с записями таблиц                *
// *                                                                 *
// *                                                                 *
// *  v. 2.0 for FCM                                                 *
// *  Create: 08.12.2004                                             *
// *  Author: FaKiR™                                                 *
// *******************************************************************

class fcmLogAction {
  var $objDb;
  var $objUtil;
  var $_db_tables;

// Конструктор собственной персоной
  function fcmLogAction(&$objDb, &$objUtil, $_db_tables) {
    $this->objDb = $objDb;
    $this->objUtil = $objUtil;
    $this->_db_tables = $_db_tables;
  }

// Добавление события
  function addLog($tableName, $recId, $action="add", $actionDesc="", $typeAction="direct") {
    if (is_array($recId)) {
      if($action=='update')
        $this->objDb->query("UPDATE ".$this->_db_tables['fcmLog']." SET fla_status='old' WHERE fla_action='update' AND fla_rec_id IN (".implode(",", $recId).")");

      foreach($recId as $key=>$value)
        $this->objDb->query("INSERT INTO ".$this->_db_tables['fcmLog']." (fla_datetime, fla_rec_id, fla_user_id, fla_table, fla_action, fla_type_action, fla_id_session, fla_action_desc) VALUES (NOW(), '".$value."', '".$_SESSION['user_sess']['id_user']."', '".$tableName."', '".$action."', '".$typeAction."', '".session_id()."', '".$actionDesc."')");

    } else {
      if($action=='update')
        $this->objDb->query("UPDATE ".$this->_db_tables['fcmLog']." SET fla_status='old' WHERE fla_action='update' AND fla_rec_id='".$recId."'");

      $strSqlQuery = "INSERT INTO ".$this->_db_tables['fcmLog']." (fla_datetime, fla_rec_id, fla_user_id, fla_table, fla_action, fla_type_action, fla_id_session, fla_action_desc) VALUES (NOW(), '".$recId."', '".$_SESSION['user_sess']['id_user']."', '".$tableName."', '".$action."', '".$typeAction."', '".session_id()."', '".$actionDesc."')";
      $this->objDb->query($strSqlQuery);
    }
    return true;
  }

  function read_log($strTable, $arrIdRec) {
    if(is_array($arrIdRec)) {
      $arrNoSortLogRec = $this->objDb->fetchall($this->objDb->query("SELECT * FROM ".$this->_db_tables['fcmLog']." WHERE fla_table='".$strTable."' AND fla_rec_id IN (".implode(",", $arrIdRec).") AND fla_status='last'"));
      $this->getUsersAdmin();
      if ( empty( $arrNoSortLogRec ) ) { // by Jimmy™ 11.10.2005
        return false;
      }
      foreach($arrNoSortLogRec as $key=>$value) {
        $arrSortLogRec[$arrNoSortLogRec[$key]['fla_rec_id']][$arrNoSortLogRec[$key]['fla_action']]['name'] = $this->arrUsers[$arrNoSortLogRec[$key]['fla_rec_id']]['name'].' ('.$this->objUtil->workDate(6, $arrNoSortLogRec[$key]['fla_datetime']).')';
        $arrSortLogRec[$arrNoSortLogRec[$key]['fla_rec_id']][$arrNoSortLogRec[$key]['fla_action']]['id'] = $this->arrUsers[$arrNoSortLogRec[$key]['fla_rec_id']]['id'];
        $arrSortLogRec[$arrNoSortLogRec[$key]['fla_rec_id']][$arrNoSortLogRec[$key]['fla_action']]['userGender'] = $this->arrUsers[$arrNoSortLogRec[$key]['fla_rec_id']]['gender'];
      }
      return $arrSortLogRec;
    } else
      return false;
  }

  function getUsersAdmin() {
    $arrNoSortUsers = $this->objDb->fetchall($this->objDb->query("SELECT id_user, f_name_user, l_name_user, u_gender FROM ".$this->_db_tables['cmsUsers']));
    foreach ($arrNoSortUsers as $key=>$value) {
      $this->arrUsers[$arrNoSortUsers[$key]['id_user']]['id'] = $arrNoSortUsers[$key]['id_user'];
      $this->arrUsers[$arrNoSortUsers[$key]['id_user']]['name'] = $arrNoSortUsers[$key]['l_name_user'].' '.$arrNoSortUsers[$key]['f_name_user'];
      $this->arrUsers[$arrNoSortUsers[$key]['id_user']]['gender'] = $arrNoSortUsers[$key]['u_gender'];
    }
  }

  function getUserInfoById($userId) {
    return $this->objDb->fetch($this->objDb->query("SELECT id_user, f_name_user, l_name_user, u_gender FROM ".$this->admin_table." WHERE id_user='".$userId."'"));
  }
}