<?php
/**
 * Split string into small strings divided by char-separator
 *
 * @param $str
 * @param int $n
 * @param string $char
 * @return string
 */
function split_string ( $str, $n = 10, $char = ' ' ) {
	$c = 0;
	$nstr = "";
	for ($i=0;$i<strlen($str);$i++) {
		$nstr .= $str[$i];$c++;
		if ($c==$n) {
			$nstr .= $char;
			$c = 0;
		}
	}

	return $nstr;
}

function trace_log($string, $filename = 'debug.log', $noTimeInFilename=true, $pre = '') {
	$dbgTrace = debug_backtrace();
	$logDir = PRJ_LOGS;
	if ( !is_dir($logDir) ) {
		mkdir($logDir, 0777);
	}
	if (!preg_match("/\.htm.?$/i", $filename) && !preg_match("/\.[log|txt|sql]/i", $filename)) {
		$filename .= '.log';
	}
	$fh = fopen($logDir.'/'.($noTimeInFilename==false ? microtime(true).'.' : '').$filename, "a+");

	ob_start();
	print_r($string);
	$debug_content = ob_get_contents();
	ob_clean();

	if (preg_match("/\.htm.?$/i", $filename)) {
		$output = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><title> DEBUG OUTPUT ".date('d.m.Y H:i:s')." [".microtime(true)."]</title></head>\n";
		$output.= "<body><div style='width:800px;color:#333'>".nl2br($debug_content)."</div></body></html>\n";
	} else {
		$output = $pre."DEBUG OUTPUT ".date('d.m.Y H:i:s')." [".microtime(true)."]\n";
		$output.= "Called from: ".(isset($dbgTrace[0]['file']) ? $dbgTrace[0]['file']:'file unknown').':'.(isset($dbgTrace[0]['line']) ? $dbgTrace[0]['line'] : '?')."\n\n";
		$output.= $debug_content." \n\n";
	}

	fwrite($fh, $output, strlen($output));
	fclose($fh);
}

function getDebugBacktrace($NL = "\n") {
	$dbgTrace = debug_backtrace();
	$tab = '';
	$dbgMsg = $NL."Debug backtrace begin:$NL";
	$strArgs = '';
	$arrArgs = array();
	$intArgStringLenght = 30;
	foreach($dbgTrace as $dbgIndex => $dbgInfo) {
		if (is_array($dbgInfo['args'])) {
			foreach($dbgInfo['args'] as $arg) {
				if ( is_string($arg) && strlen($arg) > $intArgStringLenght ) {
					$intPossibleArgLenght = strpos($arg, "\n");
					if ($intPossibleArgLenght !== false) {
						$intArgStringLenght = $intPossibleArgLenght;
					}
					$arrArgs[] = substr( $arg, 0, $intArgStringLenght )."... [$intArgStringLenght chars and more text]";
				} else {
					$arrArgs[] = $arg;
				}
			}
			$strArgs = implode(", ", $arrArgs);
		}
		$dbgMsg .= "$tab ($dbgIndex) ".( isset($dbgInfo['class']) ? 'class '.$dbgInfo['class'].'->' : 'function ' ).$dbgInfo['function']."( $strArgs ), from: ".(isset($dbgInfo['file'])?$dbgInfo['file']:'').":".(isset($dbgInfo['line'])?$dbgInfo['line']:'')."$NL $NL";
		$tab .= "\t";
	}
	$dbgMsg .= "Debug backtrace end".$NL;
	return $dbgMsg;
}

function getBrowser( $brawser = null ) {
	if ( is_null($brawser) ) {
		$brawser = $_SERVER['HTTP_USER_AGENT'];
	}

	if ( preg_match( "/Firefox\/[\d\.]+/", $brawser, $match) ) {

		$agent = ($match[0] ? $match[0] : 'Firefox');

	} else if ( preg_match( "/Chrome\/[\d\.]+/", $brawser, $match) ) {

		$agent = ($match[0] ? $match[0] : 'Chrome');

	} else if ( preg_match( "/Opera/", $brawser) ) {

		preg_match("/Version\/([\d\.]+)/", $brawser, $ver);
		$agent = 'Opera/'.($ver[1] ? $ver[1] : substr($brawser, 0, strpos($brawser, ' (')));

	} else if ( preg_match( "/Safari/", $brawser) ) {

		preg_match("/Version\/([\d\.]+)/", $brawser, $ver);
		$agent = 'Safari/'.($ver[1] ? $ver[1] : substr($brawser, strpos($brawser, 'Safari')));

	} else if ( preg_match( "/MSIE[^;]+/", $brawser, $matches) ) {

		$agent = isset($matches[0]) ? $matches[0] : 'MSIE';
	} else {

		$agent = $brawser;
	}

	return $agent;
}

function getUrlContents($url)
{
    $crl = curl_init();
    $timeout = 5;
    curl_setopt ($crl, CURLOPT_URL, $url);
    curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
    $ret = curl_exec($crl);
    curl_close($crl);
    return $ret;
}

if (!function_exists("mysql_real_escape_string")) {
    function mysql_real_escape_string($string) {
        $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
        $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

        return str_replace($search, $replace, $string);
    }
}

if (!function_exists("mysql_close")) {
    function mysql_close($connection) {
        return mysqli_close($connection);
    }
}
