<?php
/**
 * Получение административных районов по заданным ID регионов
 */
include_once("../../access_conf/web.cfg.php");
include_once("../../lib/funcs.php");

//if ($_SERVER['_IS_DEVELOPER_MODE']==1)
//trace_log($_REQUEST);


if ($_POST['get']=='admreg') {
    $arrJSON['regions'] = array();
    $arrGivenRegs = $_POST['rid'];
    if (is_array($arrGivenRegs) && !empty($arrGivenRegs)) {
        $strSqlQuery = "SELECT * FROM `site_regions_adm` WHERE `sra_sr_id` IN (".implode(",",$arrGivenRegs).") AND sra_status = 'Y' ORDER BY sra_sr_id, sra_name";
        $arrRegionsAdm = $objDb->fetchall($strSqlQuery);
        if (is_array($arrRegionsAdm) && !empty($arrRegionsAdm)) {
            $arrJSON['regions'] = $arrRegionsAdm;
        }
    } else {
        $arrJSON['err'] = 102;
    }
} else {
    $arrJSON['err'] = 101;
}

$json = json_encode($arrJSON);
echo $json;
