<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 10/9/13
 * Time         : 5:51 PM
 * Description  :
 */
include_once "funcs.php";
class User extends Db {
    private $id;

    public function __construct($id = 0)
    {
        parent::__construct();

        if ($id > 0) {
            $this->setId($id);
        } else {
            #
            if (isset($_SESSION['uAuthInfo']['uID']) && intval($_SESSION['uAuthInfo']['uID'])) {
                $this->id = $_SESSION['uAuthInfo']['uID'];
            }
        }
    }

    public function isAuthed()
    {
        return $this->id > 0 && isset($_SESSION['uAuthInfo']['uID']) && $_SESSION['uAuthInfo']['uID'] == $this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        if ($id > 0) {
            $this->id = $id;
        }
        return $this;
    }

    public function getData()
    {
        $strSqlQuery = "SELECT * FROM `site_users` WHERE `su_id` = " . $this->getId();
        return $this->fetch($strSqlQuery);
    }

    public function saveObjectToNotebook($idObject = 0, $note= '')
    {
        if ($idObject <= 0) return false;
        if ($this->id <= 0) return false;

        $strSqlQuery = "REPLACE INTO `site_users_notebook_lnk` SET"
            . " sunl_su_id = ".intval($this->id)
            . ", sunl_seo_id = ".intval($idObject)
            . (!empty($note) ? ", sunl_note = '".mysql_real_escape_string($note)."'" : '' )
        ;
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function isInNotebook($idObject = 0)
    {
        if (intval($idObject) <= 0) return false;
        $strSqlQuery = "SELECT * FROM `site_users_notebook_lnk` WHERE `sunl_su_id` = ".$this->id." AND `sunl_seo_id` = ".intval($idObject);
        return $this->fetch($strSqlQuery);
    }

    public function removeFromNotebook($idObject = 0)
    {
        if ($idObject <= 0) return false;
        if ($this->id <= 0) return false;

        $strSqlQuery = "DELETE FROM `site_users_notebook_lnk` WHERE sunl_su_id = ".$this->id." AND sunl_seo_id = ".$idObject;
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }

    public function saveCommentForObject($id, $comment)
    {
        $comment = trim($comment);
        if ($id <= 0) return false;
        if (empty($comment)) return false;

        /**
         * Check if user has Agree flag for comments
         */
        $strSqlQuery = "SELECT `su_comments_agree` FROM `site_users` WHERE `su_id` = " . $this->getId();
        $su_comments_agree = $this->fetch($strSqlQuery, 'su_comments_agree');

        if ($su_comments_agree == 'Y') {
            $strSqlQuery = "INSERT INTO `site_users_comments` SET"
                . " `suc_su_id` = ".$this->getId()
                . ", `suc_seo_id` = ".$id
                . ", `suc_comment` = '".mysql_real_escape_string($comment)."'";
            if (!$this->query($strSqlQuery)) {
                # sql error
                return false;
            } else {

                if ($this->getId() != 5508) // administrator
                {
                    // Send E-mail to Administrator
                    $objMail = new clsMail();
                    $sendTo = "tm@petrostroybaza.ru";
                    $sendFrom = SITE_ADMIN_MAIL;
                    $sendSubject = "PetroStroyBaza | Новый комментарий";
                    $sendMessage = "Уважаемый администратор сайта PetroStroyBaza.ru!\n";
                    $sendMessage .= "На сайте появился новый комментарий к объекту (ID: $id) \n";
                    $sendMessage .= "[".SITE_URL."objectinfo/$id]\n";
                    $sendMessage .= "~~~\n";
                    $sendMessage .= strip_tags($comment)."\n";
                    $sendMessage .= "~~~\n\n";
                    $sendMessage .= "Перейдите по ссылке для просмотра и ответа \n";
                    $sendMessage .= ADMINURL."object.comments?id=$id&stPage=1\n";
                    $sendMessage .= "\n\n===============================================================\n\n";
                    $sendMessage .= "Не отвечайте на данное письмо! Текст сформирован автоматически почтовым роботом.\n";

                    $sendHTMLMessage = file_get_contents(SITE_TPL_DIR.'emails/new.comment.html');
                    $linkToComment = SITE_URL.'objectinfo/'.$id;
                    $linkToEditComment = ADMINURL."object.comments?id=$id&stPage=1";
                    $sendHTMLMessage = preg_replace("/\{\%LINK_COMMENT\%\}/", $linkToComment, $sendHTMLMessage);
                    $sendHTMLMessage = preg_replace("/\{\%LINK_EDIT\%\}/", $linkToEditComment, $sendHTMLMessage);
                    $sendHTMLMessage = preg_replace("/\{\%COMMENT\%\}/", strip_tags($comment), $sendHTMLMessage);
                    $sendHTMLMessage = preg_replace("/\{\%ID\%\}/", $id, $sendHTMLMessage);

                    $sendHTMLMessage = preg_replace("/\n/", "\r\n", $sendHTMLMessage);
                    if (isset($_SERVER['_IS_DEVELOPER_MODE']) && $_SERVER['_IS_DEVELOPER_MODE'] == 1) {
                        $sendTo = "jimmy.webstudio@gmail.com"; // DEBUG
                    }

                    $objMail->new_mail( $sendFrom, $sendTo, $sendSubject, $sendMessage, $sendHTMLMessage );
                    if ($sendTo != "jimmy.webstudio@gmail.com") {
                        $objMail->add_bcc( "jimmy.webstudio@gmail.com" );
                        $objMail->add_bcc( "tmaksimova@gmail.com" );
                    }

                    $objMail->add_attachment( PRJ_SITEIMG.'content/email-bg.jpg', 'email-bg.jpg', 'image' );

                    if ( !$objMail->send() ) {
                        trace_log("Не смог отправить E-mail о новом комментарии");
                    }
                }

                return $this->insert_id();
            }
        }

        return false;
    }

    public function setCommentsAgreement()
    {
        $strSqlQuery = "UPDATE `site_users` SET"
            . " `su_comments_agree` = 'Y'"
            . " WHERE `su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        if (!isset($_SESSION['uAuthInfo']['uCommentsAgree'])) {
            $_SESSION['uAuthInfo']['uCommentsAgree'] = 'Y';
        }

        return true;
    }

    public function setCommentsAnswersAgreement($objectId)
    {
        if (!$this->isCommentsAnswersAgreement($objectId)) {
            $strSqlQuery = "INSERT INTO `site_users_comments_subs` SET"
                . " `sucs_su_id` = ". $this->getId()
                . ", `sucs_seo_id` = ".$objectId;
            if (!$this->query($strSqlQuery)) {
                # sql error
                return false;
            }
        }

        return true;
    }

    public function isCommentsAnswersAgreement($objectId)
    {
        $strSqlQuery = "SELECT * FROM `site_users_comments_subs` WHERE `sucs_su_id` = ".$this->getId()." AND `sucs_seo_id` = ".$objectId;
        $arr = $this->fetch($strSqlQuery);
        return is_array($arr) && !empty($arr);
    }

    public function removeCommentsAnswersAgreement($objectId)
    {
        $strSqlQuery = "DELETE FROM `site_users_comments_subs` WHERE `sucs_su_id` = ". $this->getId()." AND `sucs_seo_id` = ".$objectId;
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }

    public function createAutoLoginSign()
    {
        $arr = $this->getData();

        if (is_array($arr) && !empty($arr)) {
            $strSqlQuery = "UPDATE `site_users` SET"
                . " `su_autologin` = '".md5( md5( $arr['su_login']. $this->getId() ). $arr['su_email'] )."'" // ALTER TABLE `site_users` ADD `su_autologin` VARCHAR(32) NULL DEFAULT NULL COMMENT 'для автологина' ;
                . " WHERE `su_id` = ". $this->getId();
            if (!$this->query($strSqlQuery)) {
                # sql error
                return false;
            }
            return true;
        }
        return false;
    }

    public function autoLogin($hash)
    {
        $strSqlQuery = "SELECT * FROM `site_users` WHERE `su_autologin` = '" . mysql_real_escape_string($hash) ."'";
        $user = $this->fetch($strSqlQuery);

        if (is_array($user) && !empty($user)) {
            # Secret Code
            $code = (!empty($_SESSION["strLoginCode"])) ? $_SESSION["strLoginCode"] : md5(date("login %H:%i %d.%m.%Y"));

            $Auth = new wbmAuth();
            $arrUserLoginStatus = $Auth->wbmLoadUser($user['su_id']);

            if (is_array($arrUserLoginStatus) && $Auth->uID > 0) {
                unset($_SESSION["uAuthInfo"]);

                $_SESSION['signInStatus'] = true;

                setcookie("signInStatus", (empty($_SESSION["strLoginCode"]) ? $_SESSION["strLoginCode"] : $code), strtotime("+24 hours"));

                $_SESSION["strLoginCode"] = $code;
                $_SESSION['uAuthInfo'] = $arrUserLoginStatus;

                return true;
            }
        }

        return false;
    }
}
