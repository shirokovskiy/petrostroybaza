<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 10/10/13
 * Time         : 6:37 PM
 * Description  :
 */

class Spamer extends Db {

    public function getTemplatesList()
    {

    }

    public function getTemplateProcess($id)
    {
        $strSqlQuery = "SELECT t1.*, t2.susg_title FROM `subscribers_cronjobs` t1 LEFT JOIN site_users_subscriber_groups t2 ON (sc_group_id = susg_id) WHERE `sc_template_id` = ".$id." ORDER BY sc_id DESC LIMIT 1";
        return $this->fetch($strSqlQuery);
    }

    public function inProgress($tId, $gId = null)
    {
        $strSqlQuery = "SELECT t1.*, t2.susg_title FROM `subscribers_cronjobs` t1 LEFT JOIN site_users_subscriber_groups t2 ON (sc_group_id = susg_id) WHERE `sc_template_id` = ".$tId.($gId>0?" AND `sc_group_id` = ".$gId:"")." AND sc_date_finish IS NULL AND sc_progress_pause IS NULL";
        return $this->fetchall($strSqlQuery);
    }

    public function startProcess($tId, $gId)
    {
        if ($tId > 0 && $gId > 0) {
            if ( $this->inProgress($tId, $gId) != null ) {
                // есть запущенный процесс

            } else {
                // создадим процесс
                $strSqlQuery = "INSERT INTO `subscribers_cronjobs` SET"
                    ." sc_group_id = ".$gId
                    .", sc_template_id = ".$tId
                    .", sc_date_create = UNIX_TIMESTAMP()"
                ;
                if (!$this->query($strSqlQuery)) {
                    # sql error
                } else {
                    $intJobId = $this->insert_id();

                    if ($intJobId > 0) {
                        $strSqlQuery = "INSERT INTO `subscribers_sending` (ss_cronjob_id, ss_subscriber_id) SELECT $intJobId, susgl_sus_id FROM `site_users_subscriber_groups_lnk` WHERE susgl_susg_id = ".$gId;
                        if (!$this->query($strSqlQuery)) {
                            # sql error
                        }

                        return $intJobId;
                    }
                }
            }
        }

        return false;
    }

    public function pauseProcess($id)
    {
        if ($id <= 0) return false;
        $strSqlQuery = "UPDATE `subscribers_cronjobs` SET"
            ." `sc_progress_pause` = 1"
            ." WHERE `sc_id` = ".$id;
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function playProcess($id)
    {
        if ($id <= 0) return false;
        $strSqlQuery = "UPDATE `subscribers_cronjobs` SET"
            ." `sc_progress_pause` = NULL"
            ." WHERE `sc_id` = ".$id;
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function stopProcess($id)
    {
        if ($id <= 0) return false;
        $strSqlQuery = "UPDATE `subscribers_cronjobs` SET"
            ." `sc_progress_pause` = NULL"
            .", `sc_date_finish` = UNIX_TIMESTAMP()"
            ." WHERE `sc_id` = ".$id;
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function getPriorityProcess()
    {
        $strSqlQuery = "SELECT * FROM `subscribers_cronjobs` WHERE sc_progress_pause IS NULL AND sc_date_create <= UNIX_TIMESTAMP() AND sc_date_finish IS NULL ORDER BY sc_id LIMIT 1";
        return $this->fetch($strSqlQuery);
    }

    public function getSubscribers($job_id, $count = 59)
    {
        if ($job_id <= 0 || $count <= 0) return null;
        $strSqlQuery = "SELECT * FROM `subscribers_sending` LEFT JOIN `site_users_subscribers` ON (ss_subscriber_id = sus_id) WHERE `ss_cronjob_id` = ".$job_id." AND sus_id > 0 AND ss_subscriber_status IS NULL LIMIT ".$count;
        return $this->fetchall($strSqlQuery);
    }

    public function getSubscribersCountByProcessId($id)
    {
        if ($id <= 0) return null;
        $strSqlQuery = "SELECT COUNT(*) iC FROM `subscribers_cronjobs` LEFT JOIN `site_users_subscriber_groups_lnk` ON (sc_group_id = susgl_susg_id) WHERE `sc_id` = ".$id;
        return $this->fetch($strSqlQuery, 'iC');
    }

    public function getSubscribersInQueueProcessId($id)
    {
        if ($id <= 0) return null;
        $strSqlQuery = "SELECT COUNT(*) iC FROM `subscribers_sending` WHERE `ss_cronjob_id` = ".$id;
        return $this->fetch($strSqlQuery, 'iC');
    }

    public function getSubscriberEmailInfo($id)
    {
        if ($id <= 0) return null;
        $strSqlQuery = "SELECT * FROM `site_users_subscribers` WHERE `sus_id` = ".$id;
        return $this->fetch($strSqlQuery);
    }

    public function removeSubscriberEmailFromQueue($subscriber_id, $job_id)
    {
        if ($job_id <= 0 || $subscriber_id <= 0) return false;
        $strSqlQuery = "DELETE FROM `subscribers_sending` WHERE ss_subscriber_id = ".$subscriber_id." AND ss_cronjob_id = ".$job_id;
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        } else {
            $strSqlQuery = "UPDATE `subscribers_cronjobs` SET `sc_count_sent` = `sc_count_sent`+1 WHERE `sc_id` = ".$job_id;
            if (!$this->query($strSqlQuery)) {
                # sql error
                return false;
            }
        }
        return true;
    }

    public function getNewsletterProcessInfo($id)
    {
        if ($id <= 0) return null;
        $strSqlQuery = "SELECT * FROM `subscribers_cronjobs` LEFT JOIN `site_templates_email` ON (sc_template_id = ste_id) WHERE `sc_id` = ".$id." AND `ste_id` > 0 LIMIT 1";
        return $this->fetch($strSqlQuery);
    }

    public function markSubscriberInQueue($subscriber_id, $job_id)
    {
        if ($job_id <= 0 || $subscriber_id <= 0) return false;
        $strSqlQuery = "UPDATE `subscribers_sending` SET ss_subscriber_status = UNIX_TIMESTAMP() WHERE ss_subscriber_id = ".$subscriber_id." AND ss_cronjob_id = ".$job_id;
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }
        return true;
    }

    public function removeSubscriberFromGroup($user_id, $group_id)
    {
        if ($user_id <= 0 || $group_id <= 0) return false;
        $strSqlQuery = "DELETE FROM `site_users_subscriber_groups_lnk` WHERE susgl_sus_id = ".$user_id." AND susgl_susg_id = ".$group_id." LIMIT 1";
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }
        return true;
    }

    public function getSubscriberByEmail($email)
    {
        $strSqlQuery = "SELECT * FROM `site_users_subscribers` WHERE `sus_email` LIKE '".$email."'";
        return $this->fetch($strSqlQuery);
    }

    public function removeSubscriber($email)
    {
        $strSqlQuery = "DELETE FROM `site_users_subscribers` WHERE sus_email LIKE '$email'";
        return $this->query($strSqlQuery);
    }
}
