<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 5/26/13
 * Time         : 11:06 PM
 * Description  :
 */

class EstateObject extends Db {
    private $id;
    protected $prefix = 'seo_';
    public $name, $naznach, $id_adm_reg, $other_adm_reg, $address;
    public $id_etype, $id_vid, $other_vid, $other_etap, $etap_date, $sq_zem_uch, $sq_zastr, $sq_total;
    public $etazh, $karkas, $fundament, $steni, $windows, $roof, $floor, $doors, $pravo_doc, $period_prj;
    public $period_bld, $bld_volume, $status, $anons, $date_add, $date_change, $area_lo, $area_spb, $url;

    public function save() {
        if($this->isReadyToSave()) {
            global $_db_config;
            try {
                # create DB instance
                $objDb = new Db($_db_config);
                $strSqlFields = ""
                    . " seo_name                = '{$this->name}'"
                    . ", seo_naznach            = '{$this->naznach}'"
                    . ", seo_id_etype           = '{$this->id_etype}'"
                    . ( !empty($this->id_adm_reg) ? ", seo_id_adm_reg = '{$this->id_adm_reg}'" : '' )
                    . ( !empty($this->other_adm_reg) && empty($this->id_adm_reg) ? ", seo_other_adm_reg = '{$this->other_adm_reg}'" : '' )
                    . ( !empty($this->id_vid) ? ", seo_id_vid = '{$this->id_vid}'" : '' )
                    . ( !empty($this->other_vid) && empty($this->id_vid) ? ", seo_other_vid = '{$this->other_vid}'" : '' )
                    . ", seo_address            = '{$this->address}'"
                    . ", seo_other_etap         = '{$this->other_etap}'"
                    . ", seo_etap_date          = {$this->etap_date}"
                    . ", seo_sq_zem_uch         = '{$this->sq_zem_uch}'"
                    . ", seo_sq_zastr           = '{$this->sq_zastr}'"
                    . ", seo_sq_total           = '{$this->sq_total}'"
                    . ", seo_etazh              = '{$this->etazh}'"
                    . ", seo_karkas             = '{$this->karkas}'"
                    . ", seo_fundament          = '{$this->fundament}'"
                    . ", seo_steni              = '{$this->steni}'"
                    . ", seo_windows            = '{$this->windows}'"
                    . ", seo_roof               = '{$this->roof}'"
                    . ", seo_floor              = '{$this->floor}'"
                    . ", seo_doors              = '{$this->doors}'"
                    . ", seo_pravo_doc          = '{$this->pravo_doc}'"
                    . ", seo_period_prj         = '{$this->period_prj}'"
                    . ", seo_period_bld         = '{$this->period_bld}'"
                    . ", seo_bld_volume         = '{$this->bld_volume}'"
                    . ", seo_status             = '{$this->status}'"
                    . ", seo_anons              = '{$this->anons}'"
                ;

                if ( empty($this->id) ) {
                    $strSqlQuery = "INSERT INTO site_estate_objects SET ".$strSqlFields.", seo_date_add = NOW()";
                } else {
                    $strSqlQuery = "UPDATE site_estate_objects SET ".$strSqlFields.", seo_date_change = NOW() WHERE seo_id = ".$this->id;
                }

                if ( !$objDb->query( $strSqlQuery ) ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                } else {
                    if ( empty($this->id) ) {
                        $this->setId($objDb->insert_id());
                    }

                    $strSqlQuery = "UPDATE `site_estate_objects` SET `seo_url` = '".$this->url."_".$this->id."' WHERE `seo_id` = ".$this->id;
                    if (!$objDb->query($strSqlQuery)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    }
                }

            } catch (Exception $ex) {
                echo $ex->getTraceAsString();
            }
        }
    }

    public function load() {

    }

    private function isReadyToSave() {
        return ($this->id > 0);
    }

    public function setId($id) {
        if($id > 0) $this->id = $id;
    }

    public function setData($arr) {
        if (is_array($arr) && !empty($arr)) {
            foreach ($arr as $k => $val) {
                if (isset($this->$k)) {
                    $this->$k = $val;
                }
            }
        }
    }

    public function getComments($id)
    {
        if (intval($id) <= 0) {
            if ($this->id <= 0) {
                return null;
            } else {
                $id = $this->id;
            }
        }

        if(isset($_SESSION['uAuthInfo']['uID']) && $_SESSION['uAuthInfo']['uID'] > 0) {
            $strSqlQuery = "SELECT t1.*, t2.su_fname as UserName FROM `site_users_comments` t1 LEFT JOIN `site_users` t2 ON (su_id = suc_su_id AND su_status = 'Y') WHERE `suc_seo_id` = ".$id." AND suc_status = 'Y' AND suc_su_id = ".$_SESSION['uAuthInfo']['uID']." ORDER BY `suc_created`";
            return $this->fetchall($strSqlQuery);
        } else {
            return null;
        }
    }

    public function getCommentsCount($id = 0)
    {
        if (intval($id) <= 0) {
            if ($this->id <= 0) {
                return null;
            } else {
                $id = $this->id;
            }
        }

        $strSqlQuery = "SELECT COUNT(suc_id) iC FROM `site_users_comments` WHERE `suc_seo_id` = ".$id;
        return $this->fetch($strSqlQuery, 'iC');
    }

    public function getExtraInfoCount($id = 0)
    {
        if (intval($id) <= 0) {
            if ($this->id <= 0) {
                return null;
            } else {
                $id = $this->id;
            }
        }

        $strSqlQuery = "SELECT COUNT(`id`) iC FROM `psb_estate_object_extra_info` WHERE `oid` = ".$id;
        $iC = $this->fetch($strSqlQuery, 'iC');
        return $iC > 0 ? $iC : null;
    }

    public function getListInfo($id = 0)
    {
        if (intval($id) <= 0) {
            if ($this->id <= 0) {
                return null;
            } else {
                $id = $this->id;
            }
        }

        $strSqlQuery = "SELECT * FROM `psb_estate_object_extra_info` WHERE `oid` = ".$id." ORDER BY `date` DESC";
        return $this->fetchall($strSqlQuery);
    }

    public function getDataById($id, $field = '*', $returnVal = -1)
    {
        $strSqlQuery = "SELECT $field FROM `site_estate_objects` WHERE `seo_id` = ".$id;
        return $this->fetch($strSqlQuery, $returnVal);
    }

    public function getEstateTypes()
    {
        $strSqlQuery = "SELECT * FROM `site_estate_types` WHERE `set_status` = 'Y'";
        return $this->fetchall($strSqlQuery);
    }
}
