<?php
// ************************************************************************************
// Класс фрагментов страниц и шаблонов
//
// ----- Version & History -----
// 	ver. 1.0
// 		31.07.2005 - создание класса
// ************************************************************************************

class clsFragments extends Files {
	var $frgPHPPath; // путь к файлу-обработчику фрагмента
	var $frgHTMLPath; // путь к файлу-шаблону фрагмента

	var $frgPHPFileName;
	var $frgHTMLFileName;

	var $frgPHPContentFile;
	var $frgHTMLContentFile;

	var $frgTextPHPFile;
	var $frgName;

	function clsFragments($strFragmentIDName, $strFragmentName) {
		$this->frgPHPPath = SITE_PHP_FRG_DIR;
		$this->frgHTMLPath = SITE_TPL_FRG_DIR;

		$this->frgName = $strFragmentIDName; // Уникальное имя фрагмента

		$this->frgPHPFileName = $this->frgName.'.inc.php'; // Файл-обработчик фрагмента
		$this->frgHTMLFileName = $this->frgName.'.frg'; // Файл-шаблон фрагмента

		$this->frgPHPContentFile = '<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - '.date('Y.m').' */'."\n".
       	'///+++ Обработчик фрагмента: '.stripslashes($strFragmentName).' ['.$this->frgName.']'."\n".
       	'$arrTplVars[\'name.fragment\'] = \''.$this->frgName.'\';'."\n".
       	'$objTpl->tpl_load($arrTplVars[\'name.fragment\'], "'.$this->frgHTMLFileName.'");'."\n\n\n".
       	'$objTpl->tpl_array($arrTplVars[\'name.fragment\'], $arrTplVars);'."\n";
	}

  /**
   * Смена UID-имени файла-обработчика фрагмента
   *
   * @param unknown_type $strNewFrgName
   */
	function setFrgName($strNewFrgName) {
		$this->frgName = $strNewFrgName;
		$this->frgPHPFileName = $this->frgName.'.inc.php'; // Файл-обработчик фрагмента
		$this->frgHTMLFileName = $this->frgName.'.frg'; // Файл-шаблон фрагмента
	}

  /**
   * Смена каталога для файлов-обработчиков фрагмента
   *  $strNewPath - путь к папке в которой находится структура папок
   *  SITE_PHP_FRG_DIR (по умолчанию: php/fragments/)
   *
   * @param unknown_type $strNewPath
   */
	function setPHPPath($strNewPath) {
		$this->frgPHPPath = $strNewPath.SITE_PHP_FRG_DIR;
	}

  /**
   * Смена каталога для файлов-шаблонов фрагмента
   *  $strNewPath - путь к папке в которой находится структура папок
   *  SITE_TPL_FRG_DIR (по умолчанию: templates/fragments/)
   *
   * @param unknown_type $strNewPath
   */
	function setHTMLPath($strNewPath) {
		$this->frgPHPPath = $strNewPath.SITE_TPL_FRG_DIR;
	}

  /**
   * Чтение содержимого файла-обработчика текущего фрагмента
   *
   */
	function getContentPHPHandler() {
		$this->frgPHPContentFile = $this->readFile($this->frgPHPPath.$this->frgPHPFileName); // Читаем содержимое файла-обработчика фрагмента
	}

  /**
   * Чтение содержимого файла-шаблона текущего фрагмента
   *
   */
	function getContentHTMLHandler() {
		$this->frgHTMLContentFile = $this->readFile($this->frgHTMLPath.$this->frgHTMLFileName); // Читаем содержимое файла-обработчика фрагмента
	}

  /**
   * Замена строк в содержимом файлов обработчиков фрагментов
   *
   * @param unknown_type $strLastName
   * @param unknown_type $strNewName
   */
	function replaceNameInPHPContent($strLastName, $strNewName) {
		$this->frgPHPContentFile = str_replace($strLastName, $strNewName, $this->frgPHPContentFile);
	}

  /**
   * Сохранение файла-обработчика фрагмента
   *
   * @param unknown_type $bolChmod
   * @return unknown
   */
	function savePHPFile($bolChmod=true) {
		if ( !$this->writeFile($this->frgPHPContentFile, $this->frgPHPFileName, $this->frgPHPPath) ) {
			return false;
		}

		if ( $bolChmod ) {
			@chmod($this->frgPHPPath.$this->frgPHPFileName, 0664);
		}

		return true;
	}

  /**
   * Сохранение файла-шаблона фрагмента
   *
   * @param unknown_type $bolChmod
   * @return unknown
   */
	function saveHTMLFile($bolChmod=true) {
		if(!$this->writeFile($this->frgHTMLContentFile, $this->frgHTMLFileName, $this->frgHTMLPath ))
			return false;

		if($bolChmod)
			@chmod($this->frgHTMLPath.$this->frgHTMLFileName, 0664);

		return true;
	}

  /**
   * Удаление файла-обработчика фрагмента
   *
   */
	function deletePHPFile() {
		if(file_exists($this->frgPHPPath.$this->frgPHPFileName))
			@unlink($this->frgPHPPath.$this->frgPHPFileName);
	}

  /**
   * Удаление файла-шаблона фрагмента
   *
   */
	function deleteHTMLFile() {
		if(file_exists($this->frgHTMLPath.$this->frgHTMLFileName))
			@unlink($this->frgHTMLPath.$this->frgHTMLFileName);
	}
}

