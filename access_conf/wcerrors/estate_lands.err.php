<?php
// **** Обработка ошибок раздела "Недвижимость Участки"
$errMsg = array_merge($errMsg, array (
  'msgError' =>  "Ошибка"
  , 'msgChapterId' =>  "Раздел не выбран"
  , 'msgName' =>  "Наименование объекта"
  , 'msgAddress' =>  "Адрес объекта"
  , 'msgOrgName' =>  "Организация"
  , 'msgContact' =>  "Контактное лицо"
  , 'msgPhone' =>  "Контактный телефон"
));
