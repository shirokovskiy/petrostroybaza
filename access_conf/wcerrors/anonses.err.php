<?php
// **** Обработка ошибок раздела "Анонсы мероприятий"
$errMsg = array_merge($errMsg, array (
  'msgError' =>  "Ошибка"
  , 'msgAnonsName' =>  "Наименование анонса мероприятия"
  , 'msgAnonsAddress' =>  "Место проведения"
  , 'msgAnonsDate' =>  "Дата проведения мероприятия"
));

