<?php
// **** Обработка ошибок раздела "Баннеры"
$errMsg = array_merge($errMsg, array (
  'msgEmptyFileDesc'  =>  "Описание файла пустое"
  , 'msgNoFile'       =>  "Не указан файл для загрузки"
  , 'msgTooBigFile'   =>  "Слишком большой файл для загрузки"
  )
);
