<?php
// **** Обработка ошибок раздела "Строительная техника"
$errMsg = array_merge($errMsg, array (
  'msgError' =>  "Ошибка"

  , 'msgErrorEmptyFields' =>  "Слишком мало информации. Большинство полей остались не заполнены!"
	, 'msgErrorAdType' =>  "Вы не указали <b>Тип объявления</b>"
	, 'msgErrorTitle' =>  "Вы не указали <b>Заголовок объявления</b>"
	, 'msgErrorTechType' =>  "Вы не выбрали <b>Тип техники</b> в размещаемом объявлении"
	, 'msgErrorContacts' =>  "Вы не указали контактную информацию в поле <b>Координаты и контакты</b>"
));
