<?php
// **** Обработка ошибок раздела "НЕДВИЖИМОСТЬ"
$errMsg = array_merge($errMsg, array (
    'msgErrorObjName'  =>  "Наименование объекта НЕ указано"

  	, 'msgErrorReg'         =>  "НЕ выбран регион"
  	, 'msgErrorAdmReg'      =>  "НЕ выбран район"
  	, 'msgErrorObjAdr'      =>  "НЕ указан адрес объекта"
  	, 'msgErrorVid'         =>  "НЕ выбран Вид строительства"
  	, 'msgErrorEtap'        =>  "НЕ заполнено поле Этап"
  	, 'msgErrorDelObjImg'   =>  "Не верно указан запрос на удаление"
  	, 'msgErrorEstType'     =>  "Не выбран Раздел недвижимости"
  )
);
