<?php
// **** Обработка ошибок раздела "Тендеры"
$errMsg = array_merge($errMsg, array (
  'msgError' =>  "Ошибка"
  , 'msgName' =>  "Название объекта"
  , 'msgVidRabot' =>  "Вид работ"
  , 'msgTenderObjectAddress' =>  "Адрес"
  , 'msgTenderCustomer' =>  "Заказчик"
  , 'msgTenderCoordinates' =>  "Координаты"
  , 'msgTenderContacts' =>  "Контактное лицо"
));
