<?php
// **** Обработка ошибок раздела "Добавить статью"
$errMsg = array_merge($errMsg, array (
  'msgEmptyTitle'     =>  "Заголовок не может быть пустым"
  , 'msgEmptyAnonsbody' =>  "Тело статьи не может быть пустым"
  , 'msgEmptyTextbody'  =>  "Тело статьи не может быть пустым"
  )
);

