<?php
// **** Установка локалей ******************************************************
setlocale(LC_ALL, "ru_RU.utf-8");
date_default_timezone_set ( 'Europe/Moscow' );

if(!isset($_SERVER['_IS_DEVELOPER_MODE'])) $_SERVER['_IS_DEVELOPER_MODE'] = 0;

// **** Уровень ошибок *********************************************************
if ($_SERVER['_IS_DEVELOPER_MODE']==1 || (isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], array('188.134.80.143'))) ) {
    //error_reporting(E_ALL ^ E_NOTICE);
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    //error_reporting(E_ALL);
    ini_set("display_errors", "On");
} else {
    error_reporting(E_ERROR | E_WARNING);
}

if (!isset($_SERVER['DOCUMENT_ROOT']) || empty($_SERVER['DOCUMENT_ROOT'])) {
    $_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));
}

// ******  Конфигурация сайта (изменяемо) **************************************
if (!defined("DROOT")) {
    define('DROOT', $_SERVER['DOCUMENT_ROOT'].(preg_match("/.*\/$/", $_SERVER['DOCUMENT_ROOT']) ? "" : "/") );
}
// ******

//ini_set('session.save_path', DROOT."sess");
//session_set_cookie_params(0, '/', 'petrostroybaza.ru');
// session_name("petrostroybaza");
session_start();

if(!isset($_SESSION)) {
    echo "sessions OFF<br>";
    die("<hr /><p style='color:red'><b>" . __FILE__ . "</b>:<i>" . __LINE__ . "</i></p>");
}

if (isset($_GET['jimmy']) && $_GET['jimmy']=='webstudio') {
    echo("Server info!<hr />");
    phpinfo();
    die();
}

// ******
define('SITE_ID_PROJECT', 1);
$site_domain = ( $_SERVER['_IS_DEVELOPER_MODE']==2 ? "du" : "ru" );
$site_prefix = ( $_SERVER['_IS_DEVELOPER_MODE']==2 ? "stage" : "www" );
define('SITE_URL', "http://$site_prefix.petrostroybaza.$site_domain/");

// ***** НИЖЕ НЕ РЕДАКТИРОВАТЬ *************************************************
define('PRJ_STORAGE', DROOT.'storage/');
define('PRJ_LOGS', DROOT.'logs/');
define('PRJ_IMAGES', PRJ_STORAGE.'images/');
define('PRJ_SITEIMG', PRJ_STORAGE.'siteimg/');
define('PRJ_FILES', PRJ_STORAGE.'files/');

define('SITE_ACCESS_DIR', DROOT."access_conf/");
define('SITE_LIB_DIR',    DROOT."lib/");

define('SITE_PHP_DIR', DROOT."php/");
define('SITE_TPL_DIR', DROOT."templates/");

// PHP-обработчики
define('SITE_PHP_FRG_DIR',  SITE_PHP_DIR."fragments/"); // PRJ_FRG_PHP_FOLDER = PHPFRAGMENTSDIR
define('SITE_PHP_PAGE_DIR', SITE_PHP_DIR."pages/");
// Шаблоны
define('SITE_TPL_TMP_DIR',  SITE_TPL_DIR."tmp/"); // папка-буфер (кеш)
define('SITE_TPL_TPL_DIR',  SITE_TPL_DIR."tpl/");
define('SITE_TPL_PAGE_DIR', SITE_TPL_DIR."pages/");
define('SITE_TPL_FRG_DIR',  SITE_TPL_DIR."fragments/"); //PRJ_FRG_HTML_FOLDER

define('SITE_ADMIN_MAIL', "info@petrostroybaza.ru");
define('SITE_ROBOT_MAIL', "info@petrostroybaza.ru");

define('SITE_FILE_TMP', PRJ_FILES."__tmp/");

// **** Конфигурация CMS (изменяемо) *******************************************
define('CMSFOLDER', "webcontrol/"); // Папка CMS

define('ADMINURL', SITE_URL.CMSFOLDER); // Полный URL для CMS

define('ADMTPL', DROOT.CMSFOLDER."tmpl/");
define('ADMTPLGL', ADMTPL."global/");
define('ADMTPLFRG', ADMTPL."fragments/");
define('ADMPHP', DROOT.CMSFOLDER."php/");
define('ADMPHPGL', ADMPHP."global/");
define('ADMERR', SITE_ACCESS_DIR."wcerrors/");


/**
 * Для автоматической загрузки классов ####
 */
set_include_path(
    get_include_path() . PATH_SEPARATOR .
    SITE_LIB_DIR . PATH_SEPARATOR .
    SITE_LIB_DIR . 'models'
);

function psb_autoloader($class) {
    include_once $class. '.php';
}

spl_autoload_register('psb_autoloader');

// #######################################

$arrTplVars['strAdminUrl'] = $arrTplVars['cfgAdminUrl'] = ADMINURL;
$arrTplVars['strAdminImg'] = $arrTplVars['cfgAdminImg'] = ADMINURL."images/";
// **** END:Конфигурация CMS ***************************************************

$arrTplVars['cfgUrlSite'] = $arrTplVars['cfgSiteUrl'] = SITE_URL;
$arrTplVars['cfgUrlLink'] = "<a href='".SITE_URL."'>".SITE_URL."</a>";
$arrTplVars['cfgAdminMail'] = SITE_ADMIN_MAIL;
$arrTplVars['strCurrentUri'] = $_SERVER['REQUEST_URI'].( preg_match("/\?/", $_SERVER['REQUEST_URI']) ? "&" : "?" );

/**
 * Объявления переменных каталогов с изображениями
 */
$arrTplVars['cfgSiteImg'] = SITE_URL."storage/siteimg/";
$arrTplVars['cfgAllImg'] = SITE_URL."storage/images/";
$arrTplVars['cfgFilesUrl'] = SITE_URL."storage/files/";

// ***** Подключение библиотеки классов ****************************************
include_once(SITE_ACCESS_DIR."db.cfg.php");
include_once("cls.db.php");
include_once("cls.tpl.php");
include_once("cls.utils.php");

include_once("cls.error.php");
include_once("cls.mail.php");
include_once("cls.file.php");
include_once("cls.log.php");
include_once("cls.paginator.php");
include_once("cls.global.auth.php");

$objDb    = new Db($_db_config);
$objUtil  = new Util();
// -----------------------------------
$objUtil->start();
// -----------------------------------

$strUsAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
if (strstr($strUsAgent, 'opera')) $arrTplVars['strUserAgent'] = 'op';
elseif (strstr($strUsAgent, 'firefox')) $arrTplVars['strUserAgent'] = 'ff';
elseif (strstr($strUsAgent, 'netscape')) $arrTplVars['strUserAgent'] = 'nn';
elseif (strstr($strUsAgent, 'msie')) $arrTplVars['strUserAgent'] = 'ie';
else $arrTplVars['strUserAgent'] = 'ie';

$arrCntExt['zak'] = "Zak";
$arrCntExt['zas'] = "Zas";
$arrCntExt['inv'] = "Inv";
$arrCntExt['genpod'] = "GenPod";
$arrCntExt['proekt'] = "Proekt";
$arrCntExt['pod'] = "Pod";

$arrTitleExt['zak'] = "Заказчик";
$arrTitleExt['zas'] = "Застройщик";
$arrTitleExt['inv'] = "Инвестор";
$arrTitleExt['genpod'] = "Генподрядчик";
$arrTitleExt['proekt'] = "Проектировщик";
$arrTitleExt['pod'] = "Подрядчик";
$arrTitleExt['opa'] = "Отдел продаж и аренды";

$arrAdTechType['lease'] = "Сдам в аренду";
$arrAdTechType['rent'] = "Арендую";
$arrAdTechType['sell'] = "Продажа";
$arrAdTechType['buy'] = "Покупка";

$arrEtapBuildings = array("нулевой цикл", "демонтаж", "общестроительные работы, строительство каркаса", "пуско-наладка", "проектирование", "изыскательские работы", "внутренние и отделочные работы");
$arrEqEtapBuildings = array(array("нулев","подготов","участок","землян"), "демонт", array("общестроит","каркас","монтаж"), "пуско-налад", "проект", "изыск", array("внутр", "пуско-налад", "отдел"));

$arrKarkasBuildings = array("монолитный", "монолитно-кирпичный", "кирпичный", "каркасно-панельный", "металлокаркас", "железобетон");
$arrEqKarkasBuildings = array("монолит", "кирпично-монолитный", "кирпич", array("каркасно-панельн","сборно-железобет"), "металл");
