<?php
// ******** Файл доступа к базе данных
$_db_config = array (
    "host"            =>  'localhost', // via ISP Manager
    "dbname"          =>  'psbaza',
    "login"           =>  'psbuser',
    "password"        =>  'JwJbWsdQ',
    "charset"         =>  'utf8'
);

$_db_tables = array(
  'stPages'           => 'site_pages',
  'stPagesGrp'        => 'site_pages_group',
  'stMeta'            => 'site_pages_meta',
  'stImages'          => 'site_images',                 // Изображения
  'stFragments'       => 'site_fragments',              // Фрагменты страниц сайта
  'stFragmentsGroup'  => 'site_fragments_group',        // Группы фрагментов (каталоги)
  'stTpl'             => 'site_templates',              // Шаблоны сайта
  'stTplGrp'          => 'site_templates_group',        // Группы шаблонов

  'cmsConfig'         => 'cms_config',
  'cmsModules'        => 'cms_modules',
  'cmsAccess'         => 'cms_access',
  'cmsPrjMods'        => 'cms_projects_modules',
  'cmsProjects'       => 'cms_projects',
  'cmsPrjAccess'      => 'cms_projects_access',
  'cmsUserGrp'        => 'cms_users_group',
  'cmsUsers'          => 'cms_users',

  'stUsers'           => 'site_users',
  'stEstTypes'        => 'site_estate_types',
  'stAdmReg'          => 'site_regions_adm',
  'stObjects'         => 'site_estate_objects',
  'stReleases'        => 'site_releases',
  'stVid'             => 'site_estate_vid',
  'stContacts'        => 'site_estate_contacts',
  'stObjContacts'     => 'site_objects_contacts_lnk',

  'stAnalitic'        	=> 'psb_analitics',
  'stAnaliticFiles'   	=> 'psb_analitic_files',
  'stGenplan'         	=> 'psb_genplan',
  'stTenders'         	=> 'psb_tenders',
  'stAnonses'         	=> 'psb_anonses_mero',
  'stAdsAdsChapters'  	=> 'psb_ads_ads_chapters',
  'stEstateAdsChapters' => 'psb_ads_estate_chapters',
  'stAdsEstateLands'  	=> 'psb_ads_estate_lands',
  'stAds'             	=> 'psb_ads_ads',

  'stTechnics'        => 'psb_technics'
  ,'stTechTypes'      => 'psb_technic_types'
);

$_dbt = &$_db_tables;
/***/
