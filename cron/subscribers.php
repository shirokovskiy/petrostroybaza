<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 04/16/15
 * Time         : 12:03 AM
 * Description  : Send newsletters
 */
// */3 * * * * wget -O- http://www.domain.tld/cronjobs/subscribers.php >> /var/www/data/domain.tld/logs/cron.subscribers.`date +\%Y.\%m.\%d`.log 2>&1
$_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__);

include_once $_SERVER['DOCUMENT_ROOT']."/access_conf/web.cfg.php";
include_once "funcs.php";
include_once "models/Spamer.php";
$objSpamer = new Spamer();
$intCountSubscribers = 32; // кол-во подписчиков за один обход скрипта
$isBccCopySent = false;
$msgInText = $msgInHtml = '';
$isDebugMode = false;
$mailSender = 'tm@petrostroybaza.ru';
$mailSenderName = 'Петростройбаза';

/**
 * Проверить запущенные рассылки
 * За один запуск спамера берём только одну рассылку и только указанное кол-во адресатов
 */
$arrCurrentJob = $objSpamer->getPriorityProcess();
$debugLogName = "subscribers.".date('Y.m.d_H').".log";

if (is_array($arrCurrentJob) && !empty($arrCurrentJob)) {
    if($isDebugMode) trace_log('Инфо о текущем задании: $arrCurrentJob '.print_r($arrCurrentJob, true), $debugLogName);
}


if (is_array($arrCurrentJob) && !empty($arrCurrentJob)) {
    // есть работа для рассылки, получим подписчиков
    $arrSubscribers = $objSpamer->getSubscribers($arrCurrentJob['sc_id'], $intCountSubscribers);

    if (is_array($arrSubscribers) && !empty($arrSubscribers)) {
        //trace_log('$arrSubscribers '.print_r($arrSubscribers, true), $debugLogName);
        if($isDebugMode) trace_log('Получены данные о подписчиках!', $debugLogName);
    }

    /**
     * Если есть подписчики, начинаем подготовку рассылки и делаем отправку
     */
    if (is_array($arrSubscribers) && !empty($arrSubscribers)) {
        if (!is_object($objMail)) {
            $objMail = new clsMail();
        }

        $sendFrom[] = $mailSender;
        $sendFrom[] = $mailSenderName;

        $arrNewsletter = $objSpamer->getNewsletterProcessInfo($arrCurrentJob['sc_id']);// sc_group_id
        $msgSubject = $arrNewsletter['ste_title'];
        $msgInText = $strNewsletterText = $arrNewsletter['ste_body'];
        $msgInHtml = $strNewsletterHtml = $arrNewsletter['ste_html'];

        if (is_array($arrNewsletter) && !empty($arrNewsletter)) {
            if($isDebugMode) trace_log('Есть информация о письме $arrNewsletter = '.print_r($arrNewsletter, true), $debugLogName);
        }

        /**
         * Проходимся по каждому подписчику и формируем для него индивидуальное письмо
         */
        foreach ($arrSubscribers as $k => $subscriber) {
            $arrUser = $objSpamer->getSubscriberEmailInfo($subscriber['ss_subscriber_id']);

            if (is_array($arrUser) && !empty($arrUser)) {
                if($isDebugMode) trace_log('Известен подписчик $arrUser = '.print_r($arrUser, true), $debugLogName);
            }

            $sendTo = null;
            if (is_array($arrUser) && !empty($arrUser)) {
                # есть инфо о подписчике
                $sendTo = $arrUser['sus_email'];

                $msgInText  = str_replace('%SUBSCRIBER%', $arrUser['sus_name'], $strNewsletterText);
                $msgInHtml  = str_replace('%SUBSCRIBER%', $arrUser['sus_name'], $strNewsletterHtml);

                $strAddOnsTxt = "\n\n\n"."Полезный совет: если Вам нравятся наши информационные письма, и если Вы не хотите потерять их в папке Спам,\nдобавьте наш информационный адрес $mailSender в Контактную книгу Вашего почтового ящика.\n\nС уважением,\n$mailSenderName.";
                $strAddOnsHtml = 'Полезный совет: <i>если Вам нравятся наши информационные письма, и если Вы не хотите потерять их в папке Спам, в таком случае добавьте наш информационный адрес '.$mailSender.' в Контактную книгу Вашего почтового ящика.</i><br><br>С уважением,<br>'.$mailSenderName;

                //$linkUnsubscribe = SITE_URL.'unsubscribe/email/'.urlencode($sendTo).'/'.md5( md5($sendTo) . $arrUser['sus_id'] );
                //$strAddOnsTxt .= "\n\n\n"."Чтобы отписаться от рассылки кликните на ссылку: ".$linkUnsubscribe;
                //$strAddOnsHtml .= '<p style="margin: 50px 0 10px">Чтобы отписаться от рассылки кликните на ссылку: <a target=_blank href="'.$linkUnsubscribe.'">'.$linkUnsubscribe.'</a></p>';

                $sendHTMLMessage = file_get_contents(SITE_TPL_DIR.'emails/subscription.template.html');
                $sendHTMLMessage = preg_replace("/\{\%MAIL_SUBJECT\%\}/", $msgSubject, $sendHTMLMessage);
                $sendHTMLMessage = preg_replace("/\{\%MAIL_BODY_CONTENT\%\}/", $msgInHtml, $sendHTMLMessage);
                $sendHTMLMessage = preg_replace("/\{\%MAIL_BODY_FOOTER_CONTENT\%\}/", $strAddOnsHtml, $sendHTMLMessage);

                foreach(array('jpg','png','gif') as $val) {
                    if (file_exists(PRJ_FILES.'email_attachments/pic.'.$arrNewsletter['ste_id'].'.'.$val)) {
                        $ext = $val; break;
                    }
                }

                if (isset($ext)) {
                    $sendHTMLMessage = preg_replace("/\{\%BANNER\%\}/", '<img src="banner.' . $ext . '" />', $sendHTMLMessage);
                } else {
                    $sendHTMLMessage = preg_replace("/\{\%BANNER\%\}/", '', $sendHTMLMessage);
                }
            }

            if (!is_null($sendTo) && $objUtil->checkEmail($sendTo)) {
                $objMail->new_mail( $sendFrom, $sendTo, $msgSubject, $msgInText.$strAddOnsTxt, $sendHTMLMessage );
                $objMail->add_attachment( PRJ_SITEIMG.'content/email-bg.jpg', 'email-bg.jpg', 'image' ); // Background в шаблоне письма

                if (isset($ext) && !empty($ext)){
                    # Приаттачим загруженное фото к письму
                    $objMail->add_attachment( PRJ_FILES.'email_attachments/pic.'.$arrNewsletter['ste_id'].'.b.'.$ext, 'banner.'.$ext, 'image' );
                    unset($ext);
                }

                foreach(array('doc','docx','xls','xlsx','pdf') as $val) {
                    if (file_exists(PRJ_FILES.'email_attachments/document.'.$arrNewsletter['ste_id'].'.'.$val)) {
                        $ext = $val; break;
                    }
                }

                if (isset($ext)) {
                    $objMail->add_attachment( PRJ_FILES.'email_attachments/document.'.$arrNewsletter['ste_id'].'.'.$ext, 'Petrostroybaza.'.date('YmdHis').'.'.$ext, 'application/octet-stream' );
                    unset($ext);
                }

                if ( $objMail->send() !== true ) {
                    // какая то ошибка
                    // пометить данный email как не отправленный
                    trace_log('Письмо не отправлено по непонятным причинам на '.$sendTo, $debugLogName);
                    $objSpamer->markSubscriberInQueue($arrUser['sus_id'], $arrCurrentJob['sc_id']);
                } else {
                    // письмо отправлено
                    // удалить его из очереди
                    if (!$objSpamer->removeSubscriberEmailFromQueue($arrUser['sus_id'], $arrCurrentJob['sc_id'])) {
                        trace_log('Очередь не очищается - это очень плохо!', $debugLogName);
                    } else {
                        if($isDebugMode) trace_log('Письмо ушло в очередь отправки!', $debugLogName);
                    }
                }
            } else {
                trace_log('Email получателя не указан или не верен: '.$sendTo.' |', $debugLogName);
            }
        }
    } else {
        /**
         * В случае когда процесс есть а подписчиков нет,
         * необходимо закрыть рассылку.
         */
        $objSpamer->stopProcess($arrCurrentJob['sc_id']);
        trace_log('Email рассылка остановлена!', $debugLogName);
    }
}
