<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2006.11 */
///+++ Обработчик фрагмента: Разделы недвижимости [estate.chapters]
$arrTplVars['name.fragment'] = 'estate.chapters';
$objTpl->tpl_load($arrTplVars['name.fragment'], "estate.chapters.frg");

/**
 * Выводим для "прямого" показа только отмеченные для меню.
 */
$strSqlQuery = "SELECT * FROM ".$_db_tables['stEstTypes']." WHERE set_status='Y' AND set_menu='Y'";
$arrEstTypesFrg = $objDb->fetchall( $strSqlQuery );

if ( is_array($arrEstTypesFrg) ) {
  foreach ( $arrEstTypesFrg as $key => $value ) {
    $arrEstTypesFrg[$key]['strEstTypeName'] = $value['set_name'];
    $arrEstTypesFrg[$key]['strEstType'] = !empty($value['set_alias']) ? $value['set_alias'] : $value['set_id'];
  }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "estate.types", $arrEstTypesFrg);

/**
 * Остальные выводим в расширенном меню
 */
$strSqlQuery = "SELECT * FROM ".$_db_tables['stEstTypes']." WHERE set_status='Y' AND set_menu='N'";
$arrEstTypesFrgMore = $objDb->fetchall( $strSqlQuery );

if ( is_array($arrEstTypesFrgMore) ) {
  $arrIf['more.items'] = true;
  foreach ( $arrEstTypesFrgMore as $key => $value ) {
    $arrEstTypesFrgMore[$key]['strEstTypeName'] = $value['set_name'];
    $arrEstTypesFrgMore[$key]['strEstType'] = !empty($value['set_alias']) ? $value['set_alias'] : $value['set_id'];

    if ( isset($arrReqUri[1]) && !empty($arrReqUri[1]) && $arrReqUri[1] == $arrEstTypesFrgMore[$key]['strEstType'] ) {
      $arrTplVars['displayLongChapters'] = 'display:block;';
      $arrTplVars['displayShortChapters'] = 'display:none;';
    }
  }

  $objTpl->tpl_loop($arrTplVars['name.fragment'], "more.estate.types", $arrEstTypesFrgMore);
}

if ( !isset($arrTplVars['displayLongChapters']) ) {
  $arrTplVars['displayLongChapters'] = 'display:none;';
}

$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
