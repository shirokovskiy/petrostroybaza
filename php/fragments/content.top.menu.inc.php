<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2007.07 */
///+++ Обработчик фрагмента: Верхнее контент-меню [content.top.menu]
$arrTplVars['name.fragment'] = 'content.top.menu';
$objTpl->tpl_load($arrTplVars['name.fragment'], "content.top.menu.frg");

$strSqlQuery = "SELECT * FROM ".$_db_tables["stEstateAdsChapters"]." ORDER BY paec_name ";
$arrEstateAdsChapters = $objDb->fetchall( $strSqlQuery );

$strSqlQuery = "SELECT * FROM psb_ads_ads_chapters WHERE paac_status = 'Y' ORDER BY paac_id";
$arrAdsAdsChapters = $objDb->fetchall( $strSqlQuery );

$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.of.estate.ads.types", $arrEstateAdsChapters);
$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.of.ads.ads.chapters", $arrAdsAdsChapters);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
