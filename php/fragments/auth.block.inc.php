<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2006.11 */
///+++ Обработчик фрагмента: Блок авторизации [auth.block]
$arrTplVars['name.fragment'] = 'auth.block';
$objTpl->tpl_load($arrTplVars['name.fragment'], "auth.block.frg");

$arrIf['authed'] = isset($_SESSION['signInStatus']) && $_SESSION['signInStatus'] == true;
$arrIf['no.auth'] = !$arrIf['authed'];

if ($arrIf['authed']) {
    $arrTplVars['strAuthedUser'] = $_SESSION['uAuthInfo']['uCName'];
    $arrTplVars['strValiDate'] = new DateTime($_SESSION['uAuthInfo']['uDateLimit']);
    $arrTplVars['strValiDate'] = $arrTplVars['strValiDate']->format('d/m/Y');
}

$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
