<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2012.10 */
///+++ Обработчик фрагмента: Фотокартинки [fotobaza]
$arrTplVars['name.fragment'] = 'fotobaza';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fotobaza.frg");


/**
 * Фотобанк
 */
// search
$strSqlWhere = ''; // by default

if (isset($_POST['search_fotobank']) && strlen($_POST['search_fotobank']) > 2) {
	$strSqlWhere = " AND (sig_name LIKE '%".mysql_real_escape_string($_POST['search_fotobank'])."%'";
	$strSqlWhere .= " OR sig_desc LIKE '%".mysql_real_escape_string($_POST['search_fotobank'])."%'";
	$strSqlWhere .= " OR si_title LIKE '%".mysql_real_escape_string($_POST['search_fotobank'])."%'";
	$strSqlWhere .= " OR si_desc LIKE '%".mysql_real_escape_string($_POST['search_fotobank'])."%'";
	$strSqlWhere .= " OR si_tags LIKE '%".mysql_real_escape_string($_POST['search_fotobank'])."%'";
	$strSqlWhere .= " )";
	$arrTplVars['search_fotobank'] = htmlspecialchars($_POST['search_fotobank']);
}

// select rows
$strSqlQuery = "SELECT sig.*, si.si_filename FROM `site_images_group` sig LEFT JOIN `site_images` si ON (si_id = sig_img_id AND si_type='fotobank' AND si_id_rel=sig.sig_id) WHERE `sig_status` = 'Y' AND `sig_type` = 'fotobank' AND `si_filename` IS NOT NULL ".$strSqlWhere." ORDER BY sig_date_add DESC";
$arrGroups = $objDb->fetchall($strSqlQuery);

if (is_array($arrGroups) && !empty($arrGroups)) {
	$i = 0;
	foreach ($arrGroups as $key => $group) {
		$arrIf['items.set.'.$group['sig_id']] = ($i%3==0 && $i>=3);
		$arrGroups[$key]['strGroupTitle'] = htmlspecialchars($group['sig_name']);
		$arrGroups[$key]['strGroupDesc'] = htmlspecialchars($group['sig_desc']);
		$arrGroups[$key]['strGroupDate'] = date("d.m.Y", strtotime($group['sig_date_add']));
		$i++;
	}
	$arrIf['enough.groups'] = count($arrGroups) > 3;
} else {
	$arrIf['no.search.fotobank'] = !empty($strSqlWhere);
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "fotobank.groups", $arrGroups);




/**
 * CartoonBaza
 */

// search
$strSqlWhere = ''; // by default

if (isset($_POST['search_cartoonbaza']) && strlen($_POST['search_cartoonbaza']) > 2) {
	$strSqlWhere = " AND (si_title LIKE '%".mysql_real_escape_string($_POST['search_cartoonbaza'])."%'";
	$strSqlWhere .= " OR si_desc LIKE '%".mysql_real_escape_string($_POST['search_cartoonbaza'])."%'";
	$strSqlWhere .= " OR si_tags LIKE '%".mysql_real_escape_string($_POST['search_cartoonbaza'])."%'";
	$strSqlWhere .= " )";
	$arrTplVars['search_cartoonbaza'] = htmlspecialchars($_POST['search_cartoonbaza']);
}

// select rows
$strSqlQuery = "SELECT * FROM `site_images` WHERE `si_status` = 'Y' AND `si_type` = 'cartoonbaza' AND `si_filename` IS NOT NULL ".$strSqlWhere." ORDER BY si_date_add DESC";
$arrCartoons = $objDb->fetchall($strSqlQuery);

if (is_array($arrCartoons) && !empty($arrCartoons)) {
	$i = 0;
	foreach ($arrCartoons as $key => $cartoon) {
		$arrCartoons[$key]['strFilename'] = substr($cartoon['si_filename'],0,-4);
		if (!file_exists(PRJ_IMAGES.'fotobank/cartoonbaza/'.$cartoon['si_filename'])) {
			unset($arrCartoons[$key]);
			continue;
		}
		$arrCartoons[$key]['strCartoonDate'] = date("Y-m-d", strtotime($cartoon['si_date_add']));
		$arrIf['items.set.'.$cartoon['si_id']] = ($i%2==0 && $i>=2);
		$i++;
	}
	$arrIf['enough.cartoons'] = count($arrCartoons) > 2;
} else {
	$arrIf['no.search.cartoons'] = !empty($strSqlWhere);
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "cartoonbaza.list", $arrCartoons);

$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
