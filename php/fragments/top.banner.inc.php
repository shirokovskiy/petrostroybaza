<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2007.09 */
///+++ Обработчик фрагмента: Верхний баннер [top.banner]
$arrTplVars['name.fragment'] = 'top.banner';
$objTpl->tpl_load($arrTplVars['name.fragment'], "top.banner.frg");

/**
 * Покажем верхний ЛЕВЫЙ баннер
 */
$strSqlQuery = "SELECT * FROM site_images WHERE si_status = 'Y' AND si_type = 'banner' AND si_banner_type = 'top_l' ORDER BY si_id DESC LIMIT 1";
$arrTopLBanner = $objDb->fetch( $strSqlQuery );

if ( is_array( $arrTopLBanner ) && !empty( $arrTopLBanner ) ) {
    $arrIf['show.top_l.banner'] = true;
    if ( empty( $arrTopLBanner['si_banner_content'] ) ) {
        $arrTplVars['contentTopLBanner'] = "<a href='http://{$arrTopLBanner['si_url']}' target='_blank'><img src='{$arrTplVars['cfgAllImg']}banners/".$arrTopLBanner['si_filename']."' border=0 /></a>";
    } else {
        $arrTplVars['contentTopLBanner'] = $arrTopLBanner['si_banner_content'];
    }
} else {
    $arrIf['show.top_l.banner.dummy'] = true;
}


/**
 * Покажем верхний ЦЕНТРАЛЬНЫЙ баннер
 */
$strSqlQuery = "SELECT * FROM site_images WHERE si_status = 'Y' AND si_type = 'banner' AND si_banner_type = 'top' ORDER BY si_id DESC LIMIT 1";
$arrTopBanner = $objDb->fetch( $strSqlQuery );

if ( is_array( $arrTopBanner ) && !empty( $arrTopBanner ) ) {
  $arrIf['show.top.banner'] = true;
  if ( empty( $arrTopBanner['si_banner_content'] ) ) {
    $arrTplVars['contentTopBanner'] = "<a href='http://{$arrTopBanner['si_url']}' target='_blank'><img src='{$arrTplVars['cfgAllImg']}banners/".$arrTopBanner['si_filename']."' border=0 /></a>";
  } else {
    $arrTplVars['contentTopBanner'] = $arrTopBanner['si_banner_content'];
  }
} else {
  $arrIf['show.top.banner.dummy'] = true;
}

/**
 * Покажем верхний ПРАВЫЙ баннер
 */
$strSqlQuery = "SELECT * FROM site_images WHERE si_status = 'Y' AND si_type = 'banner' AND si_banner_type = 'top_r' ORDER BY si_id DESC LIMIT 1";
$arrTopRBanner = $objDb->fetch( $strSqlQuery );

if ( is_array( $arrTopRBanner ) && !empty( $arrTopRBanner ) ) {
  $arrIf['show.top_r.banner'] = true;
  if ( empty( $arrTopRBanner['si_banner_content'] ) ) {
    $arrTplVars['contentTopRBanner'] = "<a href='http://{$arrTopRBanner['si_url']}' target='_blank'><img src='{$arrTplVars['cfgAllImg']}banners/".$arrTopRBanner['si_filename']."' border=0 /></a>";
  } else {
    $arrTplVars['contentTopRBanner'] = $arrTopRBanner['si_banner_content'];
  }
} else {
  $arrIf['show.top_r.banner.dummy'] = true;
}

$objTpl->tpl_if( $arrTplVars['name.fragment'], $arrIf );
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
