<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2006.11 */
///+++ Обработчик фрагмента: Рекламная площадка левого столбца сайта [left.menu.adver]
$arrTplVars['name.fragment'] = 'left.menu.adver';
$objTpl->tpl_load($arrTplVars['name.fragment'], "left.menu.adver.frg");

$strSqlQuery = "SELECT * FROM site_images WHERE si_status = 'Y' AND si_type = 'banner' AND si_banner_type = 'left' ORDER BY si_id DESC";
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );

if ( is_array($arrSelectedRecords) ) {
  foreach ( $arrSelectedRecords as $key => $value ) {
    if (!empty($value['si_url'])) {
      $arrSelectedRecords[$key]['ahref'] = "<a href='http://{$value['si_url']}' target='_blank'>";
      $arrSelectedRecords[$key]['aend'] = "</a>";
    }
  }
}

$objTpl->tpl_loop($arrTplVars['name.fragment'], "banners", $arrSelectedRecords);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
