<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2007.09 */
///+++ Обработчик фрагмента: Правый баннер [right.banner]
$arrTplVars['name.fragment'] = 'right.banner';
$objTpl->tpl_load($arrTplVars['name.fragment'], "right.banner.frg");

$strSqlQuery = "SELECT * FROM site_images WHERE si_status = 'Y' AND si_type = 'banner' AND si_banner_type = 'right' ORDER BY si_id DESC LIMIT 1";
$arrRightBanner = $objDb->fetch( $strSqlQuery );

if ( is_array( $arrRightBanner ) && !empty( $arrRightBanner ) ) {
  $arrIf['show.right.banner'] = true;
  if ( empty( $arrRightBanner['si_banner_content'] ) ) {
    $arrTplVars['contentRightBanner'] = "<a href='http://{$arrRightBanner['si_url']}' target='_blank'><img src='{$arrTplVars['cfgAllImg']}banners/".$arrRightBanner['si_filename']."' border=0 /></a>";
  } else {
    $arrTplVars['contentRightBanner'] = $arrRightBanner['si_banner_content'];
  }
} else {
  $arrIf['show.right.banner.dummy'] = true;
}

$objTpl->tpl_if( $arrTplVars['name.fragment'], $arrIf );
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
