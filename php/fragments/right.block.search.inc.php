<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2007.01 */
///+++ Обработчик фрагмента: Поиск объектов недвижимости [right.block.search]
$arrTplVars['name.fragment'] = 'right.block.search';
$objTpl->tpl_load($arrTplVars['name.fragment'], "right.block.search.frg");

/**
 * Типы недвижимости
 */
$strSqlQuery = "SELECT set_id, set_name FROM ".$_db_tables["stEstTypes"]." WHERE set_status = 'Y' ORDER BY set_name";
$arrEstateTypes = $objDb->fetchall( $strSqlQuery );

if ( is_array( $arrEstateTypes ) ) {
    foreach ($arrEstateTypes as $k => $estateType) {
        $arrEstateTypes[$k]['sel'] = isset($_SESSION['SF']['type']) && is_array($_SESSION['SF']['type']) && in_array($estateType['set_id'],$_SESSION['SF']['type']) ? ' checked' : '';
    }
}
$objTpl->tpl_loop($arrTplVars["name.fragment"], "chapter", $arrEstateTypes);

/**
 * Вид
 */
$strSqlQuery = "SELECT sev_id, sev_name FROM site_estate_vid ORDER BY sev_name";
$arrEstateVids = $objDb->fetchall( $strSqlQuery );

if ( is_array( $arrEstateVids ) ) {
    foreach ($arrEstateVids as $k => $estateVid) {
        $arrEstateVids[$k]['sel'] = $estateVid['sev_id'] == $_SESSION['SF']['vid'] ? ' selected' : '';
    }
}
$objTpl->tpl_loop($arrTplVars["name.fragment"], "vid", $arrEstateVids);

/**
 * Период
 */
$s = 0;
for ($y = 2005; $y <= intval(date('Y')+5); $y++) {
    $arrPeriods[$s]['iYear'] = $y;
    if ($_SESSION['SF']['year'] == $y) {
        $arrPeriods[$s]['sel'] = ' selected';
    }
    $s++;
}
$objTpl->tpl_loop($arrTplVars["name.fragment"], "periods", $arrPeriods);

if (isset($_REQUEST['period'])) {
    $arrTplVars['option' . $_REQUEST['period']] = ' selected';
}

/**
 * Регионы страны
 */
$strSqlQuery = "SELECT * FROM `site_regions` ORDER BY `sr_id`";
$arrRegions = $objDb->fetchall($strSqlQuery);
if (is_array($arrRegions) && !empty($arrRegions)) {
    foreach ($arrRegions as $k => $arr) {
        $arrRegions[$k]['cbxRegionSel'] = '';
        if (is_array($_SESSION['SF']['cbxRegion']) && !empty($_SESSION['SF']['cbxRegion'])) {
            $arrRegions[$k]['cbxRegionSel'] = in_array($arr['sr_id'], $_SESSION['SF']['cbxRegion']) ? ' checked' : '';
        }
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.regions", $arrRegions);
$arrTplVars['preselectedAdmReg'] = isset($_SESSION['SF']['regionid']) ? json_encode($_SESSION['SF']['regionid']) :0;
/* * */

/**
 * Дополнительный список Этапов строительства (см. глобальный файл конфиг)
 */
$arrHtmlEtapBuildings = array();
if (isset($arrEtapBuildings)) {
    $s = 0;
    foreach($arrEtapBuildings as $val) {
        $arrHtmlEtapBuildings[$s++]['etap'] = $val;
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.etaps", $arrHtmlEtapBuildings);

$arrHtmlKarkasBuildings = array();
if (isset($arrKarkasBuildings)) {
    $s = 0;
    foreach($arrKarkasBuildings as $val) {
        $arrHtmlKarkasBuildings[$s++]['karkas'] = $val;
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.karkas", $arrHtmlKarkasBuildings);

// TODO: в будущем перенести типы в табл БД с отметкой сортировки пунктов (sort) и заменить массив в web.cfg
/*if (is_array($arrTitleExt) && !empty($arrTitleExt)) {
    $s = 0;
    foreach ($arrTitleExt as $type => $name) {
        $arrCompTypeNames[$s]['compType'] = $type;
        $arrCompTypeNames[$s]['compTypeName'] = $name;
        $s++;
    }
    $objTpl->tpl_loop($arrTplVars['name.fragment'], "list.companies.types", $arrCompTypeNames);
}*/

$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf );
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
