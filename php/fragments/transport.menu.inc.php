<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2008.11 */
///+++ Обработчик фрагмента: Транспорт [transport.menu]
$arrTplVars['name.fragment'] = 'transport.menu';
$objTpl->tpl_load($arrTplVars['name.fragment'], "transport.menu.frg");

/**
 * Вывод счётчика объявлений
 */
$strSqlQuery = "SELECT COUNT(*) iC FROM ".$_dbt['stTechnics']." WHERE pt_status='Y'";
$arrInfo = $objDb->fetch( $strSqlQuery );
$arrTplVars['intTechCounts'] = $arrInfo['iC'] > 0 ? $arrInfo['iC'] : 0;

/**
 * Количество объявлений по типам
 * Автокраны
 */
$strSqlQuery = "SELECT * FROM ".$_dbt["stTechTypes"]." ORDER BY ptt_id";
$arrTechTypes = $objDb->fetchall( $strSqlQuery );
if ( is_array($arrTechTypes) ) {
  foreach ( $arrTechTypes as $key => $value ) {
    $strSqlQuery = "SELECT COUNT(*) iC FROM ".$_dbt['stTechnics']." WHERE pt_status='Y' AND pt_tech_type_id = '{$value['ptt_id']}'";
    $arrInfoByType = $objDb->fetch( $strSqlQuery );
    $arrTplVars['intTechCounts_'.$value['ptt_alias']] = $arrInfoByType['iC'] > 0 ? $arrInfoByType['iC'] : 0;
    unset($arrInfoByType);
  }
}

$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
