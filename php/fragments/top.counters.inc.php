<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2007.01 */
///+++ Обработчик фрагмента: Коды счётчиков-невидимок [top.counters]
$arrTplVars['name.fragment'] = 'top.counters';
if ($_SERVER['_IS_DEVELOPER_MODE']) {
	$objTpl->files[$arrTplVars['name.fragment']] = "<!-- Фрагмент удалён -->";
} else
$objTpl->tpl_load($arrTplVars['name.fragment'], "top.counters.frg");


$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
