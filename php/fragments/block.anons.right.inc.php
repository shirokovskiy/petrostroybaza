<?php /* WMBM - Web Manager Building Machine (Jimmy Co.) Copyleft 2006.10 - 2006.11 */
///+++ Обработчик фрагмента: Блок анонсов [block.anons.right]
$arrTplVars['name.fragment'] = 'block.anons.right';
$objTpl->tpl_load($arrTplVars['name.fragment'], "block.anons.right.frg");

$strSqlQuery = "SELECT O.seo_id, O.seo_name, O.seo_date_change, I.si_id, I.si_filename FROM site_estate_objects O"
  . " LEFT JOIN site_images I ON (si_id_rel = seo_id)"
  . " WHERE seo_status ='Y' AND seo_anons = 'Y' ORDER BY seo_date_change DESC LIMIT 10";
$arrAnonsRecords = $objDb->fetchall( $strSqlQuery );

if ( is_array($arrAnonsRecords) ) {
  foreach ( $arrAnonsRecords as $key => $value ) {
    $arrIf['photo.'.$value['seo_id']] = !empty($value['si_filename']);
    $arrAnonsRecords[$key]['strDateUpdate'] = $objUtil->workDate(5, $value['seo_date_change']);
    $arrAnonsRecords[$key]['si_filename'] = str_replace(".b.", ".", $value['si_filename']);
  }
}

$objTpl->tpl_loop($arrTplVars['name.fragment'], "anons.objects", $arrAnonsRecords);

$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
