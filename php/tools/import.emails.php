<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 20.04.15
 * Time         : 0:46
 * Description  : Импортировать файл (списком) e-Mail адресов в указанный лист рассылки
 * php -f ./import.emails.php [groupID]
 * где groupID берём из таблицы `site_users_subscriber_groups`
 * Список можно подготовить используя скрипт из проекта Карлсбах или
 * используя текстовый редактор и регулярные выражения подготовить список eMail-ов в столбик и переименовать файл в csv
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('memory_limit', '1024M');

$root = dirname(__FILE__);
$root = $root.(preg_match("/.*\/$/", $root) ? "" : "/");

require_once($root."../../access_conf/web.cfg.php");

$csv_filename = "subscribers.csv";
$csv_delimiter = ',';
$csv_enclosure = '"';

if (isset($argv[1]) && !empty($argv[1])) {
    $sGroupId = (int)$argv[1];
}

if (intval($sGroupId) <= 0) die("No group ID for subscribers!\n");

# Проверить, есть ли группа вообще
$strSqlQuery = "SELECT susg_title FROM `site_users_subscriber_groups` WHERE `susg_id` = " . $sGroupId;
$strTitle = $objDb->fetch($strSqlQuery, 'susg_title');
if (empty($strTitle)) die("No group in Database for subscribers!\n");

if (isset($argv[2]) && !empty($argv[2])) {
    $csv_filename = (string)$argv[2];
}

$fp = fopen($root.$csv_filename, "r");
if (!$fp) die($root.$csv_filename." not found\n");
$count = 0;

/**
 * Так как каждый email единственный в своей строке, то и ряд будет состоять из одного элемента
 */
while (($row = fgetcsv($fp, 0, $csv_delimiter, $csv_enclosure)) !== false){
    if (!isset($row[0])) {
        echo "No data in row!\n".print_r($row, true)."\n";
        continue;
    }

    $email = $row[0];

    if (!$objUtil->isValidEmail($email)) {
        echo "Email is not valid: ".$email."\n";
        continue;
    }

    echo $email."\n";

    $strSqlQuery = "SELECT `sus_id` FROM `site_users_subscribers` WHERE `sus_email` LIKE '$email'";
    $sId = $objDb->fetch($strSqlQuery, 'sus_id');

    if (empty($sId)) {
        $strSqlQuery = "INSERT INTO `site_users_subscribers` SET"
            . " `sus_email` = '$email'"
            . ", `sus_date_create` = UNIX_TIMESTAMP()";
        if (!$objDb->query($strSqlQuery)) {
            # sql error - возможно дубль
            continue;
        } else
            $sId = $objDb->insert_id();
    }

    if ($sId > 0) {
        # Вставить отношение подписчика и группы
        $strSqlQuery = "INSERT INTO `site_users_subscriber_groups_lnk` SET"
            . " `susgl_sus_id` = ".$sId
            . ", `susgl_susg_id` = ".$sGroupId
            . ", `susgl_date_subscribe` = UNIX_TIMESTAMP()";
        if (!$objDb->query($strSqlQuery)) {
            # sql error - возможно дубль
            continue;
        }
    }
}
