<?php
/** Created by WMBM(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Печать объекта [printobject] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "printobject.43.tpl");
$arrIf['access'] = $_SESSION['uAuthInfo']['uAccess'] == 1;
$arrIf['no.access'] = !$arrIf['access'];
$arrIf['access.for.browser'] = false;
$arrTplVars['jsAuthed'] =
$arrTplVars['isFotobankImages'] =
$arrTplVars['jsPhoto'] = 'false'; // by default

$arrTplVars['urlLinkBack'] = SITE_URL.'objects'.(!empty($_GET['stPage'])?'?stPage='.$_GET['stPage']:'?').(!empty($_GET['reg'])?'reg='.$_GET['reg']:'');
$flagUpdateUrl = false;

if (isset($arrReqUri[1])) {
    if (is_numeric($arrReqUri[1])) {
        $intObjectId = intval($arrReqUri[1]);
        $strWhereSent = "seo_id=".$intObjectId;
        $flagUpdateUrl = true;
    } else {
        $strWhereSent = "seo_url='".mysql_real_escape_string($arrReqUri[1])."'";
    }

    $strSqlQuery = "SELECT O.*, I.si_filename, I.si_id, ET.set_name, AR.sra_name, V.sev_name, R.sr_name"
        . " FROM site_estate_objects O"
        . " LEFT JOIN site_images I ON (si_id_rel = seo_id)"
        . " LEFT JOIN site_estate_types ET ON (seo_id_etype = set_id)"
        . " LEFT JOIN site_regions_adm AR ON (seo_id_adm_reg = sra_id)"
        . " LEFT JOIN site_estate_vid V ON (seo_id_vid = sev_id)"
        . " LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id = seo_id)"
        . " LEFT JOIN site_regions R ON (sorl_sr_id = sr_id)"
        . " WHERE seo_status='Y' AND ".$strWhereSent;
    $arrObjectInfo = $objDb->fetch( $strSqlQuery );
    // Если данные найдены
    if (is_array($arrObjectInfo)) {
        if(empty($intObjectId)) $intObjectId = intval($arrObjectInfo['seo_id']);
        $arrTplVars['intObjectID']    = htmlspecialchars($arrObjectInfo['seo_id']);
        $arrTplVars['strObjectName']  = htmlspecialchars($arrObjectInfo['seo_name']);
        $arrTplVars['strChapterName'] = htmlspecialchars($arrObjectInfo['set_name']);
        $arrTplVars['strRegion']      = htmlspecialchars($arrObjectInfo['sr_name']);
        $arrTplVars['strAdmReg']      = !empty($arrObjectInfo['seo_other_adm_reg']) ? htmlspecialchars($arrObjectInfo['seo_other_adm_reg']) : htmlspecialchars($arrObjectInfo['sra_name']);
        $arrTplVars['strAddress']     = htmlspecialchars($arrObjectInfo['seo_address']);
        $arrTplVars['strObjectVid']   = htmlspecialchars($arrObjectInfo['sev_name']);
        $arrTplVars['strObjectNazn']  = nl2br(htmlspecialchars($arrObjectInfo['seo_naznach']));
        $arrIf['objnazn']   = !empty($arrTplVars['strObjectNazn']);
        $arrTplVars['strEtap']        = htmlspecialchars($arrObjectInfo['seo_other_etap']);
        $arrIf['etap']      = !empty($arrTplVars['strEtap']);
        $arrTplVars['strEtapDate']    = (!empty($arrObjectInfo['seo_etap_date']) ? date('d.m.Y', strtotime($arrObjectInfo['seo_etap_date'])) : '');
        $arrIf['etap.date'] = !empty($arrTplVars['strEtapDate']);
        $arrTplVars['strZemUch']      = htmlspecialchars($arrObjectInfo['seo_sq_zem_uch']);
        $arrIf['zem.uch']   = !empty($arrTplVars['strZemUch']);
        $arrTplVars['strSqZas']       = htmlspecialchars($arrObjectInfo['seo_sq_zastr']);
        $arrIf['sq.zas']    = !empty($arrTplVars['strSqZas']);
        $arrTplVars['strTotalSq']     = htmlspecialchars($arrObjectInfo['seo_sq_total']);
        $arrIf['total.sq']  = !empty($arrTplVars['strTotalSq']);
        $arrTplVars['strEtazh']       = htmlspecialchars($arrObjectInfo['seo_etazh']);
        $arrIf['etazh']     = !empty($arrTplVars['strEtazh']);
        $arrTplVars['strKarkas']      = htmlspecialchars($arrObjectInfo['seo_karkas']);
        $arrIf['karkas']    = !empty($arrTplVars['strKarkas']);
        $arrTplVars['strFundament']   = htmlspecialchars($arrObjectInfo['seo_fundament']);
        $arrIf['fundament'] = !empty($arrTplVars['strFundament']);
        $arrTplVars['strWalls']       = htmlspecialchars($arrObjectInfo['seo_steni']);
        $arrIf['walls']     = !empty($arrTplVars['strWalls']);
        $arrTplVars['strWindows']     = htmlspecialchars($arrObjectInfo['seo_windows']);
        $arrIf['windows']   = !empty($arrTplVars['strWindows']);
        $arrTplVars['strRoof']        = htmlspecialchars($arrObjectInfo['seo_roof']);
        $arrIf['roof']      = !empty($arrTplVars['strRoof']);
        $arrTplVars['strFloors']      = htmlspecialchars($arrObjectInfo['seo_floor']);
        $arrIf['floors']    = !empty($arrTplVars['strFloors']);
        $arrTplVars['strDoors']       = htmlspecialchars($arrObjectInfo['seo_doors']);
        $arrIf['doors']     = !empty($arrTplVars['strDoors']);
        $arrTplVars['strPravoDocs']   = htmlspecialchars($arrObjectInfo['seo_pravo_doc']);
        $arrIf['pravo.docs'] = !empty($arrTplVars['strPravoDocs']);
        $arrTplVars['strPrjPeriod']   = htmlspecialchars($arrObjectInfo['seo_period_prj']);
        $arrIf['prj.period'] = !empty($arrTplVars['strPrjPeriod']);
        $arrTplVars['strBuildPeriod'] = htmlspecialchars($arrObjectInfo['seo_period_bld']);
        $arrIf['build.period'] = !empty($arrTplVars['strBuildPeriod']);
        $arrTplVars['strVolumeBuild'] = htmlspecialchars($arrObjectInfo['seo_bld_volume']);
        $arrIf['volume.build'] = !empty($arrTplVars['strVolumeBuild']);
        $arrTplVars['intPhotoId']     = htmlspecialchars($arrObjectInfo['seo_id']);
        $arrTplVars['strFileName']    =
        $arrTplVars['strFileNameBig'] = htmlspecialchars($arrObjectInfo['si_filename']);

        if(!$arrIf['authed']){
            $arrTplVars['strFileName']   = htmlspecialchars(str_replace(".b.", ".", $arrObjectInfo['si_filename']));
        } else {
            $arrTplVars['jsAuthed'] = 'true';
        }

        //$arrIf['photo'] = !empty($arrObjectInfo['si_id']);
        $arrTplVars['jsPhoto'] = (!empty($arrObjectInfo['si_id'])?'true':'false');

        /**
         * Проверим наличие фотографий из Фотобанка
         */
        $strSqlQuery = "SELECT si_id, si_filename, si_id_rel, si_title FROM `site_images` WHERE `si_foto_object_id` = " . $intObjectId . " AND `si_status` = 'Y' AND `si_type` = 'fotobank'";
        $arrFotobankImages = $objDb->fetchall($strSqlQuery);
        $arrObjectImages = array();
        if (is_array($arrFotobankImages) && !empty($arrFotobankImages)) {
            foreach ($arrFotobankImages as $k => $a) {
                $arrObjectImages[$a['si_id']]['file'] = $a['si_filename'];
                $arrObjectImages[$a['si_id']]['gid'] = $a['si_id_rel'];
                $arrObjectImages[$a['si_id']]['title'] = htmlspecialchars($a['si_title']);
            }
        }
        $arrTplVars['arrObjectImages'] = json_encode($arrObjectImages);
        $arrTplVars['isFotobankImages'] = (is_array($arrObjectImages)&&!empty($arrObjectImages)?'true':'false');

        /**
         * Проверим тип доступа
         */
        if(isset($_SESSION['uAuthInfo']['uID']) && $_SESSION['uAuthInfo']['uID'] > 0) {
            $strSqlQuery = "SELECT * FROM `site_users_access_lnk`, `site_regions_access_type`, `site_regions_access_lnk`, `site_objects_regions_lnk`"
                ." WHERE `sual_su_id` = " . $_SESSION['uAuthInfo']['uID']." AND sual_srat_id = srat_id AND srat_id = sral_srat_id AND sral_sr_id = sorl_sr_id AND sorl_seo_id = ".$intObjectId;
            $arrUA = $objDb->fetchall($strSqlQuery);
            if (is_array($arrUA) && !empty($arrUA)) {
                // Access exists
            } else {
                $arrIf['access'] = $arrIf['no.access'] = false;
                $arrIf['not.permited'] = true;
            }
        }

        // SEO-оптимизация
        $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrObjectInfo['seo_name']))." | PetroStroyBaza.RU";
        $arrTplVars['m_description'] = $objUtil->substrText(htmlspecialchars(strip_tags($arrObjectInfo['seo_naznach'])));

        /**
         * Контактные данные объекта
         */
        if ($arrIf['access']) {
            $strSqlQuery = "SELECT * FROM site_estate_contacts"
                . " LEFT JOIN site_objects_contacts_lnk ON (socl_id_contact = sec_id".(!$arrIf['access']?" AND socl_relation = 'opa'":'').")"
                . " WHERE socl_id_object = $intObjectId AND sec_status = 'Y'";
            $arrContactRecords = $objDb->fetchall( $strSqlQuery );
        }

        if ( is_array($arrContactRecords) ) {
            foreach ( $arrContactRecords as $key => $value ) {
                $arrContactRecords[$key]['title'] = $arrTitleExt[$value['socl_relation']];
                $arrContactRecords[$key]['sec_contact'] = nl2br($value['sec_contact']);
            }
        }
        $objTpl->tpl_loop("page.contents", "contacts.info", $arrContactRecords);

        /**
         * Установим URL для объекта, если ещё не установлен
         */
        if ($flagUpdateUrl && empty($arrObjectInfo['seo_url'])) {
            $strUrl = strtolower( $objUtil->translitBadChars($objUtil->translitKyrToLat(trim($arrObjectInfo['seo_name']))) );
            $strSqlQuery = "UPDATE `site_estate_objects` SET `seo_url` = '".$strUrl."' WHERE `seo_id` = ".$intObjectId;
            if (!$objDb->query($strSqlQuery)) {
                # sql error
            }
        }

        /**
         * Проверим возможность показа адреса пользователю
         * Боты должны видеть адрес, а пользователи через браузер и без доступа - не должны.
         */
        if ($arrIf['access'] || (!preg_match("/Mozilla/i", $strUsAgent) && !preg_match("/Opera/i", $strUsAgent))) {
            $arrIf['access.for.browser'] = true;
        }

        if ($arrIf['access']) {
            $arrIf['not.in.notebook'] = !$objUser->isInNotebook($arrObjectInfo['seo_id']);
        }

    } else {
        header("Location: /error");
        exit();
    }
} else {
    header("Location: /error");
    exit();
}

$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
