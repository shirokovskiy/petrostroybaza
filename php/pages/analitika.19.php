<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Аналитика [analitika] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "analitika.19.tpl");

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if (isset($arrReqUri[1]) && !empty($arrReqUri[1]) && intval($arrReqUri[1])>0 ) {
  $intId = intval($arrReqUri[1]);
  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAnalitic']." WHERE pa_status='Y' AND pa_id='$intId'";
  $arrArticleInfo = $objDb->fetch( $strSqlQuery );

  if (!empty($arrArticleInfo)) {
    $arrArticleInfo['strDatePublArticle'] = $objUtil->workDate(2, $arrArticleInfo['pa_date_publ']);
    $arrArticleInfo['strAnonsBody'] = nl2br($arrArticleInfo['pa_anons']);
    $arrArticleInfo['strArticleBody'] = nl2br($arrArticleInfo['pa_article']);
    //$arrIf['picture'] = ( file_exists(DROOT."storage/images/analitic/article.photo.".$arrArticleInfo['pa_id'].".jpg") && $arrArticleInfo['pa_image'] == 'Y');
    $objTpl->tpl_array("page.contents", $arrArticleInfo);

    $strSqlQuery = "SELECT * FROM ".$_db_tables["stAnaliticFiles"]." WHERE paf_rel_id = '$intId'";
    $arrArticleFiles = $objDb->fetchall( $strSqlQuery );
  } else {
    header('Location: '.SITE_URL.'error');
    exit();
  }

  $arrIf['one.article'] = true;

} else if (!empty($arrReqUri[1]) && $arrReqUri[1] == 'download' && intval($arrReqUri[2])>0) {

  $objTpl->Template(SITE_TPL_TPL_DIR);
  $objTpl->tpl_load("global.tpl", "site.global.4.tpl");
  unset($objTpl->arrFrgForLoad);
  $objTpl->tpl_parse_frg($objTpl->files['global.tpl']);

  // Грузим нужный файл
  $intRecordId = intval($arrReqUri[2]);
  $strSqlQuery = "SELECT paf_filename FROM ".$_db_tables['stAnaliticFiles']." WHERE paf_status='Y' AND paf_id='$intRecordId'";
  $strFileNameInfo = $objDb->fetch( $strSqlQuery, 0 );

  if (!empty($strFileNameInfo)) {
    $SourceFile = PRJ_FILES.'analitics/'.$strFileNameInfo;

    if (file_exists($SourceFile)) {
      $sizeResFile = filesize($SourceFile);

      Header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT\r\n" );
      Header( "Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT\r\n" );
      Header( "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
      Header( "Cache-Control: post-check=0, pre-check=0, false\r\n" );
      Header( "Pragma: no-cache\r\n" );
      Header( "HTTP/1.1 200 OK\r\n" );

      Header( "Content-Disposition: attachment; filename=$strFileNameInfo\r\n" );
      Header( "Accept-Ranges: bytes\r\n" );
      Header( "Content-Type: application/force-download" );
      Header( "Content-Length: $sizeResFile\r\n\r\n" );

      readfile( $SourceFile );
      die();
    } else {
      header ("HTTP/1.0 404 Not Found");
      $arrTplVars['strMessage'] = "Ошибка! Файл не существует.";
    }
  } else {
    $arrTplVars['strMessage'] = "Ошибка! Файл не найден.";
  }

  $objTpl->Template(SITE_TPL_PAGE_DIR);
  $objTpl->tpl_load("page.contents", "empty.14.tpl");
} else {
  /**
   * Список статей
   */

  // Всего записей в базе
  $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stAnalitic']." WHERE pa_status = 'Y' AND pa_date_publ <= CURDATE()";
  $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
  // Выбрано записей
  $strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stAnalitic']." WHERE pa_status = 'Y' AND pa_date_publ <= CURDATE()";
  $arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
  // ***** BEGIN: Построение пейджинга для вывода списка
  $objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
  /**
  * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
  * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
  */
  $objPagination->strPaginatorTpl = "site.global.1.tpl";
  $objPagination->iQtyRecsPerPage = 15;
  $objPagination->strColorLinkStyle = "link-b";
  $objPagination->strColorActiveStyle = "tab-bl-b";
  $objPagination->strNameImageBack = "arrow_one_left.gif";
  $objPagination->strNameImageForward = "arrow_one_right.gif";
  // Создаем блок пэйджинга
  $objPagination->paCreate();
  // ***** END: Построение пейджинга для вывода списка
  // Запрос для выборки нужных записей
  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAnalitic']." WHERE pa_status = 'Y' AND pa_date_publ <= CURDATE()"
    . " ORDER BY pa_date_publ DESC, pa_id DESC ".$objPagination->strSqlLimit;
  $arrLastArticles = $objDb->fetchall( $strSqlQuery );
  // кол-во публикаций показанных на странице
  $arrTplVars['intQuantShowRecOnPage'] = ( is_array($arrLastArticles) ? count($arrLastArticles) : 0 );
  // Присвоение значения пэйджинга для списка
  $arrTplVars['blockPaginator'] = $objPagination->paShow();

  if ( is_array($arrLastArticles) ) {
    foreach ( $arrLastArticles as $key => $value ) {
      $arrLastArticles[$key]['strDatePublicArticle'] = date('d.m.y', strtotime($value['pa_date_publ']));
      //$arrIf['article.picture.'.$value['pa_id']] = ( file_exists(DROOT."storage/images/news/news.photo.".$value['pa_id'].".jpg") && $value['pa_image'] == 'Y');
    }
  }

  $objTpl->tpl_loop("page.contents", "last.articles", $arrLastArticles);

  $arrIf['article.archive'] = true;
}
$arrIf['is.attach.files'] = is_array($arrArticleFiles) && !empty($arrArticleFiles);
$objTpl->tpl_loop("page.contents", "attach.files", $arrArticleFiles);
$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
