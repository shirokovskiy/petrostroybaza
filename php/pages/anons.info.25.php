<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Карточка объявления Анонса мероприятия [anons.info] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "anons.info.25.tpl");
$arrTplVars['urlLinkBack'] = $_SERVER['HTTP_REFERER'];

$intRecordId = intval($arrReqUri[1]);

if (!empty($intRecordId)) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($intRecordId);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAnonses']." WHERE pam_id ='$intRecordId' AND pam_status = 'Y'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['pam_name']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes($arrInfo['pam_address']));
    $arrTplVars['strContacts'] = htmlspecialchars(stripslashes($arrInfo['pam_contacts']));
    $arrTplVars['strInfo'] = htmlspecialchars(stripslashes($arrInfo['pam_info']));
    $arrTplVars['strTime'] = htmlspecialchars(stripslashes($arrInfo['pam_action_date']));

    $arrTplVars['cbxStatus'] = $arrInfo['pam_status'] == 'Y' ? ' checked' : '';
  } else {
    $arrIf['no.anons.info'] = true;
  }
} else {
  $arrIf['no.anons.info'] = true;
}

$arrIf['is.anons.info'] = !$arrIf['no.anons.info'];

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
