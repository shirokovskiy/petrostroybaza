<?php
/** Created by WMBM(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Блокнот [notebook] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "notebook.42.tpl");
$arrTplVars['stPage'] = $stPage = ( intval($_GET['stPage']) > 0 ? $_GET['stPage'] : 1 );
$arrIf['access'] = $_SESSION['uAuthInfo']['uAccess'] == 1;
$arrIf['no.access'] = !$arrIf['access'];
$arrIf['access.for.browser'] = false;
$tpl = "page.contents";
$pathMaps = DROOT.'storage/maps/';

if (!is_object($objUser)) {
    $objUser = new User();
}

/**
 * Добавить заметку
 */
if (isset($_GET['form']) && $_GET['form']=='note' && $objUser->getId() > 0) {
    if (isset($_POST['intNoteObjectID']) && intval($_POST['intNoteObjectID']) && !empty($_POST['strNote'])) {
        // тогда запишем данные
        $intObjID = intval($_POST['intNoteObjectID']);

        $strSqlQuery = "INSERT INTO `site_users_notebook_lnk` SET"
            . " sunl_su_id = ".$objUser->getId()
            . ", sunl_seo_id = ".$intObjID
            . ", sunl_note = '".mysql_real_escape_string($_POST['strNote'])."'"
        ;
        if (!$objDb->query($strSqlQuery)) {
            # sql error
            $strSqlQuery = "UPDATE `site_users_notebook_lnk` SET"
                . " `sunl_note` = '".mysql_real_escape_string($_POST['strNote'])."'"
                . " WHERE `sunl_su_id` = ".$objUser->getId()." AND sunl_seo_id = ".$intObjID;
            if (!$objDb->query($strSqlQuery)) {
                # sql error
            }
        }

        header('Location: '.SITE_URL.'notebook?stPage='.$stPage);
        exit;
    }
}

if ($objUser->isAuthed()) {
    // Отбор региональный
    $strSqlQuery = "SELECT sral_sr_id FROM `site_regions_access_lnk` ORDER BY sral_srat_id";
    $arrRegions = $objDb->fetchcol($strSqlQuery);

    $strLeftJoin = " LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id = seo_id)";
    $strLeftJoin .= " LEFT JOIN site_users_notebook_lnk ON (sunl_seo_id = seo_id)";
    $strLeftJoin .= " LEFT JOIN site_regions tSr ON (sorl_sr_id = sr_id)";
    $strSqlWhere .= " AND sorl_sr_id IN (".implode(',' , $arrRegions).") AND sunl_su_id = ".$objUser->getId();

    // Всего записей в базе
    $strSqlQuery = "SELECT COUNT(tSeo.seo_id) AS intQuantAllRecords FROM site_estate_objects tSeo $strLeftJoin WHERE seo_status = 'Y' $strSqlWhere";
    $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
    $arrTplVars['strQuantityAllObjects'] = ($arrTplVars['intQuantAllRecords'] > 0?" &mdash; ":""). $objUtil->num2str($arrTplVars['intQuantAllRecords'], false, "obj");
    // Выбрано записей
    $strSqlQuery = "SELECT COUNT(tSeo.seo_id) AS intQuantSelectRecords FROM site_estate_objects tSeo $strLeftJoin WHERE seo_status = 'Y' $strSqlWhere";
    $arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.1.tpl";
    $objPagination->iQtyRecsPerPage = 10;
    $objPagination->strColorLinkStyle = "link-b";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strNameImageBack = "arrow_one_left.gif";
    $objPagination->strNameImageForward = "arrow_one_right.gif";
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка

    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT tSeo.*, tSi.si_filename, tSra.sra_name, tSet.set_name, tSr.sr_name, sunl_note"
        . " FROM site_estate_objects tSeo"
        . " LEFT JOIN site_images tSi ON (si_id_rel = seo_id)"
        . " LEFT JOIN site_estate_types tSet ON (seo_id_etype = set_id)"
        . " LEFT JOIN site_regions_adm tSra ON (seo_id_adm_reg = sra_id)"
        . $strLeftJoin
        . " WHERE seo_status = 'Y' $strSqlWhere GROUP BY seo_id ORDER BY seo_etap_date DESC, seo_date_change DESC, seo_date_add DESC, seo_id DESC";

    if ($arrTplVars['intQuantAllRecords'] == 0) {
        unset($_SESSION['cachedNotebookQuery']);
    }

//    unset($_SESSION['cachedNotebookQuery'], $_SESSION['cachedSearchQuery']);

    // запишем в память объекты блокнота для отображения на карте
    $cachedSql = md5($strSqlQuery . $arrTplVars['intQuantAllRecords'] . $objUser->getId() . date("YmdH"));

    if (isset($_SESSION['cachedNotebookQuery'])) {
        if ($cachedSql != $_SESSION['cachedNotebookQuery']) {
            # значит что-то изменилось в новом запросе, например поменялся набор объектов
            unset($_SESSION['cachedNotebookQuery']);
        }
    }

    if (!file_exists($pathMaps.'geo-map-nb-data.'.$cachedSql.'.json') && isset($cachedSql) && strlen($cachedSql) == 32 && !isset($_SESSION['cachedNotebookQuery']) && $arrTplVars['intQuantAllRecords'] > 0) {
        $_SESSION['cachedNotebookQuery'] = $cachedSql;
        // подготовим карту объектов
        $arrRes['type'] = 'FeatureCollection';
        $arrRes['features'] = array();
        $id = 0;

        $arrAllNotebookObjects = $objDb->fetchall( $strSqlQuery );

        foreach ( $arrAllNotebookObjects as $object ) {
            if (!empty($object['seo_coords'])) {
                // подготовим карту объектов
                $obj = new stdClass();
                $obj->type = 'Feature';
                $obj->id = $id++;
                $obj->geometry = new stdClass();
                $obj->geometry->type = 'Point';
                $obj->geometry->coordinates = explode(',', $object['seo_coords']);
                $obj->properties = new stdClass();
                $obj->properties->balloonContent = strip_tags($object['seo_name']).'<br><a target="_blank" href="'.SITE_URL.'objectinfo/'.($object['seo_url']?:$object['seo_id']).'" class="link-darkblue">'.strip_tags($object['seo_address'])."</a>";
                $obj->properties->clusterCaption = strip_tags($object['seo_name']);
                $obj->properties->hintContent = strip_tags($object['seo_name']);

                $arrRes['features'][] = $obj;
            }
        }

        # есть закешированный запрос SQL
        # запишем его в файл для дальнейшего использования
        if (!is_dir($pathMaps)) {
            if(!mkdir($pathMaps)) {
                # ошибка
                $errorCreateFolder = true;
            }
        }

        if ((!isset($errorCreateFolder) || !$errorCreateFolder) && is_array($arrRes['features']) && !empty($arrRes['features'])) {
            file_put_contents($pathMaps.'geo-map-nb-data.'.$cachedSql.'.json', json_encode($arrRes));
        }
    } else {
        $cachedSql = $_SESSION['cachedNotebookQuery'];
    }

    $strSqlQuery .= ' '.$objPagination->strSqlLimit;
    $arrObjects = $objDb->fetchall( $strSqlQuery );
    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrObjects) ? count($arrObjects) : 0 );
    // Присвоение значения пэйджинга для списка
    $arrTplVars['blockPaginator'] = $objPagination->paShow();

    if ( is_array($arrObjects) ) {
        foreach ( $arrObjects as $key => $value ) {
            $arrObjects[$key]['strAdmReg'] = !empty($value['seo_other_adm_reg']) ? htmlspecialchars($value['seo_other_adm_reg']) : htmlspecialchars($value['sra_name']);
            $arrObjects[$key]['urlParam'] = (!empty($value['seo_url']) ? $value['seo_url'] : $value['seo_id']);
            $arrIf['object.photo.'.$value['seo_id']] = ( !empty($value['si_filename']) && file_exists(DROOT."storage/images/objects/".str_replace('.b.', '.', $value['si_filename']) ) );
            /* если фотографии всё же нет, проверить нет ли фото из фотобанка */
            if (!$arrIf['object.photo.'.$value['seo_id']] && $value['seo_id']>0) {
                $strSqlQuery = "SELECT si_filename, si_id_rel FROM `site_images` LEFT JOIN `site_images_group` ON (sig_id = si_id_rel AND sig_img_id = si_id) WHERE `si_foto_object_id` = ".$value['seo_id']." AND si_type = 'fotobank' ORDER BY si_id";
                $arrImagesForObject = $objDb->fetch($strSqlQuery);
                if (is_array($arrImagesForObject) && !empty($arrImagesForObject)) {
                    $arrObjects[$key]['strImagePath'] = $arrTplVars['cfgAllImg'].'fotobank/groups/'.$arrImagesForObject['si_id_rel'].'/'.$arrImagesForObject['si_filename'];
                    $arrIf['object.photo.'.$value['seo_id']] = true;
                }
            } else {
                $arrObjects[$key]['strImagePath'] = $arrTplVars['cfgAllImg'].'objects/'.$arrObjects[$key]['si_filename'];
            }
            $arrIf['naznach.'.$value['seo_id']] = !empty($value['seo_naznach']);
            $arrObjects[$key]['seo_naznach'] = nl2br($value['seo_naznach']);
            $arrIf['etap.stroy.'.$value['seo_id']] = !empty($value['seo_other_etap']);
            if( $arrIf['etap.stroy.'.$value['seo_id']] && !empty($value['seo_etap_date']) ) {
                $arrObjects[$key]['seo_etap_date_formated'] = date('d.m.Y', strtotime($value['seo_etap_date']));
            }
            $arrObjects[$key]['strRegion'] = htmlspecialchars(stripslashes($value['sr_name']));
            $arrObjects[$key]['strNote'] = htmlspecialchars(stripslashes($value['sunl_note']));
            $arrIf['my.note.'.$value['seo_id']] = !empty($value['sunl_note']);
        }
    } else {
        $arrIf['no.objects'] = true;
    }
} else {
    // Нет доступа
}
$arrIf['is.objects'] = !$arrIf['no.objects'] && !empty($arrObjects);

if (file_exists($pathMaps.'geo-map-nb-data.'.$cachedSql.'.json') && isset($cachedSql) && strlen($cachedSql) == 32) {
    $arrIf['mapped'] = true;
}

$objTpl->tpl_loop($tpl, "objects", $arrObjects);
$objTpl->tpl_if($tpl, $arrIf);
$objTpl->tpl_array($tpl, $arrTplVars);
