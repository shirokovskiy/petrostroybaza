<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Объект подробно [objectinfo] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "objectinfo.12.tpl");

$arrTplVars['stPage'] = ( intval($_GET['stPage']) > 0 ? $_GET['stPage'] : 1 );
$arrTplVars['Reg'] = ( isset($_GET['reg']) ? $_GET['reg'] : '' );

$arrIf['access'] = $_SESSION['uAuthInfo']['uAccess'] == 1;
$arrIf['no.access'] = !$arrIf['access'];
$arrIf['access.for.browser'] = false;
$arrTplVars['jsAuthed'] =
$arrTplVars['isFotobankImages'] =
$arrTplVars['jsPhoto'] = 'false'; // by default

if (isset($_SERVER['HTTP_REFERER'])) {
    $arrTplVars['urlLinkBack'] = $_SERVER['HTTP_REFERER'];
} else {
    $arrTplVars['urlLinkBack'] = SITE_URL.'objects'.(!empty($_GET['stPage'])?'?stPage='.$_GET['stPage']:'?').(!empty($_GET['reg'])?'&reg='.$_GET['reg']:'');
}

if (isset($_SESSION['SF'])) {
    $arrTplVars['urlLinkBack'] = SITE_URL.'search?stPage='.(!empty($_GET['stPage'])?(int)$_GET['stPage']:1).(!empty($_GET['reg'])?'&reg='.$_GET['reg']:'');
    $arrTplVars['strLinkBack'] = 'вернуться к результату поиска';
} else {
    $arrTplVars['strLinkBack'] = 'вернуться к списку объектов';
}

// TODO: ALTER TABLE `site_estate_objects` ADD `seo_pa` ENUM('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N' COMMENT 'Публичный доступ' AFTER `seo_anons`;

$flagUpdateUrl = false;

if (isset($arrReqUri[1])) {
    if (is_numeric($arrReqUri[1])) {
        $intObjectId = intval($arrReqUri[1]);
        $strWhereSent = "seo_id=".$intObjectId;
        $flagUpdateUrl = true;
    } else {
        $strWhereSent = "seo_url='".mysql_real_escape_string($arrReqUri[1])."'";
    }

    $strSqlQuery = "SELECT O.*, I.si_filename, I.si_id, I.si_date_add, ET.set_name, AR.sra_name, V.sev_name, R.sr_name"
        . " FROM site_estate_objects O"
        . " LEFT JOIN site_images I ON (si_id_rel = seo_id)"
        . " LEFT JOIN site_estate_types ET ON (seo_id_etype = set_id)"
        . " LEFT JOIN site_regions_adm AR ON (seo_id_adm_reg = sra_id)"
        . " LEFT JOIN site_estate_vid V ON (seo_id_vid = sev_id)"
        . " LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id = seo_id)"
        . " LEFT JOIN site_regions R ON (sorl_sr_id = sr_id)"
        . " WHERE seo_status='Y' AND ".$strWhereSent;
    $arrObjectInfo = $objDb->fetch( $strSqlQuery );
    // Если данные найдены
    if (is_array($arrObjectInfo)) {
        if(empty($intObjectId)) $intObjectId = intval($arrObjectInfo['seo_id']);
        if(isset($arrObjectInfo['seo_pa']) && !empty($arrObjectInfo['seo_pa']) && $arrObjectInfo['seo_pa']=='Y') {
            $arrIf['access'] = true;
            $arrIf['no.access'] = !$arrIf['access'];
        }

        /**
         * Запись комментария
         */
        if (isset($_POST['type']) && $_POST['type'] == 'comment') {
            $result = false;
            if (!empty($_POST['strComment'])) {
                if (isset($_POST['cbxIAgree']) && $_POST['cbxIAgree'] == 'Y') {
                    $objUser->setCommentsAgreement();
                }
                if (isset($_POST['cbxGetComments']) && $_POST['cbxGetComments'] == 'Y') {
                    $objUser->setCommentsAnswersAgreement($intObjectId);
                } else {
                    $objUser->removeCommentsAnswersAgreement($intObjectId);
                }
                $result = $objUser->saveCommentForObject($intObjectId, $_POST['strComment']);
            }

            if ($result) {
                header('Location: '.$arrTplVars['strCurrentUri']); exit;
            } else {
                $arrIf['show_js_error_for_comment'] = true;
                $arrTplVars['strComment'] = htmlspecialchars(trim($_POST['strComment']));
            }
        }
        /*****/


        $arrTplVars['intObjectID']    = htmlspecialchars($arrObjectInfo['seo_id']);
        $arrTplVars['strObjectName']  = htmlspecialchars($arrObjectInfo['seo_name']);
        $arrTplVars['strChapterName'] = htmlspecialchars($arrObjectInfo['set_name']);
        $arrTplVars['strRegion']      = htmlspecialchars($arrObjectInfo['sr_name']);
        $arrTplVars['strAdmReg']      = !empty($arrObjectInfo['seo_other_adm_reg']) ? htmlspecialchars($arrObjectInfo['seo_other_adm_reg']) : htmlspecialchars($arrObjectInfo['sra_name']);
        $arrTplVars['strAddress']     = htmlspecialchars($arrObjectInfo['seo_address']);
        $arrTplVars['strAddressData'] = ($arrObjectInfo['sr_name']?$arrObjectInfo['sr_name'].', ':'') . $objUtil->translitBadChars($arrObjectInfo['seo_address'], false /* пробелы не трогать */ );
        $arrIf['is.address'] = !empty($arrTplVars['strAddress']);
        $arrTplVars['strObjectVid']   = htmlspecialchars($arrObjectInfo['sev_name']);
        $arrTplVars['strObjectNazn']  = nl2br($arrObjectInfo['seo_naznach']);
        $arrIf['objnazn']   = !empty($arrTplVars['strObjectNazn']);
        $arrTplVars['strEtap']        = htmlspecialchars($arrObjectInfo['seo_other_etap']);
        $arrIf['etap']      = !empty($arrTplVars['strEtap']);
        $arrTplVars['strEtapDate']    = (!empty($arrObjectInfo['seo_etap_date']) ? date('d.m.Y', strtotime($arrObjectInfo['seo_etap_date'])) : '');
        $arrIf['etap.date'] = !empty($arrTplVars['strEtapDate']);
        $arrTplVars['strZemUch']      = htmlspecialchars($arrObjectInfo['seo_sq_zem_uch']);
        $arrIf['zem.uch']   = !empty($arrTplVars['strZemUch']);
        $arrTplVars['strSqZas']       = htmlspecialchars($arrObjectInfo['seo_sq_zastr']);
        $arrIf['sq.zas']    = !empty($arrTplVars['strSqZas']);
        $arrTplVars['strTotalSq']     = htmlspecialchars($arrObjectInfo['seo_sq_total']);
        $arrIf['total.sq']  = !empty($arrTplVars['strTotalSq']);
        $arrTplVars['strEtazh']       = htmlspecialchars($arrObjectInfo['seo_etazh']);
        $arrIf['etazh']     = !empty($arrTplVars['strEtazh']);
        $arrTplVars['strKarkas']      = htmlspecialchars($arrObjectInfo['seo_karkas']);
        $arrIf['karkas']    = !empty($arrTplVars['strKarkas']);
        $arrTplVars['strFundament']   = htmlspecialchars($arrObjectInfo['seo_fundament']);
        $arrIf['fundament'] = !empty($arrTplVars['strFundament']);
        $arrTplVars['strWalls']       = htmlspecialchars($arrObjectInfo['seo_steni']);
        $arrIf['walls']     = !empty($arrTplVars['strWalls']);
        $arrTplVars['strWindows']     = htmlspecialchars($arrObjectInfo['seo_windows']);
        $arrIf['windows']   = !empty($arrTplVars['strWindows']);
        $arrTplVars['strRoof']        = htmlspecialchars($arrObjectInfo['seo_roof']);
        $arrIf['roof']      = !empty($arrTplVars['strRoof']);
        $arrTplVars['strFloors']      = htmlspecialchars($arrObjectInfo['seo_floor']);
        $arrIf['floors']    = !empty($arrTplVars['strFloors']);
        $arrTplVars['strDoors']       = htmlspecialchars($arrObjectInfo['seo_doors']);
        $arrIf['doors']     = !empty($arrTplVars['strDoors']);
        $arrTplVars['strPravoDocs']   = htmlspecialchars($arrObjectInfo['seo_pravo_doc']);
        $arrIf['pravo.docs'] = !empty($arrTplVars['strPravoDocs']);
        $arrTplVars['strPrjPeriod']   = htmlspecialchars($arrObjectInfo['seo_period_prj']);
        $arrIf['prj.period'] = !empty($arrTplVars['strPrjPeriod']);
        $arrTplVars['strBuildPeriod'] = htmlspecialchars($arrObjectInfo['seo_period_bld']);
        $arrIf['build.period'] = !empty($arrTplVars['strBuildPeriod']);
        $arrTplVars['strVolumeBuild'] = htmlspecialchars($arrObjectInfo['seo_bld_volume']);
        $arrIf['volume.build'] = !empty($arrTplVars['strVolumeBuild']);
        $arrTplVars['intPhotoId']     = htmlspecialchars($arrObjectInfo['seo_id']);
        $arrTplVars['strFileName']    =
        $arrTplVars['strFileNameBig'] = htmlspecialchars($arrObjectInfo['si_filename']);

        if(!$arrIf['authed']){
            $arrTplVars['strFileName']   = htmlspecialchars(str_replace(".b.", ".", $arrObjectInfo['si_filename']));
        } else {
            $arrTplVars['jsAuthed'] = 'true';
        }

        //$arrIf['photo'] = !empty($arrObjectInfo['si_id']);
        $arrTplVars['jsPhoto'] = (!empty($arrObjectInfo['si_id'])?'true':'false');

        /**
         * Проверим наличие фотографий из Фотобанка
         */
        $strSqlQuery = "SELECT si_id, si_filename, si_id_rel, si_title FROM `site_images` WHERE `si_foto_object_id` = " . $intObjectId . " AND `si_status` = 'Y' AND `si_type` = 'fotobank' ORDER BY si_date_add DESC";

        $arrFotobankImages = $objDb->fetchall($strSqlQuery);
        $arrObjectImages = array();
        if (is_array($arrFotobankImages) && !empty($arrFotobankImages)) {
            $sorter_js_array = 0;
            foreach ($arrFotobankImages as $k => $a) {
                $arrObjectImages[$sorter_js_array]['file'] = $a['si_filename'];
                $arrObjectImages[$sorter_js_array]['gid'] = $a['si_id_rel'];
                $arrObjectImages[$sorter_js_array]['title'] = htmlspecialchars($a['si_title']);
                $arrObjectImages[$sorter_js_array++]['id'] = $a['si_id'];
            }
        }
        $arrTplVars['arrObjectImages'] = json_encode($arrObjectImages);
        $arrTplVars['isFotobankImages'] = (is_array($arrObjectImages)&&!empty($arrObjectImages)?'true':'false');

        /**
         * Проверим тип доступа
         */
        if(isset($_SESSION['uAuthInfo']['uID']) && $_SESSION['uAuthInfo']['uID'] > 0) {
            $strSqlQuery = "SELECT * FROM `site_users_access_lnk`, `site_regions_access_type`, `site_regions_access_lnk`, `site_objects_regions_lnk`"
                ." WHERE `sual_su_id` = " . $_SESSION['uAuthInfo']['uID']." AND `sual_type` = 'estate'"
                ." AND sual_srat_id = srat_id AND srat_id = sral_srat_id AND sral_sr_id = sorl_sr_id AND sorl_seo_id = ".$intObjectId;
            $arrUA = $objDb->fetchall($strSqlQuery);
            if (is_array($arrUA) && !empty($arrUA)) {
                // Access exists
            } else {
                $arrIf['access'] = $arrIf['no.access'] = false;
                $arrIf['not.permited'] = true;
            }
        }

        // SEO-оптимизация
        $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrObjectInfo['seo_name']))." | PetroStroyBaza.RU";
        $arrTplVars['m_description'] = $objUtil->substrText(htmlspecialchars(strip_tags($arrObjectInfo['seo_naznach'])));

        /**
         * Контактные данные объекта
         */
        if ($arrIf['access']) {
            $strSqlQuery = "SELECT * FROM site_estate_contacts"
                . " LEFT JOIN site_objects_contacts_lnk ON (socl_id_contact = sec_id".(!$arrIf['access']?" AND socl_relation = 'opa'":'').")"
                . " WHERE socl_id_object = $intObjectId AND sec_status = 'Y'";
            $arrContactRecords = $objDb->fetchall( $strSqlQuery );
        }

        if ( is_array($arrContactRecords) ) {
            foreach ( $arrContactRecords as $key => $value ) {
                $arrContactRecords[$key]['title'] = $arrTitleExt[$value['socl_relation']];
                $arrContactRecords[$key]['sec_contact'] = nl2br($value['sec_contact']);
            }
        } else {
            $arrTplVars['strNoAccessContacts'] = nl2br( file_get_contents(PRJ_FILES . 'contacts.data.txt') );
        }
        $objTpl->tpl_loop("page.contents", "contacts.info", $arrContactRecords);

        /**
         * Установим URL для объекта, если ещё не установлен
         */
        if ($flagUpdateUrl && empty($arrObjectInfo['seo_url'])) {
            $strUrl = strtolower( $objUtil->translitBadChars($objUtil->translitKyrToLat(trim($arrObjectInfo['seo_name']))) );
            $strSqlQuery = "UPDATE `site_estate_objects` SET `seo_url` = '".$strUrl."' WHERE `seo_id` = ".$intObjectId;
            if (!$objDb->query($strSqlQuery)) {
                # sql error
            }
        }

        /**
         * Проверим возможность показа адреса пользователю
         * Боты должны видеть адрес, а пользователи через браузер и пользователи без доступа - не должны.
         */
        if ($arrIf['access'] || (!preg_match("/Mozilla/i", $strUsAgent) && !preg_match("/Opera/i", $strUsAgent))) {
            $arrIf['access.for.browser'] = true;
        }

        if ($arrIf['access'] && $objUser->isAuthed()) {
            $arrIf['not.in.notebook'] = !$objUser->isInNotebook($arrObjectInfo['seo_id']);
        }

        /**
         * Покажем комментарии
         */
        $EstateObject = new EstateObject();
        $arrComments = $EstateObject->getComments($intObjectId);

        if (is_array($arrComments) && !empty($arrComments)) {
            foreach ($arrComments as $k => $comment) {
                $arrComments[$k]['strComment'] = htmlspecialchars( stripslashes( trim($comment['suc_comment']) ) );
                $arrComments[$k]['CommentDate'] = $objUtil->workDate(4, $comment['suc_created']);
            }
        }

        $arrIf['comments'] = (is_array($arrComments) && !empty($arrComments));
        $objTpl->tpl_loop("page.contents", "comments", $arrComments);

        /**
         * Покажем доп.инфо
         */
        $arrDopInfo = $EstateObject->getListInfo($intObjectId);
        if (is_array($arrDopInfo) && !empty($arrDopInfo)) {
            foreach ($arrDopInfo as $k => $arr) {
                $arrDopInfo[$k]['date'] = $objUtil->workDate(3, $arr['date']);
                $arrDopInfo[$k]['content_short'] = $objUtil->substrText($arr['content'], 200, true);
                $arrDopInfo[$k]['content'] = nl2br($arr['content']);
                $arrIf['is.source.'.$arr['id']] = !empty($arr['source']);
            }
        } else {
            $arrIf['no.list.dop.info'] = true;
        }
        $objTpl->tpl_loop("page.contents", "list.dop.info", $arrDopInfo);

        $arrIf['comments.need.agree'] = $_SESSION['uAuthInfo']['uCommentsAgree'] != 'Y';

        if ($objUser->isAuthed()) {
            $arrTplVars['cbxGetComments'] = $objUser->isCommentsAnswersAgreement($intObjectId) ? ' checked' : '';
        }
    } else {
        header("Location: /error");
        exit();
    }
} else {
    header("Location: /error");
    exit();
}

$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
