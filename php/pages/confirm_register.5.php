<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Подтверждение регистрации [confirm_register] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "confirm_register.5.tpl");

if (isset($_GET['regcode']) && strlen($_GET['regcode']) == 32) {
  $strSqlQuery = "SELECT su_id FROM ".$_db_tables['stUsers']
      . " WHERE su_active='N' AND su_passw='{$_GET['regcode']}'";
  $intExistUser = $objDb->fetch( $strSqlQuery, 'su_id');

  if ($intExistUser > 0) {
    $strSqlQuery = "UPDATE ".$_db_tables['stUsers']." SET"
      . " su_status = 'Y'"
      . ", su_active = 'Y'"
      . " WHERE su_id='$intExistUser'"
      ;
    if ( !$objDb->query( $strSqlQuery ) ) {
      # sql error
      $res = print_r($strSqlQuery, true);
      $fp = @fopen(DEBUG_PATH."debug.txt", "a+");
      @fwrite($fp, date('y.m.d H:i:s')." ".$res."\n");
      @fclose($fp);
    } else {
      header('Location: '.SITE_URL.'confirm_register?errMess=msgOk');
      exit();
    }
  } else {
    $arrIf['no.such.user'] = true;
  }
} else {
  if (!isset($_GET['errMess'])) {
    $arrIf['no.such.code'] = true;
  }
}

if ($_GET['errMess']=='msgOk') {
  $arrIf['success'] = true;
}

$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
