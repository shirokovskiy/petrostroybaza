<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Тендеры [tenders] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "tenders.22.tpl");

/**
 * Выборка списка тендеров
 */
// Всего записей в базе
//$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stTenders']." WHERE pt_status = 'Y'";
//$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stTenders']." WHERE pt_status = 'Y'";
$arrTplVars['intQuantSelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
* Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
* кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
*/
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 10;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stTenders']." WHERE pt_status = 'Y' ORDER BY pt_id DESC ".$objPagination->strSqlLimit;
$arrTenders = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrTenders) ? count($arrTenders) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrTenders) ) {
//  foreach ( $arrTenders as $key => $value ) {
//  }
} else {
  $arrIf['no.tenders'] = true;
}

$arrIf['is.tenders'] = !$arrIf['no.tenders'] && !empty($arrTenders);

$objTpl->tpl_loop("page.contents", "tenders", $arrTenders);
$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
