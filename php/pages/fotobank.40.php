<?php
/** Created by WMBM(c) 2012 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Фотобанк [fotobank] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "fotobank.40.tpl");

/**
 * Получить данные о группе
 */
$intRecordId = intval($arrReqUri[1]);
$strSqlQuery = "SELECT * FROM `site_images_group` WHERE `sig_id` = " . $intRecordId." AND sig_status = 'Y' AND sig_type = 'fotobank'";
$arrGroup = $objDb->fetch($strSqlQuery);
$objTpl->tpl_array("page.contents", $arrGroup);

/**
 * Получить данные о фотках
 */
$strSqlQuery = "SELECT * FROM `site_images` WHERE `si_id_rel` = " . $intRecordId." AND si_status = 'Y' AND si_type='fotobank' ORDER BY si_date_change DESC, si_date_add, si_id";
$arrPhotos = $objDb->fetchall($strSqlQuery);
$s = 0;
foreach ($arrPhotos as $key => $photo) {
	$arrPhotos[$key]['strDate'] = date("d.m.Y", strtotime($photo['si_date_add']));
	$arrPhotos[$key]['strDateUrl'] = date("Y-m-d", strtotime($photo['si_date_add']));
	$arrPhotos[$key]['strFilename'] = substr($photo['si_filename'],0,-4);
	$arrPhotos[$key]['strClass'] = 'fotocell';
	if (++$s == 4) {
		$arrPhotos[$key]['strClass'] .= ' last';
		$s = 0;
	}

}
$objTpl->tpl_loop("page.contents", "photolist", $arrPhotos);

$objTpl->tpl_array("page.contents", $arrTplVars);
