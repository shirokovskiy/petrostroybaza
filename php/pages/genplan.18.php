<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Генплан [genplan] */
if (preg_match("/msie/i", $_SERVER['HTTP_USER_AGENT'])) {
  $arrIf['msie'] = true;
}
$arrIf['op.ff'] = !$arrIf['msie'];

if (isset($arrReqUri[1]) && intval($arrReqUri[1]) > 0 ) {
  $objTpl->Template(SITE_TPL_TPL_DIR);
  $objTpl->tpl_load("global.tpl", "site.global.4.tpl");
  unset($objTpl->arrFrgForLoad);
  $objTpl->tpl_parse_frg($objTpl->files['global.tpl']);

  /**
   * Здесь можно будет поставить проверку на авторизацию
   */
  //if ( intval($_SESSION['uAuthInfo']['uID']) > 0 ) {
    // Грузим нужный файл
    $intRecordId = intval($arrReqUri[1]);
    $strSqlQuery = "SELECT pg_filename FROM ".$_db_tables['stGenplan']." WHERE pg_status='Y' AND pg_id='$intRecordId'";
    $strFileNameInfo = $objDb->fetch( $strSqlQuery, 0 );
  //}

  if (!empty($strFileNameInfo)) {
    $SourceFile = PRJ_FILES.'genplan/'.$strFileNameInfo;

    if (file_exists($SourceFile)) {
      $sizeResFile = filesize($SourceFile);

      if (!is_object($objFile)) {
        $objFile = new Files();
      }

      $fileExt = $objFile->getFileExtension($strFileNameInfo);

      if ( ! function_exists ( 'mime_content_type ' ) ) {
        function mime_content_type ( $f ) {
          return trim ( exec ('file -bi ' . escapeshellarg ( $f ) ) ) ;
        }
      }

      $ft = mime_content_type($SourceFile);

      Header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT\r\n" );
      Header( "Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT\r\n" );
      Header( "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
      Header( "Cache-Control: post-check=0, pre-check=0, false\r\n" );
      Header( "Pragma: no-cache\r\n" );
      Header( "HTTP/1.1 200 OK\r\n" );

      Header( "Content-Disposition: attachment; filename=genplan_$intRecordId.$fileExt\r\n" );
      Header( "Accept-Ranges: bytes\r\n" );
      Header( "Content-Type: application/force-download" );
      Header( "Content-Length: $sizeResFile\r\n\r\n" );

      readfile( $SourceFile );
      die();
    } else {
      header ("HTTP/1.0 404 Not Found");
      $arrTplVars['strMessage'] = "Ошибка! Файл не существует.";
    }
  } else {
    $arrTplVars['strMessage'] = "Ошибка! Файл не найден.";
  }

  $objTpl->Template(SITE_TPL_PAGE_DIR);
  $objTpl->tpl_load("page.contents", "empty.14.tpl");

} else {
  $objTpl->Template(SITE_TPL_PAGE_DIR);
  $objTpl->tpl_load("page.contents", "genplan.18.tpl");

  $strSqlQuery = "SELECT * FROM ".$_db_tables["stGenplan"]." WHERE pg_status = 'Y' ORDER BY pg_id DESC";
  $arrGenplanList = $objDb->fetchall( $strSqlQuery );

  if ( is_array($arrGenplanList) ) {
    foreach ( $arrGenplanList as $key => $value ) {
      $arrGenplanList[$key]['strReleaseDate'] = $objUtil->workDate(5, $value['pg_date_change']);
    }
  } else {
    $arrIf['no.genplan.files'] = true;
  }

  $objTpl->tpl_loop("page.contents", "genplan", $arrGenplanList);
}

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
