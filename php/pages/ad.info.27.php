<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Карточка просмотра объявления [ad.info] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "ad.info.27.tpl");

$arrTplVars['urlLinkBack'] = $_SERVER['HTTP_REFERER'];

$intRecordId = intval($arrReqUri[1]);

if (!empty($intRecordId)) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($intRecordId);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAds']
  ." LEFT JOIN ".$_db_tables["stAdsAdsChapters"]." ON (paac_id = paa_chapter_id)"
  ." WHERE paa_id ='$intRecordId' AND paa_status = 'Y'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['paa_name']));
    $arrTplVars['strAd'] = nl2br(htmlspecialchars(stripslashes($arrInfo['paa_desc'])));
    $arrTplVars['strContacts'] = htmlspecialchars(stripslashes($arrInfo['paa_contacts']));
    $arrTplVars['strChapter'] = htmlspecialchars(stripslashes($arrInfo['paac_name']));

    $arrTplVars['cbxStatus'] = $arrInfo['paa_status'] == 'Y' ? ' checked' : '';
  } else {
    $arrIf['no.ad.info'] = true;
  }
} else {
  $arrIf['no.ad.info'] = true;
}

$arrIf['is.ad.info'] = !$arrIf['no.ad.info'];

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
