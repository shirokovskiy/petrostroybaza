<?php
/**
 * CAPTCHA
 */
if ( $_GET['get_img'] == 'captcha' ) {
	include_once("cls.captcha.php");
	$objCaptcha = new clsCaptcha();

	if ( is_object($objCaptcha) ) {
		$objCaptcha->imageCreateShow();
		// it stops here
	}
}
/***/

if ( isset($_REQUEST['frmAddTechnic']) && $_REQUEST['frmAddTechnic'] == 'true' ) {
	$intRecordID = $arrSqlData['intRecordID'] = intval(trim($_REQUEST['intRecordID']));
	$arrTplVars['intRecordID'] = ( $arrSqlData['intRecordID'] > 0 ? $arrSqlData['intRecordID'] : '');

	$arrSqlData['sbxAdType'] = mysql_real_escape_string(trim($_REQUEST['sbxAdType']));
	$arrTplVars['sbxAdType'] = htmlspecialchars(trim($_REQUEST['sbxAdType']));

	if ( empty( $arrSqlData['sbxAdType'] ) ) {
		$arrIf['error'] = true;
		$arrErrMsg[] = 'Вы не указали <b>Тип объявления</b>';
		$arrTplVars['styleAdType'] = "border:1px solid red;color:red;";
	}

	$arrSqlData['strTitle'] = mysql_real_escape_string(trim($_REQUEST['strTitle']));
	$arrTplVars['strTitle'] = htmlspecialchars(trim($_REQUEST['strTitle']));

	if ( empty( $arrSqlData['strTitle'] ) ) {
		$arrIf['error'] = true;
		$arrErrMsg[] = 'Вы не указали <b>Заголовок объявления</b>';
		$arrTplVars['styleTitle'] = "border:1px solid red;";
	}

	$intTechTypeID = $arrTplVars['intTechTypeID'] = $arrSqlData['intTechTypeID'] = intval($_REQUEST['intTechTypeID']);

	if ( empty($intTechTypeID) ) {
		$arrIf['error'] = true;
		$arrErrMsg[] = 'Вы не выбрали <b>Тип техники</b> в размещаемом объявлении';
		$arrTplVars['styleTechTypeID'] = "border:1px solid red;color:red;";
	}

	$arrSqlData['strMarka'] = mysql_real_escape_string(trim($_REQUEST['strMarka']));
	$arrTplVars['strMarka'] = htmlspecialchars(trim($_REQUEST['strMarka']));

	$arrSqlData['strModel'] = mysql_real_escape_string(trim($_REQUEST['strModel']));
	$arrTplVars['strModel'] = htmlspecialchars(trim($_REQUEST['strModel']));

	$arrSqlData['strYear'] = mysql_real_escape_string(trim($_REQUEST['strYear']));
	$arrTplVars['strYear'] = htmlspecialchars(trim($_REQUEST['strYear']));

	$arrSqlData['strDopinfo'] = mysql_real_escape_string(trim($_REQUEST['strDopinfo']));
	$arrTplVars['strDopinfo'] = htmlspecialchars(trim($_REQUEST['strDopinfo']));

	if ( empty($arrSqlData['strMarka']) && empty($arrSqlData['strModel']) && empty($arrSqlData['strYear']) && empty($arrSqlData['strDopinfo']) ) {
		$arrIf['error'] = true;
		$arrErrMsg[] = 'Слишком мало информации. Большинство полей остались не заполнены!';
		$arrTplVars['styleMarka'] =
		$arrTplVars['styleModel'] =
		$arrTplVars['styleYear'] =
		$arrTplVars['styleDopinfo'] = "border:1px solid red;";
	}

	$arrSqlData['strContacts'] = mysql_real_escape_string(trim($_REQUEST['strContacts']));
	$arrTplVars['strContacts'] = htmlspecialchars(trim($_REQUEST['strContacts']));

	if ( empty($arrSqlData['strContacts']) ) {
		$arrIf['error'] = true;
		$arrErrMsg[] = 'Вы не указали контактную информацию в поле <b>Координаты и контакты</b>';
		$arrTplVars['styleContacts'] = "border:1px solid red;";
	}

	$arrSqlData['strRentPeriod'] = mysql_real_escape_string(trim($_REQUEST['strRentPeriod']));
	$arrTplVars['strRentPeriod'] = htmlspecialchars(trim($_REQUEST['strRentPeriod']));

	$arrSqlData['strAddressWorks'] = mysql_real_escape_string(trim($_REQUEST['strAddressWorks']));
	$arrTplVars['strAddressWorks'] = htmlspecialchars(trim($_REQUEST['strAddressWorks']));

	if ( strtoupper($_SESSION['strCptCode']) != strtoupper($_POST['strCodeString']) || empty($_POST['strCodeString']) ) {
		$arrIf['error'] = true;
		$arrErrMsg[] = 'Визуальный код введён неверно';
		$arrTplVars['styleCodeString'] = "border-color:#db2929;";
	}

	if ( !$arrIf['error'] ) {
		$strSqlFields = ""
			." pt_tech_type_id = '$intTechTypeID'"
			.", pt_title = '{$arrSqlData['strTitle']}'"
			.", pt_rent_period = '{$arrSqlData['strRentPeriod']}'"
			.", pt_address_works = '{$arrSqlData['strAddressWorks']}'"
			.", pt_ad_type = '{$arrSqlData['sbxAdType']}'"
			.", pt_marka = '{$arrSqlData['strMarka']}'"
			.", pt_model = '{$arrSqlData['strModel']}'"
			.", pt_year = '{$arrSqlData['strYear']}'"
			.", pt_dopinfo = '{$arrSqlData['strDopinfo']}'"
			.", pt_contacts = '{$arrSqlData['strContacts']}'"
			.", pt_status = 'N'"
			.", pt_date_update = NOW()"
			.( intval($_SESSION['uAuthInfo']['uID']) > 0 ? ", pt_user_id = '{$_SESSION['uAuthInfo']['uID']}'" : '' )
		;

		if ( empty($intRecordID) ) {
			$strSqlQuery = "INSERT INTO ".$_dbt['stTechnics']." SET $strSqlFields, pt_date_create = NOW()";
		} else {
			$strSqlQuery = "UPDATE ".$_dbt['stTechnics']." SET $strSqlFields WHERE pt_id = '$intRecordID'";
		}

		if ( !$objDb->query( $strSqlQuery ) ) {
			$arrIf['error'] = true;
			$arrErrMsg[] = 'DB Error!!!';
		} else {
			$intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;
		}

		if ( !$arrIf['error'] ) {
			$_SESSION['saved_ok'] = true;
			header( "location:".SITE_URL."add.technic" );
			exit;
		}
	}
}

/** Created by WMBM(c) 2008 (Shirokovskiy D. aka Jimmy™).
 * Страница: Подать объявление о технике [add.technic] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load( $tpl = "page.contents", "add.technic.38.tpl");
////////////////////////////////////////////////////////////////////////////////
$arrTplVars['stPage'] = $stPage = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if ( isset( $arrReqUri[1] ) && !empty( $arrReqUri[1] ) ) {
	$strChapter = $arrTplVars['strChapter'] = $arrReqUri[1];
}

/**
 * Список типов техники
 */
$strSqlQuery = "SELECT * FROM ".$_dbt["stTechTypes"]." ORDER BY ptt_id ";
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
if ( is_array($arrSelectedRecords) && !empty($strChapter) ) {
	foreach ( $arrSelectedRecords as $key => $value ) {
		if ( is_numeric($strChapter) ) {
			$arrSelectedRecords[$key]['sel'] = $value['ptt_id'] == $strChapter ? ' selected' : '';
		} else {
			$arrSelectedRecords[$key]['sel'] = $value['ptt_alias'] == $strChapter ? ' selected' : '';
		}
	}
}
$objTpl->tpl_loop( $tpl, "technic.types", $arrSelectedRecords );
////////////////////////////////////////////////////////////////////////////////
if (isset($_SESSION['success']) && $_SESSION['success'] == true) {
	unset($_SESSION['success']);
	$arrIf['success'] = true;
	$arrTplVars['msgOk'] = "Данные успешно сохранены";
}
if (isset($_SESSION['saved_ok']) && $_SESSION['saved_ok'] == true) {
	unset($_SESSION['saved_ok']);
	$arrIf['success'] = true;
	$arrTplVars['msgOk'] = "Данные успешно отправлены на модерацию";
}
$arrIf['post.data'] = !$arrIf['success'];

if ( is_array( $arrErrMsg ) && !empty( $arrErrMsg ) ) {
	$arrIf['success'] = !$arrIf['error'] = true;
	$arrTplVars['errMsg'] = '<li>'.implode('<li>', $arrErrMsg);
}


////////////////////////////////////////////////////////////////////////////////
$objTpl->tpl_if($tpl, $arrIf );
$objTpl->tpl_array($tpl, $arrTplVars);
