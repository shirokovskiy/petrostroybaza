<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Скачать номер [download] */
if (preg_match("/msie/i", $_SERVER['HTTP_USER_AGENT'])) {
    $arrIf['msie'] = true;
}
$arrIf['op.ff'] = !$arrIf['msie'];

if (isset($arrReqUri[1]) && intval($arrReqUri[1]) > 0 ) {
    $objTpl->Template(SITE_TPL_TPL_DIR);
    $objTpl->tpl_load("global.tpl", "site.global.4.tpl");
    unset($objTpl->arrFrgForLoad);
    $objTpl->tpl_parse_frg($objTpl->files['global.tpl']);

    /**
     * Здесь можно будет поставить проверку на авторизацию
     */
    if ( intval($_SESSION['uAuthInfo']['uID']) > 0 ) {
        // Проверим, имеет ли право!?
        $strSqlQuery = "SELECT * FROM `site_users_access_lnk` LEFT JOIN `site_releases_access_lnk` ON (sual_srat_id = srlal_srat_id)"
            . " WHERE `sual_su_id` = ".$_SESSION['uAuthInfo']['uID']." AND srlal_sr_id = ".$arrReqUri[1];
        $arrUA = $objDb->fetchall($strSqlQuery);

        if (is_array($arrUA) && !empty($arrUA)) {// access exists
            // Грузим нужный файл
            $intRecordId = intval($arrReqUri[1]);
            $strSqlQuery = "SELECT sr_filename FROM site_releases WHERE sr_status='Y' AND sr_id= ".$intRecordId;
            $strFileNameInfo = $objDb->fetch( $strSqlQuery, 0 );
        } else {
            $arrTplVars['strMessage'] = "Ошибка! Нет прав доступа.";
        }
    }

    if (!empty($strFileNameInfo)) {
        $SourceFile = PRJ_FILES.'releases/'.$strFileNameInfo;

        if (file_exists($SourceFile)) {
            $sizeResFile = filesize($SourceFile);

            if (!is_object($objFile)) {
                $objFile = new Files();
            }

            $fileExt = $objFile->getFileExtension($strFileNameInfo);

            if ( ! function_exists ( 'mime_content_type' ) ) {
                function mime_content_type ( $f ) {
                    return trim ( exec ('file -bi ' . escapeshellarg ( $f ) ) ) ;
                }
            }

            $ft = mime_content_type($SourceFile);

            Header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT\r\n" );
            Header( "Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT\r\n" );
            Header( "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
            Header( "Cache-Control: post-check=0, pre-check=0, false\r\n" );
            Header( "Pragma: no-cache\r\n" );
            Header( "HTTP/1.1 200 OK\r\n" );

            Header( "Content-Disposition: attachment; filename=release_".$strFileNameInfo."\r\n" );
            Header( "Accept-Ranges: bytes\r\n" );
//        Header( "Content-Type: application/octet-stream\r\n" );
            Header( "Content-Type: application/force-download" );
            Header( "Content-Length: $sizeResFile\r\n\r\n" );

            readfile( $SourceFile );
            die();
        } else {
            header ("HTTP/1.0 404 Not Found");
            if (!$_SERVER['_IS_DEVELOPER_MODE']) {
                $SourceFile = '';
            }
            $arrTplVars['strMessage'] = "Ошибка! Файл $SourceFile не существует.";
        }
    } else {
        $arrTplVars['strMessage'] = "Ошибка! Файл не найден.";
    }

    $objTpl->Template(SITE_TPL_PAGE_DIR);
    $objTpl->tpl_load("page.contents", "empty.14.tpl");

} else {
    $objTpl->Template(SITE_TPL_PAGE_DIR);
    $objTpl->tpl_load("page.contents", "download.9.tpl");

    if ( intval($_SESSION['uAuthInfo']['uAccess']) == 1 && $_SESSION['uAuthInfo']['uID'] != 3358 ) {
        // проверим права доступа более детально, получим список регионов к которым имеет доступ юзер
        $strSqlQuery = "SELECT srat_id, srat_name FROM `site_users_access_lnk`, `site_regions_access_type`"
            ." WHERE `sual_su_id` = ".$_SESSION['uAuthInfo']['uID']." AND `sual_type` = 'magazine' AND sual_srat_id = srat_id";
        $arrUA = $objDb->fetchall($strSqlQuery);
        $objTpl->tpl_loop("page.contents", "access.types", $arrUA);

        $arrTplVars['cols'] = 0;

        if (is_array($arrUA) && !empty($arrUA)) {
            // access exists
            foreach ($arrUA as $access) {
                $strSqlQuery = "SELECT * FROM site_releases sR LEFT JOIN site_releases_access_lnk ON (sr_id = srlal_sr_id) WHERE sr_status = 'Y' AND srlal_srat_id = ".$access['srat_id']." ORDER BY sr_id DESC";
                $arrRecords = $objDb->fetchall( $strSqlQuery );

                if ( is_array($arrRecords) ) {
                    $arrTplVars['cols']++;
                    $arrIf['releases.'.$access['srat_id']] = true;
                    foreach ( $arrRecords as $key => $value ) {
                        $arrRecords[$key]['strReleaseDate'] = $objUtil->workDate(5, $value['sr_date_change']);
                    }
                    $objTpl->tpl_loop("page.contents", "releases.".$access['srat_id'], $arrRecords);
                } else {
                    $arrIf['releases.'.$access['srat_id']] = false;
                }
            }
            if ($arrTplVars['cols'] > 0) {
                $arrIf['access'] = true;
            } else {
                $arrIf['no.access'] = true;
            }

        } else {
            $arrIf['no.permissions'] = true;
        }
    } else {
        $arrIf['no.access'] = true;
    }
}

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
