<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Добавить объявление о недвижимости [add.estate_land] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "add.estate_land.33.tpl");
if (isset($_GET['errMess']) && $_GET['errMess'] == 'msgOk') {
  $arrIf['success'] = true;
  $arrTplVars['msgOk'] = "Данные успешно сохранены";
}

/**
 * Запись нового тендера от пользователя
 */
if ( isset($_POST) && !empty($_POST) && isset($_GET['new']) && $_GET['new'] == 'record' ) {
  $arrSqlData['intChapterId'] = intval(trim($_POST['intChapterId']));
  $arrTplVars['intChapterId'] = ( $arrSqlData['intChapterId'] > 0 ? $arrSqlData['intChapterId'] : '');

  if (empty($arrSqlData['intChapterId']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Не указан раздел";
  }

  $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
  $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

  if (empty($arrSqlData['strTitle']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Название (описание) объекта отсутствует";
  }

  $arrSqlData['strAddress'] = addslashes(trim($_POST['strAddress']));
  $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

  if (empty($arrSqlData['strAddress']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Адрес отсутствует";
  }

  $arrSqlData['strOrgName'] = addslashes(trim($_POST['strOrgName']));
  $arrTplVars['strOrgName'] = htmlspecialchars(stripslashes(trim($_POST['strOrgName'])));

  if (empty($arrSqlData['strOrgName']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Организация не указана";
  }

  $arrSqlData['strPhone'] = addslashes(trim($_POST['strPhone']));
  $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

  if (empty($arrSqlData['strPhone']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Телефон не указан";
  }

  $arrSqlData['strContacts'] = addslashes(trim($_POST['strContacts']));
  $arrTplVars['strContacts'] = htmlspecialchars(stripslashes(trim($_POST['strContacts'])));

  if (empty($arrSqlData['strContacts']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Контактное лицо не указано";
  }

  $arrSqlData['strInfo'] = addslashes(trim($_POST['strInfo']));
  $arrTplVars['strInfo'] = htmlspecialchars(stripslashes(trim($_POST['strInfo'])));

  if ( !$arrIf['error'] ) {
    $strSqlFields = ""
      . " pael_name = '{$arrSqlData['strTitle']}'"
      . ", pael_chapter_id = '{$arrSqlData['intChapterId']}'"
      . ", pael_contact = '{$arrSqlData['strContacts']}'"
      . ", pael_info = '{$arrSqlData['strInfo']}'"
      . ", pael_orgname = '{$arrSqlData['strOrgName']}'"
      . ", pael_phone = '{$arrSqlData['strPhone']}'"
      . ", pael_address = '{$arrSqlData['strAddress']}'"
      ;
    $strSqlQuery = "INSERT INTO ".$_db_tables['stAdsEstateLands']." SET $strSqlFields, pael_date_create = NOW()";

    if ( !$objDb->query( $strSqlQuery ) ) {
      $arrIf['error'] = true;
      $arrTplVars['errMsg'] .= "<li> Ошибка базы данных";
    } else {
      header('location: /'.$arrReqUri[0]."?errMess=msgOk");
      exit;
    }
  }
}

$arrIf['post.data'] = !$arrIf['success'];

$strSqlQuery = "SELECT * FROM ".$_db_tables["stEstateAdsChapters"]." ORDER BY paec_id ";
$arrEstateAdsChapters = $objDb->fetchall( $strSqlQuery );

if ( is_array($arrEstateAdsChapters) ) {
  foreach ( $arrEstateAdsChapters as $key => $value ) {
    $arrEstateAdsChapters[$key]['sel'] = $value['paec_id'] == $arrTplVars['intChapterId'] ? ' selected' : '';
  }
}

$objTpl->tpl_loop("page.contents", "list.of.estate_lands.chapters", $arrEstateAdsChapters);
$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
