<?php
/** Created by WMBM(c) 2012 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Фотоматериал [foto] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "foto.41.tpl");

// URL параметры
$type = trim($arrReqUri[1]);
$date = urldecode(trim($arrReqUri[2]));
$filename = urldecode(trim($arrReqUri[3]));
$intGroupID = intval(trim($arrReqUri[4]));

/**
 * Здесь прийдётся прибегнуть к маленькой хитрости.
 * Так как запрос не был рассчитан на то, что файлы для разных групп фотографий будут проименованы совершенно одинаково, и к тому же так тривиально как 1.jpg,
 * а также небыло учтено что это будет сделано в один день, то теперь возникают баги при отображении фото из той или иной группы.
 * БД отдаёт больше чем одну строку. Поиск нужно сузить за счёт ID группы, но этот параметр передавать в уже проиндексированные Яндексом ссылки нельзя.
 * Поэтому будем динамически анализировать реферал, и вытаскивать из него переменную группы.
 */
//$arrTplVars['debug'] = 'HTTP_REFERER === '. $_SERVER['HTTP_REFERER']."\n";

if (!empty($filename) && !empty($date) && !empty($type)) {
//    $arrTplVars['debug'] .= 'I`m HERE'."\n";
    if (empty($intGroupID) && !empty($_SERVER['HTTP_REFERER']) && preg_match('/fotobank/', $_SERVER['HTTP_REFERER'])) {
//        $arrTplVars['debug'] .= 'ПОПАЛ'."\n";
        $intGroupID = (int) substr($_SERVER['HTTP_REFERER'], strrpos($_SERVER['HTTP_REFERER'],'/')+1);
    }
//    $arrTplVars['debug'] .= $intGroupID;
    $arrTplVars['intGroupID'] = $intGroupID;

    // Выбераем нужную фото
	$strSqlQuery = "SELECT * FROM `site_images` WHERE `si_filename` REGEXP '^".mysql_real_escape_string($filename)."\.(jpg|png|gif)$' AND si_type = '$type' AND DATE_FORMAT(si_date_add, '%Y-%m-%d') = '".$date."' AND si_status = 'Y'".($intGroupID?" AND si_id_rel = ".$intGroupID:"")." LIMIT 1";
	$arrFoto = $objDb->fetch($strSqlQuery);

//    $arrTplVars['debug'] = $strSqlQuery;

	if (is_array($arrFoto) && !empty($arrFoto)) {
		$arrFoto['strDate'] = date("d.m.Y", strtotime($arrFoto['si_date_add']));
		if ($type=='fotobank') {
			$arrFoto['strImgUrlPath'] = SITE_URL.'storage/images/fotobank/groups/'.$arrFoto['si_id_rel'].'/';

			// NEXT photo
			$strSqlQuery = "SELECT DATE_FORMAT(si_date_add, '%Y-%m-%d') as si_date, si_filename FROM `site_images` WHERE `si_id` > ".$arrFoto['si_id']." AND si_id_rel = ".$arrFoto['si_id_rel']." AND si_type = '$type' AND si_status = 'Y'".($intGroupID?" AND si_id_rel = ".$intGroupID:"")." ORDER BY si_id";
			$arrNextFoto = $objDb->fetch($strSqlQuery);

			if (is_array($arrNextFoto) && !empty($arrNextFoto)) {
				$arrIf['next'] = true;
				$arrFoto['strNext'] = SITE_URL.'foto/'.$type.'/'.$arrNextFoto['si_date'].'/'.substr($arrNextFoto['si_filename'],0,-4).'/'.$intGroupID;
			}

			// PREVIOUS photo
			$strSqlQuery = "SELECT DATE_FORMAT(si_date_add, '%Y-%m-%d') as si_date, si_filename FROM `site_images` WHERE `si_id` < ".$arrFoto['si_id']." AND si_id_rel = ".$arrFoto['si_id_rel']." AND si_type = '$type' AND si_status = 'Y'".($intGroupID?" AND si_id_rel = ".$intGroupID:"")." ORDER BY si_id DESC";
			$arrPrevFoto = $objDb->fetch($strSqlQuery);

			if (is_array($arrPrevFoto) && !empty($arrPrevFoto)) {
				$arrIf['prev'] = true;
				$arrFoto['strPrev'] = SITE_URL.'foto/'.$type.'/'.$arrPrevFoto['si_date'].'/'.substr($arrPrevFoto['si_filename'],0,-4).'/'.$intGroupID;
			}
		} else {
			$arrFoto['strImgUrlPath'] = SITE_URL.'storage/images/fotobank/'.$type.'/';

			// NEXT photo
			$strSqlQuery = "SELECT DATE_FORMAT(si_date_add, '%Y-%m-%d') as si_date, si_filename FROM `site_images` WHERE `si_id` < ".$arrFoto['si_id']." AND si_id_rel = ".$arrFoto['si_id_rel']." AND si_type = '$type' AND si_status = 'Y'".($intGroupID?" AND si_id_rel = ".$intGroupID:"")." ORDER BY si_id DESC";
			$arrNextFoto = $objDb->fetch($strSqlQuery);

			if (is_array($arrNextFoto) && !empty($arrNextFoto)) {
				$arrIf['next'] = true;
				$arrFoto['strNext'] = SITE_URL.'foto/'.$type.'/'.$arrNextFoto['si_date'].'/'.substr($arrNextFoto['si_filename'],0,-4);
			}

			// PREVIOUS photo
			$strSqlQuery = "SELECT DATE_FORMAT(si_date_add, '%Y-%m-%d') as si_date, si_filename FROM `site_images` WHERE `si_id` > ".$arrFoto['si_id']." AND si_id_rel = ".$arrFoto['si_id_rel']." AND si_type = '$type' AND si_status = 'Y'".($intGroupID?" AND si_id_rel = ".$intGroupID:"")." ORDER BY si_id";
			$arrPrevFoto = $objDb->fetch($strSqlQuery);

			if (is_array($arrPrevFoto) && !empty($arrPrevFoto)) {
				$arrIf['prev'] = true;
				$arrFoto['strPrev'] = SITE_URL.'foto/'.$type.'/'.$arrPrevFoto['si_date'].'/'.substr($arrPrevFoto['si_filename'],0,-4);
			}
		}

        /**
         * Проверим, есть ли информация об объекте
         */
        if ($arrFoto['si_foto_object_id'] > 0) {
            $arrIf['object'] = true;

            // По выбранному ID объекта выбрать инфо
            $strSqlQuery = "SELECT seo_name, seo_address, seo_url FROM `site_estate_objects` WHERE `seo_status`='Y' AND `seo_id` = " . $arrFoto['si_foto_object_id'];
            $arrObject = $objDb->fetch($strSqlQuery);
            $arrTplVars['strObjectName'] = $arrObject['seo_name'];
            $arrTplVars['strObjectAdr'] = $arrObject['seo_address'];
            $arrTplVars['strObjectUrl'] = (!empty($arrObject['seo_url'])?$arrObject['seo_url']:$arrFoto['si_foto_object_id']);

            // Также, вытащить контакты
            $strSqlQuery = "SELECT socl_relation, sec_company, sec_id FROM `site_estate_contacts`"
            . " LEFT JOIN `site_objects_contacts_lnk` ON (sec_id = socl_id_contact)"
            . " WHERE `sec_status` = 'Y' AND `socl_id_object` = " . $arrFoto['si_foto_object_id'];
            $arrContacts = $objDb->fetchall($strSqlQuery);
            $objTpl->tpl_loop("page.contents", "contacts", $arrContacts);
        }
	} else {
		header("Location: /error");
		exit;
	}
} else {
	header("Location: /error");
	exit;
}

$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrFoto);
$objTpl->tpl_array("page.contents", $arrTplVars);
