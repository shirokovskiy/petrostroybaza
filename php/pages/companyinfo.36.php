<?php
/** Created by WMBM(c) 2008 (Shirokovskiy D. aka Jimmy™).
 * Страница: Информация о компании [companyinfo] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "companyinfo.36.tpl");

$arrTplVars['stPage'] = ( intval($_GET['stPage']) > 0 ? $_GET['stPage'] : 1 );

$arrIf['access'] = $_SESSION['uAuthInfo']['uAccess'] == 1;
$arrIf['no.access'] = !$arrIf['access'];

$arrTplVars['urlLinkBack'] = $_SERVER['HTTP_REFERER'];
if (!preg_match("/\?stPage/", $arrTplVars['urlLinkBack'])) {
    $arrTplVars['urlLinkBack'] .= (!preg_match("/\?/",$_SERVER['HTTP_REFERER'])?"?":'&')."stPage=".$arrTplVars['stPage'];
}

if (isset($arrReqUri[1]) && intval($arrReqUri[1]) > 0) {
    /**
     * Lets view company info
     */
    $intCompanyId = intval($arrReqUri[1]);

    /**
     * Let's check accessability
     */


    /**
     * Get info
     */
    $strSqlQuery = "SELECT * FROM site_estate_contacts WHERE sec_company != '' AND sec_status='Y' AND sec_id=".$intCompanyId;
    $arrCompanyInfo = $objDb->fetch( $strSqlQuery );

    if (is_array($arrCompanyInfo)) {
        $arrTplVars['intCompanyID']           = htmlspecialchars($arrCompanyInfo['sec_id']);
        $arrTplVars['strCompanyName']         = !empty($arrCompanyInfo['sec_company']) ? htmlspecialchars($arrCompanyInfo['sec_company']) : 'нет информации';
        $arrTplVars['strCompanyInn']         = !empty($arrCompanyInfo['sec_inn']) ? htmlspecialchars($arrCompanyInfo['sec_inn']) : 'нет информации';
        $arrTplVars['strContactCompanyName']  = !empty($arrCompanyInfo['sec_fio']) ? htmlspecialchars($arrCompanyInfo['sec_fio']) : 'нет информации';
        $arrTplVars['strContactPosition']     = !empty($arrCompanyInfo['sec_proff']) ? htmlspecialchars($arrCompanyInfo['sec_proff']) : 'нет информации';
        $arrTplVars['strCompanyContactInfo']  = !empty($arrCompanyInfo['sec_contact']) ? htmlspecialchars($arrCompanyInfo['sec_contact']) : 'нет информации';

        $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrCompanyInfo['sec_company']))." | PetroStroyBaza.RU";
        $arrTplVars['m_description'] = $objUtil->substrText(htmlspecialchars(strip_tags($arrCompanyInfo['sec_company'])));

        /**
         * Соберём список объектов данной компании
         */
        $strSqlQuery = "SELECT seo_id AS intObjectID, seo_name AS strObjectTitle, DATE_FORMAT(seo_etap_date,'%d.%m.%Y') AS strEtapDate"
		. ", seo_other_etap AS strEtapDesc, seo_url"
		. " FROM site_objects_contacts_lnk"
        . " LEFT JOIN site_estate_objects ON (seo_id = socl_id_object)"
        . " WHERE socl_id_contact = ".$intCompanyId." AND seo_status = 'Y' ORDER BY seo_etap_date DESC";
        $arrRelatedObjects = $objDb->fetchall( $strSqlQuery );
        if (is_array($arrRelatedObjects) && !empty($arrRelatedObjects)) {
            foreach ($arrRelatedObjects as $k => $value) {
                $arrRelatedObjects[$k]['urlParam'] = (!empty($value['seo_url']) ? $value['seo_url'] : $value['intObjectID'] );

                $arrObjectsIDs[] = $value['intObjectID'];
            }
        }

		if ( !is_array( $arrRelatedObjects ) ) {
            $arrIf['no.related.objects'] = true;
        } else {
            /**
             * Проверим тип доступа
             */
            if(isset($_SESSION['uAuthInfo']['uID']) && $_SESSION['uAuthInfo']['uID'] > 0) {
                $strSqlQuery = "SELECT * FROM `site_users_access_lnk`, `site_regions_access_type`, `site_regions_access_lnk`, `site_objects_regions_lnk`"
                    ." WHERE `sual_su_id` = " . $_SESSION['uAuthInfo']['uID']." AND sual_srat_id = srat_id AND srat_id = sral_srat_id AND sral_sr_id = sorl_sr_id"
                    ." AND sorl_seo_id IN (".implode(',',$arrObjectsIDs).")";
//                $objDb->setDebugModeOn();
                $arrUA = $objDb->fetchall($strSqlQuery);
                if (is_array($arrUA) && !empty($arrUA)) {
                    // Access exists
                } else {
                    $arrIf['access'] = $arrIf['no.access'] = false;
                    $arrIf['not.permited'] = true;
                }
            }
        }
    } else {
        header("Location: /error");
        exit();
    }
} else {
    header("Location: /error");
    exit();
}
$objTpl->tpl_loop("page.contents", "company.objects", $arrRelatedObjects );
$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
