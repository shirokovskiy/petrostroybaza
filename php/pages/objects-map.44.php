<?php
/** Created by WMBM(c) 2015 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Карта объектов [objects-map] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "objects-map.44.tpl");

# Координаты карты по умолчанию
$arrTplVars['coordX'] = "60.00";
$arrTplVars['coordY'] = "30.00";

// Чтобы не кешировать запросы
$arrTplVars['strDateRequest'] = date('Ymd');
$arrTplVars['mapsObjectsFile'] = 'geo-map-data.json'; // путь к файлу относительно корня
$arrTplVars['mapsObjectsZoom'] = '10'; // по умолчанию приближение карты
$arrTplVars['mapsObjectsTitle'] = 'Все объекты недвижимости на карте'; // по умолчанию заголовок страницы карты

switch ($_REQUEST['period']) {
    case 1:
        $file = 'geo-map-data-3month.json';
        if (file_exists(DROOT.$file)) {
            $arrTplVars['mapsObjectsFile'] = $file;
        }
        $arrTplVars['option1'] = ' selected';
        break;

    case 2:
        $file = 'geo-map-data-3-6month.json';
        if (file_exists(DROOT.$file)) {
            $arrTplVars['mapsObjectsFile'] = $file;
        }
        $arrTplVars['option2'] = ' selected';
        break;

    case 3:
        $file = 'geo-map-data-6-12month.json';
        if (file_exists(DROOT.$file)) {
            $arrTplVars['mapsObjectsFile'] = $file;
        }
        $arrTplVars['option3'] = ' selected';
        break;

    case 4:
        $file = 'geo-map-data-12month.json';
        if (file_exists(DROOT.$file)) {
            $arrTplVars['mapsObjectsFile'] = $file;
        }
        $arrTplVars['option4'] = ' selected';
        break;

    default:break;
}

if(isset($_SESSION['cachedNotebookQuery']) && strlen($_SESSION['cachedNotebookQuery']) == 32 && $_REQUEST['filter'] == 'map-nb') {
    $arrTplVars['mapsObjectsFile'] = 'storage/maps/geo-map-nb-data.'.$_SESSION['cachedNotebookQuery'].'.json';
    $arrTplVars['mapsObjectsZoom'] = '4';
    $arrTplVars['mapsObjectsTitle'] = 'Блокнот: объекты недвижимости на карте';
}

if(isset($_SESSION['cachedSearchQuery']) && strlen($_SESSION['cachedSearchQuery']) == 32 && $_REQUEST['filter'] == 'map') {
    $arrTplVars['mapsObjectsFile'] = 'storage/maps/geo-map-data.'.$_SESSION['cachedSearchQuery'].'.json';
    $arrTplVars['mapsObjectsZoom'] = '7';
    $arrTplVars['mapsObjectsTitle'] = 'Результаты поиска объектов недвижимости на карте';

    if(isset($_SESSION['SF']['cbxRegion'])) {
        if (array_intersect($_SESSION['SF']['cbxRegion'], array(3,4,6)) != false) {
            # Центральный, Московский округ - поставить координаты карты на Москву!
            $arrTplVars['coordX'] = "55.75"; /*уменьшаешь значение, передвигаешь карту вниз*/
            $arrTplVars['coordY'] = "37.68"; /*уменьшаешь значение, передвигаешь карту вправо*/
            $arrTplVars['mapsObjectsZoom'] = '6';
        }
    }
}

#echo "<!--";
#var_dump($_SESSION);
#echo "-->";

// Включить Yandex.Maps API
$arrIf['is.maps.pages'] = true;

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
