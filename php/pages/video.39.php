<?php
/** Created by WMBM(c) 2012 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Видео-сюжет [video] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "video.39.tpl");
$arrVideos = array();

if (!empty($arrReqUri[1]) ) {
    require_once 'Zend/Loader.php'; // the Zend dir must be in your include_path
    Zend_Loader::loadClass('Zend_Gdata_YouTube');

    $videoID = $arrReqUri[1];

    // Get video news
    $yt = new Zend_Gdata_YouTube();
    $videoEntry = $yt->getVideoEntry($videoID);
    $arrTplVars['urlFlash'] = $videoEntry->getFlashPlayerUrl();
//    $arrTplVars['strVideoDescr'] = iconv('UTF-8','CP1251', $videoEntry->getVideoDescription());
    $arrTplVars['strVideoDescr'] = $videoEntry->getVideoDescription();
//    $arrTplVars['strVideoTitle'] = iconv('UTF-8','CP1251', $videoEntry->getVideoTitle());
    $arrTplVars['strVideoTitle'] = $videoEntry->getVideoTitle();
    $arrTplVars['strVideoDate'] = date('d.m.Y', strtotime($videoEntry->getPublished()->getText()));

    $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrTplVars['strVideoTitle']))." | Видеосюжет | PetroStroyBaza.RU";
    $arrTplVars['m_description'] = $objUtil->substrText(htmlspecialchars(strip_tags($arrTplVars['strVideoDescr'])));

    $userName = 'Petrostroybaza';
    $videoFeed = $yt->getUserUploads($userName);
    $count = 1;

    foreach ($videoFeed as $videoEntry) {
        $arrVideos[$count]['strVideoID'] = $videoEntry->getVideoId();
        if ($arrVideos[$count]['strVideoID'] == $arrReqUri[1]) continue;
//        $arrVideos[$count]['strThumbTitle'] = iconv('UTF-8','CP1251', $videoEntry->getVideoTitle());
        $arrVideos[$count]['strThumbTitle'] = $videoEntry->getVideoTitle();
        $videoThumbnails /* array */ = $videoEntry->getVideoThumbnails();

        foreach($videoThumbnails as $videoThumbnail) {
            $arrVideos[$count]['urlVideoThumb'] = $videoThumbnail['url'];
            break; // Show only first thumb
        }

        $count++;
    }
}
$objTpl->tpl_loop("page.contents", "thumbnails", $arrVideos);
$objTpl->tpl_array("page.contents", $arrTplVars);
