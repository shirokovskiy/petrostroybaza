<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Информация пользователя [userpage] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "userpage.13.tpl");

if (isset($arrReqUri[1]) && strlen($arrReqUri[1])==32) {
    $User = new User();
    $User->autoLogin($arrReqUri[1]);
}

if (isset($_SESSION['uAuthInfo']) && is_array($_SESSION['uAuthInfo']) && intval($_SESSION['uAuthInfo']['uID'])>0 ) {
  $arrTplVars['strUserLastname'] = htmlspecialchars($_SESSION['uAuthInfo']['uLName']);
  $arrTplVars['strUserFirstname'] = htmlspecialchars($_SESSION['uAuthInfo']['uFName']);
  $arrTplVars['strUserMiddlename'] = htmlspecialchars($_SESSION['uAuthInfo']['uMName']);
  $arrTplVars['strUserCompanyName'] = htmlspecialchars($_SESSION['uAuthInfo']['uCName']);
  $arrTplVars['strUserPhone'] = htmlspecialchars($_SESSION['uAuthInfo']['uPhone']);
  $arrTplVars['strUserEmail'] = htmlspecialchars($_SESSION['uAuthInfo']['uEmail']);
  $arrTplVars['strUserLogin'] = htmlspecialchars($_SESSION['uAuthInfo']['uLogin']);
  $arrTplVars['strUserLastIP'] = htmlspecialchars($_SESSION['uAuthInfo']['lastIP']);
  $arrTplVars['strUserLastVisitDate'] = htmlspecialchars($_SESSION['uAuthInfo']['lastVisit']);
} else {
  header("location: /index");
  exit;
}

$objTpl->tpl_array("page.contents", $arrTplVars);
