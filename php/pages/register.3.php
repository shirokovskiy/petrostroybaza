<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Регистрация [register] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "register.3.tpl");

if ( isset($_POST['frmRegister']) && $_POST['frmRegister'] == 'true' ) {
	$arrSqlData['strLName'] = addslashes(trim($_POST['strLName']));
	$arrTplVars['strLName'] = htmlspecialchars(stripslashes(trim($_POST['strLName'])));

	if (empty($arrSqlData['strLName']) ) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Вы не указали <b>Фамилию</b>';
	}

	$arrSqlData['strFName'] = addslashes(trim($_POST['strFName']));
	$arrTplVars['strFName'] = htmlspecialchars(stripslashes(trim($_POST['strFName'])));

	if (empty($arrSqlData['strFName']) ) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Вы не указали <b>Имя</b>';
	}

	$arrSqlData['strMName'] = addslashes(trim($_POST['strMName']));
	$arrTplVars['strMName'] = htmlspecialchars(stripslashes(trim($_POST['strMName'])));

	$arrSqlData['strCompanyName'] = addslashes(trim($_POST['strCompanyName']));
	$arrTplVars['strCompanyName'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyName'])));

	if (empty($arrSqlData['strCompanyName']) ) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Вы не указали <b>Компанию</b>';
	}

	$arrSqlData['strPhone'] = addslashes(trim($_POST['strPhone']));
	$arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

	if (empty($arrSqlData['strPhone']) ) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Вы не указали <b>Телефон</b>';
	}

	$arrSqlData['strEmail'] = addslashes(trim($_POST['strEmail']));
	$arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

	if (empty($arrSqlData['strEmail']) || !$objUtil->checkEmail($arrSqlData['strEmail'])) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Вы не указали <b>Email</b>, или указанный Email не соответствует формату';
	} else {
		$strSqlQuery = "SELECT su_id FROM ".$_db_tables['stUsers']." WHERE su_email='{$arrSqlData['strEmail']}'";
		$intExistSuchUser = $objDb->fetch( $strSqlQuery, 'su_id' );
	}

	if ($intExistSuchUser > 0) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Такой <b>Email</b> уже зарегистрирован в системе. Выберите другой.';
	}

	$arrSqlData['strLogin'] = addslashes(trim($_POST['strLogin']));
	$arrTplVars['strLogin'] = htmlspecialchars(stripslashes(trim($_POST['strLogin'])));

	if (empty($arrSqlData['strLogin']) || strlen($arrSqlData['strLogin'])<4 ) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Вы не указали <b>Логин</b>, или Логин меньше 4 символов';
	} else {
		$strSqlQuery = "SELECT su_id FROM ".$_db_tables['stUsers']
		. " WHERE su_login='{$arrSqlData['strLogin']}'";
		$intExistSuchUser = $objDb->fetch( $strSqlQuery, 'su_id' );
	}

	if ($intExistSuchUser > 0) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Такой <b>Login</b> уже зарегистрирован в системе. Выберите другой.';
	}

	$arrSqlData['strPassword1'] = addslashes(trim($_POST['strPassword1']));
	$arrTplVars['strPassword1'] = htmlspecialchars(stripslashes(trim($_POST['strPassword1'])));

	if (empty($arrSqlData['strPassword1']) || strlen($arrSqlData['strPassword1'])<6 ) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Поле <b>Пароль</b> пустое, или меньше 6 символов';
	}

	$arrSqlData['strPassword2'] = addslashes(trim($_POST['strPassword2']));
	$arrTplVars['strPassword2'] = htmlspecialchars(stripslashes(trim($_POST['strPassword2'])));

	if (empty($arrSqlData['strPassword2']) || strlen($arrSqlData['strPassword2'])<6 ) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Поле <b>Пароль (подтверждение)</b> пустое, или меньше 6 символов';
	}

	if ($arrSqlData['strPassword1']!=$arrSqlData['strPassword2']) {
		$arrIf['error'] = true;
		$arrErrors[] = 'Пароль <b>Пароль</b> и <b>Пароль (подтверждение)</b> не совпадают';
	}

	if ( !$arrIf['error'] ) {
		$strSqlFields = ""
		. " su_lname = '{$arrSqlData['strLName']}'"
		. ", su_fname = '{$arrSqlData['strFName']}'"
		. ", su_mname = '{$arrSqlData['strMName']}'"
		. ", su_company = '{$arrSqlData['strCompanyName']}'"
		. ", su_email = '{$arrSqlData['strEmail']}'"
		. ", su_phone = '{$arrSqlData['strPhone']}'"
		. ", su_login = '{$arrSqlData['strLogin']}'"
		. ", su_passw = MD5('{$arrSqlData['strPassword1']}')"
		;

		$strSqlQuery = "INSERT INTO ".$_db_tables['stUsers']." SET $strSqlFields, su_date_add = NOW()";

		if ( !$objDb->query( $strSqlQuery ) ) {
			$arrIf['error'] = true;
			$arrErrors[] = 'msgErrorDB';
		}

		if ( !$arrIf['error'] ) {
			/**
			 * Отправить Email
			 */
			$objMail = new clsMail();

            $mail_from = 'info@petrostroybaza.ru';
			$mail_to = $arrSqlData['strEmail'];
            $mail_subject = 'Регистрация на PetroStroyBaza.Ru';
            $mail_text = 'Уважаемый '.stripslashes(trim($_POST['strLName'].' '.$_POST['strFName'])).",\n";
            $mail_text .= "Вы зарегистрировались на сайте ".SITE_URL."\n";
            $mail_text .= "Данные для доступа:\n";
            $mail_text .= "Логин   ".$arrSqlData['strLogin']."\n";
            $mail_text .= "Пароль  ".$arrSqlData['strPassword1']."\n\n";

            //добавлено 04.06.2012 Халецкий Р.А.
            $folder="storage/files/email_attachments/";
            $cntFile = $folder."emailContent.txt";
            if($hfile= @fopen($cntFile, "r")){
                $mail_text .= "\n================================\n\n";
                $mail_text .= fread($hfile, filesize($cntFile));
                $mail_text .= "\n\n";
            }
            //////////////////////////

//		       $mail_text .= "Чтобы подтвердить свою регистрацию, Вам необходимо перейти по ссылке \n";
//		       $mail_text .= SITE_URL."confirm_register?regcode=".md5($arrSqlData['strPassword1'])."\n";
            $mail_text .= "===================================\n";
            $mail_text .= "Автоматическая служба оповещения ".strtoupper(SITE_URL)."\n";
            $mail_text .= "Отправлено: ".date('d.m.Y |H:i:s|')."\n";

            $objMail->new_mail($mail_from, $mail_to, $mail_subject, $mail_text);
//            $objMail->add_attachment($folder."attachment1.doc", "document1.doc");
//            $objMail->add_attachment($folder."attachment2.doc", "document2.doc");
			if (file_exists($folder."attachment.doc")) {
				$objMail->add_attachment($folder."attachment.doc", "document.doc");
			}
            $sentResult = $objMail->send();
            if(!$sentResult){
//                die("<hr />Result:$sentResult<p style='color:red'><b>" . __FILE__ . "</b>:<i>" . __LINE__ . "</i></p>");
            }

		    header('location:'.$arrTplVars['cfgUrlSite'].'register?errMess=msgOk');
		    exit;
	    }
	}
}

if ($arrIf['error']) {
	$arrTplVars['errMsg'] = '<ul class=err style="margin-top:0">';
	foreach ($arrErrors as $error) {
	    $arrTplVars['errMsg'] .= '<li>'.$error.'</li>';
	}
	$arrTplVars['errMsg'] .= '</ul>';
}

if ($_GET['errMess']=='msgOk') {
	$arrIf['reg.success'] = true;
}

$arrIf['send.reg'] = !$arrIf['reg.success'];

$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
