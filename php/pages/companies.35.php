<?php
/** Created by WMBM(c) 2008 (Shirokovskiy D. aka Jimmy™).
 * Страница: Компании [companies] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "companies.35.tpl");

$arrTplVars['stPage'] = ( intval($_GET['stPage']) > 0 ? $_GET['stPage'] : 1 );

// Общий дефолтный запрос, если не выбран регион, т.е. ВСЕ регионы
$strSqlQuery = "SELECT sral_sr_id FROM `site_regions_access_lnk` ORDER BY sral_srat_id";
// Если выбрана группа региональная
if (isset($_GET['reg']) && in_array($_GET['reg'], array('spb','msk'))) {
    if ($_GET['reg']=='spb') {
        $strSqlQuery = "SELECT sral_sr_id FROM `site_regions_access_lnk` WHERE sral_srat_id = 1";
        $arrTplVars['rbcSpbChecked'] = 'checked';
    }
    if ($_GET['reg']=='msk') {
        $strSqlQuery = "SELECT sral_sr_id FROM `site_regions_access_lnk` WHERE sral_srat_id = 2";
        $arrTplVars['rbcMskChecked'] = 'checked';
    }
    $arrTplVars['Reg'] = $_GET['reg'];
} else {
    $arrTplVars['rbcAllChecked'] = 'checked';
}
$arrRegions = $objDb->fetchcol($strSqlQuery);

$strSqlWhereType = null;
$strCmpType = '';

/**
 * Проверим второй параметр адресной строки, тип списка компаний
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    $strCmpType = $arrReqUri[1];
} else {
    $arrIf['show.company.types'] = true;
}

/**
 * При пролистывании найденых компаний, название берём из сессии
 */
if ( isset($_GET['stPage']) && !empty($_SESSION['strCompanyName']) ) {
    $_REQUEST['strCompanyName'] = $_SESSION['strCompanyName'];
}

// Фильтруем по наименованию компании
if ( !empty($_REQUEST['strCompanyName']) && strlen($_REQUEST['strCompanyName']) > 1 ) {
    $strSqlWhereCompanyName = " sec_company LIKE '%".trim($_REQUEST['strCompanyName'])."%'";

    $arrTplVars['strCompanyName'] = htmlspecialchars($_REQUEST['strCompanyName']);
    $_SESSION['strCompanyName'] = $_REQUEST['strCompanyName'];
} else {
    $strSqlWhereCompanyName = " sec_company != ''";

    $_SESSION['strCompanyName'] = '';
}

/**
 * Отметим наименование раздела компаний
 */
if ( array_key_exists( $strCmpType, $arrTitleExt ) ) {
    $arrIf['chapter'] = true;
    $arrTplVars['strChapterName'] = $arrTitleExt[$strCmpType].($strCmpType=='inv' ? 'ы' : 'и');
}

/**
 * Фильтрует по типу недвижимости
 */
if (isset($_GET['type'])) {
    $strSqlWhereType = "AND seo_id_etype = '".intval($_GET['type'])."'";
    $arrTplVars['ch'.$_GET['type']] = ' checked';
}

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(DISTINCT(sec_company)) AS intQuantAllRecords FROM site_estate_contacts"
    ." LEFT JOIN site_objects_contacts_lnk ON (socl_id_contact=sec_id)"
    ." LEFT JOIN site_estate_objects ON (socl_id_object=seo_id)"
    ." LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id=seo_id)"
    ." WHERE $strSqlWhereCompanyName "
    .(!empty($strCmpType) ? " AND socl_relation='$strCmpType'" : '' ) // если указано по какому отношению ищем, напр. Заказчик
    ." $strSqlWhereType "
    ." AND sec_status = 'Y' AND sec_date_change > DATE_SUB(\"".date("Y-m-d")."\", INTERVAL 2 YEAR) AND (sorl_sr_id IN (".implode(',' , $arrRegions).") OR sorl_sr_id IS NULL)";
#echo "<!-- $strSqlQuery -->".PHP_EOL;
$arrTplVars['intQuantAllRecords'] = $arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');

// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 50;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
if (isset($arrIf['show.company.types']) && $arrIf['show.company.types'] === true) $objPagination->iQtyLinksPerPage = 15;
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM site_estate_contacts"
    ." LEFT JOIN site_objects_contacts_lnk ON (socl_id_contact=sec_id)"
    ." LEFT JOIN site_estate_objects ON (socl_id_object=seo_id)"
    ." LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id=seo_id)"
    ." WHERE $strSqlWhereCompanyName ".(!empty($strCmpType) ? " AND socl_relation='$strCmpType'" : '' )
    ." $strSqlWhereType AND sec_status = 'Y' AND sec_date_change > DATE_SUB(\"".date("Y-m-d")."\", INTERVAL 2 YEAR) AND (sorl_sr_id IN (".implode(',' , $arrRegions).") OR sorl_sr_id IS NULL)"
    ." GROUP BY sec_company ORDER BY sec_date_change DESC, sec_date_add DESC, sec_company, sec_id".$objPagination->strSqlLimit;
#echo "<!-- $strSqlQuery -->".PHP_EOL;
$arrCompanies = $objDb->fetchall( $strSqlQuery );

// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrCompanies) ? count($arrCompanies) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

/**
 * Подготовим вывод компаний
 */
if ( is_array($arrCompanies) ) {
    foreach ( $arrCompanies as $key => $value ) {
        $arrCompanies[$key]['strCompanyName'] = htmlspecialchars( $value['sec_company'] );
        $arrCompanies[$key]['strCompanyChapterName'] = $arrTitleExt[$value['socl_relation']];
    }
} else {
    $arrIf['no.companies'] = true;
}

$arrIf['is.companies'] = !$arrIf['no.companies'] && !empty($arrCompanies);

/**
 * Типы недвижимости
 */
$Types = new EstateObject();
$arrTypes = $Types->getEstateTypes();
$objTpl->tpl_loop("page.contents", "list.est.types", $arrTypes);

$objTpl->tpl_loop("page.contents", "companies", $arrCompanies);
$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
