<?php
/** Created by WMBM(c) 2008 (Shirokovskiy D. aka Jimmy™).
 * Страница: Техника [technics] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load($tpl = "page.contents", "technics.37.tpl");

$arrTplVars['stPage'] = $stPage = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);
$arrIf['nopage'] = !isset($_GET['stPage']);

/**
 * Проверим второй параметр адресной строки
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
	$strTechType = $arrReqUri[1];
	$strSqlQuery = "SELECT * FROM ".$_db_tables['stTechTypes']
		." WHERE (ptt_alias='$strTechType' OR ptt_id='$strTechType') LIMIT 1";
	$arrTechType = $objDb->fetch( $strSqlQuery );

	if ( is_array( $arrTechType ) ) {
		$intTechTypeID = $arrTechType['ptt_id'];
		$arrTplVars['m_title'] = "Техника > ".$arrTechType['ptt_name']." | PetroStroyBaza.RU";
		$arrIf['chapter'] = true;
		$arrTplVars['strChapterName'] = $arrTechType['ptt_name'];
		$arrTplVars['strChapterAlias'] = !empty($arrTechType['ptt_alias'])?$arrTechType['ptt_alias']:$arrTechType['ptt_id'];
	} else {
		header("Location: /technics");
		exit();
	}
}
$strSqlWhere = "";
if (!empty($intTechTypeID)) {
	$strSqlWhere .= " AND pt_tech_type_id = '$intTechTypeID'";
}

// Let's check if there is new parameter - type of ad
if (isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
	$strAdType = mysql_real_escape_string($arrReqUri[2]);
	$strSqlWhere .= " AND pt_ad_type = '$strAdType'";
	$arrTplVars['strAdTypeName'] = $arrAdTechType[$strAdType];
	$arrIf['ad_type'] = true;
	$arrTplVars['strAdTypeAlias'] =  $strAdType;
}

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stTechnics']." WHERE pt_status = 'Y'";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stTechnics']." WHERE pt_status = 'Y' $strSqlWhere";
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');

$arrIf['show_extra_number_ads'] = $arrTplVars['intQuantAllRecords']!=$arrTplVars['intQuantSelectRecords'];

// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 10;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stTechnics']
//  . " LEFT JOIN ".$_db_tables["stImages"]." ON (si_id_rel = seo_id)"
	. " LEFT JOIN ".$_db_tables["stTechTypes"]." ON (pt_tech_type_id = ptt_id)"
//  . " LEFT JOIN ".$_db_tables["stAdmReg"]." ON (seo_id_adm_reg = sar_id)"
	. " WHERE pt_status = 'Y' $strSqlWhere ORDER BY pt_date_update DESC ".$objPagination->strSqlLimit;
$arrTechnics = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrTechnics) ? count($arrTechnics) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrTechnics) ) {
	foreach ( $arrTechnics as $key => $arr_tech_ad ) {
		$arrTechnics[$key]['strAdType'] = $arrAdTechType[$arr_tech_ad['pt_ad_type']];
		$arrTechnics[$key]['strAdTypeUrl'] = SITE_URL.$arrReqUri[0].'/'.$arr_tech_ad['ptt_alias'].'/'.$arr_tech_ad['pt_ad_type'];
		if ( !empty($arr_tech_ad['pt_year']) ) {
			$arrTechnics[$key]['strYear'] = ', '.$arr_tech_ad['pt_year'].'г.';
		}
		$arrTechnics[$key]['strDopinfo'] = nl2br(htmlspecialchars( $arr_tech_ad['pt_dopinfo'] ));
		$arrTechnics[$key]['strContacts'] = nl2br(htmlspecialchars( $arr_tech_ad['pt_contacts'] ));
		$arrTechnics[$key]['strAdDate'] = date("d.m.Y", strtotime($arr_tech_ad['pt_date_create']));
//    	$arrIf['object.photo.'.$value['seo_id']] = ( !empty($value['si_filename']) && file_exists(DROOT."storage/images/technics/".str_replace('.b.', '.', $value['si_filename']) ) );
		$arrIf['is.rent.period.'.$arr_tech_ad['pt_id']] = !empty($arr_tech_ad['pt_rent_period']);
		$arrIf['is.address.works.'.$arr_tech_ad['pt_id']] = !empty($arr_tech_ad['pt_address_works']);
		$arrIf['is.year.'.$arr_tech_ad['pt_id']] = !empty($arr_tech_ad['pt_year']);
		$arrIf['is.marka.'.$arr_tech_ad['pt_id']] = !empty($arr_tech_ad['pt_marka']);
		$arrIf['is.model.'.$arr_tech_ad['pt_id']] = !empty($arr_tech_ad['pt_model']);
	}
} else {
	$arrIf['no.technics'] = true;
}
$arrIf['is.technics'] = !$arrIf['no.technics'] && !empty($arrTechnics);

$objTpl->tpl_loop( $tpl, "technics", $arrTechnics);

$objTpl->tpl_if( $tpl, $arrIf );
$objTpl->tpl_array( $tpl, $arrTplVars );
