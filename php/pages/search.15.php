<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Поиск объектов недвижимости [search] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "search.15.tpl");

if (isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'results') {
    unset($_SESSION['SF']);
    header('location: /search');
    exit;
}

$arrTplVars['stPage'] = $stPage = ( intval($_GET['stPage']) > 0 ? $_GET['stPage'] : 1 );

$arrIf['access'] = $_SESSION['uAuthInfo']['uAccess'] == 1;
$arrIf['no.access'] = !$arrIf['access'];
$arrIf['access.for.browser'] = false;
$arrTplVars['intQuantSelectRecords'] = 0;
$pathSearchMaps = DROOT.'storage/maps/';

if (!is_object($objUser)) {
    include_once "models/User.php";
    $objUser = new User();
}

$arrObjects = array();

if (isset($_POST['frmSearchObjects']) || isset($_SESSION['SF'])) {
    if (isset($_SESSION['SF']) && isset($_GET['stPage'])) {
        $_POST = $_SESSION['SF'];

    } else if (!empty($_POST)) {
        if (isset($_SESSION['SF'])) {
            unset($_SESSION['SF']);
        }
        $_SESSION['SF'] = $_POST;
    }

	$intObjID = $arrTplVars['intObjID'] = $arrSqlData['intObjID'] = intval($_POST['intObjID']);
	if ( intval( $intObjID ) > 0 ) {
		$arrSearchParams[] = " seo_id = '$intObjID'";
	} else {
		$arrTplVars['intObjID'] = null;
	}

	$arrSqlData['strObjectNameSearch'] = addslashes(trim($_POST['strObjectNameSearch']));
	$arrTplVars['strObjectNameSearch'] = htmlspecialchars(stripslashes(trim($_POST['strObjectNameSearch'])));

	if (!empty($arrSqlData['strObjectNameSearch']) ) {
		$arrSearchParams[] = " seo_name LIKE '%{$arrSqlData['strObjectNameSearch']}%'";
	}

	$arrSqlData['strObjectAddressSearch'] = addslashes(trim($_POST['strObjectAddressSearch']));
	$arrTplVars['strObjectAddressSearch'] = htmlspecialchars(stripslashes(trim($_POST['strObjectAddressSearch'])));

	if (!empty($arrSqlData['strObjectAddressSearch']) ) {
		$arrSearchParams[] = " seo_address LIKE '%{$arrSqlData['strObjectAddressSearch']}%'";
		$is_ext_search = true;
	}

	$arrSqlData['object_etap'] = addslashes(trim($_POST['object_etap']));
	$arrTplVars['object_etap'] = htmlspecialchars(stripslashes(trim($_POST['object_etap'])));

	if (!empty($arrSqlData['object_etap']) ) {
        /* Предварительная проверка на ключевые фразы */
        $isPrebuildEtap = array_search($arrSqlData['object_etap'], $arrEtapBuildings);
        if ($isPrebuildEtap !== false) {
            $strSqlSent = " (seo_other_etap LIKE '%{$arrSqlData['object_etap']}%'";
            $arrSqlData['object_etap'] = $arrEqEtapBuildings[$isPrebuildEtap];
            if (is_array($arrSqlData['object_etap'])) {
                foreach ($arrSqlData['object_etap'] as $string) {
                    $strSqlSent .= " OR seo_other_etap LIKE '%{$string}%'";
                }
            } else
                $strSqlSent .= " OR seo_other_etap LIKE '%{$arrSqlData['object_etap']}%'";
            $strSqlSent .= ")";
        } else {
            $strSqlSent = " seo_other_etap LIKE '%{$arrSqlData['object_etap']}%'";
        }

		$arrSearchParams[] = $strSqlSent;
        unset($strSqlSent);
		$is_ext_search = true;
	}

	$arrSqlData['strKarkasSearch'] = addslashes(trim($_POST['strKarkasSearch']));
	$arrTplVars['strKarkasSearch'] = htmlspecialchars(stripslashes(trim($_POST['strKarkasSearch'])));

    if (!empty($arrSqlData['strKarkasSearch']) && strlen($arrSqlData['strKarkasSearch']) > 2 ) {
        /* Предварительная проверка на ключевые фразы */
        $isPrebuildKarkas = array_search($arrSqlData['strKarkasSearch'], $arrKarkasBuildings);
        if ($isPrebuildKarkas !== false) {
            $strSqlSent = " (seo_karkas LIKE '%{$arrSqlData['strKarkasSearch']}%'";
            $arrSqlData['strKarkasSearch'] = $arrEqKarkasBuildings[$isPrebuildKarkas];
            if (is_array($arrSqlData['strKarkasSearch'])) {
                foreach ($arrSqlData['strKarkasSearch'] as $string) {
                    $strSqlSent .= " OR seo_karkas LIKE '%{$string}%'";
                }
            } elseif(!empty($arrSqlData['strKarkasSearch']) && strlen($arrSqlData['strKarkasSearch'])>2)
                $strSqlSent .= " OR seo_karkas LIKE '%{$arrSqlData['strKarkasSearch']}%'";
            $strSqlSent .= ")";
        } else {
            $strSqlSent = " seo_karkas LIKE '%{$arrSqlData['strKarkasSearch']}%'";
        }

        $arrSearchParams[] = $strSqlSent;
        unset($strSqlSent);
        $is_ext_search = true;
    } else {
        $arrSqlData['strKarkasSearch'] =
        $arrTplVars['strKarkasSearch'] = ''; // чтобы назад в шаблон не попало короткое название из двух или менее букв. Пусть клиент вводит заново!
    }

	$arrSqlData['strCompanyNameSearch'] = addslashes(trim($_POST['strCompanyNameSearch']));
	$arrTplVars['strCompanyNameSearch'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyNameSearch'])));

	if (!empty($arrSqlData['strCompanyNameSearch']) ) {
		$arrSearchParams[] = " (sec_company LIKE '%{$arrSqlData['strCompanyNameSearch']}%' OR sec_inn LIKE '{$arrSqlData['strCompanyNameSearch']}')";
		$strLeftJoin .= " LEFT JOIN ".$_db_tables["stObjContacts"]." ON (socl_id_object = seo_id)";
		$strLeftJoin .= " LEFT JOIN ".$_db_tables["stContacts"]." ON (sec_id = socl_id_contact)";
		$is_ext_search = true;
	}

	$arrSqlData['type'] = $_POST['type'];
//	$arrTplVars['type'] = ( $arrSqlData['type'] > 0 ? $arrSqlData['type'] : '');

	if (!empty($arrSqlData['type']) && is_array($arrSqlData['type'])) {
		$arrSearchParams[] = " seo_id_etype IN (".implode(',',$arrSqlData['type']).")";
	}

	$arrSqlData['vid'] = intval(trim($_POST['vid']));
	$arrTplVars['vid'] = ( $arrSqlData['vid'] > 0 ? $arrSqlData['vid'] : '');

	if (!empty($arrSqlData['vid']) ) {
		$arrSearchParams[] = " seo_id_vid = '{$arrSqlData['vid']}'";
		$is_ext_search = true;
	}

	$arrSqlData['year'] = intval(trim($_POST['year']));
	$arrTplVars['year'] = ( $arrSqlData['year'] > 0 ? $arrSqlData['year'] : '');

	if (!empty($arrSqlData['year']) ) {
		$arrSearchParams[] = " seo_period_bld LIKE '%{$arrSqlData['year']}%'";
		$is_ext_search = true;
	}

    /**
     * Выбор региона
     */
    $arrRegions = $_POST['cbxRegion'];
    if (is_array($arrRegions) && !empty($arrRegions)) {
        $strLeftJoin2 = " LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id = seo_id)";
        $strLeftJoin2 .= " LEFT JOIN site_regions ON (sorl_sr_id = sr_id)";
        $arrSearchParams[] = " sorl_sr_id IN (".implode(',' , $arrRegions).")";
        $is_ext_search = true;
    }

    $arrSqlData['regionid'] = is_int($_POST['regionid']) ? intval(trim($_POST['regionid'])) : $_POST['regionid'];

	if (!empty($arrSqlData['regionid'])) {
        if (is_int($arrSqlData['regionid'])) {
            $arrSearchParams[] = " seo_id_adm_reg = {$arrSqlData['regionid']}";
            $is_ext_search = true;
        } elseif (is_array($arrSqlData['regionid']) && !empty($arrSqlData['regionid'][0])) {
            $arrSearchParams[] = " seo_id_adm_reg IN (".implode(',',$arrSqlData['regionid']).")";
            $is_ext_search = true;
        }
	}

	$arrSqlData['region_other'] = addslashes(trim($_POST['region_other']));
	$arrTplVars['region_other'] = htmlspecialchars(stripslashes(trim($_POST['region_other'])));

	if (!empty($arrSqlData['region_other']) ) {
		$arrSearchParams[] = " seo_other_adm_reg LIKE '%{$arrSqlData['region_other']}%'";
		$is_ext_search = true;
	}

    $arrSearchParams[] = " `seo_etap_date` BETWEEN CASE WHEN seo_other_etap REGEXP 'пуско-налад|внутр|благоустройств' THEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR), '%Y-%m-%d') ELSE DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR), '%Y-01-01') END AND CURDATE()";

    if (isset($_REQUEST['period'])) {
        $arrTplVars['option'.$_REQUEST['period']] = ' selected';

        switch ($_REQUEST['period']) {
            case 1:
                $arrSearchParams[] = " `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 3 MONTH) AND NOW()";
                break;

            case 2:
                $arrSearchParams[] = " `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AND NOW()";
                break;

            case 3:
                $arrSearchParams[] = " `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 12 MONTH) AND NOW()";
                break;

            case 4:
                $arrSearchParams[] = " `seo_date_change` < DATE_SUB(CURDATE(), INTERVAL 12 MONTH)";
                break;

            case 5:
                $arrSearchParams[] = " `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND NOW()";
                break;

            default:

                break;
        }
    }

	if ( is_array( $arrSearchParams ) ) {
		$strSearchParams = implode(" AND ", $arrSearchParams);
	}

	if (!empty($strSearchParams)) {
		// Всего записей в базе
		$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_estate_objects"
            .$strLeftJoin.$strLeftJoin2." WHERE seo_status = 'Y' AND $strSearchParams";
		$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');

		// Выбрано записей
		$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM site_estate_objects"
            .$strLeftJoin.$strLeftJoin2." WHERE seo_status = 'Y' AND $strSearchParams";
		$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');

		// ***** BEGIN: Построение пейджинга для вывода списка
		$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
		/**
        * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
        * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
        */
        $objPagination->strPaginatorTpl = "site.global.1.tpl";
        $objPagination->iQtyLinksPerPage = 10;
        $objPagination->iQtyRecsPerPage = 20;
        $objPagination->strColorLinkStyle = "link-b";
        $objPagination->strColorActiveStyle = "tab-bl-b";
        $objPagination->strNameImageBack = "arrow_one_left.gif";
        $objPagination->strNameImageForward = "arrow_one_right.gif";
        // Создаем блок пэйджинга
        $objPagination->paCreate();
        // ***** END: Построение пейджинга для вывода списка
        // Запрос для выборки нужных записей
        $strSqlQuery = "SELECT * FROM site_estate_objects".$strLeftJoin
        . " LEFT JOIN `site_images` ON (si_id_rel = seo_id)"
        . " LEFT JOIN `site_estate_types` ON (seo_id_etype = set_id)"
        . " LEFT JOIN `site_regions_adm` ON (seo_id_adm_reg = sra_id)"
        . " LEFT JOIN `site_objects_regions_lnk` ON (sorl_seo_id = seo_id)"
        . " LEFT JOIN `site_regions` ON (sorl_sr_id = sr_id)"
        . " WHERE `seo_status` = 'Y' AND $strSearchParams GROUP BY seo_id"
        . " ORDER BY seo_etap_date DESC, seo_date_change DESC, seo_date_add DESC, seo_id DESC";

        $_SESSION['cachedSearchQueries'][] = $_SESSION['cachedSearchQuery'] = $cachedSql = md5($strSqlQuery . date('Ymd'));

        if (!file_exists($pathSearchMaps.'geo-map-data.'.$cachedSql.'.json') && isset($cachedSql) && strlen($cachedSql) == 32) {
            // подготовим карту объектов
            $arrRes['type'] = 'FeatureCollection';
            $arrRes['features'] = array();
            $id = 0;

            $arrAllSearchedObjects = $objDb->fetchall( $strSqlQuery );

            if (is_array($arrAllSearchedObjects) && !empty($arrAllSearchedObjects)) {
                foreach ( $arrAllSearchedObjects as $object ) {
                    if (!empty($object['seo_coords'])) {
                        // подготовим карту объектов
                        $obj = new stdClass();
                        $obj->type = 'Feature';
                        $obj->id = $id++;
                        $obj->geometry = new stdClass();
                        $obj->geometry->type = 'Point';
                        $obj->geometry->coordinates = explode(',', $object['seo_coords']);
                        $obj->properties = new stdClass();
                        $obj->properties->balloonContent = strip_tags($object['seo_name']).'<br><a target="_blank" href="'.SITE_URL.'objectinfo/'.($object['seo_url']?:$object['seo_id']).'" class="link-darkblue">'.strip_tags($object['seo_address'])."</a>";
                        $obj->properties->clusterCaption = strip_tags($object['seo_name']);
                        $obj->properties->hintContent = strip_tags($object['seo_name']);

                        $arrRes['features'][] = $obj;
                    }
                }
            } else {
                # ошибка
                $errorCreateFolder = true;
            }

            # есть закешированный запрос SQL
            # запишем его в файл для дальнейшего использования
            if (!is_dir($pathSearchMaps)) {
                if(!mkdir($pathSearchMaps)) {
                    # ошибка
                    $errorCreateFolder = true;
                }
            }

            if (!isset($errorCreateFolder) || !$errorCreateFolder) {
                file_put_contents($pathSearchMaps.'geo-map-data.'.$cachedSql.'.json', json_encode($arrRes));
            }
        }

        if (file_exists($pathSearchMaps.'geo-map-data.'.$cachedSql.'.json') && isset($cachedSql) && strlen($cachedSql) == 32) {
            $arrIf['mapped'] = true;
        }

        $strSqlQuery .= " ".$objPagination->strSqlLimit;

        $arrObjects = $objDb->fetchall( $strSqlQuery );

		// кол-во публикаций показанных на странице
		$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
		// Присвоение значения пэйджинга для списка
		$arrTplVars['blockPaginator'] = $objPagination->paShow();

        if ( is_array($arrObjects) ) {
            foreach ( $arrObjects as $key => $value ) {
                $arrObjects[$key]['strAdmReg'] = !empty($value['seo_other_adm_reg']) ? htmlspecialchars($value['seo_other_adm_reg']) : htmlspecialchars($value['sra_name']);
                $arrIf['object.photo.'.$value['seo_id']] = ( !empty($value['si_filename']) && file_exists(DROOT."storage/images/objects/".str_replace('.b.', '.', $value['si_filename']) ) );
                $arrIf['naznach.'.$value['seo_id']] = !empty($value['seo_naznach']);
                $arrIf['etap.stroy.'.$value['seo_id']] = !empty($value['seo_other_etap']);
                if( $arrIf['etap.stroy.'.$value['seo_id']] && !empty($value['seo_etap_date']) ) {
                	$arrObjects[$key]['seo_etap_date_formated'] = date('d.m.Y', strtotime($value['seo_etap_date']));
                }
                $arrObjects[$key]['strRegion'] = htmlspecialchars($value['sr_name']);

                // проверка объекта в блокноте
                if ($arrIf['authed']) {
                    $arrIf['not.in.notebook.'.$value['seo_id']] = !$objUser->isInNotebook($value['seo_id']);
                }
            }
        } else {
            $arrIf['no.objects'] = true;
        }
    } else {
        $arrIf['no.query'] = true;
    }
}

/**
 * Проверим возможность показа адреса пользователю
 * Боты должны видеть адрес, а пользователи через браузер и без доступа - не должны.
 */
if ($arrIf['access'] || (!preg_match("/Mozilla/i", $strUsAgent) && !preg_match("/Opera/i", $strUsAgent))) {
    $arrIf['access.for.browser'] = true;
}

if ( $is_ext_search ) {
  $arrIf['js.show.full.search'] = true;
}

$arrIf['no.objects'] = empty($arrObjects);
$arrIf['is.objects'] = !$arrIf['no.objects'] && !empty($arrObjects);

$objTpl->tpl_loop("page.contents", "objects", $arrObjects);
$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
