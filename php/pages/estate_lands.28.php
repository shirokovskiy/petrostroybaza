<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Объявления раздела Недвижимость и Участки [estate_lands] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "estate_lands.28.tpl");

if ( !empty($arrReqUri[1]) ) {
  $intChapterId = intval($arrReqUri[1]);
  $strSqlQuery = "SELECT paec_name FROM ".$_db_tables['stEstateAdsChapters']. " WHERE paec_id='$intChapterId'";
  $arrTplVars['strSubChapter'] = $objDb->fetch( $strSqlQuery, 'paec_name' );

  if (intval($intChapterId) > 0) {
    $strSqlFilter .= " AND pael_chapter_id = '$intChapterId'";
  }
}

/**
 * Выборка списка объявлений
 */
// Всего записей в базе
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stAdsEstateLands']." WHERE pael_status = 'Y' $strSqlFilter";
$arrTplVars['intQuantSelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
* Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
* кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
*/
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 10;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stAdsEstateLands']
//." LEFT JOIN ".$_db_tables["stEstateAdsChapters"]." ON (paec_id = pael_chapter_id)"
." WHERE pael_status = 'Y' $strSqlFilter ORDER BY pael_id DESC ".$objPagination->strSqlLimit;
$arrAds = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrAds) ? count($arrAds) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrAds) ) {
//  foreach ( $arrAds as $key => $value ) {
//  }
} else {
  $arrIf['no.estate_lands'] = true;
}

$arrIf['is.estate_lands'] = !$arrIf['no.estate_lands'] && !empty($arrAds);

$objTpl->tpl_loop("page.contents", "estate_lands", $arrAds);
$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
