<?php
/** Created by WMBM(c) 2008 (Shirokovskiy D. aka Jimmy™).
 * Страница: Напомнить пароль [recovery] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "recovery.34.tpl");

if ( isset( $_POST['recovery']) && $_POST['recovery'] == 'true' ) {
    $arrSqlData['strEmailToRecover'] = mysql_real_escape_string(trim($_REQUEST['strEmailToRecover']));
    $arrTplVars['strEmailToRecover'] = htmlspecialchars(trim($_REQUEST['strEmailToRecover']));

    if ( empty($arrSqlData['strEmailToRecover']) ) {
        $arrIf['no.email'] = true;
    } else {
        /**
         * Поиск клиента
         */
        $strSqlQuery = "SELECT su_id, su_login, su_passw FROM ".$_db_tables['stUsers']." WHERE su_email='{$arrSqlData['strEmailToRecover']}'";
        $arrExistSuchUser = $objDb->fetch( $strSqlQuery );

        if ( is_array( $arrExistSuchUser ) && !empty( $arrExistSuchUser['su_login'] ) && !empty( $arrExistSuchUser['su_passw'] ) ) {
            # Логин и Пароль есть, осталось только отправить
            if ( !is_object( $objMail ) ) {
                include_once( "cls.mail.php" );
                $objMail = new clsMail();
            }

            $newPassword = $objUtil->getRandomPassw();

            if ( strlen( $newPassword ) == 7 ) {
                $strSqlQuery = "UPDATE ".$_db_tables['stUsers']." SET"
                    . " su_passw = md5('$newPassword')"
                    . " WHERE su_id='".$arrExistSuchUser['su_id']."'"
                ;
                if ( !$objDb->query( $strSqlQuery ) ) {
                    # sql error
                    $GLOBALS['manStatusError'] = 1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                }

                $sendTo = $arrSqlData['strEmailToRecover'];
                $sendFrom = "info@petrostroybaza.ru";
                $sendSubject = "PetroStroyBaza | Password recovery";
                $sendMessage = "Уважаемый пользователь сайта PetroStroyBAza!\n";
                $sendMessage .= "Вы, или кто-то другой, запросили напомнить пароль для авторизации на сайте:\n";
                $sendMessage .= "Логин: {$arrExistSuchUser['su_login']}\n";
                $sendMessage .= "Пароль: $newPassword\n";
                $sendMessage .= "\n";
                $sendMessage .= "По всем вопросам обращайтесь по телефону или посредствам E-mail указаных на странице http://www.petrostroybaza.ru/contacts/\n";
                $sendMessage .= "\n\n";
                $sendMessage .= "Не отвечайте на данное письмо! Текст сформирован автоматически почтовым роботом.\n";

                $sendMessage .= "\n\n===============================================================\n\n";

                $sendMessage .= "Dear subscriber of PetroStroyBAza!\n";
                $sendMessage .= "You, or someone else, requested password recovery:\n";
                $sendMessage .= "Login: {$arrExistSuchUser['su_login']}\n";
                $sendMessage .= "Password: $newPassword\n";
                $sendMessage .= "\n";
                $sendMessage .= "If any questions don't hesitate to call or Email us by contacts on http://www.petrostroybaza.ru/contacts/\n";
                $sendMessage .= "\n\n";
                $sendMessage .= "Please, don't answer on this Email! This is automatically sended message.\n";

                $objMail->new_mail( $sendFrom, $sendTo, $sendSubject, $sendMessage );
//                $objMail->add_bcc( "jimmy.webstudio@gmail.com" );
                if ( !$objMail->send() ) {
                    $arrIf['not.sent.password'] = true;
                } else {
                    $arrIf['sent'] = true;
                }
            } else {
                $arrIf['no.new.password'] = true;
            }
        } else {
            $arrIf['no.such.email'] = true;
        }
    }
}

$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
