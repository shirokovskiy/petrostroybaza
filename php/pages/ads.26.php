<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Список объявлений по разделам [ads] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "ads.26.tpl");

if ( !empty($arrReqUri[1]) ) {
  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAdsAdsChapters']. " WHERE paac_alias='{$arrReqUri[1]}'";
  $arrChapter = $objDb->fetch( $strSqlQuery );
  $intChapterId = $arrChapter['paac_id'];

  if (intval($intChapterId)) {
    $strSqlFilter .= " AND paa_chapter_id = '$intChapterId'";
    $arrTplVars['strSubChapter'] = htmlspecialchars($arrChapter['paac_name']);
  }
}

/**
 * Выборка списка объявлений
 */
// Всего записей в базе
//$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stAds']." WHERE pt_status = 'Y'";
//$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stAds']." WHERE paa_status = 'Y' $strSqlFilter";
$arrTplVars['intQuantSelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
* Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
* кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
*/
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 10;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stAds']
//." LEFT JOIN ".$_db_tables["stAdsAdsChapters"]." ON (paac_id = paa_chapter_id)"
." WHERE paa_status = 'Y' $strSqlFilter ORDER BY paa_id DESC ".$objPagination->strSqlLimit;
$arrAds = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrAds) ? count($arrAds) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrAds) ) {
//  foreach ( $arrAds as $key => $value ) {
//  }
} else {
  $arrIf['no.ads'] = true;
}

$arrIf['is.ads'] = !$arrIf['no.ads'] && !empty($arrAds);

$objTpl->tpl_loop("page.contents", "ads", $arrAds);
$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
