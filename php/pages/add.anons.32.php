<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Добавить анонс мероприятия [add.anons] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "add.anons.32.tpl");

if (isset($_GET['errMess']) && $_GET['errMess'] == 'msgOk') {
  $arrIf['success'] = true;
  $arrTplVars['msgOk'] = "Данные успешно сохранены";
}

/**
 * Запись нового анонса от пользователя
 */
if ( isset($_POST) && !empty($_POST) && isset($_GET['new']) && $_GET['new'] == 'record' ) {
  $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
  $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

  if (empty($arrSqlData['strTitle']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Заголовок отсутствует";
  }

  $arrSqlData['strAddress'] = addslashes(trim($_POST['strAddress']));
  $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

  if (empty($arrSqlData['strAddress']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Адрес не указан";
  }

  $arrSqlData['strContacts'] = addslashes(trim($_POST['strContacts']));
  $arrTplVars['strContacts'] = htmlspecialchars(stripslashes(trim($_POST['strContacts'])));

  if (empty($arrSqlData['strContacts']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Контактное лицо не указано";
  }

  $arrSqlData['strTime'] = addslashes(trim($_POST['strTime']));
  $arrTplVars['strTime'] = htmlspecialchars(stripslashes(trim($_POST['strTime'])));

  if (empty($arrSqlData['strTime']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Дата и время не указаны";
  }

  $arrSqlData['strInfo'] = mysql_real_escape_string(trim($_POST['strInfo']));
  $arrTplVars['strInfo'] = htmlspecialchars(trim($_POST['strInfo']));

  if ( !$arrIf['error'] ) {
    $strSqlFields = ""
      . " pam_name = '{$arrSqlData['strTitle']}'"
      . ", pam_action_date = '{$arrSqlData['strTime']}'"
      . ", pam_address = '{$arrSqlData['strAddress']}'"
      . ", pam_contacts = '{$arrSqlData['strContacts']}'"
      . ", pam_info = '{$arrSqlData['strInfo']}'"
      ;
    $strSqlQuery = "INSERT INTO ".$_db_tables['stAnonses']." SET $strSqlFields, pam_date_create = NOW()";

    if ( !$objDb->query( $strSqlQuery ) ) {
      $arrIf['error'] = true;
      $arrTplVars['errMsg'] .= "<li> Ошибка базы данных";
    } else {
      header('location: /'.$arrReqUri[0]."?errMess=msgOk");
      exit;
    }
  }
}

$arrIf['post.data'] = !$arrIf['success'];

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
