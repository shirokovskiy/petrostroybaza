<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Добавить тендер [add.tender] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "add.tender.30.tpl");

if (isset($_GET['errMess']) && $_GET['errMess'] == 'msgOk') {
  $arrIf['success'] = true;
  $arrTplVars['msgOk'] = "Данные успешно сохранены";
}

/**
 * Запись нового тендера от пользователя
 */
if ( isset($_POST) && !empty($_POST) && isset($_GET['new']) && $_GET['new'] == 'record' ) {
  $arrSqlData['strTenderName'] = addslashes(trim($_POST['strTenderName']));
  $arrTplVars['strTenderName'] = htmlspecialchars(stripslashes(trim($_POST['strTenderName'])));

  if (empty($arrSqlData['strTenderName']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Название тендера отсутствует";
  }

  $arrSqlData['strVidRabot'] = addslashes(trim($_POST['strVidRabot']));
  $arrTplVars['strVidRabot'] = htmlspecialchars(stripslashes(trim($_POST['strVidRabot'])));

  if (empty($arrSqlData['strVidRabot']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Вид работ не указан";
  }

  $arrSqlData['strAddress'] = addslashes(trim($_POST['strAddress']));
  $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

  if (empty($arrSqlData['strAddress']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Адрес не указан";
  }

  $arrSqlData['strCustomer'] = addslashes(trim($_POST['strCustomer']));
  $arrTplVars['strCustomer'] = htmlspecialchars(stripslashes(trim($_POST['strCustomer'])));

  if (empty($arrSqlData['strCustomer']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Заказчик не указан";
  }

  $arrSqlData['strCoordinates'] = addslashes(trim($_POST['strCoordinates']));
  $arrTplVars['strCoordinates'] = htmlspecialchars(stripslashes(trim($_POST['strCoordinates'])));

  if (empty($arrSqlData['strCoordinates']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Координаты не указаны";
  }

  $arrSqlData['strContacts'] = addslashes(trim($_POST['strContacts']));
  $arrTplVars['strContacts'] = htmlspecialchars(stripslashes(trim($_POST['strContacts'])));

  if (empty($arrSqlData['strContacts']) ) {
    $arrIf['error'] = true;
    $arrTplVars['errMsg'] .= "<li> Контактное лицо не указано";
  }

  $arrSqlData['strInfo'] = mysql_real_escape_string(trim($_POST['strInfo']));
  $arrTplVars['strInfo'] = htmlspecialchars(trim($_POST['strInfo']));

  if ( !$arrIf['error'] ) {
    $strSqlFields = ""
      . " pt_name = '{$arrSqlData['strTenderName']}'"
      . ", pt_vid_rabot = '{$arrSqlData['strVidRabot']}'"
      . ", pt_address = '{$arrSqlData['strAddress']}'"
      . ", pt_customer = '{$arrSqlData['strCustomer']}'"
      . ", pt_coordinates = '{$arrSqlData['strCoordinates']}'"
      . ", pt_contacts = '{$arrSqlData['strContacts']}'"
      . ", pt_usefull_info = '{$arrSqlData['strInfo']}'"
      ;
    $strSqlQuery = "INSERT INTO ".$_db_tables['stTenders']." SET $strSqlFields, pt_date_create = NOW()";

    if ( !$objDb->query( $strSqlQuery ) ) {
      $arrIf['error'] = true;
      $arrTplVars['errMsg'] .= "<li> Ошибка базы данных";
    } else {
      header('location: /'.$arrReqUri[0]."?errMess=msgOk");
      exit;
    }
  }
}

$arrIf['post.data'] = !$arrIf['success'];

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
