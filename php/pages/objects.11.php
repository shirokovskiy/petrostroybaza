<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Список объектов [objects] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load($tpl = "page.contents", "objects.11.tpl");

$arrTplVars['stPage'] = $stPage = ( intval($_GET['stPage']) > 0 ? $_GET['stPage'] : 1 );
$strSqlWhere = $strLeftJoin = '';
$arrIf['access'] = $_SESSION['uAuthInfo']['uAccess'] == 1;
$arrIf['no.access'] = !$arrIf['access'];
$arrIf['access.for.browser'] = false;

if (isset($_SESSION['SF'])) {
    unset($_SESSION['SF']);
}

if (!is_object($objUser)) {
    include_once "models/User.php";
    $objUser = new User();
}

/**
 * Проверим второй параметр адресной строки, типа раздел недвижимости
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    $strEstType = $arrReqUri[1];
    $strSqlQuery = "SELECT * FROM site_estate_types WHERE (set_alias='$strEstType' OR set_id='$strEstType') LIMIT 1";
    $arrEstType = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrEstType ) ) {
        $intEstTypeId = $arrEstType['set_id'];
        $arrTplVars['m_title'] = "Объекты > ".$arrEstType['set_name']." | PetroStroyBaza.RU";
        $arrIf['chapter'] = true;
        $arrTplVars['strChapterName'] = $arrEstType['set_name'];
    } else {
        header("Location: /objects");
        exit();
    }
}

if (!empty($intEstTypeId)) {
    $strSqlWhere .= " AND seo_id_etype = '$intEstTypeId'";
}

$strSqlQuery = "SELECT sral_sr_id FROM `site_regions_access_lnk` ORDER BY sral_srat_id";
// Если выбрана группа региональная
if (isset($_GET['reg']) && in_array($_GET['reg'], array('spb','msk'))) {
    if ($_GET['reg']=='spb') {
        $strSqlQuery = "SELECT sral_sr_id FROM `site_regions_access_lnk` WHERE sral_srat_id = 1";
        $arrTplVars['rbcSpbChecked'] = 'checked';
    }
    if ($_GET['reg']=='msk') {
        $strSqlQuery = "SELECT sral_sr_id FROM `site_regions_access_lnk` WHERE sral_srat_id = 2";
        $arrTplVars['rbcMskChecked'] = 'checked';
    }
    $arrTplVars['Reg'] = $_GET['reg'];
} else {
    $arrTplVars['rbcAllChecked'] = 'checked';
}
$arrRegions = $objDb->fetchcol($strSqlQuery);

$strLeftJoin = " LEFT JOIN site_objects_regions_lnk ON (sorl_seo_id = seo_id)";
$strLeftJoin .= " LEFT JOIN site_regions tSr ON (sorl_sr_id = sr_id)";
$strSqlWhere .= " AND sorl_sr_id IN (".implode(',' , $arrRegions).") AND seo_etap_date BETWEEN CASE WHEN seo_other_etap REGEXP 'пуско-налад|внутр|благоустройств' THEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR), '%Y-%m-%d') ELSE DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR), '%Y-01-01') END AND CURDATE()";

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(tSeo.seo_id) AS intQuantAllRecords FROM site_estate_objects tSeo $strLeftJoin WHERE seo_status = 'Y' $strSqlWhere";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(tSeo.seo_id) AS intQuantSelectRecords FROM site_estate_objects tSeo $strLeftJoin WHERE seo_status = 'Y' $strSqlWhere";
$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 20;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT tSeo.seo_name, tSeo.seo_other_adm_reg, tSeo.seo_url, tSeo.seo_id, tSeo.seo_naznach, tSeo.seo_other_etap"
    . ", tSeo.seo_etap_date, tSeo.seo_address, tSi.si_filename, tSra.sra_name, tSet.set_name, tSr.sr_name"
    . " FROM site_estate_objects tSeo"
    . " LEFT JOIN site_images tSi ON (si_id_rel = seo_id)"
    . " LEFT JOIN site_estate_types tSet ON (seo_id_etype = set_id)"
    . " LEFT JOIN site_regions_adm tSra ON (seo_id_adm_reg = sra_id)"
    . $strLeftJoin
    . " WHERE seo_status = 'Y' ".($_GET['images']==1 ? " AND si_id_rel IS NOT NULL" : '')."$strSqlWhere GROUP BY seo_id ORDER BY seo_etap_date DESC, seo_date_change DESC, seo_date_add DESC, seo_id DESC ".$objPagination->strSqlLimit;
//if ($_SERVER['REMOTE_ADDR'] == '188.134.80.143') {
//    echo $strSqlQuery;
//    die("<hr /><p style='color:red'><b>" . __FILE__ . "</b>:<i>" . __LINE__ . "</i><br>" . date('d.m.Y  H:i:s') . "</p>");
//}
$arrObjects = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrObjects) ? count($arrObjects) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrObjects) ) {
    foreach ( $arrObjects as $key => $value ) {
        $arrObjects[$key]['strAdmReg'] = !empty($value['seo_other_adm_reg']) ? htmlspecialchars($value['seo_other_adm_reg']) : htmlspecialchars($value['sra_name']);
        $arrObjects[$key]['urlParam'] = (!empty($value['seo_url']) ? $value['seo_url'] : $value['seo_id']);

        /* проверить нет ли фото из фотобанка */
        if ($value['seo_id']>0) {
            $strSqlQuery = "SELECT si_filename, si_id_rel FROM `site_images` LEFT JOIN `site_images_group` ON (sig_id = si_id_rel AND sig_img_id = si_id) WHERE `si_foto_object_id` = ".$value['seo_id']." AND si_type = 'fotobank' ORDER BY si_id DESC"; // самую свежую фото
            $arrImagesForObject = $objDb->fetch($strSqlQuery); // только одну
            if (is_array($arrImagesForObject) && !empty($arrImagesForObject)) {
                $arrObjects[$key]['strImagePath'] = $arrTplVars['cfgAllImg'].'fotobank/groups/'.$arrImagesForObject['si_id_rel'].'/'.$arrImagesForObject['si_filename'];
                $arrIf['object.photo.'.$value['seo_id']] = file_exists(DROOT."storage/images/fotobank/groups/".$arrImagesForObject['si_id_rel']."/".$arrImagesForObject['si_filename'] );
            }
        }

        if (!$arrIf['object.photo.'.$value['seo_id']]) {
            $arrIf['object.photo.'.$value['seo_id']] = ( !empty($value['si_filename']) && file_exists(DROOT."storage/images/objects/".str_replace('.b.', '.', $value['si_filename']) ) );
        }

        if (empty($arrObjects[$key]['strImagePath']) && !empty($arrObjects[$key]['si_filename'])) {
            $arrObjects[$key]['strImagePath'] = $arrTplVars['cfgAllImg'].'objects/'.$arrObjects[$key]['si_filename'];
        }
        $arrIf['naznach.'.$value['seo_id']] = !empty($value['seo_naznach']);
        $arrObjects[$key]['seo_naznach'] = nl2br($value['seo_naznach']);
        $arrIf['etap.stroy.'.$value['seo_id']] = !empty($value['seo_other_etap']);
        if( $arrIf['etap.stroy.'.$value['seo_id']] && !empty($value['seo_etap_date']) ) {
            $arrObjects[$key]['seo_etap_date_formated'] = date('d.m.Y', strtotime($value['seo_etap_date']));
        }
        $arrObjects[$key]['strRegion'] = htmlspecialchars($value['sr_name']);
        $arrIf['is.address.'.$value['seo_id']] = !empty($value['seo_address']);
        $arrObjects[$key]['strAddressData'] = $objUtil->translitBadChars($value['seo_address'], false);
        // проверка объекта в блокноте
        if ($arrIf['authed']) {
            $arrIf['not.in.notebook.'.$value['seo_id']] = !$objUser->isInNotebook($value['seo_id']);
        }
    }
} else {
    $arrIf['no.objects'] = true;
}

/**
 * Проверим возможность показа адреса пользователю
 * Боты должны видеть адрес, а пользователи через браузер и без доступа - не должны.
 */
if ($arrIf['access'] || (!preg_match("/Mozilla/i", $strUsAgent) && !preg_match("/Opera/i", $strUsAgent))) {
    $arrIf['access.for.browser'] = true;
}
$arrIf['is.objects'] = !$arrIf['no.objects'] && !empty($arrObjects);

$objTpl->tpl_loop($tpl, "objects", $arrObjects);
$objTpl->tpl_if($tpl, $arrIf);
$objTpl->tpl_array($tpl, $arrTplVars);
