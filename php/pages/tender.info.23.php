<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Карточка объявления тендера [tender.info] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "tender.info.23.tpl");

$arrTplVars['urlLinkBack'] = $_SERVER['HTTP_REFERER'];

$intRecordId = intval($arrReqUri[1]);

if (!empty($intRecordId)) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($intRecordId);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stTenders']." WHERE pt_id ='$intRecordId' AND pt_status = 'Y'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strTenderName'] = htmlspecialchars(stripslashes($arrInfo['pt_name']));
    $arrTplVars['strVidRabot'] = htmlspecialchars(stripslashes($arrInfo['pt_vid_rabot']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes($arrInfo['pt_address']));
    $arrTplVars['strCustomer'] = htmlspecialchars(stripslashes($arrInfo['pt_customer']));
    $arrTplVars['strCoordinates'] = htmlspecialchars(stripslashes($arrInfo['pt_coordinates']));
    $arrTplVars['strContacts'] = htmlspecialchars(stripslashes($arrInfo['pt_contacts']));
    $arrTplVars['strInfo'] = htmlspecialchars(stripslashes($arrInfo['pt_usefull_info']));

    $arrTplVars['cbxStatus'] = $arrInfo['pt_status'] == 'Y' ? ' checked' : '';
  } else {
    $arrIf['no.tender.info'] = true;
  }
} else {
  $arrIf['no.tender.info'] = true;
}

$arrIf['is.tender.info'] = !$arrIf['no.tender.info'];

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
