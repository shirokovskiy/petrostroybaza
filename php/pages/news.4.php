<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Новости [news] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "news.4.tpl");

include_once 'Zend/Loader.php'; // the Zend dir must be in your include_path
try {
	Zend_Loader::loadClass('Zend_Gdata_YouTube');
} catch (Exception $ex) {
	//echo $ex->getTraceAsString();
}

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);
$flagUpdate = false;

/**
 * Если указана конкретная новость - покажем её
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
	if(isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
		$nDate = $arrReqUri[1];
		$nUrl = trim($arrReqUri[2]);
		$strSqlQuery = "SELECT * FROM site_news WHERE sn_status='Y' AND sn_url='".$nUrl."' AND sn_date_publ='".$nDate."'";
	} else {
		$intId = intval($arrReqUri[1]);
		$strSqlQuery = "SELECT * FROM site_news WHERE sn_status='Y' AND sn_id=".$intId;
		$flagUpdate = true;
	}
	$arrNewsInfo = $objDb->fetch( $strSqlQuery );
	// Проверим, существует ли такая новость, и если да, то обработаем для вывода
	if (!empty($arrNewsInfo)) {
		if(!$flagUpdate) {
			$intId = intval($arrNewsInfo['sn_id']);
		}
		$arrNewsInfo['strDatePublNews'] = $objUtil->workDate(2, $arrNewsInfo['sn_date_publ']);
		$arrNewsInfo['strNewsBody'] = nl2br($arrNewsInfo['sn_body']);
		$arrIf['picture'] = ( file_exists(DROOT."storage/images/news/news.photo.".$arrNewsInfo['sn_id'].".jpg") && $arrNewsInfo['sn_image'] == 'Y');
		$objTpl->tpl_array("page.contents", $arrNewsInfo);

		$arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrNewsInfo['sn_title'])).' | '.$arrTplVars['m_title'];
		$arrTplVars['m_description'] = $objUtil->substrText(htmlspecialchars(strip_tags($arrNewsInfo['sn_body'])));

		$arrTplVars['strUniqueMainPhoto'] = $arrIf['picture'] ? '?pic='.time() : '';

		// Показать картинки, если они есть
		if ($arrNewsInfo['sn_image']=='Y') {
			# Дополнительные картинки есть только в том случае, если есть оснавная
			$strSqlQuery = "SELECT si_id,si_desc FROM site_images WHERE si_status='Y' AND si_id_rel = ".$intId." AND si_type = 'photorep'";
			$arrMoreImages = $objDb->fetchall( $strSqlQuery );
			$arrIf['pic_group'] = is_array($arrMoreImages) && !empty($arrMoreImages);
		}

		// Check attachments
		$newsAttachedFiles = PRJ_FILES.'news_attachments/'.$intId.'/';
		if (is_dir($newsAttachedFiles)) {
			$exclude_list = array(".", "..");
			$arrAttachedFiles = array_diff(scandir($newsAttachedFiles), $exclude_list);
			if (is_array($arrAttachedFiles)) {
				$arrIf['is.attached.files'] = !empty($arrAttachedFiles);
				foreach ($arrAttachedFiles as $k => $v) {
					$arrAttachedFilesList[$k]['name'] = $v;
					$arrAttachedFilesList[$k]['at.url'] = $arrTplVars['cfgFilesUrl'].'news_attachments/'.$intId.'/'.$v;
				}
			}
		}

		/**
		 * Обновим URL новости, если её нет
		 */
		if ($flagUpdate && empty($arrNewsInfo['sn_url']) && $intId > 0){
			$strUrl = strtolower( $objUtil->translitBadChars($objUtil->translitKyrToLat(trim($arrNewsInfo['sn_title']))) );
			$strSqlQuery = "UPDATE `site_news` SET `sn_url` = '$strUrl' WHERE `sn_id` = $intId";
			if (!$objDb->query($strSqlQuery)) {
				# sql error
			}
		}
	} else {
		header('Location: '.SITE_URL.'error');
		exit();
	}

	$objTpl->tpl_loop( "page.contents", "list", $arrMoreImages );
	$objTpl->tpl_loop( "page.contents", "attachments", $arrAttachedFilesList );
	$arrIf['novelty'] = true;

} else {
	/**
	 * Список новостей
	 */

	// Всего записей в базе
	$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `site_news` WHERE sn_status = 'Y' AND sn_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
	// Выбрано записей
	$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM `site_news` WHERE sn_status = 'Y' AND sn_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$arrTplVars['intQuantSelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
	// ***** BEGIN: Построение пейджинга для вывода списка
	$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
	/**
	 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
	 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
	 */
	$objPagination->strPaginatorTpl = "site.global.1.tpl";
	$objPagination->iQtyRecsPerPage = 15;
	$objPagination->strColorLinkStyle = "link-b";
	$objPagination->strColorActiveStyle = "tab-bl-b";
	$objPagination->strNameImageBack = "arrow_one_left.gif";
	$objPagination->strNameImageForward = "arrow_one_right.gif";
	// Создаем блок пэйджинга
	$objPagination->paCreate();
	// ***** END: Построение пейджинга для вывода списка
	// Запрос для выборки нужных записей
	$strSqlQuery = "SELECT * FROM `site_news`"
		. " WHERE sn_status = 'Y' AND sn_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')"
		. " ORDER BY sn_date_publ DESC, sn_id DESC ".$objPagination->strSqlLimit;
	$arrLastNews = $objDb->fetchall( $strSqlQuery );
	// кол-во публикаций показанных на странице
	$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrLastNews) ? count($arrLastNews) : 0 );
	// Присвоение значения пэйджинга для списка
	$arrTplVars['blockPaginator'] = $objPagination->paShow();

	if ( is_array($arrLastNews) ) {
		foreach ( $arrLastNews as $key => $value ) {
			$arrLastNews[$key]['strDatePublicNews'] = date('d.m.y', strtotime($value['sn_date_publ']));
			$arrIf['news.picture.'.$value['sn_id']] = ( file_exists(DROOT."storage/images/news/news.photo.".$value['sn_id'].".jpg") && $value['sn_image'] == 'Y');
			$arrLastNews[$key]['strUniquePhoto'] = $arrIf['news.picture.'.$value['sn_id']] ? '?pic='.time() : '';
			$arrLastNews[$key]['urlParam'] = (!empty($value['sn_url']) ? ((!empty($value['sn_date_publ']) ? $value['sn_date_publ'].'/' : '').$value['sn_url']) : $value['sn_id'] );
		}
	}

	$objTpl->tpl_loop("page.contents", "last.news", $arrLastNews);
	$objTpl->tpl_loop("page.contents", "last.news.js", $arrLastNews);

	$arrIf['news.list'] = true;

	// Get video news
	$userName = 'Petrostroybaza';
	try {
		$yt = new Zend_Gdata_YouTube();
		$videoFeedCount = $yt->getUserUploads($userName)->count();
        $arrTplVars['totalVideos'] = $videoFeedCount;
	} catch (Exception $ex) {
		//echo $ex->getTraceAsString();
	}

	$arrTplVars['vPage'] = (!empty($_GET['vPage']) ? intval($_GET['vPage']) : 1);
	$intRecordsPerPage = 4;

	// ***** BEGIN: Построение пейджинга для вывода списка
	$objPaginVideo = new tplPaginator($videoFeedCount, SITE_TPL_TPL_DIR, 'vPage');
	$objPaginVideo->strPaginatorTpl = "site.global.1.tpl";
	$objPaginVideo->iQtyRecsPerPage = $intRecordsPerPage;
	$objPaginVideo->strColorLinkStyle = "link-b";
	$objPaginVideo->strColorActiveStyle = "tab-bl-b";
	$objPaginVideo->strNameImageBack = "arrow_one_left.gif";
	$objPaginVideo->strNameImageForward = "arrow_one_right.gif";
	$objPaginVideo->paCreate();
	$arrTplVars['videoPaginator'] = $objPaginVideo->paShow();
	// ***** END: Построение пейджинга для вывода списка

	try {
		$query = $yt->newVideoQuery();
		$query->setStartIndex($objPaginVideo->intRecordStart);
		$query->setMaxResults($intRecordsPerPage);
		$query->setOrderBy('updated');
		$query->setAuthor($userName);

		$videoFeed = $yt->getVideoFeed($query);
	} catch (Exception $ex) {
//		echo $ex->getTraceAsString();
	}

	$count = 1;
	$arrVideos = array();
	foreach ($videoFeed as $videoEntry) {
		$arrVideos[$count]['thId'] = $count;
		$arrVideos[$count]['strVideoID'] = $videoEntry->getVideoId();
//		$arrVideos[$count]['strVideoTitle'] = iconv('UTF-8','CP1251', $videoEntry->getVideoTitle());
		$arrVideos[$count]['strVideoTitle'] = $videoEntry->getVideoTitle();
//		$arrVideos[$count]['strVideoDescr'] = iconv('UTF-8','CP1251', $videoEntry->getVideoDescription());
		$arrVideos[$count]['strVideoDescr'] = $videoEntry->getVideoDescription();
		$arrVideos[$count]['strVideoDescrCutted'] = $objUtil->substrText($arrVideos[$count]['strVideoDescr'], 100, true);
		$arrVideos[$count]['strVideoDate'] = date('d.m.Y', strtotime($videoEntry->getPublished()->getText()));
		$arrVideos[$count]['urlFlash'] = $videoEntry->getFlashPlayerUrl();
		$videoThumbnails /* array */ = $videoEntry->getVideoThumbnails();

		foreach($videoThumbnails as $videoThumbnail) {
			#echo '<div id="ytapiplayer'.$player_id.'"></div>';
			#echo "<img id='th$player_id' src=\"" . $videoThumbnail['url'] . "\" width='200' onclick='videoInit( \"$fp_url\", $player_id )' />\n";
			$arrVideos[$count]['urlVideoThumb'] = $videoThumbnail['url'];
			break; // Show only first thumb
		}

		// printVideoEntry($videoEntry, $count);
		$count++;
	}

	$objTpl->tpl_loop("page.contents", "last.videos", $arrVideos);
}

$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
