<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Главная страница сайта (обложка) [index] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "index.1.tpl");

include_once 'Zend/Loader.php'; // the Zend dir must be in your include_path
try {
	Zend_Loader::loadClass('Zend_Gdata_YouTube');
} catch (Exception $e) {
	//echo $e->getMessage();
	//echo "<hr />\n";
}

/**
 * Показать главную новость
 */
$strSqlQuery = "SELECT * FROM `site_news` WHERE `sn_main`='Y' ORDER BY sn_id DESC LIMIT 1";
$arrMainNews = $objDb->fetch($strSqlQuery);
$arrTplVars['strMainTitle'] = $arrMainNews['sn_title'];
$arrTplVars['strMainText'] = $objUtil->substrText( $arrMainNews['sn_body'], 600 );
$arrTplVars['strMainDate'] = $objUtil->workDate(5, $arrMainNews['sn_date_publ']); // date("d.m.Y", strtotime($arrMainNews['sn_date_publ']));
$arrTplVars['strMainImg'] = $arrMainNews['sn_title'];
$arrTplVars['intMainImgId'] = $arrMainNews['sn_id'];
$arrTplVars['mainUrlParam'] = (!empty($arrMainNews['sn_url']) ? ((!empty($arrMainNews['sn_date_publ']) ? $arrMainNews['sn_date_publ'].'/' : '').$arrMainNews['sn_url']) : $arrMainNews['sn_id'] );
$arrIf['is.main.news'] = !empty($arrMainNews['sn_id']);
$arrIf['main.image'] = $arrMainNews['sn_image'] == 'Y';


/**
 * Показать список новостей на главной странице (свежие N штук)
 */
$countNews = 33;
$arrTplVars['nPage'] = (!empty($_GET['nPage']) ? intval($_GET['nPage']) : 1);
// Всего записей в базе
$arrTplVars['intQuantAllRecords'] =
// Выбрано записей
$arrTplVars['intQuantSelectRecords'] = $countNews;


// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR, 'nPage');
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 10;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка

$strSqlQuery = "SELECT * FROM `site_news` WHERE sn_status='Y' AND sn_main='N' ORDER BY sn_date_publ DESC, sn_id DESC ".$objPagination->strSqlLimit;
$arrLastNews = $objDb->fetchall( $strSqlQuery );

// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrLastNews) ? count($arrLastNews) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['newsPaginator'] = $objPagination->paShow();

if ( is_array($arrLastNews) ) {
	foreach ( $arrLastNews as $key => $value ) {
		$arrLastNews[$key]['strDatePublicNews'] = date('d.m.y', strtotime($value['sn_date_publ']));
		$arrIf['news.picture.'.$value['sn_id']] = ( file_exists(DROOT."storage/images/news/news.photo.".$value['sn_id'].".jpg") && $value['sn_image'] == 'Y');
		$arrLastNews[$key]['strUniquePhoto'] = $arrIf['news.picture.'.$value['sn_id']] ? '?pic='.time() : '';
		$arrLastNews[$key]['urlParam'] = (!empty($value['sn_url']) ? ((!empty($value['sn_date_publ']) ? $value['sn_date_publ'].'/' : '').$value['sn_url']) : $value['sn_id'] );
	}
}
$objTpl->tpl_loop("page.contents", "last.news", $arrLastNews);
$objTpl->tpl_loop("page.contents", "last.news.js", $arrLastNews);
unset($objPagination, $arrTplVars['intQuantAllRecords'], $arrTplVars['intQuantSelectRecords'], $arrTplVars['intQuantShowRecOnPage']);
// ===============

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);
$intRecordsPerPage = 4;

// Get video news
$userName = 'Petrostroybaza';

try {
	$yt = new Zend_Gdata_YouTube();
	$videoFeedCount = $yt->getUserUploads($userName)->count();
} catch (Exception $e) {
//	echo $e->getMessage();
//	echo "<hr />\n";
}

// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($videoFeedCount, SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = $intRecordsPerPage;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

try {
	$query = $yt->newVideoQuery();
	$query->setStartIndex($objPagination->intRecordStart);
	$query->setMaxResults($intRecordsPerPage);
	$query->setOrderBy('updated');
	$query->setAuthor($userName);

	$videoFeed = $yt->getVideoFeed($query);
} catch(Exception $e) {
//	echo $e->getMessage();
//	echo "<hr />\n";
}

#$videoFeed = $yt->getUserUploads($userName);
$count = 1;
$arrVideos = array();
if (isset($videoFeed) && !empty($videoFeed))
foreach ($videoFeed as $videoEntry) {

	# $videoEntry->getVideoId() - Идентификатор видео

	$arrVideos[$count]['thId'] = $count;
	$arrVideos[$count]['strVideoID'] = $videoEntry->getVideoId();
//	$arrVideos[$count]['strVideoTitle'] = iconv('UTF-8','CP1251', $videoEntry->getVideoTitle());
	$arrVideos[$count]['strVideoTitle'] = $videoEntry->getVideoTitle();
//	$arrVideos[$count]['strVideoDescr'] = iconv('UTF-8','CP1251', $videoEntry->getVideoDescription());
	$arrVideos[$count]['strVideoDescr'] = $videoEntry->getVideoDescription();
	$arrVideos[$count]['strVideoDescrCutted'] = $objUtil->substrText($arrVideos[$count]['strVideoDescr'], 120, true);
	$arrVideos[$count]['strVideoDate'] = date('d.m.Y', strtotime($videoEntry->getPublished()->getText()));
	$arrVideos[$count]['urlFlash'] = $videoEntry->getFlashPlayerUrl();
	$videoThumbnails /* array */ = $videoEntry->getVideoThumbnails();

	foreach($videoThumbnails as $videoThumbnail) {
		#echo '<div id="ytapiplayer'.$player_id.'"></div>';
		#echo "<img id='th$player_id' src=\"" . $videoThumbnail['url'] . "\" width='200' onclick='videoInit( \"$fp_url\", $player_id )' />\n";
		$arrVideos[$count]['urlVideoThumb'] = $videoThumbnail['url'];
		break; // Show only first thumb
	}

	// printVideoEntry($videoEntry, $count);
	$count++;
}

$objTpl->tpl_loop("page.contents", "last.videos", $arrVideos);

$arrIf['version.dev'] = ($_GET['version']=='dev' || $_SERVER['REMOTE_ADDR']=='127.0.0.1');

$objTpl->tpl_if("page.contents", $arrIf);
$objTpl->tpl_array("page.contents", $arrTplVars);
