<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Форма добавления объявления [add.ad] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "add.ad.31.tpl");


if (isset($_GET['errMess']) && $_GET['errMess'] == 'msgOk') {
	$arrIf['success'] = true;
	$arrTplVars['msgOk'] = "Данные успешно сохранены";
}

/**
 * Запись нового тендера от пользователя
 */
if ( isset($_POST) && !empty($_POST) && isset($_GET['new']) && $_GET['new'] == 'record' ) {
	$arrSqlData['intChapterId'] = intval(trim($_POST['intChapterId']));
	$arrTplVars['intChapterId'] = ( $arrSqlData['intChapterId'] > 0 ? $arrSqlData['intChapterId'] : '');

	if (empty($arrSqlData['intChapterId']) ) {
		$arrIf['error'] = true;
		$arrTplVars['errMsg'] .= "<li> Не указан раздел";
	}

	$arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
	$arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

	if (empty($arrSqlData['strTitle']) ) {
		$arrIf['error'] = true;
		$arrTplVars['errMsg'] .= "<li> Заголовок объявления отсутствует";
	}

	$arrSqlData['strAd'] = addslashes(trim($_POST['strAd']));
	$arrTplVars['strAd'] = htmlspecialchars(stripslashes(trim($_POST['strAd'])));

	if (empty($arrSqlData['strAd']) ) {
		$arrIf['error'] = true;
		$arrTplVars['errMsg'] .= "<li> Текст объявления отсутствует";
	}

	$arrSqlData['strContacts'] = addslashes(trim($_POST['strContacts']));
	$arrTplVars['strContacts'] = htmlspecialchars(stripslashes(trim($_POST['strContacts'])));

	if (empty($arrSqlData['strContacts']) ) {
		$arrIf['error'] = true;
		$arrTplVars['errMsg'] .= "<li> Контактное лицо не указано";
	}

	if ( !$arrIf['error'] ) {
		$strSqlFields = ""
			. " paa_name = '{$arrSqlData['strTitle']}'"
			. ", paa_chapter_id = '{$arrSqlData['intChapterId']}'"
			. ", paa_contacts = '{$arrSqlData['strContacts']}'"
			. ", paa_desc = '{$arrSqlData['strAd']}'"
		;
		$strSqlQuery = "INSERT INTO `psb_ads_ads` SET $strSqlFields, paa_date_create = NOW()";
		if ( !$objDb->query( $strSqlQuery ) ) {
			$arrIf['error'] = true;
			$arrTplVars['errMsg'] .= "<li> Ошибка базы данных";
		} else {
			header('location: /'.$arrReqUri[0]."?errMess=msgOk");
			exit;
		}
	}
}

$arrIf['post.data'] = !$arrIf['success'];

$strSqlQuery = "SELECT * FROM `psb_ads_ads_chapters` WHERE paac_status = 'Y' ORDER BY paac_id";
$arrAdsChapters = $objDb->fetchall( $strSqlQuery );

if ( is_array($arrAdsChapters) ) {
	foreach ( $arrAdsChapters as $key => $value ) {
		$arrAdsChapters[$key]['sel'] = $value['paac_id'] == $arrTplVars['intChapterId'] ? ' selected' : '';
	}
}

$objTpl->tpl_loop("page.contents", "arrAdsChapters", $arrAdsChapters);
$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
