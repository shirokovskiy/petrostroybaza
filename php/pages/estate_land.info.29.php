<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Карточка объявления раздела Недвижимость и Участки [estate_land.info] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "estate_land.info.29.tpl");
$arrTplVars['urlLinkBack'] = $_SERVER['HTTP_REFERER'];

$intRecordId = intval($arrReqUri[1]);

if (!empty($intRecordId)) {
  $intRecordId = $arrTplVars['intRecordId'] = intval($intRecordId);

  $strSqlQuery = "SELECT * FROM ".$_db_tables['stAdsEstateLands']
  ." LEFT JOIN ".$_db_tables["stEstateAdsChapters"]." ON (paec_id = pael_chapter_id)"
  ." WHERE pael_id ='$intRecordId' AND pael_status = 'Y'";
  $arrInfo = $objDb->fetch( $strSqlQuery );

  if ( is_array( $arrInfo )) {
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['pael_name']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes($arrInfo['pael_address']));
    $arrTplVars['strContacts'] = htmlspecialchars(stripslashes($arrInfo['pael_contacts']));
    $arrTplVars['strOrgName'] = htmlspecialchars(stripslashes($arrInfo['pael_orgname']));
    $arrTplVars['strContacts'] = htmlspecialchars(stripslashes($arrInfo['pael_contact']));
    $arrTplVars['strChapter'] = htmlspecialchars(stripslashes($arrInfo['paec_name']));
    $arrTplVars['strInfo'] = htmlspecialchars(stripslashes($arrInfo['pael_info']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes($arrInfo['pael_phone']));

    $arrTplVars['cbxStatus'] = $arrInfo['pael_status'] == 'Y' ? ' checked' : '';
  } else {
    $arrIf['no.estate_land.info'] = true;
  }
} else {
  $arrIf['no.estate_land.info'] = true;
}

$arrIf['is.estate_land.info'] = !$arrIf['no.estate_land.info'];

$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
