<?php
/** Created by WMBM(c) 2007 (Shirokovskiy D. aka Jimmy™).
 * Страница: Объявления раздела Анонсы мероприятий [anonses] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.contents", "anonses.24.tpl");
/**
 * Выборка списка тендеров
 */
// Всего записей в базе
//$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stAnonses']." WHERE pam_status = 'Y'";
//$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantSelectRecords FROM ".$_db_tables['stAnonses']." WHERE pam_status = 'Y'";
$arrTplVars['intQuantSelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantSelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantSelectRecords'], SITE_TPL_TPL_DIR);
/**
* Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
* кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
*/
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->iQtyRecsPerPage = 10;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM ".$_db_tables['stAnonses']." WHERE pam_status = 'Y' ORDER BY pam_id DESC ".$objPagination->strSqlLimit;
$arrAnonses = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantShowRecOnPage'] = ( !empty($arrAnonses) ? count($arrAnonses) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrAnonses) ) {
//  foreach ( $arrAnonses as $key => $value ) {
//  }
} else {
  $arrIf['no.anonses'] = true;
}

$arrIf['is.anonses'] = !$arrIf['no.anonses'] && !empty($arrAnonses);

$objTpl->tpl_loop("page.contents", "anonses", $arrAnonses);
$objTpl->tpl_array("page.contents", $arrTplVars);
$objTpl->tpl_if("page.contents", $arrIf);
