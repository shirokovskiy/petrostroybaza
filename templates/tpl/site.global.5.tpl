<!-- START: Информация о показанных записях, кол-ве записей, постраничный вывод -->
<table cellpadding="1" cellspacing="1" width="98%">
<tr>
	<td><span>Всего записей : <b>{intQuantAllRecords}</b>&nbsp;|&nbsp;Выбрано записей : <b>{intQuantSelectRecords}</b>&nbsp;|&nbsp;Показано записей: <b>{intQuantShowRecOnPage}</b></span></td>
	<td align="right">

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td><span>Страницы:</span></td>
<if pa.prev.arrow.pages>
    <td><a href="{strLinkBack}"><img src="{strAdminImg}{strNameImageBack}" border="0" alt="Назад" hspace="2" vspace="3"></a></td>
</if pa.prev.arrow.pages>
<loop pa.lst.pages>
    <td width="12" align="center">&nbsp;{strLink}&nbsp;</td>
</loop pa.lst.pages>
<if pa.next.arrow.pages>
    <td align="center"><a href="{strLinkForward}"><img src="{strAdminImg}{strNameImageForward}" border="0" alt="Далее" hspace="2" vspace="3"></a></td>
</if pa.next.arrow.pages>
  </tr>
  </table>

  </td>
</tr>
</table>
<!-- END: Информация о показанных записях, кол-ве записей, постраничный вывод -->
