<!-- START: Информация о показанных записях, кол-ве записей, постраничный вывод -->
<table cellpadding="1" cellspacing="1" class="paginator">
<tr>
	<td>&nbsp;</td>
	<td align="right">

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td><span class="pagin-title">Страницы:</span></td>
<if pa.prev.arrow.pages>
    <td><a href="{strLinkBack}"><img src="{strAdminImg}{strNameImageBack}" border="0" alt="Назад" hspace="2" vspace="3"></a></td>
</if pa.prev.arrow.pages>
<loop pa.lst.pages>
    <td width="12" align="center" class="pagin-link">&nbsp;{strLink}&nbsp;</td>
</loop pa.lst.pages>
<if pa.next.arrow.pages>
    <td align="center"><a href="{strLinkForward}"><img src="{strAdminImg}{strNameImageForward}" border="0" alt="Далее" hspace="2" vspace="3"></a></td>
</if pa.next.arrow.pages>
  </tr>
  </table>

  </td>
</tr>
</table>
<!-- END: Информация о показанных записях, кол-ве записей, постраничный вывод -->
