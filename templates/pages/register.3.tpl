<div class="cnt">

<div style="padding:5px;">
<h1 class="title">Форма регистрации</h1>

<if reg.success>
<p style="margin:0"><span class="success">Ваша регистрация прошла успешно<br>
На указанный Email выслано письмо для подтверждения регистрации. Если Ваша компания является подписчиком журнала "Строительный еженедельник "ПЕТРОСТРОЙ", Вам будет открыт доступ.</span></p>

<script language="JavaScript" type="text/javascript">
function goToMain() {
  window.location.href = "{cfgUrlSite}register";
}

function doRefresh() {
  setTimeout( 'goToMain()' , 3500 );
}

doRefresh();
</script>
</if reg.success>

<if send.reg>
<p style="margin:7px 0 0 0;">Для полного доступа к базе строительных объектов сайта «ПетроСтройБаза» Вам необходимо заполнить форму.<br>
Пожалуйста, заполните все поля, отмеченые знаком «<font color="red">*</font>» </p>
</div>

<if error>
<p style="margin:0">{errMsg}</p>
</if error>

<form action="{cfgUrlSite}register" method="POST">
<input type="hidden" name="frmRegister" value="true"/>
<table cellPadding="0" cellSpacing="0" class="t-format-1">
  <tr>
    <td width="140">
      Фамилия <font color="red">*</font>
    </td>
    <td width="320">
      <input type="text" name="strLName" class="flat" value="{strLName}" maxlength=80 style="width:300px">
    </td>
  </tr>
  <tr>
    <td>
      Имя <font color="red">*</font>
    </td>
    <td>
      <input type="text" name="strFName" class="flat" value="{strFName}" maxlength=80 style="width:200px">
    </td>
  </tr>
  <tr>
    <td>
      Отчество
    </td>
    <td>
      <input type="text" name="strMName" class="flat" value="{strMName}" maxlength=80 style="width:200px">
    </td>
  </tr>
  <tr>
    <td>
      Компания <font color="red">*</font>
    </td>
    <td>
      <input type="text" name="strCompanyName" class="flat" value="{strCompanyName}" maxlength=80 style="width:300px">
    </td>
  </tr>
  <!--
  <tr>
    <td>
      Должность
    </td>
    <td>
      <input type="text" name="strPosition" class="flat" value="{strPosition}" maxlength=255 style="width:200px">
    </td>
  </tr>-->
  <tr>
    <td>
      Телефон <font color="red">*</font>
    </td>
    <td>
      <input type="text" name="strPhone" class="flat" value="{strPhone}" maxlength=80 style="width:200px">
    </td>
  </tr>
  <!--<tr>
    <td>
      Факс
    </td>
    <td>
      <input type="text" name="strPosition" class="flat" value="{strPosition}" maxlength=80 style="width:200px">
    </td>
  </tr>-->
  <tr>
    <td>
      Электронный адрес <font color="red">*</font>
    </td>
    <td>
      <input type="text" name="strEmail" class="flat" value="{strEmail}" maxlength=255 style="width:200px">
    </td>
  </tr>
  <!--<tr>
    <td>
      Юридический адрес
    </td>
    <td>
      <input type="text" name="strPosition" class="flat" value="{strPosition}" maxlength=255 style="width:300px">
    </td>
  </tr>
  <tr>
    <td>
      Фактический адрес
    </td>
    <td>
      <input type="text" name="strPosition" class="flat" value="{strPosition}" maxlength=255 style="width:300px">
    </td>
  </tr>-->
  <tr>
    <td>
      Логин <font color="red">*</font> <br /> <span class="txt-page-ob-desc">(не менее 4 символов)</span>
    </td>
    <td>
      <input type="text" name="strLogin" class="flat" value="{strLogin}" maxlength=64 style="width:140px">
    </td>
  </tr>
  <tr>
    <td>
      Пароль <font color="red">*</font>
      <br /> <span class="txt-page-ob-desc">(не менее 6 символов)</span>
    </td>
    <td>
      <input type="password" name="strPassword1" class="flat" maxlength=64 style="width:140px">
    </td>
  </tr>
  <tr>
    <td>
      Пароль <font color="red">*</font>
      <br /><span class="txt-page-ob-desc">(подтверждение)</span>
    </td>
    <td>
      <input type="password" name="strPassword2" class="flat" maxlength=64 style="width:140px">
    </td>
  </tr>
  <tr>
    <td colspan="2" align="right">
      <input type="submit" name="submit" class="standart" value="Зарегистрировать">
    </td>
  </tr>
</table>
</form>

<div style="padding: 5px">
<span >После регистрации, на Ваш электронный адрес автоматически будет отправлено письмо,
содержащее ссылку, по которой будет необходимо перейти для подтверждения Вашей регистрации.
В противном случае, введённые данные, сохранённые на сайте будут удалены через 7 дней.
Наши менеджеры свяжутся с вами в ближайшее время.</span>
</div>
</if send.reg>

</div>
