<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:0;"><b>ДОБАВИТЬ ТЕНДЕР</b></p>

<div class="cnt">
<if error>
{errMsg}
</if error>
<if success>
<center>
<p><span class="success">{msgOk}</span><br>
<span class="sm-gr">Вся публикуемая Вами информация будет проверена модераторами сайта, прежде чем появится в списках объявлений!</span></p>
<p><a href="{cfgUrlSite}add.tender">Хотите добавить ещё?</a></p>
</center>
</if success>

<if post.data>
<form action="?new=record" method="POST">
<table cellPadding="5" cellSpacing="0" width="100%" style="border-top:1px solid #F9C36D;" class="vtop">
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Название объекта</td>
    <td class="tdR1">
      <input type="text" name="strTenderName" class="flat" value="{strTenderName}" maxlength=80 style="width:300px">
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Вид работ</td>
    <td class="tdR1">
      <input type="text" name="strVidRabot" class="flat" value="{strVidRabot}" maxlength=255 style="width:400px">
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Адрес объекта</td>
    <td class="tdR1">
      <textarea name="strAddress" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;" class="flat">{strAddress}</textarea>
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Заказчик</td>
    <td class="tdR1">
      <textarea name="strCustomer" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;" class="flat">{strCustomer}</textarea>
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Координаты</td>
    <td class="tdR1">
      <textarea name="strCoordinates" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;" class="flat">{strCoordinates}</textarea>
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Контактное лицо</td>
    <td class="tdR1">
      <textarea name="strContacts" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;" class="flat">{strContacts}</textarea>
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Полезная информация</td>
    <td class="tdR1">
      <textarea name="strInfo" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;" class="flat">{strInfo}</textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="right">
      <input type="submit" name="submit" class="standart" value="Сохранить">
    </td>
  </tr>
</table>
</form>
</if post.data>
<p><a href="{cfgUrlSite}tenders">вернуться к списку тендеров</a></p>

</div>