<div id="dropdown-region" class="dropdown-box">
    <div class="selector">
        <ul>
            <li><input type="radio" id="o_all" name="show" value="" {rbcAllChecked} /><label for="o_all"> - показать все объекты</label></li>
            <li><input type="radio" id="o_spb" name="show" value="spb" {rbcSpbChecked} /><label for="o_spb"> - объекты Северо-Запада</label></li>
            <li><input type="radio" id="o_msk" name="show" value="msk" {rbcMskChecked} /><label for="o_msk"> - объекты Московского региона</label></li>
        </ul>
    </div>
    <span class="note">Выберите регион...</span>
    <div class="arrows"></div>
</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#dropdown-region .arrows').toggle(function(){
        // down
        $('#dropdown-region .note').hide();
        $('#dropdown-region').animate({height: 80, width: 250}, 400, function(){
            $('#dropdown-region .arrows').css({'background-image':'url(/storage/siteimg/darr_up.gif)'});
            $('#dropdown-region .selector').css({'display':'block'});
        });
    }, function(){
        // up
        $('#dropdown-region .selector').css({'display':'none'});
        $('#dropdown-region').animate({height: 16, width: 210}, 400, function(){
            $('#dropdown-region .arrows').css({'background-image':'url(/storage/siteimg/darr_down.gif)'});
            $('#dropdown-region .note').show(500);
        });
    });

    $('#dropdown-region input').click(function () {
        location.search = $.param({'reg': $(this).val()}, true);
    });
});
</script>

<div class="cnt">
<h1 class="title">Объекты <if chapter>&gt; {strChapterName} <span class="obj-count">({intQuantSelectRecords})</span></if chapter></h1>
</div>


<if is.objects>
<div class="cnt">{blockPaginator}</div>
<table cellPadding="0" cellSpacing="0" width="100%" class="vtop" style="border-top:2px solid #D2D5E4;">
<loop objects>
    <tr>
        <td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:7px 7px 3px 0;"></td>
        <td style="border-bottom:1px solid #EEEEEE">

            <table class="object-short-info" cellPadding="0" cellSpacing="0">
                <tr>
                    <td valign="top">
                        <if authed><div class="add_to_notebook">
                            <if not.in.notebook.{seo_id}><a href="{strCurrentUri}addtonb={seo_id}"><img src="/storage/siteimg/btn_add_to_blocknote.png" title="Добавить в блокнот"/></a></if not.in.notebook.{seo_id}><ifelse not.in.notebook.{seo_id}><img src="/storage/siteimg/btn_in_blocknote.png" title="В блокноте"/></ifelse not.in.notebook.{seo_id}>
                        </div></if authed>

                        <a href="{cfgUrlSite}objectinfo/{urlParam}?stPage={stPage}&reg={Reg}" class="link-blue"><h1 class="title1">{seo_name}</h1></a>
                        <p class="txt-block">
                            <span class="txt-bold-gray">ID: {seo_id}</span><br />
<if naznach.{seo_id}>
              <span class="txt-bold-gray">Назначение:</span> {seo_naznach}<br />
</if naznach.{seo_id}>
                <span class="txt-bold-gray">Регион:</span> {strRegion}<br>
                <span class="txt-bold-gray">Район:</span> {strAdmReg}<br />
<if access.for.browser>
                <span class="txt-bold-gray">Адрес:</span> {seo_address} <if is.address.{seo_id}><a href="javascript:void(0)" data-address="{strAddressData}" class="maps">адрес на карте</a></if is.address.{seo_id}><br />
</if access.for.browser>
                <span class="txt-bold-gray">Раздел недвижимости:</span> {set_name}<br />
<if etap.stroy.{seo_id}>
                <span class="txt-bold-gray">Дата обновления информации:</span> {seo_etap_date_formated}<br />
                <span class="txt-bold-gray">Описание этапа:</span> {seo_other_etap}<br />
</if etap.stroy.{seo_id}>
            </p>
          </td>
<if object.photo.{seo_id}>
          <td width="110" valign="top" align="left"><a href="{strImagePath}" class="fancybox"><img src="{strImagePath}" width="120" class="grayBrd"></a></td>
</if object.photo.{seo_id}>
        </tr>
      </table>

    </td>
  </tr>
</loop objects>
</table>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        if ($('a.fancybox')) // Show Popup block
            $('a.fancybox').fancybox({
                'autoSize': false
                , 'width': 1200
                , 'autoDimensions': false
                , padding: 3
                , margin: 5
            });
    });
</script>

<div class="cnt">{blockPaginator}</div>
</if is.objects>

<if no.objects>
<p class="err">Нет объектов данного раздела недвижимости</p>
</if no.objects>


<if chapter>
<div style="margin-top:10px">
<a href="{cfgUrlSite}objects" >Все объекты</a>
</div>
</if chapter>
