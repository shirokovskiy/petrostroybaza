<div class="clearer"></div>

<div class="cnt">
    <div id="video">
        <h2>{strVideoTitle}</h2>
        <div id="ytapiplayer1" style="width:680px; min-height:480px; margin: 0 auto">You need Flash player 8+ and JavaScript enabled to view this video.</div>
        <p class="date">{strVideoDate}</p>
    </div>
    <p class="video-descr">{strVideoDescr}</p>
</div>

<ul id="mycarousel" class="jcarousel-skin-tango">
<loop thumbnails>
    <li><a href="{cfgSiteUrl}video/{strVideoID}"><img src="{urlVideoThumb}" width="160" height="120" alt="{strThumbTitle}" title="{strThumbTitle}" /></a>
        <p>{strThumbTitle}</p>
    </li>
</loop thumbnails>
</ul>


<script type="text/javascript">
    var params = { allowScriptAccess: "always" };
    var atts = { id: "myytplayer" };
    var videoW = 640;
    var videoH = 490;

    function videoInit(vid, url){
        swfobject.embedSWF( url + "&enablejsapi=1&playerapiid=" + vid,
                            "ytapiplayer" + vid, videoW, videoH, "8", null, null, params, { id: "myytplayer"+vid });
    }

    function onYouTubePlayerReady(playerId) {
        var ytp = document.getElementById("myytplayer" + playerId);
        ytp.playVideo();
        ytp.addEventListener("onStateChange", "onYtpStateChange");
    }

    function onYtpStateChange(newState) {
        // If video is stop
        if (newState==0){
            // alert("Player's new state: " + newState);
            console.log("Player's new state: " + newState);
        }
    }

    function mycarousel_initCallback(carousel)
    {
        // Disable autoscrolling if the user clicks the prev or next button.
        carousel.buttonNext.bind('click', function() {
            carousel.startAuto(0);
        });

        carousel.buttonPrev.bind('click', function() {
            carousel.startAuto(0);
        });

        // Pause autoscrolling if the user moves with the cursor over the clip.
        carousel.clip.hover(function() {
            carousel.stopAuto();
        }, function() {
            carousel.startAuto();
        });
    };

    $(document).ready(function() {
        videoInit(1, '{urlFlash}');

        jQuery('#mycarousel').jcarousel({
            // Configuration goes here
            //scroll: 1
            auto: 10,
            wrap: 'circular',
            initCallback: mycarousel_initCallback
        });
    });


</script>