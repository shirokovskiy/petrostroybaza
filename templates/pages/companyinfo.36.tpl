<div class="cnt">
    <h1 class="txt-bb info-title">Информация о компании: <span class="title2">{strCompanyName}</span></h1>

    <table cellPadding="5" cellSpacing="0" width="100%" style="margin-top:20px;border-top:1px solid #F9C36D;" class="vtop">
        <tr>
            <td class="tdL1" width="25%" bgcolor="#EFEFEF">ID компании</td>
            <td class="tdR1"><b>{intCompanyID}</b> &nbsp;</td>
        </tr>
        <tr>
            <td class="tdL1" width="25%" bgcolor="#EFEFEF">Наименование компании</td>
            <td class="tdR1"><b style="font-size:14px">{strCompanyName}</b> &nbsp;</td>
        </tr>
        <tr>
            <td class="tdL1" width="25%" bgcolor="#EFEFEF">ИНН</td>
            <td class="tdR1"><b>{strCompanyInn}</b> &nbsp;</td>
        </tr>
        <tr>
            <td class="tdL1" width="25%" bgcolor="#EFEFEF">Тел.</td>
            <td class="tdR1">
                <if access>
                {strContactCompanyName} &nbsp;
                </if access>
                <if no.access>
                    <span class="redtext">нет доступа</span>
                </if no.access>
                <if not.permited>
                    <span class="redtext">нет прав просмотра данной информации</span>
                </if not.permited>
            </td>
        </tr>
        <tr>
            <td class="tdL1" width="25%" bgcolor="#EFEFEF">E-mail</td>
            <td class="tdR1">
                <if access>
                {strContactPosition} &nbsp;
                </if access>
                <if no.access>
                    <span class="redtext">нет доступа</span>
                </if no.access>
                <if not.permited>
                    <span class="redtext">нет прав просмотра данной информации</span>
                </if not.permited>
            </td>
        </tr>
        <tr>
            <td class="tdL1" width="25%" bgcolor="#EFEFEF">Контактные лица, сотрудники</td>
            <td class="tdR1">
                <if access>
                {strCompanyContactInfo} &nbsp;
                </if access>
                <if no.access>
                    <span class="redtext">нет доступа</span>
                </if no.access>
                <if not.permited>
                    <span class="redtext">нет прав просмотра данной информации</span>
                </if not.permited>
            </td>
        </tr>
        <tr>
            <td class="tdL1" width="25%" bgcolor="#EFEFEF">Объекты данной компании</td>
            <td class="tdR1">
                <if access>
                    <loop company.objects>
                        <p style="margin:0 0 3px 0"><a href="{cfgSiteUrl}objectinfo/{urlParam}" class="link-blue">{strObjectTitle}</a>
                            <br />Дата этапа: <i class="date">{strEtapDate}</i>
                            <br />Описание этапа: <i>{strEtapDesc}</i></p>
                    </loop company.objects>
                    <if no.related.objects>
                        <span class="redtext">нет связаных объектов с этой компанией</span>
                    </if no.related.objects>
                </if access>
                <if no.access>
                    <span class="redtext">нет доступа</span>
                </if no.access>
                <if not.permited>
                    <span class="redtext">нет прав просмотра данной информации</span>
                </if not.permited>
            </td>
        </tr>
        <if not.authed>
            <tr>
                <td colspan="2" class="tdL1" bgcolor="FFEFEF">
                    Для того, чтобы просмотреть полную информацию о компании, Вы должны быть <a href="{cfgUrlSite}register" class="link-blue">зарегистрированным</a> пользователем.<br>
                    Если Вы уже зарегистрированы, введите свой логин и пароль в блоке авторизации слева.
                </td>
            </tr>
        </if not.authed>
        <if no.access>
            <tr><td colspan="2" class="tdL1" bgcolor="FFEFEF">Доступ к базе объектов и компаний является <b class="redtext">платной</b> услугой. Свяжитесь с <a href="{cfgSiteUrl}contacts">администрацией сайта</a> для уточнения условий.</td></tr>
        </if no.access>
        <if not.permited>
            <tr><td colspan="2" class="tdL1" bgcolor="FFEFEF">Доступ к базе объектов и компаний является <b class="redtext">платной</b> услугой. Свяжитесь с <a href="{cfgSiteUrl}contacts">администрацией сайта</a> для уточнения условий.</td></tr>
        </if not.permited>
    </table>


    <p>
        <a href="{urlLinkBack}">вернуться к списку компаний</a>
    </p>
</div>
