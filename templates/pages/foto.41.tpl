<div id="pageContent" class="foto-page"><!-- {debug} -->
	<h3 class="title">{sig_name}</h3>

	<div id="box_main_photo">
		<a href="{strImgUrlPath}{si_filename}" title="{si_title}"><img src="{strImgUrlPath}s_m_a_l_l_{si_filename}" alt="{si_title}" title="{si_title}" class="main-photo" /></a>
	</div>
	<div id="box_control_photo" class="clearer">
		<div id="box_arrows">
			<if prev>
			<a href="{strPrev}"><img src="{cfgSiteImg}photo-arrow-left.gif" alt="предыдущее фото" title="предыдущее фото" /></a>
			</if prev>
			<if next>
			<a href="{strNext}"><img src="{cfgSiteImg}photo-arrow-right.gif" alt="следующее фото" title="следующее фото" /></a>
			</if next>
		</div>
		<div id="box_info">
			Чтобы узнать стоимость фотографии и приобрести права на использование данного изображения, свяжитесь с менеджером отдела продаж

			<p>E-mail: <a href="mailto:tm@petrostroybaza.ru">tm@petrostroybaza.ru</a></p>
			<p>Тел. +7 (812) 714-4241</p>
		</div>
	</div>

	<table>
		<tr><td>Заголовок:</td><td><a href="{cfgSiteUrl}fotobank/{intGroupID}" title="{si_title}">{si_title}</a></td></tr>
		<tr><td>Описание:</td><td>{si_desc}</td></tr>
		<tr><td>Дата создания:</td><td>{strDate}</td></tr>
		<tr><td>Фотограф:</td><td>{si_reporter}</td></tr>
		<tr><td></td><td>&nbsp;</td></tr>
		<tr><td>Оригинальный номер:</td><td>{si_id}</td></tr>
		<tr><td>Ключевые слова:</td><td>{si_tags}</td></tr>
        <if object>
        <tr><td class="vtop">Объект:</td><td><a href="{cfgSiteUrl}objectinfo/{strObjectUrl}">{strObjectName}</a><br />{strObjectAdr}</td></tr>
        <tr><td class="vtop">Компании:</td><td>
                <loop contacts>
                    <a href="{cfgSiteUrl}companyinfo/{sec_id}">{sec_company}</a><br />
                </loop contacts>
        </td></tr>
        </if object>
	</table>
</div>

<script type="text/javascript">
//<![CDATA[
jQuery(document).ready(function ($) {
	$('#box_main_photo a').fancybox({'titlePosition': 'over'});
	$('#box_main_photo a').jqzoom({zoomWidth: 270, zoomHeight: 340, preloadText:'Гружу фото', showEffect:'fadein'});
});
//]]>
</script>

<frg fotobaza>
