<div class="cnt">
    <span class="txt-bb" style="margin-left:10px; font-size:14px;">Результаты поиска объектов (найдено {intQuantSelectRecords})<if mapped><a
                    href="/objects-map?filter=map" style="margin-left: 20px" class="map-sign">смотреть на карте</a></if mapped></span>
</div>


<if is.objects>
    <div class="cnt">{blockPaginator}</div>
    <table cellPadding="0" cellSpacing="0" width="100%" class="vtop search-table-list" style="border-top:2px solid #D2D5E4">
        <loop objects>
            <tr>
                <td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:7px 7px 3px 0;"></td>
                <td style="border-bottom:1px solid #EEEEEE">

                    <table cellPadding="0" cellSpacing="0" width="100%" style="margin:3px 0 3px 0">
                        <tr>
                            <td valign="top">
                                <if authed><div class="add_to_notebook">
                                    <if not.in.notebook.{seo_id}><a href="{strCurrentUri}addtonb={seo_id}"><img src="/storage/siteimg/btn_add_to_blocknote.png" title="Добавить в блокнот"/></a></if not.in.notebook.{seo_id}>
                                    <ifelse not.in.notebook.{seo_id}><img src="/storage/siteimg/btn_in_blocknote.png" title="В блокнот"/></ifelse not.in.notebook.{seo_id}>
                                </div></if authed>

                                <a href="{cfgUrlSite}objectinfo/{seo_id}?stPage={stPage}" class="link-blue"><h1 class="title1">{seo_name}</h1></a>
                                <p class="txt-block">
                                    <span class="txt-bold-gray">ID: {seo_id}</span><br>
                                    <if naznach.{seo_id}>
                                    <span class="txt-bold-gray">Назначение:</span> {seo_naznach}<br>
                                    </if naznach.{seo_id}>
                                    <span class="txt-bold-gray">Регион:</span> {strRegion}<br>
                                    <span class="txt-bold-gray">Район:</span> {strAdmReg}<br>
                                    <if access.for.browser>
                                        <span class="txt-bold-gray">Адрес:</span> {seo_address}<br>
                                    </if access.for.browser>
                                    <span class="txt-bold-gray">Раздел недвижимости:</span> {set_name}<br>
                                    <if etap.stroy.{seo_id}>
                                    <span class="txt-bold-gray">Дата обновления информации:</span> {seo_etap_date_formated}<br>
                                    <span class="txt-bold-gray">Описание этапа:</span> {seo_other_etap}<br>
                                    </if etap.stroy.{seo_id}>
                                </p>
                            </td>
                        <if object.photo.{seo_id}>
                            <td width="110" valign="top" align="left"><a href="{cfgUrlSite}objectinfo/{seo_id}?stPage={stPage}"><img src="{cfgAllImg}objects/{si_filename}" width="120" class="grayBrd"></a></td>
                        </if object.photo.{seo_id}>
                        </tr>
                    </table>
                </td>
            </tr>
        </loop objects>
    </table>

    <div class="cnt">{blockPaginator}</div>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#resetSearch').show();
        });
    </script>
</if is.objects>

<if no.objects>
    <div  style="border-top:2px solid #D2D5E4;">
        <p class="err">Не найдено объектов, удовлетворяющих запросу<br>
            Попробуйте изменить параметры поиска.</p>
        <br>
        <p>Если Вы только начали поиск, выберите и/или введите нужные параметры в форме поиска справа =&gt;</p>
    </div>
</if no.objects>


<if no.query>
    <div style="margin-top:10px">
<span class="err"><b>Внимание!!!</b> Запрос составлен неверно.<br>
Постарайтесь не использовать слова, состоящих менее чем из 2-ух символов.<br>
Чем короче слово, тем дольше обрабатывается запрос поиска.
</span>
    </div>
</if no.query>
