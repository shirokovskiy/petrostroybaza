<h1 class="title">{mapsObjectsTitle}</h1>
<if not.authed>
    <p><span class="redtext">Вы не авторизованы</span></p>
    <p>Введите свой логин и пароль в верхнем правом углу страницы сайта.</p>
    <p>Если у Вас ещё нет логина и пароля, пройдите <a href="/register" class="link-blue">регистрацию</a></p>
</if not.authed>

<if authed>
    <div id="map"></div>
<style type="text/css">
    #map {width: 90%; min-height: 600px; margin:10px auto; border: 1px solid #001A85}
</style>

<script type="text/javascript">
    //
    ymaps.ready(initYMap);

    function initYMap () {
        var myMap = new ymaps.Map('map', {
                        center: [{coordX}, {coordY}],// by default Saint-Petersburg
                        zoom: {mapsObjectsZoom}
                    }),
            objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32
            });

        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        objectManager.objects.options.set('preset', 'islands#redDotIcon');
        objectManager.clusters.options.set('preset', 'islands#redClusterIcons');
        myMap.geoObjects.add(objectManager);

        $.ajax({
            url: "{mapsObjectsFile}?date={strDateRequest}"
        }).done(function(data) {
            objectManager.add(data);
        });
    }
</script>
</if authed>
