<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:5px"><b>ОБЪЯВЛЕНИЕ</b></p>

<div class="cnt">
<if is.estate_land.info>
<table cellPadding="5" cellSpacing="0" width="100%" style="margin-top:20px;border-top:1px solid #F9C36D;" class="vtop">
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Раздел</td>
    <td class="tdR1">{strChapter} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Объект</td>
    <td class="tdR1"><b>{strTitle}</b> &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Адрес</td>
    <td class="tdR1">{strAddress} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Организация</td>
    <td class="tdR1">{strOrgName} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Контактное лицо</td>
    <td class="tdR1">{strContacts} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Телефон</td>
    <td class="tdR1">{strPhone} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Полезная информация</td>
    <td class="tdR1">{strInfo} &nbsp;</td>
  </tr>
</table>
</if is.estate_land.info>

<if no.estate_land.info>
<p class="err">Нет информации для просмотра</p>
</if no.estate_land.info>

<p><a href="{cfgUrlSite}add.estate_land">добавить новое объявление</a></p>
<p><a href="{urlLinkBack}">вернуться к списку объявлений</a></p>

</div>