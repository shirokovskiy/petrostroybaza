<div class="cnt">
    <h1 class="title">Мой блокнот{strQuantityAllObjects}</h1>

    <p>Блокнот содержит список объектов, добавленных ранее в разделах недвижимости.<if mapped><a
                    href="/objects-map?filter=map-nb" style="margin-left: 20px" class="map-sign">Все объекты блокнота на карте</a></if mapped></p>

    <if authed>
        <if is.objects>
            {blockPaginator}

            <table cellPadding="0" cellSpacing="0" width="100%" class="vtop" style="border-top:2px solid #D2D5E4">
                <loop objects>
                    <tr>
                        <td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:7px 7px 3px 0;"></td>
                        <td style="border-bottom:1px solid #EEEEEE">

                            <table class="object-short-info" cellPadding="0" cellSpacing="0">
                                <tr>
                                    <td valign="top">
                                        <div class="add_to_notebook">
                                            <a href="javascript:void(0)" class="tip editNote" obj-id="{seo_id}" title="Заметка про {seo_name}"><img src="/storage/siteimg/btn_edit_note.png" alt="Редактировать заметку про объект" /></a>
                                            <a href="javascript:void(0)" class="tip" onclick="confirmAction('{strCurrentUri}rmfromnb={seo_id}', 'Заметка и объект будут удалены из блокнота?')" title="Удалить из блокнота"><img src="/storage/siteimg/btn_remove_from_blocknote.jpg" alt="Удалить из блокнота" /></a>
                                        </div>
                                        <a href="{cfgUrlSite}objectinfo/{urlParam}?stPage={stPage}&reg={Reg}" class="link-blue"><h1 class="title1">{seo_name}</h1></a>
                                        <p class="txt-block">
                                            <span class="txt-bold-gray">ID: {seo_id}</span><br />
                                            <if naznach.{seo_id}>
                                                <span class="txt-bold-gray">Назначение:</span> {seo_naznach}<br />
                                            </if naznach.{seo_id}>
                                            <span class="txt-bold-gray">Регион:</span> {strRegion}<br>
                                            <span class="txt-bold-gray">Район:</span> {strAdmReg}<br />
                                            <if access.for.browser>
                                                <span class="txt-bold-gray">Адрес:</span> {seo_address}<br />
                                            </if access.for.browser>
                                            <span class="txt-bold-gray">Раздел недвижимости:</span> {set_name}<br />
                                            <if etap.stroy.{seo_id}>
                                                <span class="txt-bold-gray">Дата обновления информации:</span> {seo_etap_date_formated}<br />
                                                <span class="txt-bold-gray">Описание этапа:</span> {seo_other_etap}<br />
                                            </if etap.stroy.{seo_id}>
                                            <if my.note.{seo_id}>
                                                <span class="txt-bb">Моя заметка:</span> <span id="note{seo_id}">{strNote}</span><br />
                                            </if my.note.{seo_id}>
                                        </p>
                                    </td>
                                <if object.photo.{seo_id}>
                                    <td width="110" valign="top" align="left"><a href="{cfgUrlSite}objectinfo/{urlParam}?stPage={stPage}&reg={Reg}"><img src="{strImagePath}" width="120" class="grayBrd"></a></td>
                                </if object.photo.{seo_id}>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </loop objects>
            </table>

            <div style="display: none">
                <a href="#popup_note_form" class="fancybox"></a>
                <div id="popup_note_form">
                    <form action="{strCurrentUri}form=note" method="post">
                        <input type="hidden" name="intNoteObjectID" id="intNoteObjectID" value=""/>
                        <textarea id="strNote" name="strNote" style="word-wrap:break-word" class="plane"></textarea>
                        <p><input type="submit" value="сохранить" class="standart"/></p>
                    </form>
                </div>
            </div>

        </if is.objects>

        <if no.objects>
            <p class="err">Нет объектов добавленных в блокнот</p>
        </if no.objects>
    </if authed>

    <if not.authed>
        <p><span class="redtext">Вы не авторизованы</span></p>
        <p>Введите свой логин и пароль в верхнем правом углу страницы сайта.</p>
        <p>Если у Вас ещё нет логина и пароля, пройдите <a href="/register" class="link-blue">регистрацию</a></p>
    </if not.authed>
</div>

<style type="text/css">
    #popup_note_form {width: 400px; height: 240px; padding: 20px}
    #popup_note_form textarea {width: 380px; height: 200px; padding: 0; margin: 0}
</style>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('a.fancybox').fancybox({
            'titleShow': false
        });

        $('.add_to_notebook a.tip.editNote').click(function () {
            if ($(this).attr('obj-id') > 0) {
                var objID = $(this).attr('obj-id');
                $('#intNoteObjectID').val( objID );
                $('#strNote').empty().text( $('#note'+objID).text() );
                $('.fancybox').trigger('click');
            } else {
                $.fancybox('Ошибка! Не смогу Вам помочь. Я сломался.');
            }
        });
    });
</script>
