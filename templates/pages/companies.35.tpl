<div id="dropdown-region" class="dropdown-box">
    <div class="selector">
        <ul>
            <li><input type="radio" id="o_all" name="show" value="" {rbcAllChecked} /><label for="o_all"> - показать все компании</label></li>
            <li><input type="radio" id="o_spb" name="show" value="spb" {rbcSpbChecked} /><label for="o_spb"> - компании Северо-Запада</label></li>
            <li><input type="radio" id="o_msk" name="show" value="msk" {rbcMskChecked} /><label for="o_msk"> - компании Московского региона</label></li>
        </ul>
    </div>
    <span class="note">Выберите регион...</span>
    <div class="arrows"></div>
</div>

<div id="dropdown-est-type" class="dropdown-box">
    <div class="selector">
        <ul>
<loop list.est.types>
            <li><input type="radio" id="o_type{set_id}" name="type" value="{set_id}"{ch{set_id}}/><label for="o_type{set_id}"> - {set_name}</label></li>
</loop list.est.types>
        </ul>
    </div>
    <span class="note">Выберите тип недвижимости...</span>
    <div class="arrows"></div>
</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#dropdown-region .arrows').toggle(function(){
        // down
        $('#dropdown-region .note').hide();
        $('#dropdown-region').animate({height: 80, width: 240}, 400, function(){
            $('#dropdown-region .arrows').css({'background-image':'url(/storage/siteimg/darr_up.gif)'});
            $('#dropdown-region .selector').css({'display':'block'});
        });
    }, function(){
        // up
        $('#dropdown-region .selector').css({'display':'none'});
        $('#dropdown-region').animate({height: 16, width: 150}, 400, function(){
            $('#dropdown-region .arrows').css({'background-image':'url(/storage/siteimg/darr_down.gif)'});
            $('#dropdown-region .note').show(500);
        });
    });

    /*$('#o_all').click(function(){
        window.location.href = location.pathname;
    });

    $('#o_spb').click(function(){
        window.location.href = location.pathname + '?reg=spb';
    });

    $('#o_msk').click(function(){
        window.location.href = location.pathname + '?reg=msk';
    });*/

    $('#dropdown-region input').click(function () {
        var value = $(this).val();
        insertParam('reg', value);
    });

    $('#dropdown-est-type .arrows').toggle(function(){
        // down
        $('#dropdown-est-type .note').hide();
        $('#dropdown-est-type').animate({height: 250, width:328}, 400, function(){
            $('#dropdown-est-type .arrows').css({'background-image':'url(/storage/siteimg/darr_up.gif)'});
            $('#dropdown-est-type .selector').css({'display':'block'});
        });
    }, function(){
        // up
        $('#dropdown-est-type .selector').css({'display':'none'});
        $('#dropdown-est-type').animate({height: 16, width:210}, 400, function(){
            $('#dropdown-est-type .arrows').css({'background-image':'url(/storage/siteimg/darr_down.gif)'});
            $('#dropdown-est-type .note').show(500);
        });
    });

    var intervals = 0;
    var timer = 0;
    var state = true;
    var period = 250;

    intervals = setInterval(function(){
        if (timer > 4) {
            clearInterval(intervals);
            intervals = null;
        }
        timer++;

        if ( state ) {
            $( ".dropdown-box" ).animate({
                backgroundColor: "#F87E47"
            }, period );
        } else {
            $( ".dropdown-box" ).animate({
                backgroundColor: "#D2D5E4"
            }, period );
        }
        state = !state;
    }, period);

    $('#dropdown-est-type input').click(function () {
        var value = $(this).val();
        insertParam('type', value);
    });
});
</script>

<div class="cnt breadcrumbs">
    <a href="{cfgSiteUrl}companies">Компании</a> <if chapter><span class="txt-bb">&gt; {strChapterName}</span></if chapter> <span class="obj-count">({intQuantSelectRecords})</span>
</div>

<form method="POST">
    <div style="padding:3px;width:570px">
        <div class="fl" style="padding:3px 5px 0 0;font-size:13px">Фильтр по наименованию компании: </div>
        <div class="fl">
            <input type="text" id="strCompanyName" name="strCompanyName" class="flat search-field" value="{strCompanyName}" maxlength=255 />
        </div>
        <div class="fl" style="padding-top:1px; padding-left: 3px">
            <input type="submit" id="submit" name="submit" class="standart finger" value="Искать" style="height:22px" />
        </div>
        <div style="clear:both;"></div>
    </div>
</form>

<if is.companies>
<div class="cnt">{blockPaginator}</div>
<table cellPadding="0" cellSpacing="0" width="100%" class="vtop" style="border-top:2px solid #D2D5E4;">
<loop companies>
    <tr>
        <td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:7px 7px 3px 0"></td>
        <td style="border-bottom:1px solid #EEEEEE;padding:5px 0 5px 1px">
            <a href="{cfgSiteUrl}companyinfo/{sec_id}?stPage={stPage}" class="link-blue"><span class="title1">{strCompanyName}</span></a> <if show.company.types><a href="{cfgSiteUrl}companies/{socl_relation}" class="link-dy chapter_type">{strCompanyChapterName}</a></if show.company.types>
        </td>
    </tr>
</loop companies>
</table>

<div class="cnt">{blockPaginator}</div>
</if is.companies>

<if no.companies>
<p class="err">Компании не найдены</p>
</if no.companies>
