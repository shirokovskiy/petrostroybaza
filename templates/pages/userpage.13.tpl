<p style="MARGIN: 10px 5px 10px 0px"><strong>Информация пользователя</strong></p>
<table cellspacing="0" cellpadding="0" class="t-format-1">
  <tbody>
    <tr>
      <td width="140">Фамилия :</td>
      <td width="320">{strUserLastname}</td>
    </tr>
    <tr>
      <td>Имя :</td>
      <td>{strUserFirstname}</td>
    </tr>
    <tr>
      <td>Отчество :</td>
      <td>{strUserMiddlename}</td>
    </tr>
    <tr>
      <td>Компания :</td>
      <td>{strUserCompanyName}</td>
    </tr>
    <tr>
      <td>Телефон :</td>
      <td>{strUserPhone}</td>
    </tr>
    <tr>
      <td>Электронный адрес :</td>
      <td>{strUserEmail}</td>
    </tr>
    <tr>
      <td>Логин :</td>
      <td>{strUserLogin}</td>
    </tr>
    <tr>
      <td>Последний вход :</td>
      <td>{strUserLastVisitDate}</td>
    </tr>
    <tr>
      <td>IP :</td>
      <td>{strUserLastIP}</td>
    </tr>
  </tbody>
</table>

<p>
    <a href="{cfgSiteUrl}notebook/">Мой блокнот</a>
</p>