<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:5px"><b>ОБЪЯВЛЕНИЯ</b> <span class="">{strSubChapter}</span></p>

<if is.ads>
<div class="cnt">
{blockPaginator}
</div>

<table cellPadding="0" cellSpacing="0" width="100%" class="vtop">
<loop ads>
  <tr>
    <td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:6px 7px 3px 0;"></td>
    <td style="border-bottom:1px solid #EEEEEE">
      <p style="margin:3px 0 3px 0" class="txt-block">{paa_name} <img src="{cfgSiteImg}space.gif" width="10" height="1" border="0">  <a href="{cfgUrlSite}ad.info/{paa_id}" class="link-blue">Подробнее...</a></p>
    </td>
  </tr>
</loop ads>
</table>
</if is.ads>


<if no.ads>
<div class="cnt">
<p class="err">Нет объявлений для просмотра</p>
</div>
</if no.ads>

<p style="margin:5px 15px 0 0; text-align: right;"><a href="{cfgUrlSite}add.ad">Добавить объявление ...</a></p>