<div class="cnt">
<h1 class="txt-bb info-title">Объект недвижимости: <span class="title2">{strObjectName}</span></h1>
<div class="box-object-photos">
    <a id="zoomer" rel="pic_group"><img src="{cfgAllImg}objects/{strFileName}" id="main_object_image" class="yellBrd object-img hidden" alt="{strObjectName}" /></a>
</div>

<table cellPadding="5" cellSpacing="0" class="vtop object-features">
<tr><td class="tdL1">ID объекта</td>
    <td class="tdR1"><b>{intObjectID}</b>  </td>
</tr>
<tr><td class="tdL1">Наименование объекта</td>
    <td class="tdR1">{strObjectName}  </td>
</tr>
<tr><td class="tdL1">Раздел недвижимости</td>
    <td class="tdR1">{strChapterName}  </td>
</tr>
<if objnazn>
    <tr><td class="tdL1">Назначение объекта</td>
        <td class="tdR1">{strObjectNazn}  </td>
    </tr>
</if objnazn>
<tr><td class="tdL1">Регион</td>
    <td class="tdR1">{strRegion}  </td>
</tr>
<tr><td class="tdL1">Район</td>
    <td class="tdR1">{strAdmReg}  </td>
</tr>
<if access.for.browser>
    <tr><td class="tdL1">Адрес</td>
        <td class="tdR1">{strAddress}  </td>
    </tr>
</if access.for.browser>



<if access>
    <!--Is Access-->
    <if object.vid>
        <tr>
            <td class="tdL1">Вид строительства</td>
            <td class="tdR1">{strObjectVid}  </td>
        </tr>
    </if object.vid>

    <if etap>
        <tr>
            <td class="tdL1">Этап</td>
            <td class="tdR1">{strEtap}  </td>
        </tr>
    </if etap>

    <if etap.date>
        <tr>
            <td class="tdL1">Дата обновления информации</td>
            <td class="tdR1">{strEtapDate}  </td>
        </tr>
    </if etap.date>

    <if zem.uch>
        <tr>
            <td class="tdL1">Площадь земельного участка</td>
            <td class="tdR1">{strZemUch}  </td>
        </tr>
    </if zem.uch>

    <if sq.zas>
        <tr>
            <td class="tdL1">Площадь застройки</td>
            <td class="tdR1">{strSqZas}  </td>
        </tr>
    </if sq.zas>

    <if total.sq>
        <tr>
            <td class="tdL1">Общая площадь</td>
            <td class="tdR1">{strTotalSq}</td>
        </tr>
    </if total.sq>

    <if etazh>
        <tr>
            <td class="tdL1">Этажность</td>
            <td class="tdR1">{strEtazh}</td>
        </tr>
    </if etazh>

    <if karkas>
        <tr>
            <td class="tdL1">Каркас</td>
            <td class="tdR1">{strKarkas}</td>
        </tr>
    </if karkas>

    <if fundament>
        <tr>
            <td class="tdL1">Фундамент</td>
            <td class="tdR1">{strFundament}</td>
        </tr>
    </if fundament>

    <if walls>
        <tr>
            <td class="tdL1">Стены</td>
            <td class="tdR1">{strWalls}</td>
        </tr>
    </if walls>

    <if windows>
        <tr>
            <td class="tdL1">Окна</td>
            <td class="tdR1">{strWindows}</td>
        </tr>
    </if windows>

    <if roof>
        <tr>
            <td class="tdL1">Кровля</td>
            <td class="tdR1">{strRoof}</td>
        </tr>
    </if roof>

    <if floors>
        <tr>
            <td class="tdL1">Полы</td>
            <td class="tdR1">{strFloors}</td>
        </tr>
    </if floors>

    <if doors>
        <tr>
            <td class="tdL1">Двери, ворота</td>
            <td class="tdR1">{strDoors}</td>
        </tr>
    </if doors>

    <if pravo.docs>
        <tr>
            <td class="tdL1">Правоустанавливающий документ</td>
            <td class="tdR1">{strPravoDocs}</td>
        </tr>
    </if pravo.docs>

    <if prj.period>
        <tr>
            <td class="tdL1">Сроки проектирования</td>
            <td class="tdR1">{strPrjPeriod}</td>
        </tr>
    </if prj.period>

    <if build.period>
        <tr>
            <td class="tdL1">Сроки строительства</td>
            <td class="tdR1">{strBuildPeriod}</td>
        </tr>
    </if build.period>

    <if volume.build>
        <tr>
            <td class="tdL1">Строительный объём</td>
            <td class="tdR1">{strVolumeBuild}</td>
        </tr>
    </if volume.build>
</if access>

<loop contacts.info>
    <tr>
        <td class="tdL1" align="right">{title}</td>
        <td class="tdR1">
            <table cellPadding="1" cellSpacing="2" width="100%" style="margin-top:-2px">
                <tr>
                    <td width="15%">Компания</td>
                    <td><a href="{cfgSiteUrl}companyinfo/{sec_id}">{sec_company}</a></td>
                </tr>
                <tr>
                    <td>Контакт</td>
                    <td>{sec_fio}</td>
                </tr>
                <tr>
                    <td>Должность</td>
                    <td>{sec_proff}</td>
                </tr>
                <tr>
                    <td>Доп.инфо</td>
                    <td>{sec_contact}</td>
                </tr>
            </table>
        </td>
    </tr>
</loop contacts.info>




<if not.authed>
    <tr>
        <td colspan="2" class="tdL1" bgcolor="FFEFEF">
            Для того, чтобы просмотреть полную информацию об объекте, Вы должны быть <a href="{cfgUrlSite}register" class="link-blue">зарегистрированным</a> пользователем и подписчиком журнала "Строительный еженедельник "ПЕТРОСТРОЙ".<br />
            Если Вы уже зарегистрированы, введите свой логин и пароль в блоке авторизации слева.
        </td>
    </tr>
</if not.authed>



<if no.access>
    <tr>
        <td colspan="2" class="tdL1" bgcolor="FFEFEF">
            Доступ к базе объектов является <b class="redtext">платной</b> услугой. Информация об администрации сайта на странице <a href="{cfgSiteUrl}contacts">контакты</a>.
        </td>
    </tr>
</if no.access>



<if not.permited>
    <tr>
        <td colspan="2" class="tdL1" bgcolor="FFEFEF">
            Вы <span class="err">не подписаны</span> на объекты данного региона!<br />
            Для получения доступа и дополнительной информации, пожалуйста, свяжитесь с <a href="{cfgSiteUrl}contacts" class="link-blue-over">администрацией сайта</a>.
        </td>
    </tr>
</if not.permited>
</table>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        window.print();
    });
</script>
