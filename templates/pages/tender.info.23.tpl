<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:5px"><b>ИНФОРМАЦИЯ О ТЕНДЕРЕ</b></p>

<div class="cnt">
<if is.tender.info>
<table cellPadding="5" cellSpacing="0" width="100%" style="margin-top:20px;border-top:1px solid #F9C36D;" class="vtop">
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Название объекта</td>
    <td class="tdR1"><b>{strTenderName}</b> &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Вид работ</td>
    <td class="tdR1">{strVidRabot} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Адрес объекта</td>
    <td class="tdR1">{strAddress} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Заказчик</td>
    <td class="tdR1">{strCustomer} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Координаты</td>
    <td class="tdR1">{strCoordinates} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Контактное лицо</td>
    <td class="tdR1">{strContacts} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Полезная информация</td>
    <td class="tdR1">{strInfo} &nbsp;</td>
  </tr>
</table>
</if is.tender.info>

<if no.tender.info>
<p class="err">Нет информации для просмотра</p>
</if no.tender.info>

<p><a href="{cfgUrlSite}add.tender">добавить новый тендер</a></p>
<p><a href="{urlLinkBack}">вернуться к списку тендеров</a></p>

</div>