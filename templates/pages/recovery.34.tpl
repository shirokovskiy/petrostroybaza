<div class="cnt">

<div style="padding:5px;">
<span class="txt-bb">Восстановление пароля</span>

<p>Укажите E-mail адрес, который Вы использовали при регистрации и Вам будет отправлен новый сгенерированый пароль для авторизации на сайте.</p>

<if sent>
<span class="success"><b>Напоминание отправлено на указаный E-mail</b></span>
</if sent>

<if no.such.email>
<span class="err"><b>Ошибка! Нет такого E-mail адреса в нашей БД сайта</b></span>
</if no.such.email>

<if no.email>
<span class="err"><b>Ошибка! Поле E-mail осталось пустым</b></span>
</if no.email>

<if no.new.password>
<span class="err"><b>Ошибка! Новый пароль небыл сгенерирован</b></span>
</if no.new.password>

<if not.sent.password>
<span class="err"><b>Ошибка! Не могу отправить Email. Попробуйте позже.</b></span>
</if not.sent.password>

<form method="POST">
<input type="hidden" name="recovery" value="true"/>
<input type="text" name="strEmailToRecover" class="flat" value="{strEmailToRecover}" maxlength=255 style="width:300px;height:18px;" />
<input type="submit" name="submit" class="standart" value="Получить пароль" />
</form>

</div>
</div>
