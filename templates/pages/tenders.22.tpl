<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:5px"><b>ТЕНДЕРЫ</b></p>

<if is.tenders>
<div class="cnt">
{blockPaginator}
</div>

<table cellPadding="0" cellSpacing="0" width="100%" class="vtop">
<loop tenders>
  <tr>
    <td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:6px 7px 3px 0;"></td>
    <td style="border-bottom:1px solid #EEEEEE">
      <p style="margin:3px 0 3px 0" class="txt-block">{pt_name} <img src="{cfgSiteImg}space.gif" width="10" height="1" border="0">  <a href="{cfgUrlSite}tender.info/{pt_id}" class="link-blue">Подробнее...</a></p>
    </td>
  </tr>
</loop tenders>
</table>
</if is.tenders>


<if no.tenders>
<p class="err">Нет тендеров для просмотра</p>
</if no.tenders>

<p style="margin:5px 15px 0 0; text-align: right;"><a href="{cfgUrlSite}add.tender">Добавить тендер ...</a></p>