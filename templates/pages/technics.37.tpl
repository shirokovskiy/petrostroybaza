<div class="cnt breadcrumbs">
	<a href="{cfgSiteUrl}technics/">Строительная Техника</a> <if chapter>> <a href="{cfgSiteUrl}technics/{strChapterAlias}/">{strChapterName}</a></if chapter> <if ad_type>> <a href="{cfgSiteUrl}technics/{strChapterAlias}/{strAdTypeAlias}">{strAdTypeName}</a></if ad_type> </span>
</div>

<div class="tooltip-wrapper">
	<div class="tooltip  border-radius-10" id="ad_add" style="display: none">
		<p>Хотите арендовать строительную технику?</p>
		<p>Разместите бесплатное объявление на нашем сайте!</p>
		<p>Это позволит сэкономить время и выбрать наилучшее предложение!</p>
		<p class="ads-link"><a href="{cfgUrlSite}add.technic/pnevmotech">Разместить своё объявление</a></p>
	</div>
</div>


<if is.technics>
	<div style="margin:1px; text-align: center">
		<b style="font-size:12px">В настоящее время на сайте всего объявлений: <span style="color:#ff9408; font-size:14px">{intQuantAllRecords}</span><if show_extra_number_ads>, выбрано - <span style="color:#ff9408; font-size:14px">{intQuantSelectRecords}</span></if show_extra_number_ads></b>
	</div>


	<div class="cnt">
	{blockPaginator}
	</div>
	<table cellPadding="0" cellSpacing="0" width="100%" class="vtop" style="border-top:2px solid #D2D5E4;">
		<loop technics>
			<tr>
				<td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:7px 7px 3px 0;"></td>
				<td style="border-bottom:1px solid #EEEEEE">

					<table class="ad-info-box" cellPadding="0" cellSpacing="0" width="100%" style="margin:3px 0 3px 0">
						<tr>
							<td valign="top">
								<span class="title1">{pt_title}</span> <b>&gt;</b> <a class="link-gro" href="{cfgSiteUrl}technics/{ptt_alias}">{ptt_name}</a> [{strAdDate}]
								<p class="txt-block">
									<span class="txt-bold-gray">ID: {pt_id}</span><br />
									<span class="txt-bold-gray">Тип объявления:</span> <a href="{strAdTypeUrl}">{strAdType}</a><br />
									<if is.rent.period.{pt_id}><span class="txt-bold-gray">Срок аренды:</span> {pt_rent_period}<br /></if is.rent.period.{pt_id}>
									<if is.address.works.{pt_id}><span class="txt-bold-gray">Адрес проведения работ:</span> {pt_address_works}<br /></if is.address.works.{pt_id}>
									<if is.marka.{pt_id}><span class="txt-bold-gray">Марка:</span> {pt_marka}<br /></if is.marka.{pt_id}>
									<if is.model.{pt_id}><span class="txt-bold-gray">Модель:</span> {pt_model}<br /></if is.model.{pt_id}>
									<if is.year.{pt_id}><span class="txt-bold-gray">Год выпуска:</span> {pt_year}<br /></if is.year.{pt_id}>
									<span class="txt-bold-gray">Технические характеристики:</span> {strDopinfo}<br />
									<span class="txt-bold-gray">Контактная информация:</span> {strContacts}<br />
								</p>
							</td>
							<if object.photo.{seo_id}>
								<td width="110" valign="top" align="left"><a href="{cfgUrlSite}objectinfo/{seo_id}?stPage={stPage}"><img src="{cfgAllImg}technics/{si_filename}" width="120" class="grayBrd"></a></td>
							</if object.photo.{seo_id}>
						</tr>
					</table>

				</td>
			</tr>
		</loop technics>
	</table>

	<div class="cnt">
	{blockPaginator}
	</div>
</if is.technics>

<if no.technics>
	<p class="err">Нет объявлений в данном разделе техники</p>
</if no.technics>

<if chapter>
	<div style="margin-top:10px">
		<a href="{cfgUrlSite}technics" >Вся техника</a>
	</div>
</if chapter>

<div style="margin-top:5px">
	<a href="{cfgUrlSite}add.technic/{strChapterAlias}?stPage={stPage}">Дать объявление</a>
</div>

<if nopage>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#ad_add").delay(2000).fadeIn(1000).click(function(){
			jQuery(this).fadeOut();
		});
	});
</script>
</if nopage>
