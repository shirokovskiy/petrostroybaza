<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:5px"><b>НЕДВИЖИМОСТЬ И УЧАСТКИ</b> <span class="">{strSubChapter}</span></p>

<if is.estate_lands>
<div class="cnt">
{blockPaginator}
</div>

<table cellPadding="0" cellSpacing="0" width="100%" class="vtop">
<loop estate_lands>
  <tr>
    <td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:6px 7px 3px 0;"></td>
    <td style="border-bottom:1px solid #EEEEEE">
      <p style="margin:3px 0 3px 0" class="txt-block">{pael_name} <img src="{cfgSiteImg}space.gif" width="10" height="1" border="0">  <a href="{cfgUrlSite}estate_land.info/{pael_id}" class="link-blue">Подробнее...</a></p>
    </td>
  </tr>
</loop estate_lands>
</table>
</if is.estate_lands>


<if no.estate_lands>
<div class="cnt">
<p class="err">Нет объявлений для просмотра</p>
</div>
</if no.estate_lands>

<p style="margin:5px 15px 0 0; text-align: right;"><a href="{cfgUrlSite}add.estate_land">Добавить объявление ...</a></p>