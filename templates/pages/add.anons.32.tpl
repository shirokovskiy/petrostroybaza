<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:0;"><b>ДОБАВИТЬ АНОНС</b></p>

<div class="cnt">
<if error>
{errMsg}
</if error>
<if success>
<center>
<p><span class="success">{msgOk}</span><br>
<span class="sm-gr">Вся публикуемая Вами информация будет проверена модераторами сайта, прежде чем появиться в списках объявлений!</span></p>
<p><a href="{cfgUrlSite}add.anons">Хотите добавить ещё?</a></p>
</center>
</if success>

<if post.data>
<form action="?new=record" method="POST">
<table cellPadding="5" cellSpacing="0" width="100%" style="border-top:1px solid #F9C36D;" class="vtop">
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Заголовок <font color="red">*</font></td>
    <td class="tdR1">
      <input type="text" name="strTitle" class="flat" value="{strTitle}" maxlength=80 style="width:300px">
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Место проведения <font color="red">*</font></td>
    <td class="tdR1">
      <textarea name="strAddress" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;" class="flat">{strAddress}</textarea>
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Организатор, Контактное лицо <font color="red">*</font></td>
    <td class="tdR1">
      <textarea name="strContacts" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;" class="flat">{strContacts}</textarea>
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Дата и время проведения <font color="red">*</font></td>
    <td class="tdR1">
      <input type="text" name="strTime" class="flat" value="{strTime}" maxlength=80 style="width:300px">
    </td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Полезная информация</td>
    <td class="tdR1">
      <textarea name="strInfo" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;" class="flat">{strInfo}</textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="right">
      <input type="submit" name="submit" class="standart" value="Сохранить">
    </td>
  </tr>
</table>
</form>
</if post.data>
<p><a href="{cfgUrlSite}anonses">вернуться к списку анонсов</a></p>

</div>