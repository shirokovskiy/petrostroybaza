<script type="text/javascript">
//<![CDATA[
    var params = { allowScriptAccess: "always" };
    var atts = { id: "myytplayer" };
    var videoW = 640;
    var videoH = 490;

    function videoInit(vid, url){
        swfobject.embedSWF( url + "&enablejsapi=1&playerapiid=" + vid,
                            "ytapiplayer" + vid, videoW, videoH, "8", null, null, params, { id: "myytplayer"+vid });
    }

    function showVideo(vid, vurl){
        $.fancybox({
            'href':'#video'+vid
            ,'autoScale':false
            ,'width':680
            ,'height':600
            ,'wmode':'transparent'
            ,'scrolling':'no'
            ,'allowfullscreen':true
            ,'onComplete': function(){videoInit(vid, vurl)}
        });
    }

    function onYouTubePlayerReady(playerId) {
        var ytp = document.getElementById("myytplayer" + playerId);
        ytp.playVideo();
        ytp.addEventListener("onStateChange", "onYtpStateChange");
    }

    function onYtpStateChange(newState) {
        // If video stop
        if (newState==0){
            // alert("Player's new state: " + newState);
            console.log("Player's new state: " + newState);
            $.fancybox.close();
        }
    }

    $(document).ready(function() {
<loop last.news.js>
<if news.picture.{sn_id}>
        $("a#hrefImg{sn_id}").fancybox({
                        'titleShow'		: false,
                        'transitionIn'	: 'elastic',
                        'transitionOut'	: 'elastic'
    });
</if news.picture.{sn_id}>
</loop last.news.js>
});
//]]>
</script>

<if is.main.news>
	<div style="clear: both;"></div>
<div id="mainNovelty" class="clearer">
	<h1>{strMainTitle}</h1>
	<if main.image>
		<div class="main-photo-block"><a href="{cfgUrlSite}news/{mainUrlParam}"><img src="{cfgAllImg}news/news.photo.{intMainImgId}.b.jpg" alt="{strMainTitle}" title="{strMainTitle}" /></a>
			<div>{strNewsImageTitle}</div>
		</div>

	</if main.image>
	<p class="date">{strMainDate}</p>
	<p>{strMainText} <a href="{cfgUrlSite}news/{mainUrlParam}" class="link-blue">Подробнее...</a></p>
</div>
</if is.main.news>

<div class="col-news">
    <div class="cnt">
        <h1 class="title3">СВЕЖИЕ НОВОСТИ</h1>
    </div>

    <div id="news_list" class="news-list">
<loop last.news>
		<div class="news-box clearer<if news.picture.{sn_id}> pictured</if news.picture.{sn_id}>">
			<div class="news-text">
				<a href="{cfgUrlSite}news/{urlParam}" class="link-blue"><span class="txt-bb">{strDatePublicNews}</span></a>
				<if news.picture.{sn_id}>
					<div class="fr news-image"><a href="{cfgAllImg}news/news.photo.{sn_id}.b.jpg{strUniquePhoto}" id="hrefImg{sn_id}"><img src="{cfgAllImg}news/news.photo.{sn_id}.jpg{strUniquePhoto}" border="0" height="120" alt="{sn_img_desc}" title="{sn_img_desc}" /></a></div>
				</if news.picture.{sn_id}>
				<p class="txt-block">{sn_title} <a href="{cfgUrlSite}news/{urlParam}" class="link-blue">&raquo; подробнее...</a></p>
			</div>
		</div>
</loop last.news>
    </div>
    <div class="cnt">{newsPaginator}</div>
</div>

<frg fotobaza>
