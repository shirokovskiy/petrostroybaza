<if news.list>
	<script type="text/javascript">
		var params = { allowScriptAccess: "always" };
		var atts = { id: "myytplayer" };
		var videoW = 640;
		var videoH = 490;

		function videoInit(vid, url){
			swfobject.embedSWF( url + "&enablejsapi=1&playerapiid=" + vid,
					"ytapiplayer" + vid, videoW, videoH, "8", null, null, params, { id: "myytplayer"+vid });
		}

		function showVideo(vid, vurl){
			$.fancybox({
				'href':'#video'+vid
				,'autoScale':false
				,'width':680
				,'height':600
				,'wmode':'transparent'
				,'scrolling':'no'
				,'allowfullscreen':true
				,'onComplete': function(){videoInit(vid, vurl)}
			});
		}

		function onYouTubePlayerReady(playerId) {
			var ytp = document.getElementById("myytplayer" + playerId);
			ytp.playVideo();
			ytp.addEventListener("onStateChange", "onYtpStateChange");
		}

		function onYtpStateChange(newState) {
			// If video is stop
			if (newState==0){
				console.log("Player's new state: " + newState);
				$.fancybox.close();
			}
		}
	</script>

	<div class="fl col-news">
		<div class="cnt">
			<span class="txt-bb" style="margin-left:10px; font-size:14px;">Все новости сайта</span>
		{blockPaginator}
		</div>

		<div class="news-list">
			<loop last.news>
				<div class="news-cell">

					<div class="news-box">
						<div class="fl news-text">
							<a href="{cfgUrlSite}news/{urlParam}" class="link-blue"><span class="txt-bb">{strDatePublicNews}</span></a>
							<p class="txt-block">{sn_title} <a href="{cfgUrlSite}news/{urlParam}" class="link-blue">Подробнее...</a></p>
						</div>
						<if news.picture.{sn_id}>
							<div class="fr news-image"><a href="{cfgAllImg}news/news.photo.{sn_id}.b.jpg{strUniquePhoto}" id="hrefImg{sn_id}"><img src="{cfgAllImg}news/news.photo.{sn_id}.jpg{strUniquePhoto}" border="0" width="200" /></a></div>
						</if news.picture.{sn_id}>
						<div class="clearer"></div>
					</div>

				</div>
				<div class="clearer"></div>
			</loop last.news>
		</div>

		<div class="cnt">
		{blockPaginator}
		</div>
	</div>

	<div class="fr col-videos">
		<div class="videos-title">
			ПЕТРОСТРОЙ <span>TV</span>
		</div>
		<loop last.videos>
			<div class="video-box">
				<div style="display: none">
					<div id="video{thId}" style="text-align: center">
						<h2>{strVideoTitle}</h2>
						<div id="ytapiplayer{thId}" style="width:680px; min-height:510px;"><!--You need Flash player 8+ and JavaScript enabled to view this video.--></div>
						<p class="video-descr" style="text-align: justify">{strVideoDescr}</p>
					</div>
				</div>
				<a id="thumb{thId}" href="{cfgSiteUrl}video/{strVideoID}"><img src="{urlVideoThumb}" class="fl thumb" /></a>
				<p class="video-title"><a href="{cfgSiteUrl}video/{strVideoID}">{strVideoTitle}</a></p>
				<div class="clearer"></div>
				<div class="video-date">
					<img src="{cfgSiteImg}content/icon.video.png" class="fl" onclick="showVideo({thId}, '{urlFlash}')"/>
					<span class="date">{strVideoDate}</span>
				</div>
				<p class="video-descr">{strVideoDescrCutted}</p>
			</div>
		</loop last.videos>
		<div class="cnt">{videoPaginator}</div>
	</div>
	<div class="clearer"></div>


	<script type="text/javascript">
		$(document).ready(function() {
<loop last.news.js>
	<if news.picture.{sn_id}>
			$("a#hrefImg{sn_id}").fancybox({
				'titleShow'		: false,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
			});
	</if news.picture.{sn_id}>
</loop last.news.js>
		});
	</script>
</if news.list>









<if novelty>
	<script type="text/javascript">
		$(document).ready(function() {
<if picture>
			$("a#pictureMain").fancybox({
				'titleShow'		: true
				, 'titlePosition'	: 'over'
			});
</if picture>
<if pic_group>
			$("a[rel=pic_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Фото ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});
</if pic_group>
		});
	</script>
	<div class="cnt">
		<if picture>
			<a href="{cfgAllImg}news/news.photo.{sn_id}.b.jpg{strUniqueMainPhoto}" id="pictureMain"><img src="{cfgAllImg}news/news.photo.{sn_id}.jpg{strUniqueMainPhoto}" width="200" border="0" align="right" style="margin:0 0 5px 10px" alt="{sn_img_desc}"></a>
		</if picture>
		<h1 class="title1">{sn_title}</h1>
		<span class="sm-gyl">{strDatePublNews}</span>
		<p style="margin:12px 0 4px 0">{strNewsBody}</p>

		<div>
			<loop list>
				<div style="float:left">
					<a href="{cfgAllImg}news/news.photo.{sn_id}.{si_id}.b.jpg{strUniquePhoto}" rel="pic_group"><img src="{cfgAllImg}news/news.photo.{sn_id}.{si_id}.jpg{strUniquePhoto}" width="100" border="0" style="margin:0 0 5px 10px;" alt="{si_desc}" /></a>&nbsp;&nbsp;
				</div>
			</loop list>
			<div style="clear: both;"></div>
		</div>
		<if is.attached.files>
			<h4 class="title2">Дополнительный материал:</h4>
			<ul class="news-attachments">
				<loop attachments>
					<li><a class="link-blue" href="javascript:void(0)" onclick="jsShowModal('{at.url}', 800, 600)">{name}</a></li>
				</loop attachments>
			</ul>
		</if is.attached.files>

		<p>« <a href="{cfgUrlSite}news?stPage={stPage}">к списку новостей</a></p>
	</div><!-- // cnt -->
</if novelty>
