<div class="cnt">
<h1 class="txt-bb info-title">Объект недвижимости: <span class="title2">{strObjectName}</span></h1>
<div class="box-object-photos">
    <a href="{urlLinkBack}" class="roll_back_link">&lt; {strLinkBack}</a> <a id="zoomer" rel="pic_group"><img src="{cfgAllImg}objects/{strFileName}" id="main_object_image" class="yellBrd object-img hidden" alt="{strObjectName}" /></a>
</div>

<table cellPadding="5" cellSpacing="0" class="vtop object-features">
<tr><td class="tdL1">ID объекта</td>
    <td class="tdR1"><b>{intObjectID}</b></td>
</tr>
<tr><td class="tdL1">Наименование объекта</td>
    <td class="tdR1">{strObjectName}</td>
</tr>
<tr><td class="tdL1">Раздел недвижимости</td>
    <td class="tdR1">{strChapterName}</td>
</tr>
<if objnazn>
<tr><td class="tdL1">Назначение объекта</td>
    <td class="tdR1">{strObjectNazn}</td>
</tr>
</if objnazn>
<tr><td class="tdL1">Регион</td>
    <td class="tdR1">{strRegion}</td>
</tr>
<tr><td class="tdL1">Район</td>
    <td class="tdR1">{strAdmReg}</td>
</tr>
<if access.for.browser>
<tr><td class="tdL1">Адрес</td>
    <td class="tdR1">{strAddress} <if is.address><a href="javascript:void(0)" data-address="{strAddressData}" class="link-blue maps" title="Показать на Яндекс.Картах">адрес на карте</a></if is.address></td>
</tr>
</if access.for.browser>



<if access>
<!--Is Access-->
<if object.vid>
  <tr>
    <td class="tdL1">Вид строительства</td>
    <td class="tdR1">{strObjectVid}</td>
  </tr>
</if object.vid>

<if etap>
  <tr>
    <td class="tdL1">Этап</td>
    <td class="tdR1">{strEtap}</td>
  </tr>
</if etap>

<if etap.date>
  <tr>
    <td class="tdL1">Дата обновления информации</td>
    <td class="tdR1">{strEtapDate}</td>
  </tr>
</if etap.date>

<if zem.uch>
  <tr>
    <td class="tdL1">Площадь земельного участка</td>
    <td class="tdR1">{strZemUch}</td>
  </tr>
</if zem.uch>

<if sq.zas>
  <tr>
    <td class="tdL1">Площадь застройки</td>
    <td class="tdR1">{strSqZas}</td>
  </tr>
</if sq.zas>

<if total.sq>
  <tr>
    <td class="tdL1">Общая площадь</td>
    <td class="tdR1">{strTotalSq}</td>
  </tr>
</if total.sq>

<if etazh>
  <tr>
    <td class="tdL1">Этажность</td>
    <td class="tdR1">{strEtazh}</td>
  </tr>
</if etazh>

<if karkas>
  <tr>
    <td class="tdL1">Каркас</td>
    <td class="tdR1">{strKarkas}</td>
  </tr>
</if karkas>

<if fundament>
  <tr>
    <td class="tdL1">Фундамент</td>
    <td class="tdR1">{strFundament}</td>
  </tr>
</if fundament>

<if walls>
  <tr>
    <td class="tdL1">Стены</td>
    <td class="tdR1">{strWalls}</td>
  </tr>
</if walls>

<if windows>
  <tr>
    <td class="tdL1">Окна</td>
    <td class="tdR1">{strWindows}</td>
  </tr>
</if windows>

<if roof>
  <tr>
    <td class="tdL1">Кровля</td>
    <td class="tdR1">{strRoof}</td>
  </tr>
</if roof>

<if floors>
  <tr>
    <td class="tdL1">Полы</td>
    <td class="tdR1">{strFloors}</td>
  </tr>
</if floors>

<if doors>
  <tr>
    <td class="tdL1">Двери, ворота</td>
    <td class="tdR1">{strDoors}</td>
  </tr>
</if doors>

<if pravo.docs>
  <tr>
    <td class="tdL1">Правоустанавливающий документ</td>
    <td class="tdR1">{strPravoDocs}</td>
  </tr>
</if pravo.docs>

<if prj.period>
  <tr>
    <td class="tdL1">Сроки проектирования</td>
    <td class="tdR1">{strPrjPeriod}</td>
  </tr>
</if prj.period>

<if build.period>
  <tr>
    <td class="tdL1">Сроки строительства</td>
    <td class="tdR1">{strBuildPeriod}</td>
  </tr>
</if build.period>

<if volume.build>
  <tr>
    <td class="tdL1">Строительный объём</td>
    <td class="tdR1">{strVolumeBuild}</td>
  </tr>
</if volume.build>
</if access>

<loop contacts.info>
    <tr>
        <td class="tdL1" align="right">{title}</td>
        <td class="tdR1">
            <table cellPadding="1" cellSpacing="2" width="100%" style="margin-top:-2px">
                <tr>
                    <td width="15%">Компания</td>
                    <td><a href="{cfgSiteUrl}companyinfo/{sec_id}">{sec_company}</a></td>
                </tr>
                <tr>
                    <td width="15%">ИНН</td>
                    <td><a href="{cfgSiteUrl}companyinfo/{sec_id}">{sec_inn}</a></td>
                </tr>
                <tr>
                    <td>Контакт</td>
                    <td>{sec_fio}</td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>{sec_proff}</td>
                </tr>
                <tr>
                    <td>Доп.инфо</td>
                    <td>{sec_contact}</td>
                </tr>
            </table>
        </td>
    </tr>
</loop contacts.info>




<if not.authed>
<if no.access>
    <tr>
        <td class="tdL1" align="right">Заказчик</td>
        <td class="tdR1">
            <table cellPadding="1" cellSpacing="2" width="100%" style="margin-top:-2px">
                <tr>
                    <td width="15%">Компания</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="15%">ИНН</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Контакт</td>
                    <td>{strNoAccessContacts}</td>
                </tr>
                <tr>
                    <td>Должность</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Доп.инфо</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</if no.access>
</if not.authed>


<if not.permited>
    <tr>
        <td colspan="2" class="tdL1" bgcolor="FFEFEF">
            Вы <span class="err">не подписаны</span> на объекты данного региона!<br />
            Для получения доступа и дополнительной информации, пожалуйста, свяжитесь с <a href="{cfgSiteUrl}contacts" class="link-blue-over">администрацией сайта</a>.
        </td>
    </tr>
</if not.permited>
</table>

<div class="clearer" style="margin-top:10px">
    <div class="half fl">
        <div class="bottom-buttons">
            <table class="tbl-buttons">
                <tr><if authed><if not.in.notebook>
                    <td align="center"><img src="{cfgSiteImg}btn-blocknote.gif" alt="Добавить в блокнот объект с ID={intObjectID}" title="Добавить в блокнот объект с ID={intObjectID}" onclick="gotoUrl('{strCurrentUri}addtonb={intObjectID}')"/>
                        <label>Добавить в блокнот</label>
                    </td>
                </if not.in.notebook>
                    <ifelse not.in.notebook>
                        <td align="center"><img src="{cfgSiteImg}btn_remove_from_blocknote_b.jpg" alt="Удалить из блокнота объект с ID={intObjectID}" title="Удалить из блокнота объект с ID={intObjectID}" onclick="confirmAction('{strCurrentUri}rmfromnb={intObjectID}', 'Заметка и объект будут удалены из блокнота?')"/>
                            <label>Удалить из блокнота</label>
                        </td>
                    </ifelse not.in.notebook></if authed>
                    <td align="center"><img src="{cfgSiteImg}btn-print.gif" alt="Отправить на печать" title="Отправить на печать" onclick="jsShowModal('{cfgSiteUrl}printobject/{intObjectID}', 800, 600, 'yes', 'yes', 'print{intObjectID}')"/>
                        <label>Отправить на печать</label>
                    </td>
                    <td align="center">
                        <a href="{urlLinkBack}" class="roll_back_link">{strLinkBack}</a>
                    </td>
                </tr>
            </table>
        </div>

        <if comments>
        <!--КОММЕНТАРИИ-->
        <div class="comments_block">
            <h1 class="txt-bb info-title">Комментарии к объекту недвижимости</h1>

            <loop comments>
                <div class="comment-record"><b>{UserName}</b>, {CommentDate} <if authed.j><a href="#" class="write">Ответить</a></if authed.j>
                    <p>{strComment}</p></div>
            </loop comments>
        </div>
        </if comments>

        <if authed>
        <div class="add_comments_block">
            <form action="" method="post" id="formComment">
                <input type="hidden" name="type" value="comment"/>
                <h1 id="comment_form_title" class="txt-bb info-title">Написать комментарий к объекту</h1>
                <div>
                    <textarea id="strComment" name="strComment"
                              style="overflow-y:auto;word-wrap:break-word"
                              class="plane">{strComment}</textarea>
                </div>
                <!--<div class="">-->
                <!--<input type="checkbox" id="cbxGetAnswers" name="cbxGetAnswers" value="Y" class="plane"/>-->
                <!--<label for="cbxGetAnswers">- Получать ответы на комментарии по эл.почте</label>-->
                <!--</div>-->
                <div class="">
                    <input type="checkbox" id="cbxGetComments" name="cbxGetComments" value="Y" {cbxGetComments} class="plane"/>
                    <label for="cbxGetComments">- Получать сообщения о новых комментариях по объекту</label>
                </div>
                <if comments.need.agree>
                    <div class="">
                        <input type="checkbox" id="cbxIAgree" name="cbxIAgree" value="Y" class="plane"/>
                        <label for="cbxIAgree" id="lblIAgree">- Я прочитал и согласен с <a href="#comment_agreement" class="fancybox link-blue">правилами комментирования</a></label>
                    </div>
                </if comments.need.agree>
                <ifelse comments.need.agree>
                    <div class="row">
                        Ранее Вы согласились с <a href="#comment_agreement" class="fancybox link-blue">правилами комментирования</a>
                    </div>
                </ifelse comments.need.agree>
            </form>
            <div class="button-set">
                <!--<button id="btnAddCommentFile" class="border-radius-5">Добавить файл</button>-->
                <button id="btnSendComment" class="border-radius-5">Отправить</button>
            </div>
        </div>

        <script type="text/javascript">
            var jsCommentError = 0;
            jQuery(document).ready(function ($) {
                $('#btnSendComment').click(function () {
                    if (!isEmpty($('#strComment').val())) {
                        $('#formComment').submit();
                    } else {
                        alert("Внимание! Нет текста комментария.");
                    }
                });

                if (jsCommentError==1) {
                    alert("Ошибка: необходимо согласиться с Правилами комментирования");
                    $('#lblIAgree').addClass('blink_me');
                }

                $('#cbxIAgree').click(function () {
                    if ($(this).is(':checked')) {
                        $('#lblIAgree').removeClass('blink_me');
                    }
                });
            });
        </script>
        <if show_js_error_for_comment>
            <script type="text/javascript">
                var jsCommentError = 1;
            </script>
        </if show_js_error_for_comment>
        </if authed>
    </div>

    <!--SEPARATOR-->

    <div class="half fl">
        <div class="box_dop_info border-radius-5">
            <h1 class="title">Дополнительная информация об объекте</h1>

            <if no.list.dop.info>
                <p>Пока нет дополнительной информации для данного объекта.</p>
            </if no.list.dop.info>
            <loop list.dop.info>
            <div class="row_dop_info">
                <span class="txt-b">{date}</span> <if is.source.{id}> <span class="txt-b">- {source}</span> </if is.source.{id}>: {title}<br />
                {content_short} <a href="#dopinfo{id}" class="fb link-topmenu-l">подробнее</a>
            </div>
            <div class="hidden">
                <div id="dopinfo{id}" class="row_dop_info_hidden">
                    <span class="txt-b">{date}</span> <if is.source.{id}> <span class="txt-b">- {source}</span> </if is.source.{id}>: {title}
                    <p>{content}</p>
                </div>
            </div>
            </loop list.dop.info>
        </div>
    </div>
</div>

</div><!--end:cnt-->

<div style="display: none">
    <div id="comment_agreement" style="width: 80%">
        <h1>Почему на сайте petrostroybaza.ru удаляются комментарии?</h1>

        <h3>При комментировании тех или иных материалов запрещены:</h3>

        <ul>
            <li>Призывы к войне, свержению существующего строя, терроризму (в т.ч. хакерским атакам), экстремизму.</li>
            <li>Пропаганда фашизма, геноцида.</li>
            <li>Пропаганда курения, наркомании.</li>
            <li>Разжигание межнациональной, межрелигиозной, социальной розни, грубые высказывания в адрес представителей любых национальностей, рас и вероисповеданий.</li>
            <li>Угрозы физической расправы, убийства, сексуального насилия.</li>
            <li>Описание средств и способов суицида, любое подстрекательство к его совершению.</li>
            <li>Переход на личности, оскорбления в адрес официальных и публичных лиц (в т.ч. умерших), грубые выражения, оскорбления и принижения других участников комментирования, их родных или близких.</li>
            <li>Публикация заведомо ложной, непроверенной, клеветнической информации.</li>
            <li>Нарушение права несовершеннолетних лиц и/или пропаганда причинения им вреда в любой форме</li>
            <li>Оскорбление журналистов и других сотрудников petrostroybaza, авторов petrostroybaza, модераторов, администрации сайта, руководства издания, читателей petrostroybaza, грубые высказывания об изданиях petrostroybaza.</li>
            <li>Присвоение чужих имен и фамилий, комментирование от чужого имени.</li>
            <li>Распространение персональных данных, нарушение тайны переписки и связи.</li>
            <li>Брань (в т.ч. измененное написание мата).</li>
            <li>Дублирование комментариев (флуд).</li>
            <li>Бессмысленные комментарии (флейм).</li>
            <li>Комментарии, не относящиеся к темам статей (офф-топ).</li>
            <li>Реклама других сайтов (в т. ч. ссылки на другие сайты). Реклама товаров и услуг.</li>
            <li>Сообщения, оставленные не на русском языке.</li>
            <li>Сообщения, содержащие более 2000 символов и пробелов.</li>
            <li>Прочие нарушения законодательства РФ.</li>
        </ul>

        <b>Комментарии, нарушающие правила поведения на сайте petrostroybaza.ru, удаляются без предупреждения. При вторичном размещении уже удалённого сообщения, модератор вправе заблокировать («забанить») пользователя.</b>

        <h3>Почему на сайте petrostroybaza.ru не публикуются ваши комментарии?</h3>

        <ul>
            <li>Администрация сайта оставляет за собой право по собственному усмотрению или решению автора закрыть материал для комментирования.</li>
            <li>Возможно, вы попали в черный список.</li>
        </ul>
    </div>
</div>

<script type="text/javascript">
    var isAuthed = {jsAuthed},
    isPhoto = {jsPhoto},
    isFotobankImages = {isFotobankImages},
    arrObjectImages = {arrObjectImages};

    jQuery(document).ready(function ($) {
        if (isPhoto) {
            $('#main_object_image').removeClass('hidden');
        }

        if ($('a.fancybox')) // Show Popup block
            $('a.fancybox').fancybox();

        $(".row_dop_info a.fb").fancybox({
            'autoSize': true
            , 'width': 678
            , 'autoDimensions': false
        });

        if (isFotobankImages) {
            for(var iid in arrObjectImages) {
                $('#zoomer').after('<a rel="fotobank_group" href="{cfgAllImg}fotobank/groups/'+arrObjectImages[iid].gid+'/'+arrObjectImages[iid].file+'"><img src="{cfgAllImg}fotobank/groups/'+arrObjectImages[iid].gid+'/t_h_u_m_b_'+arrObjectImages[iid].file+'" alt="'+arrObjectImages[iid].title+'" class="yellBrd object-img preview" /></a>');
            }

            $("a[rel=fotobank_group]").fancybox({
                'transitionIn'  : 'none',
                'transitionOut' : 'none',
                'titlePosition' : 'over',
                padding: 3,
                margin: 5
            });
        }

        $('#main_object_image').addClass('preview');
        $('a[rel=pic_group]').addClass('finger');
        $('#zoomer').attr('href', '{cfgAllImg}objects/{strFileNameBig}');

        $("a[rel=pic_group]").fancybox({
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'titlePosition' : 'over',
            padding: 3,
            margin: 5
        });
    });
</script>
