<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:5px"><b>ОБЪЯВЛЕНИЕ</b></p>

<div class="cnt">
<if is.ad.info>
<table cellPadding="5" cellSpacing="0" width="100%" style="margin-top:20px;border-top:1px solid #F9C36D;" class="vtop">
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Раздел</td>
    <td class="tdR1">{strChapter} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Заголовок</td>
    <td class="tdR1"><b>{strTitle}</b> &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Текст объявления</td>
    <td class="tdR1">{strAd} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Контактное лицо</td>
    <td class="tdR1">{strContacts} &nbsp;</td>
  </tr>
</table>
</if is.ad.info>

<if no.ad.info>
<p class="err">Нет информации для просмотра</p>
</if no.ad.info>

<p><a href="{cfgUrlSite}add.ad">добавить новое объявление</a></p>
<p><a href="{urlLinkBack}">вернуться к списку объявлений</a></p>

</div>