<if article.archive>
<div class="cnt">
<span class="txt-bb" style="margin-left:10px; font-size:14px;">Все статьи раздела "Аналитика"</span>
{blockPaginator}
</div>

<table cellPadding="0" cellSpacing="0" width="100%" class="vtop" style="margin-top:10px">
<loop last.articles>
  <tr>
    <td width="25"><img src="{cfgSiteImg}content/item.long.arrow.gif" border="0" style="margin:6px 7px 3px 0;"></td>
    <td style="border-bottom:1px solid #EEEEEE">
      <table cellPadding="0" cellSpacing="0" width="100%" style="margin:3px 0 3px 0">
        <tr>
          <td>
            <a href="{cfgUrlSite}analitika/{pa_id}" class="link-blue"><span class="txt-bb" style="font-family:Tahoma;color:#9E9E9E">{strDatePublicArticle}</span> <span class="txt-bb" style="font-family:Tahoma;">{pa_title}</span> </a>
            <p class="txt-block">{pa_anons} <a href="{cfgUrlSite}analitika/{pa_id}" class="link-blue">Подробнее...</a></p>
          </td>
<!-- if article.picture.{sn_id}>
          <td width="110" align="left"><a href="{cfgUrlSite}news/{sn_id}"><img src="{cfgAllImg}news/news.photo.{sn_id}.jpg" border="0"></a></td>
</i f article.picture.{sn_id} -->
        </tr>
      </table>

    </td>
  </tr>
</loop last.articles>
</table>

<div class="cnt">
{blockPaginator}
</div>
</if article.archive>

<if one.article>
<div class="cnt">
<!-- i f picture>
<a href="#" onClick="window.open('{cfgAllImg}analitic/article.photo.{sn_id}.b.jpg', 'photoNews{sn_id}', 'width=430, height=300')">
<img src="{cfgAllImg}analitic/article.photo.{sn_id}.b.jpg" width="200" border="0" align="right" style="margin:0 0 5px 10px"></a>
</i f picture -->
<span class="sm-gyl">{strDatePublArticle}</span>
<p class="title1" style="margin-top:15px">{pa_title}</p>
<p style="margin:12px 0 4px 0"><b>{strAnonsBody}</b></p>
<p style="margin:12px 0 4px 0">{strArticleBody}</p>

<if is.attach.files>
<div align="right">
<p style="margin:2px 0 3px 0">Прикреплённые файлы:</p>
<loop attach.files>
<a href="javascript:void(0)" onClick="jsShowModal('{cfgUrlSite}analitika/download/{paf_id}', 780, 580, 'yes', 'yes', 'Analitika{paf_id}')" class="link-blue-over">{paf_title}</a><br />
</loop attach.files>
</div>
</if is.attach.files>

<p><a href="{cfgUrlSite}analitika?stPage={stPage}">к списку статей</a></p>
</div>
</if one.article>