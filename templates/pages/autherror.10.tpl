<table cellspacing="0" cellpadding="0" width="100%">
  <tbody>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>
      <div style="COLOR: #db2828; FONT-FAMILY: Verdana"><strong style="FONT-SIZE: 12px; LINE-HEIGHT: 20px">Ошибка авторизации</strong>.<br />
      Вероятно Вы ввели неправильный логин и/или пароль.<br />
      Сверьте данные с письмом, которое Вы получали при регистрации.<br />
      Проверьте также раскладку клавиатуры, <strong>логин</strong> и <strong>пароль</strong> должны состоять только из латинских символов, и возможно цифр.<br />
      Проверьте, не включён ли &quot;<strong>Caps Lock</strong>&quot; на клавиатуре, регистр символов также важен при авторизации. </div>
      </td>
    </tr>
  </tbody>
</table>