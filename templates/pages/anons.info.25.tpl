<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:5px"><b>АНОНС МЕРОПРИЯТИЯ</b></p>

<div class="cnt">
<if is.anons.info>
<table cellPadding="5" cellSpacing="0" width="100%" style="margin-top:20px;border-top:1px solid #F9C36D;" class="vtop">
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Заголовок </td>
    <td class="tdR1"><b>{strTitle}</b> &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Дата, время проведения</td>
    <td class="tdR1">{strTime} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Организатор, Контактное лицо</td>
    <td class="tdR1">{strContacts} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Место проведения</td>
    <td class="tdR1">{strAddress} &nbsp;</td>
  </tr>
  <tr>
    <td class="tdL1" width="25%" bgcolor="#EFEFEF">Полезная информация</td>
    <td class="tdR1">{strInfo} &nbsp;</td>
  </tr>
</table>
</if is.anons.info>

<if no.anons.info>
<p class="err">Нет информации для просмотра</p>
</if no.anons.info>

<p><a href="{cfgUrlSite}add.anons">добавить новый анонс</a></p>
<p><a href="{urlLinkBack}">вернуться к списку анонсов</a></p>

</div>