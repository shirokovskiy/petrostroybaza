<p class="title3" style="margin-left:25px;margin-top:10px;margin-bottom:0;"><b>ДОБАВИТЬ ОБЪЯВЛЕНИЕ</b></p>

<div class="cnt">
	<if error><div class="errBlock">{errMsg}</div></if error>
	<if success>
		<center>
			<p><span class="success">{msgOk}</span><br>
				<span class="sm-gr">Вся публикуемая Вами информация будет проверена модераторами сайта, прежде чем появится в списках объявлений!</span></p>
			<p><a href="{cfgUrlSite}add.technic">Хотите добавить ещё?</a></p>
		</center>
	</if success>

	<if post.data>
		<form action="?new=record" method="POST">
			<input type="hidden" id="frmAddTechnic" name="frmAddTechnic" value="true"/>
			<table cellPadding="5" cellSpacing="0" width="100%" style="border-top:1px solid #F9C36D;" class="vtop">
				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Тип объявления</td>
					<td class="tdR1">
						<select id="sbxAdType" name="sbxAdType" class="flat" style="width:90%;{styleAdType}">
							<option value="">выберите...</option>
							<option value="rent"{rent_sel}>Аренда сниму</option>
							<option value="lease"{lease_sel}>Аренда сдам</option>
							<option value="sell"{sell_sel}>Продам</option>
							<option value="buy"{buy_sel}>Куплю</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Заголовок объявления</td>
					<td class="tdR1">
						<input type="text" id="strTitle" name="strTitle" class="flat" value="{strTitle}" maxlength=255 style="width:100%;{styleTitle}" />
					</td>
				</tr>
				<tr>
					<td class="tdL1" colspan="2" bgcolor="#EFEFEF"><b>Параметры техники</b></td>
				</tr>
				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Тип техники</td>
					<td class="tdR1">
						<select id="sbxTechTypeID" name="intTechTypeID" class="flat" style="width:90%;{styleTechTypeID}">
							<option value="">выберите...</option>
							<loop technic.types>
								<option value="{ptt_id}"{sel}>{ptt_name}</option>
							</loop technic.types>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Срок аренды</td>
					<td class="tdR1">
						<input type="text" id="strRentPeriod" name="strRentPeriod" class="flat" value="{strRentPeriod}" maxlength=80 style="width:240px;{styleRentPeriod}">
					</td>
				</tr>
				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Адрес проведения работ</td>
					<td class="tdR1">
						<input type="text" id="strAddressWorks" name="strAddressWorks" class="flat" value="{strAddressWorks}" maxlength=80 style="width:90%;{styleAddressWorks}">
					</td>
				</tr>
				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Производитель / Марка</td>
					<td class="tdR1">
						<input type="text" id="strMarka" name="strMarka" class="flat" value="{strMarka}" maxlength=80 style="width:240px;{styleMarka}">
					</td>
				</tr>
				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Модель</td>
					<td class="tdR1">
						<input type="text" id="strModel" name="strModel" class="flat" value="{strModel}" maxlength=128 style="width:240px;{styleModel}">
					</td>
				</tr>
				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Год выпуска</td>
					<td class="tdR1">
						<input type="text" id="strYear" name="strYear" class="flat" value="{strYear}" maxlength=4 style="width:50px;{styleYear}" />
					</td>
				</tr>

				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Технические характеристики и другая полезная информация </td>
					<td class="tdR1">
						<textarea name="strDopinfo" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;{styleDopinfo}" class="flat">{strDopinfo}</textarea>
					</td>
				</tr>

				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Координаты и контакты</td>
					<td class="tdR1">
						<textarea name="strContacts" style="width:90%;height:46px;overflow-y:visible;word-wrap:break-word;{styleContacts}" class="flat">{strContacts}</textarea>
					</td>
				</tr>

				<tr>
					<td class="tdL1" width="25%" bgcolor="#EFEFEF">Введите символы с изображения</td>
					<td class="tdR1">
						<!--CAPTCHA-->
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td style="padding-top:1px"><img src="?get_img=captcha" border="0"></td>
								<td style="padding-left:10px"><input type="text" name="strCodeString" class="flat" maxlength="4" style="width:56px;font-size:12px;letter-spacing:5px;{styleCodeString}" onKeyUp="if(this.value.length >= this.maxLength) this.blur(); return false;"></td>
							</tr>
						</table>
						<!--//CAPTCHA-->
					</td>
				</tr>

				<tr>
					<td colspan="2" align="right">
						<input type="submit" name="submit" class="standart" value="Отправить на проверку">
					</td>
				</tr>
			</table>
		</form>
	</if post.data>
	<p><a href="{cfgUrlSite}technics/{strChapter}?stPage={stPage}">вернуться к списку техники</a></p>

</div>