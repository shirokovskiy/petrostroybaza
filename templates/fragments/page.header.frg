<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>{m_title}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="{m_keywords}" />
    <meta name="description" content="{m_description}" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="all" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="classification" content="Недвижимость" />
    <meta name="Document-state" content="Dynamic" />
    <meta name="author" content="www.phpwebstudio.com" />
    <meta name="mailru-domain" content="CLnkR0iHD2d7fvp8" />
    <link rel="icon" href="{cfgSiteImg}favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="{cfgSiteImg}favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{cfgUrlSite}styles/general.css" />
    <link rel="stylesheet" type="text/css" href="{cfgUrlSite}styles/jquery.jqzoom.css" />
    <link rel="stylesheet" type="text/css" href="{cfgUrlSite}styles/br.{strUserAgent}.css" />
<if is.maps.pages>
    <script type="text/javascript" src="//api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
</if is.maps.pages>
    <script type="text/javascript" src="{cfgSiteUrl}js/gui.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}js/jquery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}js/jquery/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
	<script type="text/javascript" src="{cfgSiteUrl}js/jquery/fancybox/jquery.fancybox-1.3.1.js"></script>
	<script type="text/javascript" src="{cfgSiteUrl}js/swfobject/swfobject.js"></script>
	<script type="text/javascript" src="{cfgSiteUrl}js/jquery/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="{cfgSiteUrl}js/jquery/jquery.tools.min.js"></script>
	<script type="text/javascript" src="{cfgSiteUrl}js/jquery/jqzoom/jquery.jqzoom-core-pack.js"></script>
	<script type="text/javascript" src="{cfgSiteUrl}js/jquery/jquery-ui-1.10.4.custom.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{cfgSiteUrl}js/jquery/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{cfgSiteUrl}styles/skins/tango/skin.css" />
	<link rel="stylesheet" type="text/css" href="{cfgSiteUrl}styles/jquery.ui/jquery-ui-1.10.4.custom.min.css" />

    <script type="text/javascript">
//        var mapNote = false;

        jQuery(document).ready(function(){
            $(".tip").tooltip({ show: { effect: "slideDown", duration: 300 }, position: { my: "center top+5"} });
<if is.maps.pages>
            if (typeof ymaps !== "undefined")
            {
                ymaps.ready(function () {
                    $('.maps').show().click(function () {
                        var address = $(this).attr('data-address');

                        if (!isEmpty(address)) {
//                        if (!mapNote) {
//                            $.fancybox("<strong>ВНИМАНИЕ!</strong> Скорость загрузки карты зависит от сервиса &laquo;Яндекс.Карты&raquo;.<br/>Дождитесь полной загрузки изображения при изменении масштаба.<br/>Если Вы заметили несоответствие адреса объекта на карте,<br/>не постесняйтесь сообщить об этом администрации сайта.");
//                            mapNote = true;
//                        }
                            $('#mapBox').show();
                            showEstateObjectMap(address);
                        }
                    });
                });
            }


            $('#mapBox .close').click(function () {
                $('#map').empty();
                $('#mapBox').hide();
            });
</if is.maps.pages>
        });
    </script>

    <if show.ga>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-61504760-1', 'auto');
        ga('send', 'pageview');
    </script>
    </if show.ga>
</head>

<body bgcolor="#FEFEFE" text="#333333" link="#777777" vlink="#696969" alink="#3E3E3E" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<script language="JavaScript" src="{cfgSiteUrl}js/wz_tooltip.js"></script>
