<style type="text/css">
.banner-area {width:380px;height:80px;text-align:center;vertical-align:middle;font:18px Tahoma bold;float:left;margin-left:10px;margin-bottom:7px;overflow: hidden;box-shadow: 0 0 2px #DFDFDF}
.banner-area.empty {font:18px Tahoma bold; color: #DFDFDF}
.banner-area.first {margin-left:0}
</style>
<!-- TOP BANNER-->
<div>
<if show.top_l.banner.dummy>
    <div class="banner-area empty first">
        Banner 380 x 80<br>
        Advertisement Area still empty<br />
        Здесь может быть ваша реклама
    </div>
</if show.top_l.banner.dummy>

<if show.top_l.banner>
    <div class="banner-area first">{contentTopLBanner}</div>
</if show.top_l.banner>

<!-- /// -->

<if show.top.banner.dummy>
  <div class="banner-area empty">
  Banner 380 x 80<br>
  Advertisement Area still empty<br />
  Здесь может быть ваша реклама
  </div>
</if show.top.banner.dummy>

<if show.top.banner>
    <div class="banner-area">{contentTopBanner}</div>
</if show.top.banner>

<!-- /// -->

<if show.top_r.banner.dummy>
  <div class="banner-area empty">
  Banner 380 x 80<br>
  Advertisement Area still empty<br />
  Здесь может быть ваша реклама
  </div>
</if show.top_r.banner.dummy>

<if show.top_r.banner>
    <div class="banner-area">{contentTopRBanner}</div>
</if show.top_r.banner>

    <div style="clear:both"></div>
</div>
<!-- // TOP BANNER-->
