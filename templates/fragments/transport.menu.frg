<table cellPadding="0" cellSpacing="0" width="100%" class="vtop">
	<tr>
		<td width="25"><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" style="margin:3px 7px 3px 11px;" /></td>
		<td style="padding-bottom:7px"><a href="{cfgUrlSite}technics" class="link-topmenu-l tip" style="font-size:15px" title="В нашей базе уже {intTechCounts} объявлений">Аренда техники</a></td>
	</tr>
	<tr>
		<td width="25"><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" style="margin:3px 7px 3px 11px;" /></td>
		<td style="padding-bottom:7px"><a href="{cfgUrlSite}add.technic" class="link-blue" style="font-size:12px">Добавить объявление</a></td>
	</tr>
</table>

<div class="icons-block">
	<div style="text-align:center;">
		<a href="{cfgUrlSite}technics/avtokran"><img id="avtokran" src="{cfgSiteImg}avtokran_.gif" onMouseOver="showTooltip('avtokran_tooltip')" /></a>
		<div class="tooltip border-radius-10" id="avtokran_tooltip" style="display: none">
			<h3>Автокраны ({intTechCounts_avtokran})</h3>
			<p><a href="{cfgUrlSite}technics/avtokran/lease">сдам в аренду</a></p>
			<p><a href="{cfgUrlSite}technics/avtokran/rent">возьму в аренду</a></p>
			<p><a href="{cfgUrlSite}technics/avtokran/sell">продам</a></p>
			<p><a href="{cfgUrlSite}technics/avtokran/buy">куплю</a></p>
			<p class="ads-link"><a href="{cfgUrlSite}add.technic/avtokran">добавить объявление</a></p>
		</div>
	</div>
	<div style="padding:5px;">
		<div style="float:left;">
			<a href="{cfgUrlSite}technics/samosval"><img id="gruzovik" src="{cfgSiteImg}gruzovik_.gif" onMouseOver="showTooltip('samosval_tooltip');" /></a>
			<div class="tooltip border-radius-10" id="samosval_tooltip" style="display: none">
				<h3>Самосвалы ({intTechCounts_samosval})</h3>
				<p><a href="{cfgUrlSite}technics/samosval/lease">сдам в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/samosval/rent">возьму в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/samosval/sell">продам</a></p>
				<p><a href="{cfgUrlSite}technics/samosval/buy">куплю</a></p>
				<p class="ads-link"><a href="{cfgUrlSite}add.technic/samosval">добавить объявление</a></p>
			</div>
		</div>
		<div style="float:right;">
			<a href="{cfgUrlSite}technics/pogruzchik"><img id="grejder" src="{cfgSiteImg}grejder_.gif" onMouseOver="showTooltip('pogruzchik_tooltip');" /></a>
			<div class="tooltip border-radius-10" id="pogruzchik_tooltip" style="display: none">
				<h3>Погрузчики ({intTechCounts_pogruzchik})</h3>
				<p><a href="{cfgUrlSite}technics/pogruzchik/lease">сдам в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/pogruzchik/rent">возьму в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/pogruzchik/sell">продам</a></p>
				<p><a href="{cfgUrlSite}technics/pogruzchik/buy">куплю</a></p>
				<p class="ads-link"><a href="{cfgUrlSite}add.technic/pogruzchik">добавить объявление</a></p>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div style="padding:5px;">
		<div style="float:left;">
			<a href="{cfgUrlSite}technics/dortech"><img id="katok" src="{cfgSiteImg}katok_.gif" onMouseOver="showTooltip('dortech_tooltip')" /></a>
			<div class="tooltip border-radius-10" id="dortech_tooltip" style="display: none">
				<h3>Дорожная техника ({intTechCounts_dortech})</h3>
				<p><a href="{cfgUrlSite}technics/dortech/lease">сдам в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/dortech/rent">возьму в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/dortech/sell">продам</a></p>
				<p><a href="{cfgUrlSite}technics/dortech/buy">куплю</a></p>
				<p class="ads-link"><a href="{cfgUrlSite}add.technic/dortech">добавить объявление</a></p>
			</div>
		</div>
		<div style="float:right;">
			<a href="{cfgUrlSite}technics/buldoser"><img id="ggrejder" src="{cfgSiteImg}ggrejder_.gif" onMouseOver="showTooltip('buldoser_tooltip')" /></a>
			<div class="tooltip border-radius-10" id="buldoser_tooltip" style="display: none">
				<h3>Бульдозеры ({intTechCounts_buldoser})</h3>
				<p><a href="{cfgUrlSite}technics/buldoser/lease">сдам в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/buldoser/rent">возьму в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/buldoser/sell">продам</a></p>
				<p><a href="{cfgUrlSite}technics/buldoser/buy">куплю</a></p>
				<p class="ads-link"><a href="{cfgUrlSite}add.technic/buldoser">добавить объявление</a></p>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div style="text-align:center;">
		<a href="{cfgUrlSite}technics/ekskavator"><img id="kovsh" src="{cfgSiteImg}ekskavator.gif" onMouseOver="showTooltip('ekskavator_tooltip')" /></a>
		<div class="tooltip border-radius-10" id="ekskavator_tooltip" style="display: none">
			<h3>Экскаваторы ({intTechCounts_ekskavator})</h3>
			<p><a href="{cfgUrlSite}technics/ekskavator/lease">сдам в аренду</a></p>
			<p><a href="{cfgUrlSite}technics/ekskavator/rent">возьму в аренду</a></p>
			<p><a href="{cfgUrlSite}technics/ekskavator/sell">продам</a></p>
			<p><a href="{cfgUrlSite}technics/ekskavator/buy">куплю</a></p>
			<p class="ads-link"><a href="{cfgUrlSite}add.technic/ekskavator">добавить объявление</a></p>
		</div>
	</div>
	<div style="padding:5px 18px 5px 18px;">
		<div style="float:left;">
			<a href="{cfgUrlSite}technics/kran"><img id="kran" src="{cfgSiteImg}kran_.gif" onMouseOver="showTooltip('kran_tooltip')" /></a>
			<div class="tooltip border-radius-10" id="kran_tooltip" style="display: none">
				<h3>Краны башенные ({intTechCounts_kran})</h3>
				<p><a href="{cfgUrlSite}technics/kran/lease">сдам в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/kran/rent">возьму в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/kran/sell">продам</a></p>
				<p><a href="{cfgUrlSite}technics/kran/buy">куплю</a></p>
				<p class="ads-link"><a href="{cfgUrlSite}add.technic/kran">добавить объявление</a></p>
			</div>
		</div>
		<div style="float:right;">
			<a href="{cfgUrlSite}technics/pnevmotech"><img id="gkran" src="{cfgSiteImg}gkran_.gif" onMouseOver="showTooltip('pnevmotech_tooltip')" /></a>
			<div class="tooltip border-radius-10" id="pnevmotech_tooltip" style="display: none">
				<h3>Свайные установки ({intTechCounts_pnevmotech})</h3>
				<p><a href="{cfgUrlSite}technics/pnevmotech/lease">сдам в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/pnevmotech/rent">возьму в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/pnevmotech/sell">продам</a></p>
				<p><a href="{cfgUrlSite}technics/pnevmotech/buy">куплю</a></p>
				<p class="ads-link"><a href="{cfgUrlSite}add.technic/pnevmotech">добавить объявление</a></p>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div style="padding:5px">
		<div style="float:left;">
			<a href="{cfgUrlSite}technics/robots"><img id="robots" src="{cfgSiteImg}robots.gif" onMouseOver="showTooltip('robots_tooltip')" /></a>
			<div class="tooltip border-radius-10" id="robots_tooltip" style="display: none">
				<h3>Роботы для демонтажных и строительных работ ({intTechCounts_robots})</h3>
				<p><a href="{cfgUrlSite}technics/robots/lease">сдам в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/robots/rent">возьму в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/robots/sell">продам</a></p>
				<p><a href="{cfgUrlSite}technics/robots/buy">куплю</a></p>
				<p class="ads-link"><a href="{cfgUrlSite}add.technic/robots">добавить объявление</a></p>
			</div>
		</div>
		<div style="float:right;margin-left:5px">
			<a href="{cfgUrlSite}technics/naves"><img id="naves" src="{cfgSiteImg}naves.gif" onMouseOver="showTooltip('naves_tooltip')" /></a>
			<div class="tooltip border-radius-10" id="naves_tooltip" style="display: none">
				<h3>Навесное и дополнительное оборудование ({intTechCounts_naves})</h3>
				<p><a href="{cfgUrlSite}technics/naves/lease">сдам в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/naves/rent">возьму в аренду</a></p>
				<p><a href="{cfgUrlSite}technics/naves/sell">продам</a></p>
				<p><a href="{cfgUrlSite}technics/naves/buy">куплю</a></p>
				<p class="ads-link"><a href="{cfgUrlSite}add.technic/naves">добавить объявление</a></p>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	// make all tooltips opacity = 0
	jQuery('.tooltip').mouseleave(function(){
		jQuery(this).delay(800).fadeOut(600);
	});
});

function showTooltip( name ) {
//	jQuery('.tooltip').hide();
//	jQuery('#'+name).show();
	jQuery('.tooltip').hide();
	jQuery('#'+name).fadeIn();
}
</script>
