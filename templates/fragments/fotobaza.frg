<div id="fotobasa">
	<div class="block-left">
		<div class="box-fotobank border-radius-5">
			<div class="head-box border-radius-5">
				<h1>Фотобанк</h1>
				<p class="p1">стройплощадки</p>
				<p class="p2">техника</p>
				<p class="p3">мероприятия</p>
			</div>
			<div class="search-box clearer"><form id="search_fotobank_form" name="search_fotobank_form" method="POST">
				<label>Поиск по разделу </label>
				<input type="text" id="search_fotobank" name="search_fotobank" value="{search_fotobank}" class="flat" maxlength="255"/>
                <input type="submit" value="go" style="display: none;" />
				<img id="search_fotobank_btn" src="{cfgSiteImg}content/search_right_btn.png" alt="Искать" title="Искать" class="clearer"/>
			</form>
            <a name="searchFotobank"></a>
            <script type="text/javascript">
                //<![CDATA[
                jQuery(document).ready(function ($) {
                    $('#search_fotobank_btn').click(function () {
                        if (window.location.href.indexOf('#')<=0)
                            window.location.href += '#searchFotobank';
                        $('#search_fotobank_form').submit();
                    });
                });
                //]]>
            </script>
			</div>
<if no.search.fotobank>
            <p class="err">По данному запросу ничего не найдено!</p>
</if no.search.fotobank>
			<div id="fotolenta" class="vertical-scrollable">
				<div class="items">
					<div>
						<loop fotobank.groups>
							<if items.set.{sig_id}>
					</div><div>
							</if items.set.{sig_id}>
						<div class="fotorecord clearer">
							<a href="{cfgSiteUrl}fotobank/{sig_id}"><img src="{cfgAllImg}fotobank/groups/{sig_id}/t_h_u_m_b_{si_filename}" alt="{strGroupTitle}" title="{strGroupTitle}" /></a>
							<p><span class="date">{strGroupDate}</span><br /><a href="{cfgSiteUrl}fotobank/{sig_id}" class="">{strGroupTitle}</a></p>
						</div>
						</loop fotobank.groups>
					</div>
				</div>
			</div>
<if enough.groups>
			<div id="fotobank_arrows" class="actions">
				<a class="prev">&laquo; предыдущие</a>&nbsp;<a class="next">следующие &raquo;</a>
			</div>

			<script type="text/javascript">
				//<![CDATA[
				jQuery(document).ready(function ($) {
					$("#fotolenta").scrollable({ vertical: true });
				});
				//]]>
			</script>
</if enough.groups>
		</div>
	</div>



	<div class="block-right clearer">
		<div class="box-cartoons border-radius-10">
			<div class="head-box border-radius-5">
				<h1>Cartoon <span>Baza</span></h1>
				<p class="p3">карикатуры</p>
			</div>
			<div class="search-box clearer"><form id="search_cartoonbaza_form" name="search_cartoonbaza_form" method="POST">
				<label>Поиск по разделу </label>
				<input type="text" id="search_cartoonbaza" name="search_cartoonbaza" value="{search_cartoonbaza}" class="flat" maxlength="255"/>
                <input type="submit" value="go" style="display: none;" />
				<img id="search_cartoonbaza_btn" src="{cfgSiteImg}content/search_right_btn.png" alt="Искать" title="Искать" class="clearer"/>
			</form>
            <a name="searchCartoonBaza"></a>
            <script type="text/javascript">
                //<![CDATA[
                jQuery(document).ready(function ($) {
                    $('#search_cartoonbaza_btn').click(function () {
                        if (window.location.href.indexOf('#')<=0)
                            window.location.href += '#searchCartoonBaza';
                        $('#search_cartoonbaza_form').submit();
                    });
                });
                //]]>
            </script>
			</div>
<if no.search.cartoons>
            <p class="err">По данному запросу ничего не найдено!</p>
</if no.search.cartoons>
			<div id="cartoonlenta" class="vertical-scrollable">
				<div class="items">
					<div>
<loop cartoonbaza.list>
						<if items.set.{si_id}>
					</div><div>
						</if items.set.{si_id}>
						<div class="fotorecord item">
							<a href="{cfgSiteUrl}foto/cartoonbaza/{strCartoonDate}/{strFilename}"><img src="{cfgAllImg}fotobank/cartoonbaza/t_h_u_m_b_{si_filename}" alt="{si_title}" title="{si_title}" /></a>
						</div>
</loop cartoonbaza.list>
					</div>
				</div>
			</div>
<if enough.cartoons>
			<div id="cartoon_arrows" class="actions">
				<a class="prev">&laquo; предыдущие</a>&nbsp;<a class="next">следующие &raquo;</a>
			</div>

			<script type="text/javascript">
				//<![CDATA[
				jQuery(document).ready(function ($) {
					$("#cartoonlenta").scrollable({ vertical: true });
				});
				//]]>
			</script>
</if enough.cartoons>
		</div>
	</div>
</div>

