<script language="JavaScript" type="text/javascript">
<!--
function showExtSearch () {
    showBlock('moreSearchFields');
    hideBlock('linkSearchFields');
}

function hideExtSearch () {
    hideBlock('moreSearchFields');
    showBlock('linkSearchFields');
}

/**
 * Send request to get regions
 */
function getAdmRegions() {
    var urlApi = '/lib/api/check.adm.regions.js';
    urlApi += '?unique='+getUnique();

    var selectedRegions = [];
    $("input[name='cbxRegion[]']:checked").each(function(){
        selectedRegions.push($(this).val());
    });

    //
    var senData = {
        'get'   :'admreg'
        , 'rid' :selectedRegions
    };

//                            var arrRegs = {1:"СПб:", 2:"ЛО:", 3:"Мск:", 4:"МО:"};

    $("#regionid").empty().removeAttr('multiple').attr('title', "Район / Округ можно выбрать указав Регион")
            .append('<option id="zerorgn" selected="selected" value="">регион не выбран</option>');

    if(selectedRegions.length>0) {
        $("#zerorgn").remove();
        $.post(urlApi, senData, function(json, status){
            // get answer here
            if (status == 'success') {
                if (json.regions && json.regions.length > 0) {
                    $('#regionid').attr('multiple', 'multiple').attr('title', "Для выбора нескольких пунктов удерживайте кнопку Ctrl");
                    $.each(json.regions, function(key, optionSet) {
                        $('#regionid')
                                .append($("<option></option>")
                                        .attr("value", optionSet.sra_id)
                                        .attr("selected", function(){
                                            if (preselectedAdmReg.length > 0) {
                                                for(var j=0; j<preselectedAdmReg.length; j++) {
                                                    if(optionSet.sra_id == preselectedAdmReg[j]) {
                                                        return true;
                                                    }
                                                }
                                            }
                                            return false;
                                        } )
                                        .text(optionSet.sra_name));
                    });
                }
            } else {
                alert('Ошибка сервера!');
            }
        }, 'json');
    }
}

$(function(){
    $("#object_etap").keypress(function(){
        if ($("#popup-etap-list").is(':visible')) {
            $("#popup-etap-list").hide();
        }
    });
    $("#object_etap").click(function(){
        $("#popup-etap-list").toggle();
    });
    $("#popup-etap-list>ul>li").click(function(){
        $("#object_etap").val( $(this).text() );
        $("#popup-etap-list").toggle();
    });
    //-----
    $("#object_karkas").keypress(function(){
        if ($("#popup-karkas-list").is(':visible')) {
            $("#popup-karkas-list").hide();
        }
    });
    $("#object_karkas").click(function(){
        $("#popup-karkas-list").toggle();
    });
    $("#popup-karkas-list>ul>li").click(function(){
        $("#object_karkas").val( $(this).text() );
        $("#popup-karkas-list").toggle();
    });
    $("#resetSearch").click(function(){
        gotoUrl("/search?reset=results");
    });
});
//-->
</script>


<!-- Поиск объектов недвижимости -->
<form id="search_objects_form" action="/search" method="post"><input type="hidden" name="frmSearchObjects" value="1"/>
<table id="form_table">
    <tr><td class="title"><span class="txt-bb">Поиск объектов:</span></td></tr>

    <tr>
        <td style="padding:2px">

            <table cellSpacing="4">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="padding-left:2px">ID объекта</td>
                                <td><input type="text" id="intObjID" name="intObjID" class="flat" value="{intObjID}" maxlength="7" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td>Тип недвижимости</td></tr>
                <tr>
                    <td id="cell-estate-type">
                        <table>
                            <loop chapter>
                            <tr>
                                <td valign="top"><input type="checkbox" id="estateType{set_id}" name="type[]" value="{set_id}" {sel} /></td>
                                <td>{set_name}</td>
                            </tr>
                            </loop chapter>
                        </table>
                    </td>
                </tr>

                <tr><td style="padding-top:7px">Наименование объекта</td></tr>
                <tr><td><input type="text" name="strObjectNameSearch" class="flat" value="{strObjectNameSearch}" maxlength="80" /></td></tr>
            </table>

            <table cellSpacing="4" id="moreSearchFields">
                <tr>
                    <td>
                        <loop list.regions>
                            <input type="checkbox" id="cbxRegion{sr_id}" name="cbxRegion[]" value="{sr_id}" {cbxRegionSel} /> <label class="cbx-search-label"
                                for="cbxRegion{sr_id}">{sr_name}</label><br>
                        </loop list.regions>
                    </td>
                </tr>


                <tr><td>Район / Округ</td></tr>
                <tr><td><select name="regionid[]" id="regionid" class="flat tip" title="Район / Округ можно выбрать указав Регион"></select>
                    <script type="text/javascript">
                        //<![CDATA[
                        var preselectedAdmReg = {preselectedAdmReg};

                        jQuery(document).ready(function ($) {
                            $("input[name='cbxRegion[]']").each(function(){
                                $(this).click(function(){
                                    getAdmRegions();
                                });
                            });

                            getAdmRegions();
                        });
                        //]]>
                    </script>
                    </td>
                </tr>

                <tr><td>Другой район</td></tr>
                <tr><td><input type="text" name="region_other" class="flat" value="{region_other}" maxlength="80" /></td></tr>

                <tr><td>Адрес</td></tr>
                <tr><td><input type="text" name="strObjectAddressSearch" class="flat" value="{strObjectAddressSearch}" maxlength="80" /></td></tr>

                <tr><td>Вид строительства</td></tr>
                <tr><td><select name="vid" class="flat">
                            <option value="">...</option>
<loop vid>                  <option value="{sev_id}"{sel}>{sev_name}</option>
</loop vid>
                        </select>
                    </td>
                </tr>

                <tr><td>Этап строительства</td></tr>
                <tr><td><div class="search-block-etap">
                            <input type="text" id="object_etap" name="object_etap" class="flat" value="{object_etap}" maxlength="128" />
                            <div class="search-popup-block-etap" id="popup-etap-list">
                                <ul>
<loop list.etaps>                   <li>{etap}</li>
</loop list.etaps>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>

                <tr><td>Каркас</td></tr>
                <tr><td><div class="search-block-etap">
                            <input type="text" id="object_karkas" name="strKarkasSearch" class="flat" value="{strKarkasSearch}" maxlength="128" />
                            <div class="search-popup-block-etap" id="popup-karkas-list">
                                <ul>
<loop list.karkas>                   <li>{karkas}</li>
</loop list.karkas>
                                </ul>
                            </div>
                        </div>
                </td></tr>

                <tr><td>Имя/ИНН компании</td></tr>
                <tr><td><input type="text" name="strCompanyNameSearch" class="flat" value="{strCompanyNameSearch}" maxlength="80" /></td></tr>

                <tr><td>Сроки строительства</td></tr>
                <tr><td><select name="year" class="flat">
                            <option value="">...</option>
<loop periods>              <option value="{iYear}" {sel}>{iYear}</option>
</loop periods>         </select>
                    </td>
                </tr>

                <tr><td>Актуальность</td></tr>
                <tr><td><select name="period" class="flat">
                            <option value="">...</option>
                            <option value="5"{option5}>до месяца</option>
                            <option value="1"{option1}>до 3 месяцев</option>
                            <option value="2"{option2}>до 6 месяцев</option>
                            <option value="3"{option3}>до 12 месяцев</option>
                            <option value="4"{option4}>старше 12 месяцев</option>
                        </select>
                    </td>
                </tr>

                <tr><td align="center"><a href="javaScript:void(0)" onClick="hideExtSearch()">свернуть поиск</a></td></tr>
            </table>

            <table cellSpacing="4" id="linkSearchFields">
                <tr><td align="center"><a href="javaScript:void(0)" onClick="showExtSearch()" class="link-dy">расширенный поиск</a></td></tr>
            </table>

            <table cellSpacing="4">
                <tr><td align="center"><input type="submit" name="submit" class="btn-orange finger" value="искать" /> <input type="button" name="reset" class="btn-orange finger" id="resetSearch" value="сбросить" /></td></tr>
            </table>

            <table cellSpacing="4" id="listOfSearchLinks">
                <tr>
                    <td><img src="{cfgSiteImg}map3.png" style="margin-left: 7px" /></td>
                    <td><a href="{cfgSiteUrl}objects-map" class="link-dy">Карта объектов</a></td>
                </tr>
                <tr>
                    <td><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" /></td>
                    <td><a href="{cfgSiteUrl}companies">Все компании</a></td>
                </tr>
                <tr>
                    <td><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" /></td>
                    <td><a href="{cfgSiteUrl}companies/genpod">Генподрядчики</a></td>
                </tr>
                <tr>
                    <td><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" /></td>
                    <td><a href="{cfgSiteUrl}companies/proekt">Проектировщики</a></td>
                </tr>
                <tr>
                    <td><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" /></td>
                    <td><a href="{cfgSiteUrl}companies/zak">Заказчики</a></td>
                </tr>
                <tr>
                    <td><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" /></td>
                    <td><a href="{cfgSiteUrl}companies/zas">Застройщики</a></td>
                </tr>
                <tr>
                    <td><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" /></td>
                    <td><a href="{cfgSiteUrl}companies/inv">Инвесторы</a></td>
                </tr>
                <tr>
                    <td><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" /></td>
                    <td><a href="{cfgSiteUrl}companies/pod">Подрядчики</a></td>
                </tr>
                <tr>
                    <td><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" /></td>
                    <td><a href="{cfgSiteUrl}companies/opa">Отделы продаж и аренды</a></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>

<if js.show.full.search>
<script language="JavaScript" type="text/javascript">
<!--
showExtSearch();
//-->
</script>
</if js.show.full.search>
