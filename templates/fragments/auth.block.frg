<if no.auth>
<div class="auth-block">
    <form method="POST">
        <input type="hidden" name="frmAuthUser" value="true"/>
        <table cellPadding="1" cellSpacing="1">
            <tr><td class="txt-bb">Логин</td>
                <td><input type="text" name="strLoginAuth" class="flat" value="{strLoginAuth}" maxlength=80></td>
            </tr>
            <tr><td class="txt-bb">Пароль</td>
                <td><input type="password" name="strPasswAuth" class="flat" maxlength=80></td>
            </tr>
            <tr><td colspan="2" align="right"><input type=image src="{cfgSiteImg}left/btn.auth.enter2.gif" name="submitAuth" /></td></tr>
            <tr><td colspan="2"><a href="{cfgUrlSite}register" class="link-darkblue"><b>Регистрация</b></a></td></tr>
            <tr><td colspan="2"><a href="{cfgUrlSite}recovery" class="link-darkblue"><b>Восстановление пароля</b></a></td></tr>
        </table>
    </form>
</div>
</if no.auth>

<if authed>
<div class="auth-block authed-success">
    <div id="account_info_box">
        <table cellPadding="0" cellSpacing="0" width="100%">
            <tr><td><span class="welcome">Добро пожаловать,</span>
                    <h3 class="txt-bb">{strAuthedUser}</h3>
                    <span class="subs">подписка активна до {strValiDate}</span>
                </td>
            </tr>
            <tr><td style="padding-top:5px">
                    <a href="{cfgUrlSite}userpage"><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" border="0" style="margin-right:5px"></a>
                    <a href="{cfgUrlSite}userpage" class="link-darkblue">Личные данные</a>
                </td>
            </tr>
            <tr><td style="padding-top:8px">
                    <a href="?logout=1"><img src="{cfgSiteImg}left/menu.item.blue.arrow.gif" border="0" style="margin-right:5px"></a>
                    <a href="?logout=1" class="link-darkblue">Выход</a>
                </td>
            </tr>
        </table>
        <div id="blocknote">
            <a href="{cfgSiteUrl}notebook"><img src="{cfgSiteImg}top/icon_blocknote.jpg" alt="Блокнот"/></a>
        </div>
    </div>
</div>
</if authed>

