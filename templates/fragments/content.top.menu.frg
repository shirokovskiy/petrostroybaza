<!-- content.top.menu -->
<div id="main_top_menu_block" class="clearer">
	<div id="item_menu"><a href="{cfgUrlSite}tenders" class="link-topmenu-u">Тендеры</a>
		<div id="sm">
			<div><a href="{cfgUrlSite}tenders" class="linkH">Список тендеров</a></div>
			<div><a href="{cfgUrlSite}add.tender" class="linkH">Добавить свой тендер</a></div>
		</div>
	</div>

	<div id="item_menu"><a href="{cfgUrlSite}ads" class="link-topmenu-u">Объявления</a>
		<div id="sm">
			<loop list.of.ads.ads.chapters>
				<div><a href="{cfgUrlSite}ads/{paac_alias}" class="linkH">{paac_name}</a></div>
			</loop list.of.ads.ads.chapters>
		</div>
	</div>
	<div id="item_menu"><a href="{cfgUrlSite}anonses" class="link-topmenu-u">Анонсы мероприятий</a>
	</div>
	<div id="item_menu"><a href="{cfgUrlSite}estate_lands" class="link-topmenu-u">Недвижимость/Участки</a>
		<div id="sm">
			<loop list.of.estate.ads.types>
				<div><a href="{cfgUrlSite}estate_lands/{paec_id}" class="linkH">{paec_name}</a></div>
			</loop list.of.estate.ads.types>
		</div>
	</div>
	<div style="clear:both; display: none;"></div>
</div>

<script language="JavaScript" type="text/javascript">
	startList = function() {
		var resultOutput = "";
		if (document.all&&document.getElementById) {
			navRoot = document.getElementById("main_top_menu_block");
			for (i=0; i<navRoot.childNodes.length; i++) {
				node = navRoot.childNodes[i];
				if (node.nodeName=="DIV" && node.id == 'item_menu') {
					node.onmouseover=function() { this.className+=" over"; }
					node.onmouseout=function() { this.className=this.className.replace(" over", ""); }
				}
			}
		}
	}
	window.onload=startList;
</script>
