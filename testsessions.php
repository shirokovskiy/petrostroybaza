<?php
// page1.php

session_start();

echo 'Welcome to page #1';

$_SESSION['favcolor'] = 'green';
$_SESSION['animal']   = 'cat';
$_SESSION['searchDATA']   = 'searchDATA'; // NEW TEST VAR
$_SESSION['time']     = time();

// Works if session cookie was accepted
echo '<br /><a href="page2.php">page 2</a>';

// Or maybe pass along the session id, if needed
echo '<br /><a href="page2.php?' . SID . '">page 2</a>';

echo "<br /><pre>";

echo $_SESSION['favcolor']."\n"; // green
echo $_SESSION['animal']."\n";   // cat
echo $_SESSION['searchDATA']."\n";   // cat
echo date('Y m d H:i:s', $_SESSION['time'])."\n";
