<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 01.04.15
 * Time         : 15:44
 * Description  : Геокодирование адресов базы данных объектов за прошедший астрономический год
 * Скрипт запускается регулярно через крон
 */

set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
ini_set('memory_limit', '512M'); // possibly more by default
umask(0);

require_once( dirname(__FILE__).DIRECTORY_SEPARATOR.'access_conf'.DIRECTORY_SEPARATOR.'web.cfg.php' );

// TODO: ALTER TABLE `site_estate_objects` ADD `seo_coords` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `seo_address`;

define('API_KEY', 'AFTcG1UBAAAAoWzjAgIARXzbdFWwbLZJ8JlVNDIy0aAgwtEAAAAAAAAAAACPJlvRt_TNMKQYg7o_9OsEVHI2ag==');



/**
 * Получить порцию адресов из БД, которые ещё небыли определены на Яндекс Картах по геопозициям
 */
$strSqlQuery = "SELECT seo_id,seo_address,sr_name FROM `site_estate_objects`"
    ." LEFT JOIN `site_objects_regions_lnk` ON (`sorl_seo_id` = `seo_id`)"
    ." LEFT JOIN `site_regions` ON (`sorl_sr_id` = `sr_id`)"
    ." WHERE `seo_coords` IS NULL"
    ." AND `seo_address` NOT LIKE '' AND `seo_address` IS NOT NULL ORDER BY RAND() LIMIT 20";
$arr = $objDb->fetchall($strSqlQuery);
/**
 * Если список получен, пробуем с ним работать
 */
if (is_array($arr) && !empty($arr)) {
    foreach($arr as $object) {
        if (!empty($object['sr_name'])) {
            if (!preg_match("/".$object['sr_name']."/i", $object['seo_address'])) {
                $object["seo_address"] = 'регион ' . $object['sr_name'] . ',' . $object['seo_address'];
            }
        }

        $object["seo_address"] = 'Россия, '. $object['seo_address'];

        try {
            // Обращение к http-геокодеру
            $yandex_api_url = 'https://geocode-maps.yandex.ru/1.x/?geocode='.urlencode($object["seo_address"]).'&key='.API_KEY.'&results=1';
//            file_put_contents('YandexMaps.'.date("His").'.txt',file_get_contents($yandex_api_url));
            $xml = simplexml_load_file($yandex_api_url); //unset($yandex_api_url);
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }

        // Если геокодировать удалось, то записываем в БД
        $found = $xml->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found;
        if ($found >= 1) {
            // подготовить координаты для последующего использования в Яндекс.Картах АПИ
            $coords = explode(' ', $xml->GeoObjectCollection->featureMember->GeoObject->Point->pos);
            // в первой версии API 1.x широта и долгота поменяны местами
            // исправим это
            if (is_array($coords) && !empty($coords)) {
                $coords = array_reverse($coords);
                $coords = implode(',',$coords);
            } else {
                die('Something goes wrong!'."\n");
            }

            $strSqlQuery = "UPDATE `site_estate_objects` SET"
                . " `seo_coords` = '$coords'"
                . " WHERE `seo_id` = ".$object['seo_id'];
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                die('Database update error');
            }
        } else {
            echo "Найдено $found для адреса: ".$object["seo_address"]."\n";//.$yandex_api_url."\n";
        }
    }
}
/**
 * Подготовим data.json для Карты объектов
 * Для этого выберем все объекты с координатами из БД
 */
$strSqlQuery = "SELECT seo_id,seo_name,seo_address,seo_coords,seo_url FROM `site_estate_objects` WHERE `seo_coords` IS NOT NULL AND `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 YEAR) AND NOW() AND seo_etap_date BETWEEN CASE WHEN seo_other_etap REGEXP 'пуско-налад|внутр|благоустройств' THEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR), '%Y-%m-%d') ELSE DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR), '%Y-01-01') END AND CURDATE() AND `seo_address` NOT LIKE '' AND `seo_address` IS NOT NULL ORDER BY `seo_date_change` DESC LIMIT 7000";
$arr = $objDb->fetchall($strSqlQuery);
$arrRes = array();
if (is_array($arr) && !empty($arr)) {
    # определим корневую структуру
    $arrRes['type'] = 'FeatureCollection';
    $arrRes['features'] = array();

//    echo "Есть данные для JSON\n";

    $id = 0;

    try {
        foreach($arr as $object) {
            $obj = new stdClass();
            $obj->type = 'Feature';
            $obj->id = $id++;
            $obj->geometry = new stdClass();
            $obj->geometry->type = 'Point';
            $obj->geometry->coordinates = explode(',', $object['seo_coords']);
            $obj->properties = new stdClass();
            $obj->properties->balloonContent = strip_tags($object['seo_name']).'<br><a target="_blank" href="'.SITE_URL.'objectinfo/'.($object['seo_url']?:$object['seo_id']).'" class="link-darkblue">'.strip_tags($object['seo_address'])."</a>";
            $obj->properties->clusterCaption = strip_tags($object['seo_name']);
            $obj->properties->hintContent = strip_tags($object['seo_name']);

            $arrRes['features'][] = $obj;
        }
    } catch (Exception $ex) {
        echo $ex->getTraceAsString();
    }
}

try {
    file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'geo-map-data.json', json_encode($arrRes));
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
}

/**
 * Подготовим data.json для Карты объектов
 * Для этого выберем все объекты с координатами из БД не старше 1-го месяца
 */
$strSqlQuery = "SELECT seo_id,seo_name,seo_address,seo_coords,seo_url FROM `site_estate_objects` WHERE `seo_coords` IS NOT NULL AND `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND NOW() AND seo_etap_date BETWEEN CASE WHEN seo_other_etap REGEXP 'пуско-налад|внутр|благоустройств' THEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 MONTH), '%Y-%m-%d') ELSE DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 MONTH), '%Y-01-01') END AND CURDATE() AND `seo_address` NOT LIKE '' AND `seo_address` IS NOT NULL ORDER BY `seo_date_change` DESC LIMIT 7000";
$arr = $objDb->fetchall($strSqlQuery);
$arrRes = array();
if (is_array($arr) && !empty($arr)) {
    # определим корневую структуру
    $arrRes['type'] = 'FeatureCollection';
    $arrRes['features'] = array();

//    echo "Есть данные для JSON\n";

    $id = 0;

    try {
        foreach($arr as $object) {
            $obj = new stdClass();
            $obj->type = 'Feature';
            $obj->id = $id++;
            $obj->geometry = new stdClass();
            $obj->geometry->type = 'Point';
            $obj->geometry->coordinates = explode(',', $object['seo_coords']);
            $obj->properties = new stdClass();
            $obj->properties->balloonContent = strip_tags($object['seo_name']).'<br><a target="_blank" href="'.SITE_URL.'objectinfo/'.($object['seo_url']?:$object['seo_id']).'" class="link-darkblue">'.strip_tags($object['seo_address'])."</a>";
            $obj->properties->clusterCaption = strip_tags($object['seo_name']);
            $obj->properties->hintContent = strip_tags($object['seo_name']);

            $arrRes['features'][] = $obj;
        }
    } catch (Exception $ex) {
        echo $ex->getTraceAsString();
    }
}

try {
    file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'geo-map-data-1month.json', json_encode($arrRes));
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
}

/**
 * Подготовим data.json для Карты объектов
 * Для этого выберем все объекты с координатами из БД от 1-го до 3-х месяцев
 */
$strSqlQuery = "SELECT seo_id,seo_name,seo_address,seo_coords,seo_url FROM `site_estate_objects` WHERE `seo_coords` IS NOT NULL AND `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 3 MONTH) AND DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND seo_etap_date BETWEEN CASE WHEN seo_other_etap REGEXP 'пуско-налад|внутр|благоустройств' THEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 3 MONTH), '%Y-%m-%d') ELSE DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 3 MONTH), '%Y-01-01') END AND DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND `seo_address` NOT LIKE '' AND `seo_address` IS NOT NULL ORDER BY `seo_date_change` DESC LIMIT 7000";
$arr = $objDb->fetchall($strSqlQuery);
$arrRes = array();
if (is_array($arr) && !empty($arr)) {
    # определим корневую структуру
    $arrRes['type'] = 'FeatureCollection';
    $arrRes['features'] = array();

    $id = 0;

    try {
        foreach($arr as $object) {
            $obj = new stdClass();
            $obj->type = 'Feature';
            $obj->id = $id++;
            $obj->geometry = new stdClass();
            $obj->geometry->type = 'Point';
            $obj->geometry->coordinates = explode(',', $object['seo_coords']);
            $obj->properties = new stdClass();
            $obj->properties->balloonContent = strip_tags($object['seo_name']).'<br><a target="_blank" href="'.SITE_URL.'objectinfo/'.($object['seo_url']?:$object['seo_id']).'" class="link-darkblue">'.strip_tags($object['seo_address'])."</a>";
            $obj->properties->clusterCaption = strip_tags($object['seo_name']);
            $obj->properties->hintContent = strip_tags($object['seo_name']);

            $arrRes['features'][] = $obj;
        }
    } catch (Exception $ex) {
        echo $ex->getTraceAsString();
    }
}

try {
    file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'geo-map-data-3month.json', json_encode($arrRes));
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
}

/**
 * Подготовим data.json для Карты объектов
 * Для этого выберем все объекты с координатами из БД от 3-х до 6-ти месяцев
 */
$strSqlQuery = "SELECT seo_id,seo_name,seo_address,seo_coords,seo_url FROM `site_estate_objects` WHERE `seo_coords` IS NOT NULL AND `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AND DATE_SUB(CURDATE(), INTERVAL 3 MONTH) AND seo_etap_date BETWEEN CASE WHEN seo_other_etap REGEXP 'пуско-налад|внутр|благоустройств' THEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 6 MONTH), '%Y-%m-%d') ELSE DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 6 MONTH), '%Y-01-01') END AND DATE_SUB(CURDATE(), INTERVAL 3 MONTH) AND `seo_address` NOT LIKE '' AND `seo_address` IS NOT NULL ORDER BY `seo_date_change` DESC LIMIT 7000";
$arr = $objDb->fetchall($strSqlQuery);
$arrRes = array();
if (is_array($arr) && !empty($arr)) {
    # определим корневую структуру
    $arrRes['type'] = 'FeatureCollection';
    $arrRes['features'] = array();

    $id = 0;

    try {
        foreach($arr as $object) {
            $obj = new stdClass();
            $obj->type = 'Feature';
            $obj->id = $id++;
            $obj->geometry = new stdClass();
            $obj->geometry->type = 'Point';
            $obj->geometry->coordinates = explode(',', $object['seo_coords']);
            $obj->properties = new stdClass();
            $obj->properties->balloonContent = strip_tags($object['seo_name']).'<br><a target="_blank" href="'.SITE_URL.'objectinfo/'.($object['seo_url']?:$object['seo_id']).'" class="link-darkblue">'.strip_tags($object['seo_address'])."</a>";
            $obj->properties->clusterCaption = strip_tags($object['seo_name']);
            $obj->properties->hintContent = strip_tags($object['seo_name']);

            $arrRes['features'][] = $obj;
        }
    } catch (Exception $ex) {
        echo $ex->getTraceAsString();
    }
}

try {
    file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'geo-map-data-3-6month.json', json_encode($arrRes));
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
}


/**
 * Подготовим data.json для Карты объектов
 * Для этого выберем все объекты с координатами из БД от 6-ти до 12-ти месяцев
 */
$strSqlQuery = "SELECT seo_id,seo_name,seo_address,seo_coords,seo_url FROM `site_estate_objects` WHERE `seo_coords` IS NOT NULL AND `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 12 MONTH) AND DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AND seo_etap_date BETWEEN CASE WHEN seo_other_etap REGEXP 'пуско-налад|внутр|благоустройств' THEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 12 MONTH), '%Y-%m-%d') ELSE DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 12 MONTH), '%Y-01-01') END AND DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AND `seo_address` NOT LIKE '' AND `seo_address` IS NOT NULL ORDER BY `seo_date_change` DESC LIMIT 7000";
$arr = $objDb->fetchall($strSqlQuery);
$arrRes = array();
if (is_array($arr) && !empty($arr)) {
    # определим корневую структуру
    $arrRes['type'] = 'FeatureCollection';
    $arrRes['features'] = array();

    $id = 0;

    try {
        foreach($arr as $object) {
            $obj = new stdClass();
            $obj->type = 'Feature';
            $obj->id = $id++;
            $obj->geometry = new stdClass();
            $obj->geometry->type = 'Point';
            $obj->geometry->coordinates = explode(',', $object['seo_coords']);
            $obj->properties = new stdClass();
            $obj->properties->balloonContent = strip_tags($object['seo_name']).'<br><a target="_blank" href="'.SITE_URL.'objectinfo/'.($object['seo_url']?:$object['seo_id']).'" class="link-darkblue">'.strip_tags($object['seo_address'])."</a>";
            $obj->properties->clusterCaption = strip_tags($object['seo_name']);
            $obj->properties->hintContent = strip_tags($object['seo_name']);

            $arrRes['features'][] = $obj;
        }
    } catch (Exception $ex) {
        echo $ex->getTraceAsString();
    }
}

try {
    file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'geo-map-data-6-12month.json', json_encode($arrRes));
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
}


/**
 * Подготовим data.json для Карты объектов
 * Для этого выберем все объекты с координатами из БД от 12-ти месяцев
 */
$strSqlQuery = "SELECT seo_id,seo_name,seo_address,seo_coords,seo_url FROM `site_estate_objects` WHERE `seo_coords` IS NOT NULL AND `seo_date_change` BETWEEN DATE_SUB(CURDATE(), INTERVAL 36 MONTH) AND DATE_SUB(CURDATE(), INTERVAL 12 MONTH) AND seo_etap_date BETWEEN CASE WHEN seo_other_etap REGEXP 'пуско-налад|внутр|благоустройств' THEN DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 36 MONTH), '%Y-%m-%d') ELSE DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 36 MONTH), '%Y-01-01') END AND DATE_SUB(CURDATE(), INTERVAL 12 MONTH) AND `seo_address` NOT LIKE '' AND `seo_address` IS NOT NULL ORDER BY `seo_date_change` DESC LIMIT 7000";
$arr = $objDb->fetchall($strSqlQuery);
$arrRes = array();
if (is_array($arr) && !empty($arr)) {
    # определим корневую структуру
    $arrRes['type'] = 'FeatureCollection';
    $arrRes['features'] = array();

    $id = 0;

    try {
        foreach($arr as $object) {
            $obj = new stdClass();
            $obj->type = 'Feature';
            $obj->id = $id++;
            $obj->geometry = new stdClass();
            $obj->geometry->type = 'Point';
            $obj->geometry->coordinates = explode(',', $object['seo_coords']);
            $obj->properties = new stdClass();
            $obj->properties->balloonContent = strip_tags($object['seo_name']).'<br><a target="_blank" href="'.SITE_URL.'objectinfo/'.($object['seo_url']?:$object['seo_id']).'" class="link-darkblue">'.strip_tags($object['seo_address'])."</a>";
            $obj->properties->clusterCaption = strip_tags($object['seo_name']);
            $obj->properties->hintContent = strip_tags($object['seo_name']);

            $arrRes['features'][] = $obj;
        }
    } catch (Exception $ex) {
        echo $ex->getTraceAsString();
    }
}

try {
    file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'geo-map-data-12month.json', json_encode($arrRes));
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
}


die(date('Ymd His')."\n");
