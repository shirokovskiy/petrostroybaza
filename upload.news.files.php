<?php
/**
 * Created by Dmitry Shirokovskiy.
 * Email: info@phpwebstudio.com
 * Date: 9/2/12
 * Time: 7:50 PM
 * News files uploader
 */
header("Content-Type: content=text/html; charset=utf-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Pragma: no-cache");

$uploaddir = getcwd().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'news_attachments'.DIRECTORY_SEPARATOR;
if (!is_dir($uploaddir)) {
	if (!mkdir($uploaddir)) {
		header("HTTP/1.1 403 Forbidden");
		die('Не могу создать директорию '.$uploaddir);
	}
}
if (isset($_GET['nid']) && intval($_GET['nid'])>0) {
	#
	$news_attch_files_dir = $uploaddir.$_GET['nid'].DIRECTORY_SEPARATOR;
	if(!is_dir($news_attch_files_dir)) {
		if (!mkdir($news_attch_files_dir)) {
			header("HTTP/1.1 403 Forbidden");
			die('Не могу создать директорию '.$news_attch_files_dir);
		}
	} else {
		if( isset($_GET['a']) && $_GET['a']=='del') {
			$fn=$news_attch_files_dir.urldecode($_GET['filename']);
			if( file_exists($fn)) {
				unlink($fn);
			}
			die();
		}
	}
}

/*$fh = fopen(getcwd().DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'debug.log', "a+");
fwrite($fh, date('H:i:s d.M ').'FILES='.print_r($_FILES, true)."\n");
fclose($fh);

$fh = fopen(getcwd().DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'debug.log', "a+");
fwrite($fh, date('H:i:s d.M ').'POST='.print_r($_POST, true)."\n");
fclose($fh);*/

if (is_array($_FILES) && !empty($_FILES) && !$_FILES['file']['error']) {
	$uploadfile = $news_attch_files_dir.basename($_FILES['file']['name']);
	if (file_exists($uploadfile)) {
		header("HTTP/1.1 403 Forbidden");
		die('Файл "'.$_FILES['file']['name'].'" уже существует!');
	}
	if (is_uploaded_file($_FILES['file']['tmp_name'])) {
		if ( false == move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile) ) {
			header("HTTP/1.1 403 Forbidden");
			die('Не могу записать загруженый файл!');
		} else {
			echo "OK";
		}
	} else {
		header("HTTP/1.1 403 Forbidden");
		die('Файл не загружен!');
	}
} else {
	header("HTTP/1.1 403 Forbidden");
	die('Не могу загрузить файл!');
}
